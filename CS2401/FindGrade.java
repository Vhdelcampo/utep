import java.io.*;
import java.util.*;


public class FindGrade {

  public static final int NUM_SCORE_TYPES = 2;  // Constant to store the number of different types of scores

  public static void main(String[] args) {
    Scanner scan = null;
    int[] quizArray = null;    // array to store quiz grades
    int[] labArray = null;    // array to store lab grades
    int[] attendanceArray = null;     
    int[] midtermsArray = null;
    int[] finalArray = null;
    
    String name;

    try {
      scan = new Scanner(new File("input.txt")); //opening input file and holding scanner ref
    } catch (FileNotFoundException e) {
      e.printStackTrace();
      return;
    }

    // each iteration is for single exam type (ie: Quizzes is the 1st one)
    for (int i = 0; i < NUM_SCORE_TYPES; i++) {

      name = scan.next(); // name of the score (in 2nd iteration it will 'Labs')
      int numScores = scan.nextInt(); // number of scores of this types
      int maxGrade = scan.nextInt();

      if (name.equals("Quizzes")) {
        quizArray = new int[numScores]; // Allocating  grade array if exam type is Quizzes
        readScores(quizArray, numScores, scan);
      } else if (name.equals("Labs")) {
        labArray = new int[numScores];
        readScores(labArray, numScores, scan);
      }
      else if (name.equals("Lab_attendance")) {
          attendanceArray = new int[numScores];
          readScores(attendanceArray, numScores, scan);
        }
      else if (name.equals("Midterms")) {
          midtermsArray = new int[numScores];
          readScores(midtermsArray, numScores, scan);
        }
      else if (name.equals("Final")) {
          finalArray = new int[numScores];
          readScores(finalArray, numScores, scan);
        }
    }	

    System.out.println("Quizzes: " + Arrays.toString(quizArray));
    System.out.println("Labs: " + Arrays.toString(labArray));
    System.out.println("Lab_attendance " + Arrays.toString(attendanceArray));
    System.out.println("Midterms " + Arrays.toString(midtermsArray));
    System.out.println("Final " + Arrays.toString(finalArray));
    
  }

  // This method reads in a certain number of scores into the given array
  // Using a method allows you to write this code once and use it for all of the different types of exams by passing in the correct array
  public static void readScores(int[] scoreArray, int numScores, Scanner scan) {
    for (int i = 0; i < numScores; i++) { // each iteration is for reading a single score
      scoreArray[i] = scan.nextInt();
    }
  }

  // this method should normalize the scores in a particular array
  public static void normalizeScores(int[] scoreArray, int maxScore) {
    // implement this!
  }

  // write a method to calculate the average score

  // write a method to calculate the weighted average
}