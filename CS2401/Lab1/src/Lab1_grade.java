/**
 * Lab #1
 * 
 * Objective: The goal of this assignment is to refresh your memory of 1-dimensional arrays, 
 * in preparation for the following lab on multi-dimensional arrays.
 * Assignment: Your project is to write a program to assist with tracking your grade in this course. 
 * 
 * @author Victor Del Campo
 * #80509902
 * 2014-02-09
 *
 */

import java.io.*;
import java.util.*;

public class Lab1_grade {

  public static final int NUM_SCORE_TYPES = 5;  // Constant to store the number of different types of scores
  public static final double[] percentArray = {.1 , .3, .05, .3, .25}; // percent worth or weight of each section according to syllabus
  																	   // order is {quiz, lab, lab_attendance, midterms, finals}
  
  public static void main(String[] args) {
    Scanner scan = null;
    int[] quizArray = null;    // array to store quiz grades
    int[] labArray = null;    // array to store lab grades
    int[] attendanceArray = null;     
    int[] midtermsArray = null;
    int[] finalArray = null;

    double[] normalArray = new double[NUM_SCORE_TYPES]; // Array where the normalized scores will be stored
    int position = 0; // position where each normal score will go to
    
    String name;

    try {
      scan = new Scanner(new File("input.txt")); //opening input file and holding scanner reference
    } catch (FileNotFoundException e) {
      e.printStackTrace();
      return;
    }

    // each iteration is for single exam type (ie: Quizzes is the 1st one)
    for (int i = 0; i < NUM_SCORE_TYPES; i++) {

      name = scan.next(); // name of the score (in 2nd iteration it will 'Labs')
      int numScores = scan.nextInt(); // number of scores of this types
      int maxGrade = scan.nextInt(); // max grade individual can attain for that section

      if (name.equals("Quizzes")) {
    	  quizArray = new int[numScores]; // Allocating  grade array if exam type is Quizzes
    	  readScores(quizArray, numScores, scan, maxGrade); // grab the integers from the input.txt in a row and store in array
    	  storeInNormalArray(quizArray, normalArray, maxGrade, position); // Calculate normalized score and store it in the normalArray
    	  position++; // changes the position where next normalized score will be stored;
      } 
      else if (name.equals("Labs")) {
    	  labArray = new int[numScores];
    	  readScores(labArray, numScores, scan, maxGrade);
    	  storeInNormalArray(labArray, normalArray, maxGrade, position);
    	  position++;
      }
      else if (name.equals("Lab_attendance")) {
          attendanceArray = new int[numScores];
          readScores(attendanceArray, numScores, scan, maxGrade);
          storeInNormalArray(attendanceArray, normalArray, maxGrade, position);
          position++;
      }
      else if (name.equals("Midterms")) {
          midtermsArray = new int[numScores];
          readScores(midtermsArray, numScores, scan, maxGrade);
          storeInNormalArray(midtermsArray, normalArray, maxGrade, position);
          position++;
      }
      else if (name.equals("Final")) {
          finalArray = new int[numScores];
          readScores(finalArray, numScores, scan, maxGrade);
          storeInNormalArray(finalArray, normalArray, maxGrade, position);
          position++;
      }
    }
    
    String[] stringArray = formatArray(normalArray); // Converts normalArray into stringArray with correct format
    
    System.out.println(
    		"Quizzes: " 			+ stringArray[0] + "\n" +
    		"Labs: " 				+ stringArray[1] + "\n" +
    		"Lab attendance: " 		+ stringArray[2] + "\n" +
    		"Midterms: " 			+ stringArray[3] + "\n" +
    		"Final: " 				+ stringArray[4] + "\n" +
    		"Overall Average is: " 	+ weightedAverage(normalArray));
  }

	// This method reads in a certain number of scores into the given array
	// Using a method allows you to write this code once and use it for all of
	// the different types of exams by passing in the correct array
	public static void readScores(int[] scoreArray, int numScores, Scanner scan, int maxGrade) {
		for (int i = 0; i < numScores; i++) { // each iteration is for reading a single score
			scoreArray[i] = scan.nextInt();
			if (scoreArray[i] < 0 || scoreArray[i] > maxGrade) // if user has wrong (negative or greater than max) input
				throw new InputMismatchException();
		}
	}

	// Calculates the normalized score for each section and stores it in the normalArray
	// Each time the method is called the position will have increased by one and the normal will be stored in the next empty position
	// -1 means the scoreArray is empty and is used so that the normalArray will remain an integer array
	public static void storeInNormalArray(int[] scoreArray, double normalArray[], int maxGrade, int position){
		if (Arrays.toString(scoreArray) != "[]" ) // When array is not empty
			normalArray[position] = (average(scoreArray) * (100.0 / maxGrade)); // normal is average * (100/maxGrade)
		else // when array is empty
			normalArray[position] = -1; // -1 is used to signify empty scoreArray
	}

	// Returns the average for an integer array
	public static double average(int[] scores) {
		double sum = 0;
		for (int i = 0; i < scores.length; i++)
			sum += scores[i];
		double average = (sum / scores.length); // average is sum divided by number of elements
		return average;
	}
	
	// Calculates the weighted average
	// The formula is to add the normalized score of each array times the percent weight and then 
	// divide by 1 minus the weight of any not available scores
	// (normalized score of each array * weight) / (1 - (sum of weight of all not available scores)
	public static char weightedAverage(double[]normalArray){
		double sum = 0;
		double temp = 1;
		for (int i = 0; i < normalArray.length;i++){
			if (normalArray[i] >= 0) // If score is available
				sum += normalArray[i] * percentArray[i];
			else // if score is not available
				temp -= percentArray[i];
		}
		return toLetter((Math.round(sum/temp))); // rounds the final score and converts it to a letter grade (A,B,C,D,F)
												 // and finally returns it
	}
	
	// Method to change number grade to a letter grade
	public static char toLetter(double score) {
		   char letter;
		   if (score >= 90)
		      letter = 'A';
		   else if (score >= 80) 
		      letter = 'B';
		   else if (score >= 70) 
		      letter = 'C';
		   else if (score >= 60)
		      letter = 'D';
		   else
			  letter = 'F';
		   return letter;
	}
	
	// Changes each individual grade to a string, then either adds percent sign or to changes it to "Not Applicable"
	public static String[] formatArray(double[]normalArray){
		String[]stringArray = new String [normalArray.length];
		for(int i = 0; i < normalArray.length; i++){
			if (normalArray[i] >= 0){ // If score is available
				stringArray[i] = String.valueOf(Math.round(normalArray[i])); // converts integer to string and stores it
				stringArray[i] += "%"; // appends percent sign to each grade
			}
			else // if score is not available
				stringArray[i] = "Not Applicable";
		}
		return stringArray;
	}
}