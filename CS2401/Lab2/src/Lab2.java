/**
 * Lab #2
 * 
 * Objective: The goal of this assignment is to practice using 2-dimensional arrays.
 * Your assignment is to write a program that will allow a player to attempt solving a Numbrosia puzzle. 
 * Extra credit options 1 and 2 are implemented
 * 
 * @author Victor Del Campo
 * #80509902
 * 2014-02-16
 *
 */

import java.io.*;
import java.lang.Math;
import java.util.*;

public class Lab2 {
	public static int [][] board = new int[5][5]; // 2D Array to use for board
	public static int [][] solvedBoard = new int[5][5]; // 2D Array filled with zeroes
	public static int [][][] grid = new int[100][5][5]; // Create a 3D array to store up to 100 previous boards
	public static int k = 0; // First position of 3D array
	public static Scanner scan = null; // Scanner for input.txt file
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in); // Scanner for instructions to begin game
		Scanner scanner = new Scanner(System.in); // Scanner for instructions inside the game
		
		try {
		      scan = new Scanner(new File("input2.txt")); // Try to open input.txt file
		    } catch (FileNotFoundException e) {
		      e.printStackTrace();
		      return;
		    }
		
		// Print welcome statement
		System.out.println(	
		"Hello and welcome to Numbrasia, a game where you are given a 5x5 grid of numbers and must turn them all to zero. " + "\n" +
		"There are two kinds of moves: rotation and math moves. " + "\n" +
		"Rotation moves allow you to rotate a row left/right or a column up/down by one space. " + "\n" +
		"Math moves allow you to add one to every number in a row/column or subtract one from every number in a row/column" + "\n" +
		"A key to all the moves is displayed below (i means you must input an integer)" + "\n\n" +
		"i rr for rotate right row i" + "\n" +
		"i rl for rotate left row i" + "\n" +
		"i ru for rotate up column i" + "\n" +
		"i rd for rotate down column i" + "\n" +
		"i +r for add 1 to row i" + "\n" +
		"i +c for add 1 to column i" + "\n" +
		"i -r for subtract 1 from row i" + "\n" +
		"i -c for subtract 1 from column i" + "\n" +
		"i undo to go back i number of times" + "\n"
		);
		
		System.out.println(
		"Do you want use the board on the input.txt file or do you want a randomly filled board with \n" +
		"different difficulties according to your decision?\n" +
		"Type 1 to use the input.txt file \n" +
		"Type 2 if you want a randomly filled board.");
		int choice = sc.nextInt();
		if (choice == 1){ // Use input.txt
			fillArrayWithInput(board); // Use method to fill array from text file
		}
		else if (choice == 2){ // Use random board
			System.out.println(
			"You have chosen to use a random board. Type in one of three listed difficulties below:\n" +
			"easy" + "\n" +
			"medium" + "\n" +
			"hard");
			String difficulty = sc.next();
			if (difficulty.equals("easy") ||  difficulty.equals("medium") || difficulty.equals("hard"))
				fillArrayRandomly(board, difficulty); // Use method to fill array according to difficulty
			else
				throw new InputMismatchException(); // when difficulty is not valid
		}
		else
			throw new InputMismatchException(); // when choice is not valid
		
		System.out.println("The game will now begin. Game will not end untill the board is solved. Enjoy!\n");
		printBoard(board);
		boolean solved = false;
		
		while (solved == false){ // while board is not solved
			System.out.print("Make your move: ");
			String request[] = new String[2]; // Create String array to fill with user input.
			request[0] = scanner.next(); // First array position is filled with a number
			request[1] = scanner.next(); // Second array position is filled with two characters
			int num = 0; // Initialize num
			String s = (request[1]);
			
			// Try to parse number, otherwise throw exception and proceed to the modifyArray method
			// and print "not a valid move"
			try { num = Integer.parseInt(request[0]) - 1; }catch (Exception e){} // num is subtracted by 1 to match how arrays start at 0
			
			// Try to modify according to s and num given. If user inputs something an invalid the move will fail and the while loop will
			// continue until a valid move is given
			try { 
				modifyArray(board, s, num); }
			catch (Exception e){
				System.out.println("\nINVALID MOVE\n");}

			printBoard(board);
			solved = areArraysEqual(solvedBoard, board); // Use method to compare arrays and return false or true
			if (solved == true)
				System.out.println("Cogratulations, you have solved this board"); // Ends while loop and prints winning message
			}
		// Close scanners
		sc.close();
		scanner.close();
		scan.close();
	}

	public static void fillArrayWithInput(int[][]a){
		for (int i = 0; i < a.length; i++){
			for (int j = 0; j < a[i].length; j++){
				a[i][j] = scan.nextInt(); // Array value becomes next integer in text file
			}
		}
	}
	// Copies contents of 2D array a into array b
	public static void copyArray(int[][] a, int[][] b){
		for (int i = 0; i < a.length; i++){
			for (int j = 0; j < a[i].length; j++){
				b[i][j] = a[i][j];
			}
		}
	}
	
	public static void fillArrayRandomly(int[][]a, String difficulty){
		copyArray(solvedBoard, a); // Start with the solved array and then modify it
		int timesRandomlyChange = 0; // Number of times to keep mixing up board
		if (difficulty.equals("easy"))
			timesRandomlyChange = 2; // change two times
		if (difficulty.equals("medium"))
			timesRandomlyChange = 5;
		if (difficulty.equals("hard"))
			timesRandomlyChange = 9;
		for (int k = 0; k < timesRandomlyChange; k++){ // Continue changing array according to difficulty
			for (int i = 0; i < board.length; i++){
				for (int j = 0; j < board[i].length; j++){
					a[i][j] += (int)(Math.random() * 2); // Randomly add 1 or 0
				}
			}
		}
	}
	
	public static void printBoard(int [][]board){
		System.out.print(" ");
		for (int i = 0; i < board[0].length; i ++) // Prints top border of array
			System.out.print("---"); // Border lines
		System.out.println();
		for (int i = 0; i < board.length; i++){
			for (int j = 0; j < board[i].length; j++){
				if (j == 0) // Prints left edge at the beginning of a row
					System.out.print("|"); // Edge line
				if (board[i][j] < 0)
					// Negative numbers have an character so only one space is needed
					System.out.print(board[i][j] + " "); 
				else // Otherwise put two spaces between each number
					System.out.print(" " + board[i][j] + " ");
				if (j == board[i].length -1) // Prints right edge at the end of a row
					System.out.print("|"); // Edge line
			}
			System.out.println();
		}
		System.out.print(" ");
		for (int i = 0; i < board[0].length; i++) // Prints bottom border of board
			System.out.print("---"); // Border lines
		System.out.println();
	}
	
	public static void modifyArray(int[][]board,String s, int num){
		if (s.equals("rr")){ // Check s for matching statement
			storeMovein3dArray(board); // Put board into grid or history
			rotateRowRight(board[num]); // Use method to change board
		}
		else if (s.equals("rl")){
			storeMovein3dArray(board);
			rotateRowLeft(board[num]);
		}
		else if (s.equals("ru") || s.equals("cu")){
			storeMovein3dArray(board);
			rotateColumnUp(board, num);
		}
		else if (s.equals("rd") || s.equals("cd")){
			storeMovein3dArray(board);
			rotateColumnDown(board, num);
		}
		else if (s.equals("+r") || s.equals("r+")){
			storeMovein3dArray(board);
			addToRow(board[num]);
		}
		else if (s.equals("-r") || s.equals("r-")){
			storeMovein3dArray(board);
			subtractToRow(board[num]);
		}
		else if (s.equals("+c") || s.equals("c+")){
			storeMovein3dArray(board);
			addToColumn(board, num);
		}
		else if (s.equals("-c") || s.equals("c-")){
			storeMovein3dArray(board);
			subtractToColumn(board, num);
		}
		else if (s.equals("undo") && k != 0){ // Cannot undo more than the position board started
			k -= (num + 1); // Move position of grid or history a number of times equal to num + 1
			copyArray(grid[k], board); // Copies grid at position k which contains a previous board, to current board
		}
		else {
			System.out.println("\nINVALID MOVE"); // When user input is faulty, print message and continue while loop
		}
		System.out.println();
	}
	
	public static void storeMovein3dArray(int[][]a){
		for (int i = 0; i < board.length; i++){
			for (int j = 0; j < board[i].length; j++){
				grid[k][i][j] = board[i][j];
			}
		}
		k++; // Change to next slot to store board in
	}
	
	public static boolean areArraysEqual(int[][]a, int[][]b){
		if (a.length != b.length) // Rows must be the same
			return false;
		for (int i = 0; i < board.length; i++){
			if (a[i].length != b[i].length) // Columns must be the same
				return false;
			for (int j = 0; j < board[i].length; j++){
				if (a[i][i] != b[i][j]) // Array values must be the same
					return false;
			}
		}
		return true;
	}
	
	public static void rotateRowLeft(int[]a){
		int first = a[0];
		for (int i = 0; i < a.length-1; i++){
			a[i] = a[i + 1];
		}
		a[a.length - 1] = first; // last becomes first
	}
	
	public static void rotateRowRight(int[]a){
		int last = a[a.length - 1];
		for (int i = a.length - 1; i > 0; i--){ // start at end of row and go left each time
			a[i] = a[i-1];
		}
		a[0] = last; // first becomes last
	}
	
	public static void addToRow(int[]a){
		for (int i = 0; i < a.length; i++)
			a[i]++;
	}
	
	public static void subtractToRow(int[]a){
		for (int i = 0; i < a.length; i++)
			a[i]--;
	}
	
	public static void rotateColumnUp(int[][]a, int column){
		int top = a[0][column];
		for (int i = 0; i < a.length-1; i++){ // start at the top of the column and keep going down
			a[i][column] = a[i + 1][column];
		}
		a[a.length - 1][column] = top; // bottom becomes top
	}
	
	public static void rotateColumnDown(int[][]a, int column){
		int bottom = a[a.length - 1][column];
		for (int i = a.length - 1; i > 0; i--){ // start at the bottom of the column and keep going up
			a[i][column] = a[i-1][column];
		}
		a[0][column] = bottom; // top becomes bottoms
	}
	
	public static void addToColumn(int[][]a, int column){
		for (int i = 0; i < a.length; i++)
			a[i][column]++;
	}
	
	public static void subtractToColumn(int[][]a, int column){
		for (int i = 0; i < a.length; i++)
			a[i][column]--;
	}
}
