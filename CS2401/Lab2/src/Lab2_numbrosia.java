import java.io.*;
import java.util.*;

public class Lab2_numbrosia {
	public static int [][] board = new int[5][5];
	
	public static void main(String[] args) {
		Scanner scan = null;
		
		try {
		      scan = new Scanner(new File("input.txt")); //opening input file and holding scanner ref
		    } catch (FileNotFoundException e) {
		      e.printStackTrace();
		      return;
		    }
		
		for (int i = 0; i < board.length; i++){
			for (int j = 0; j < board[i].length; j++){
				board[i][j] = scan.nextInt();
			}
		}
		

		
		System.out.print("\n");
		
		Scanner scanner = new Scanner(System.in);
		
		int[][] solvedArray = new int[5][5];
		
		while (board != solvedArray){
			printBoard(board);
			String request[] = new String[2];
			int number = 0;
			System.out.println("Input next Instruction: ");
			request[0] = scanner.next();
			request[1] = scanner.next();
			System.out.println("number is " + request[0]);
			System.out.println("request string is " + request[1]);
			number = Integer.parseInt(request[0]);
			
			if (request[1] == "rr"){
				System.out.println("OOOOO");
				rotateRowRight(board[number]);
			}
		}
	}
	
	public static void printBoard(int [][]board){
		for (int i = 0; i < board.length; i++){
			System.out.println();
			for (int j = 0; j < board[i].length; j++){
				if (board[i][j] < 0)
					System.out.print(board[i][j] + " ");
				else
					System.out.print(" " + board[i][j] + " ");
			}
		}
		System.out.print("\n");
	}
		
	public static void rotateRowLeft(int[]a){
		int temp = a[0];
		for (int i = 0; i < a.length-1; i++){
			a[i] = a[i + 1];
		}
		a[a.length - 1] = temp;
	}
	
	public static void rotateRowRight(int[]a){
		int temp = a[a.length - 1];
		for (int i = a.length - 1; i > 0; i--){
			a[i] = a[i-1];
		}
		a[0] = temp;
	}
	
	public static void addToRow(int[]a){
		for (int i = 0; i < a.length; i++)
			a[i]++;
	}
	
	public static void subtractToRow(int[]a){
		for (int i = 0; i < a.length; i++)
			a[i]--;
	}
	
	public static void rotateColumnUp(int[][]a, int column){
		int temp = a[0][column];
		for (int i = 0; i < a.length-1; i++){
			a[i][column] = a[i + 1][column];
		}
		a[a.length - 1][column] = temp;
		}
	
	public static void rotateColumnDown(int[][]a, int column){
		int temp = a[a.length - 1][column];
		for (int i = a.length - 1; i > 0; i--){
			a[i][column] = a[i-1][column];
		}
		a[0][column] = temp;
		}
	

	public static void addToColumn(int[][]a, int column){
		for (int i = 0; i < a.length; i++)
			a[i][column]++;
	}
	

	public static void subtractToColumn(int[][]a, int column){
		for (int i = 0; i < a.length; i++)
			a[i][column]--;
	}
	
}
