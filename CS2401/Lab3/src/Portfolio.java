public class Portfolio {
	private Stock[] folio;
	private int n;
	
	Portfolio(int size){
		n = size;
		folio = new Stock[n];
		initializeStocks(); // Create the stocks in the array
	}
	
	// Create random stocks to be put inside the portfolio stock array
	public void initializeStocks(){
		double randomPrice;
		for (int i = 0; i < folio.length; i++){
			folio[i] = new Stock(
					randomName(),
					randomSymbol(), 
					10000/n,
					randomPrice = Math.random() * 101, // random number between 0 and 100
					randomPrice
					);
		}
	}
	
	// Returns a random string of length 1 to 5 using the uppercase alphabet
	public String randomSymbol(){
		int randomLength = (int)(Math.random() * 5 + 1); // random integer between 1 and 5
		String symbol = "";
		for (int i = 0; i < randomLength; i++){
			// Random integer between 65 and 90 or decimal representation of the uppercase alphabet
			int rand = (int)(Math.random() * (90 - 65) + 65 + 1); 
			symbol += (char)rand; // Appends character converted from integer
		}
		return symbol;
	}
	
	// Returns a random string of length 1 to 20 using the lowercase alphabet
	public String randomName(){
		int randomLength = (int)(Math.random() * 20 + 1); // Sets the length of the name to be a random length between 1 and 20
		String name = ""; 
		for (int i = 0; i < randomLength; i++){
			// Random number between 97 and 122 or decimal representation of lowercase alphabet.
			int rand = (int)(Math.random() * (122 - 97) + 97 + 1);  
			name += (char)rand; // Appends character converted from integer
		}
		return name;
	}
	
	public int getSize(){
		return n;
	}
	
	public void setSize(int size){
		n = size;
	}
	
	public Stock[] getStockArray(){
		return folio;
	}
	
	public Stock getStockAtPosition(int position ){
		return folio[position];
	}
	
	// Returns the value of each stock added together
	public double getTotalValueOfPortfolio(){
		double value = 0;
		for (int i = 0; i < folio.length;i++){
			value += folio[i].getTotalValue();
		}
		return value;
	}
	
	// Update each stock once, representing a random walk of a year.
	public void updateValues(){
			for (int i = 0; i < folio.length;i++){
				folio[i].updateValue();
			}
	}
	
	// Returns a copy of the current Portfolio
	public Portfolio copyOfPortfolio(){
		int length = getSize();
		Portfolio copy = new Portfolio(length); // Creates new portfolio of equal length
		for (int i = 0; i < length; i++){
			copy.getStockArray()[i] = getStockArray()[i].copyOfStock();
		}
		return copy;
	}
	
	
	
}