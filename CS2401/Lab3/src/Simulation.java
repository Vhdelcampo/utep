import java.text.*;

/*
 * Victor Del Campo
 * Assignment: Create a simulation of stock annual gains with different portfolios.
 * Instructor Marine
 * TA Lauren
 * Due Date: 2014-03-02
 * Objective: To practice using and creating objects and methods, and running a basic simulation. 
 * 
 */

public class Simulation {
	static int numberOfStocks = 100;
	static int years = 30;
	static int simulationsToRun = 100;
	
	public static void main(String[] args){
		System.out.println(
		simulationsToRun + " Simulations will run using these values:" + "\n" +
		"Years: " + years + "\n" +
		"Number of stocks: " + numberOfStocks + "\n" +
		"Annual Gain: 5%" + "\n" +
		"Variance: 25%\n");
		
		// Creates Portfolio object which is an array of type Stock whose size is determined by the parameter
		Portfolio omaha = new Portfolio(numberOfStocks);
		 // Use a method to simulate the change of stocks over time
		simulate(omaha);
	}
	
	public static void simulate(Portfolio port){
		double initialValue = port.getTotalValueOfPortfolio();
		double sum = 0;
		double maximum = 0;
		double minimum = Integer.MAX_VALUE;
		double totalValue = 0;
		for (int i = 0; i < simulationsToRun; i++){ // Run up to the number of simulations
			Portfolio copy = port.copyOfPortfolio(); // Create a copy of the portfolio to use during each simulation
			for (int j = 0; j < years; j++){ // Run up to the number of years
				copy.updateValues(); // Do a random walk on the portfolio copy that simulates the passing of a year
				totalValue = copy.getTotalValueOfPortfolio();
				sum += totalValue; // Sum will be the the value of all portfolios added together
				if (totalValue > maximum)
					maximum = totalValue;
				if (totalValue < minimum)
					minimum = totalValue;
			}
		}
		
		double average = sum / (simulationsToRun * years); // Average = sum of all numbers / amount of numbers
		double difference = (average - initialValue); // Change in the portfolio
		double percentChange = difference/initialValue;  // (difference)/initial value 
		
		// Setting up formatting for the print statement
	    DecimalFormat df = new DecimalFormat("$###,###.00"); // Format Stock Prices to add commas and have two numbers after the decimal point
	    DecimalFormat p = new DecimalFormat("###,###.00%"); // Format percent difference to percent form with two numbers after the decimal point
		
		System.out.println(
		"Initial Value of Portfolio: " + df.format(initialValue) + "\n" +
		"Average Value after Simulation: " + df.format(average) + "\n\n" +
		"Gain or Loss: " + df.format(difference) + "\n" +
		"Percent Change: " + p.format(percentChange) + "\n" +
		"Greatest Portfolio Value Ever Attained: " + df.format(maximum) + "\n" +
		"Smallest Portfolio Value Ever Attained: " + df.format(minimum) + "\n");
		
	}
	


}
