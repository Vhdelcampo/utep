import java.util.*;

public class Stock {
	
	private String name;
	private String symbol;
	private int shares;
	private double purchasePrice;
	private double currentPrice;
	
	private double annualGain = 1.05;
	private double variance = .25;
	
	Stock(String n, String s, int sh, double purchase, double current){
		name = n;
		symbol = s;
		shares = sh;
		purchasePrice = purchase;
		currentPrice = current;
	}
	
	
	public String getName(){
		return name;
	}
	
	public String getSymbol(){
		return symbol;
	}
	
	public int getShares(){
		return shares;
	}
	
	public double getPurchasePrice(){
		return purchasePrice;
	}
	
	public double getCurrentPrice(){
		return currentPrice;
	}
	
	public void setName(String n){
		name = n;
	}
	
	public void setSymbol(String s){
		symbol = s;
	}
	
	public void setShares(int s){
		shares = s;
	}
	
	public void setPurchasePrice(double purchase){
		purchasePrice = purchase;
	}
	
	public void setCurrentPrice(double current){
		currentPrice = current;
	}
	
	public double getTotalValue(){
		return shares * currentPrice;
	}
	
	public void updateValue(){
		// Annual gain is 1.05 and variance is 25%
		currentPrice *=  annualGain + new Random().nextGaussian() * variance;
	}
	
	public Stock copyOfStock(){
		Stock copy = new Stock(
				getName(),
				getSymbol(),
				getShares(),
				getPurchasePrice(),
				getCurrentPrice()
				);
		return copy;
	}
}
