

public class Tile {
	public int x;
	public int y;
	public int score;
	public Tile next;
	
	Tile(){
	}
	
	Tile(int x_coordinate, int y_coordinate, int s){
		x = x_coordinate;
		y = y_coordinate;
		score = s;
	}
	
	Tile(int x_coordinate, int y_coordinate, int s, Tile l){
		x = x_coordinate;
		y = y_coordinate;
		score = s;
		next = l;
	}
}
