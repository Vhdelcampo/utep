/* 
 * Victor Del Campo
 * Lab 4 
 * Instructor Martine
 * TA Lauren
 * Due Date: March 18
 * 
 */

import java.util.*;
public class main{
	
	static int rows	    = 10;
	static int columns  = 10;

	static int startingX = 9; static int startingY = 0;
	static int endingX =   0; static int endingY =   9;
	
	static int[][] board = randomBoard(rows, columns);
	public static int startingTileNumber 		= board[startingX][startingY];
	public static int endingTileNumber  = board[endingX][endingY];
	
	static Scanner sc = new Scanner(System.in);
	
	public static void main(String[] args){
		int x = startingX;
		int y = startingY;
		Path userPath = new Path(startingX, startingY, board[x][y]);
		printBoard(board, x, y);
		
		while (x != endingX || y != endingY){
			System.out.println("Your position is currently (" + x + ", " + y + ") and the number at that position is: " + board[x][y]);
			System.out.print("Enter your next move: ");
			String move = sc.next();
			
			if (move.equals ("up") || move.equals ("u")){
				x--;
				if (isValidMove(x, y) == false){
					System.out.println("Invalid Move");
					x++; // reverses move
				}
				else{
					userPath.insertTile(x, y, board[x][y]);
				}
			}
			else if (move.equals("down") || move.equals ("d")){
				x++;
				if (isValidMove(x, y) == false){
					System.out.println("Invalid Move");
					x--; // reverses move
				}

				else{
					userPath.insertTile(x, y, board[x][y]);
				}
			}
			else if (move.equals("right") || move.equals ("r")){
				y++;
				if (isValidMove(x, y) == false){
					System.out.println("Invalid Move");
					y--; // reverses move
				}
				else{
					userPath.insertTile(x, y, board[x][y]);
				}
			}
			else if (move.equals ("left") || move.equals ("l")){
				y--;
				if (isValidMove(x, y) == false){
					System.out.println("Invalid Move");
					y++; // reverses move
				}
				else{
					userPath.insertTile(x, y, board[x][y]);
				}
			}
			else 
				System.out.println("Invalid Input");
			
			System.out.println();
			printBoard(board,x,y);
		}
		System.out.println("The Path you chose is:");
		userPath.printPath();
		System.out.println("Value of Path is: " + userPath.getTotalValue());
		
		System.out.println();
		Path maximumPath = getMaximumPathWithSimulations(board);
		System.out.println("Greatest value path is:");
		maximumPath.printPath();
		System.out.println("Value of greatest value path is: " + maximumPath.getTotalValue());
		sc.close();
		
		System.out.println();
		System.out.println("New path will be created to test the other methods in the Path class");
		Path test = new Path();
		test.insertTile(1, 1, 1);
		test.insertTile(2, 2, 2);
		test.insertTile(3, 3, 3);
		test.insertTile(4, 4, 4);
		test.printPath();
		System.out.println("Method to delete tiles in range of 1 to 3 will be invoked");
		test.deleteTilesInRangeOf(1, 3);
		test.printPath();
		Path d = new Path();
		d.insertTile(2, 2, 2);
		d.insertTile(3, 3, 3);
		System.out.println("New detour path will be created");
		d.printPath();
		System.out.println("Detour path will be inserted after the first tile and be checked to make sure it is valid");
		test.insertDetourAfterPosition(0, d);
		System.out.println("New path after detour is inserted");
		test.printPath();
		System.out.println("Now a method will try to find if score coordinate (1, 1) is in the path");
		System.out.println(test.isCoordinateInPath(1, 1));
	}
	
	public static boolean isValidMove(int x, int y){
		if (x < 0 || x > rows - 1)
			return false;
		if (y < 0 || y > columns - 1)
			return false;
		return true;
	}
	public static Path getMaximumPathWithSimulations(int[][] board){
		Path maximumPath = getRandomPath(board);
		int iterations = 1000;
		for (int i = 0; i < iterations; i++){
			Path randomPath = getRandomPath(board);
			int maximumPathValue = maximumPath.getTotalValue();
			int randomValue   = randomPath.getTotalValue();
			if (randomValue > maximumPathValue){
				maximumPath = randomPath;
			}
		}
		return maximumPath;
	}
	
	public static Path getRandomPath(int [][] board){
		int x = startingX;
		int y = startingY;
		Path testPath = new Path(startingX, startingY, startingTileNumber);
		Tile nextTile;
		Tile currentTile;
		// Create an array to hold history of Tiles that are in the path
		// Rows represent how many moves can be stored and
		// Columns represent the x and y coordinates of each tile
		int[][] visitedTiles = new int[100][2]; 
		// Create for loop to fill in values that will not conflict as the array is used
		for (int i = 0; i < visitedTiles.length; i++){
			for (int j = 0; j < visitedTiles[i].length; j++){
				visitedTiles[i][j] = 100; // fill with values
			}
		}
		int count = 0; // Count will keep track of which row to store tile values in
		int attempts = 0;
		while ( attempts < 100 && (x != endingX || y != endingY)){ // While last tile is not ending tile
			attempts++;
			currentTile = testPath.last; // Current tile is latest tile in path or last tile in the path
			nextTile = getRandomAdjacentTile(board, testPath); // Randomly create a tile that is adjacent to current tile in the path
			if (isInArray(visitedTiles, nextTile) == false){ // If  randomly generated nextTile already exists in visitedTiles array that contains history of path
				int[] coordinateArray = {currentTile.x, currentTile.y}; // Coordinate values of tile
				visitedTiles[count] = coordinateArray; // Add the currentTile coordinates to visitedTiles array or history
				testPath.insertTile(nextTile);
				count++; // Move to the next row
			}
			x = testPath.last.x; // x updated to newest tile in path
			y = testPath.last.y; // y updated to newest tile in path
		}
		if (testPath.last.x == endingX && testPath.last.y == endingY)
			return testPath;
		else
			return getValidPath(board);
	}
	
	public static Path getValidPath(int [][]board){
		Path validPath = new Path();
		for (int i = 0; i < columns; i++){ // Make path going to bottom right corner
			validPath.insertTile(new Tile(rows - 1, i, board[rows - 1][i]));
		}
		for (int i = rows - 2; i >= 0; i--){ // Make path going from bottom right corner to top right corner
			validPath.insertTile(new Tile(i, columns - 1, board[i][columns - 1]));
		}
		return validPath;
	}
	
	public static Tile getRandomAdjacentTile(int[][] board, Path testPath){
		int x = testPath.last.x;
		int y = testPath.last.y;
		if (x == (rows - 1) && y == (columns - 1)) // bottom right corner
			x--;
		else if (x == 0 && y == 0) // top left corner
			y++;
		else if (x == 0){ // top edge
			if (Math.random() < .5)
				y++;
			else
				x++;
		}
		else if (x == (rows - 1)){ // bottom edge
			if (Math.random() < .5)
				y++;
			else
				x--;
		}
		else if (y == 0){ // left edge
			if (Math.random() < .5)
				y++;
			else
				x--;
		}
		else if (y == (columns - 1)){ // right edge
			if (Math.random() < .5)
				y--;
			else
				x--;
		}
		else { // center of board
			double random = Math.random();
			if (random < .25)
				y++;
			else if (random < .5)
				y--;
			else if (random < .75)
				x--;
			else
				x++;
		}
		return new Tile(x, y, board[x][y]);
	}
	
	public static int[][] randomBoard(int rowLength, int columnLength){
		int[][] board = new int[rowLength][columnLength];
		for (int i = 0; i < rowLength; i++){
			for (int j = 0; j < columnLength; j++){
				board[i][j] = randomNumber();
			}
		}
		return board;
	}
	
	public static int randomNumber(){
		double random = Math.random();
		if (random >= .5)
			return (int)(Math.random() * 11);
		return (int)(Math.random() * -11);
	}
	
	public static boolean isInArray(int[][] array, Tile t){
		int[] coordinateArray = {t.x, t.y};
		for (int i = 0; i < array.length; i++){
			if (array[i][0] == coordinateArray[0] && array[i][1] == coordinateArray[1])
				return true;
		}
		return false;
		}
	
	public static void printBoard(int [][]board, int x, int y){
		System.out.print("    ");
		for (int k = 0; k < board[0].length; k++)
			System.out.print(k + "   ");
		System.out.println();
		System.out.print("   ");
		for (int i = 0; i < board[0].length; i ++) // Prints top border of array
			System.out.print("----"); // Border lines
		System.out.println();
		for (int i = 0; i < board.length; i++){
			for (int j = 0; j < board[i].length; j++){
				if (j == 0){ // Prints left edge at the beginning of a row and row number
					System.out.print(i + " |"); // Edge line
				}
				if (i == x && j == y) // Current place in board
					System.out.print(">o< ");
				else if (i == endingX && j == endingY)
					System.out.print("END ");
				else if (board[i][j] < 0 && board[i][j] > -10){
					// Negative numbers have an character so only one space is needed
					System.out.print(board[i][j] + "  ");}
				else if (board[i][j] <= -10 && board[i][j] > -100)
					System.out.print(board[i][j] + " ");
				else if (board[i][j] >= 10 && board[i][j] < 100 ) // Otherwise put two spaces between each number
					System.out.print(" " + board[i][j] + " ");
				else
					System.out.print(" " + board[i][j] + "  ");
				
				if (j == board[i].length -1) // Prints right edge at the end of a row
					System.out.print("|"); // Edge line
			}
			System.out.println();
		}
		System.out.print("   ");
		for (int i = 0; i < board[0].length; i++) // Prints bottom border of board
			System.out.print("----"); // Border lines
		System.out.println();
	}
	
}