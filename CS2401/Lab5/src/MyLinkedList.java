
public class MyLinkedList {

	// ATTRIBUTES: ***********************************************************************
	// the only essential attribute of the Linked List: its first node
    private Node first;
    // additional possible attributes include a reference variable to the linked
    // list's last node
    private Node last;

    // METHODS: ***************************************************************************
    
    // CONSTRUCTOR(S): --------------------------------------------------------------------
    //LinkedList constructors
    public MyLinkedList() {
	    first = null;
	    last = null;
    }

    // ACCESSORS: -------------------------------------------------------------------------
    public Node getFirst() {
    	return first;
    }

    public Node getLast() {
    	return last;
    }
    
    // MUTATORS: --------------------------------------------------------------------------
    public void setFirst(Node N) {
    	first = N;
    }

    public void setLast(Node N) {
    	last = N;
    	last.setNext(null);
    }

    // OTHER FUNCTIONALITIES: --------------------------------------------------------------
    
    //Returns true if list is empty, false otherwise	
    public boolean isEmpty() {
	    return first == null;
    }

    //Inserts a new node at the start of the list
    public void insertFront(int d1, double d2) {
	    Node newN = new Node(d1, d2); // new node to be inserted
	    newN.setNext(first); // will come before the original first, so it needs to point to it

	    if (first == null) { // if the list if originally empty, the new node is also the last one
	    	last = newN;
    	}
	    first = newN; // in any case, the new node becomes the new first node
    }

    //Inserts a new node at the end of the list
    public void insertEnd(int d1, double d2) {
	    Node newN = new Node(d1, d2); // the new node we want to insert
	    newN.setNext(null); // since it will be the last node, its link needs to point to null
	    if (first == null) { // i.e., if the list is originally empty
	    	first = newN;
	    	last = newN;
	    }
	    else { // if the list originally contains at least one node
	    	last.setNext(newN); // plug the original last to the new node
	    	last = newN; // move the pointer to last to the new node
	    }
    }
    
    public void insertEnd(Node newN) {
	    newN.setNext(null); // since it will be the last node, its link needs to point to null
	    if (first == null) { // i.e., if the list is originally empty
	    	first = newN;
	    	last = newN;
	    }
	    else { // if the list originally contains at least one node
	    	last.setNext(newN); // plug the original last to the new node
	    	last = newN; // move the pointer to last to the new node
	    }
    }

    //Inserts a new node N1 after a node of the list containing a given information d1, d2
    public void insertNodeAfter(int d1, double d2, Node N1) {
	    Node current = first;
	    while (((current.getData1() != d1) || (current.getData2() != d2)) && (current != null)) {
	    	current = current.getNext();
	    }
	    // when we exit the while loop, it is either because 
	    // (1) current is N, and we need to insert N1 right there
	    // or (2) because current is null, and we do not insert
	    if (current != null) { // case (1)
	    	N1.setNext(current.getNext());
	    	current.setNext(N1);
	    
	    	if (N1.getNext() == null) {
	    		last = N1;
	    	}
	    }
	} 

    //Inserts a node with given data after a node of the list containing a given information d1, d2
    public void insertDataAfter(int d1, double d2, int d3, double d4) {
	    Node current = first;
	    while (((current.getData1() != d1) || (current.getData2() != d2)) && (current != null)) {
	    	current = current.getNext();
	    }
	    // when we exit the while loop, it is either because 
	    // (1) current is N, and we need to insert N1 right there
	    // or (2) because current is null, and we do not insert
	    if (current != null) { // case (1)
	    	// case (2): we do not do anything, there is no else
	    	Node N1 = new Node(d3,d4);
	    	N1.setNext(current.getNext());
	    	current.setNext(N1);

	    	if (N1.getNext() == null) {
	    		last = N1;
	    	}
	    }
    } 

    //Deletes the node at the start of the list
    public void deleteFront() {
    	if (first != null) { // we only proceed if the list has elements
    		// otherwise we do not do anything: there is no elses
    		if (first.getNext()==null) { // i.e., there is originally only one element
    			// therefore we empty the list and set first and last to null
    			last = null;
    		}	
    		// if there is more than one element, we just set first to be the second element
    		first = first.getNext();
    	}
    }

    //Deletes the node at the end of the list
    public void deleteEnd() {
    	Node current = first;
    	if (current != null) { // we only proceed if the list has at least one element
    		// otherwise we do not do anything to the list: there is no else
    		if (current.getNext()==null) { // i.e., if the list has exactly one element
    			// then we modify the list to become the empty list
    			first = null;
    			last = null;
    		}
    		else { // the list has at least two elements
    			while (current.getNext().getNext() !=null) {
    				current = current.getNext();
    			}	
    			current.setNext(null);
    			last = current;
    		}
    	}
    }

    //Prints list data
    public void printList() {
    	Node current = first;
 
    	System.out.print("List: ");
	    while(current != null) {
		    current.printNode();
		    current = current.getNext();
	    }
	    System.out.println();
    }

    // returns the size of the current object MyLinkedList
    public int size() {
    	int count = 0;
    	Node current = first;
    	while (current != null) {
    		count++;
    		current = current.getNext();
    	}
    	return count;
    }

    // translates the content of two arrays into a new list of type MyLinkedList 
    public static void translateArrayToList(MyLinkedList list, int[] arrayI, double[] arrayD) {
    	// assumes that the linked list we are using is empty at first
    	// although if not, it will just keep copying the array at the end of the 
    	// current content
    	if (arrayI.length == arrayD.length) {
    		for (int i=0; i< arrayI.length; i++) {
    			list.insertEnd(arrayI[i], arrayD[i]);
    		}
    	} // if the arrays are of different lenght, we do not modify the current list
    	else {
    		System.out.println("input arrays are not valid");
    	}
    }
    
    // MAIN METHOD: *************************************************************************
    // the main method here tests the above methods
    public static void main(String[] args) {
	    MyLinkedList list = new MyLinkedList();
	    list.insertFront(0,0.5);
	    list.insertEnd(1,1.5);
	    list.insertEnd(2,2.5);
	    list.insertEnd(3,3.5);
	    list.printList();
	    
	    list.deleteEnd();
	    list.deleteEnd();
	    list.printList();
	    
	    int d1=10;
	    double d2=10.5;
	    Node N1 = new Node(d1,d2);
	    list.insertNodeAfter(0, 0.5, N1);
	    list.printList();
	    list.insertDataAfter(10, 10.5, 11, 11.5);
	    list.printList();
	    System.out.println("the size of my list is: " + list.size());
	    
	    MyLinkedList list2 = new MyLinkedList();
	    int[] arrayI = {1,2,3,4};
	    double[] arrayD = {5,6,7,8};
	    translateArrayToList(list2, arrayI, arrayD);
	    list2.printList();
    }

}
