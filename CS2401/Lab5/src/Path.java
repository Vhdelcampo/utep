/* 
 * Victor Del Campo
 * Lab 4 
 * Instructor Martine
 * TA Lauren
 * Due Date: March 18
 */

public class Path {
	private Tile first;
	private Tile last;
	
	Path(){
		this.first = null;
		this.last  = first;
	}
	
	Path(Tile n){
		this.first = n;
		this.last  = first;
	}
	
	Path(Path p){
		this.first = p.first;
		this.last  = p.last;
	}
	
	Path(int x, int y, int score){
		this.first = new Tile(x, y, score);
		this.last = first;
	}
	
	public boolean checkEmpty(){
		if (first == null)
			return true;
		return false;
	}
	
	public void printPath(){
		Tile current = first;
		while (current != null){
			if (current.next != null)
				System.out.print("(" + current.x + ", " + current.y + ")--> ");
			else
				System.out.print("(" + current.x + ", " + current.y + ")");
			current = current.next;
		}
		System.out.println();
	}
	
	public void insertTile(int x, int y, int s){
		Tile t = new Tile(x, y, s);
		if (first == null){
			first = t;
			last = first;
		}
		else
			last.next = t;
			this.last = t;
	}
	
	public void insertTile(Tile t){
		if (first == null){
			first = t;
			last = first;
		}
		else
			last.next = t;
			last = t;
	}
	
	public void deleteTileAfterTile(Tile t){
		if (t.next.next == null) // if node is second to last element
			this.last = t;
		t.next = t.next.next;
	}
	
	public void deleteLast(){
		Tile current = first;
		while(current.next.next != null){ // Traverse to the second to last node of the list
			current = current.next;
		}
		current.next = null;
		this.last = current;
	}
	
//	public void insertDetourAfterPosition(int position, Path n){
//		Tile head = n.first;
//		Tile current = n.first; // Create current to traverse detour path to be inserted
//		while (current.next != null){ // Traverse detour path
//			// x and y values of the tiles in the detour must be valid (inside the board).
//			if (current.x > (main.rows - 1) || current.y > (main.columns - 1) ||
//				current.x < 0 || current.y < 0) 
//				System.out.println("At least one of the tiles in the detour path to be inserted is not in the board");
//			current = current.next;
//		}
//		// Last element must be ending Tile
//		if (current.score != main.endingTileNumber) 
//			System.out.println("Last tile in detour path is not ending tile");
//		
//		current = first; // Use current to traverse path of board
//		int count = 0; // Use count to keep track of position
//		while (current != null && count != position){
//			count++;
//			// Tile that will be inserted must be adjacent to current tile (x and y values must not deviate more than 1)
//			if (Math.abs(current.x - head.next.x) > 1 || Math.abs(current.y - head.next.y) > 1) 
//				System.out.println("Tile that will be inserted is not adjacent");
//			current = current.next;
//		}
//		current.next = head;
//	}
	

	
//	public void deleteTileAfterPosition(int position){
//		Tile current = first;
//		int count = 0;
//		while (current.next != null || count < position){
//			current = current.next;
//		}
//		current.next = current.next.next;
//		if (current.next == null)
//			last = current;
//		if (last.score != main.endingTileNumber)
//			System.out.println("error");
//	}
	
	public void deleteTilesInRangeOf(int p1, int p2){
		Tile current = first;
		int count = 0;
		while (count < p1 - 1){
			count++;
			current = current.next;
		}
		
		Tile temp = current;
		while (count <= p2 - 1){
			count++;
			temp = temp.next;
		}
		current.next = temp.next;
		
		if (current.next == null)
			last = current;
	}
	
	public boolean isScoreInPath(int number){
		Tile current = first;
		while (current != null){
			if (current.score == number)
				return true;
			current = current.next;
		}
		return false;
	}
	
	public boolean isCoordinateInPath(int x, int y){
		Tile current = first;
		while (current != null){
			if (current.x == x && current.y == y)
				return true;
			current = current.next;
		}
		return false;
	}
	
	public int getTotalGain(){
		int sum = 0;
		Tile current = first;
		while (current != null){
			sum += current.score;
		}
		return sum;
	}
	
	public int size(){
		int count = 0; // Create counter to count the number of nodes
		if (first == null) // if list is empty
			return count;
		Tile current = first;
		while(current != null){ // Traverse until the last element of the list
			current = current.next;
			count++;
		}
		return count;
	}
	
	public int getTotalValue(){
		Tile current = first;
		int sum = 0;
		while (current != null){
			sum += current.score;
			current = current.next;
		}
		return sum;
	}
	
	public Tile getSecondToLast(){
		Tile current = first;
		if (current.next == null)
			return current;
		while (current.next.next != null){
			current = current.next;
		}
		return current;
	}
	
}