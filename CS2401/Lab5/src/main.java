/* 
 * Victor Del Campo
 * Lab 5
 * Instructor Martine
 * TA Lauren
 * Due Date: March 30
 * Objective: Create methods for finding GCD, converting from decimal to binary and vice versa,
 * and create the method to find all valid paths in the board used in the previous lab
 * Purpose: Practice recursion
 */

public class main {
	public static void main(String[] args){
		
		System.out.print("The GCD of 100 and 30 is: ");
		System.out.println(GCD(100, 30));
		
		System.out.print("10 in Binary form is: ");
		System.out.println(toBinary(10));
		
		System.out.print("1111111111111111111111111111111 in Decimal form is: ");
		System.out.println(toDecimal("1111111111111111111111111111111")); // 2^32 - 1
		
		// Create array to then convert it into a linked list
		double[] arr = {10.0, 20.0, 30.0, 40.0, 50.0, 60.0};
		MyLinkedList l = toLinkedListFromArray(arr);
		
		// Print list forwards then backwards
		l.printList();
		System.out.println("List will now be printed backwards");
		printReverse(l.getFirst());
		
		// Print numbers greater than threshold in list
		System.out.println("Numbers greater than fifteen in list: " + greaterThanThreshold(l.getFirst(), 15));
		
		// The search method will find all valid paths in the a board given starting coordinates.
		// Each time it finds a valid path it increases timesWon by 1.
		search(startingX, startingY);
		System.out.println("Number of valid paths in a " + rows + "x" + columns + " board: "+ timesWon);
	}
	
	public static int GCD(int m, int n){
		if (m % n == 0)
			return n;
		else
			return GCD(n, m % n);
	}
	
	public static int toDecimal(String binary){
		int power = binary.length() - 1;
		int count = 0;
		int sum = 0;
		return toDecimal(binary, count, power, sum);
	}
	
	private static int toDecimal(String binary, int count, int power, int sum){
		if (count == binary.length())
			return sum;
		if (binary.charAt(count) == '1'){
			sum += (int)Math.pow(2, power);
		}
		power--; count++;
		return toDecimal(binary, count, power, sum);
	}
	
	public static String toBinary(int n){
		int remainder = 0;
		String binary = "";
		return toBinary(n, remainder, binary);
	}
	
	private static String toBinary(int n, int remainder, String binary){
		remainder = n % 2;
		if (n == 0)
			return binary;
		if (remainder == 0)
			binary = "0" + binary;
		if (remainder == 1)
			binary = "1" + binary;
		return toBinary(n / 2, remainder, binary);
	}
	
	public static void printReverse(Node n){
		if (n.getNext() == null)
			n.printNode();
		else{
			printReverse(n.getNext());
			n.printNode();
		}
		System.out.println();
	}
	
	public static int greaterThanThreshold(Node n, double threshold){
		int count = 0;
		return greaterThanThreshold(n, threshold, count);
	}
	
	public static int greaterThanThreshold(Node n, double threshold, int count){
		if (n == null){
			return count;
		}
		if (n.getData2() > threshold){
			count++;
		}
		return greaterThanThreshold(n.getNext(), threshold, count);
	}
	
	public static MyLinkedList toLinkedListFromArray(double[] a){
		MyLinkedList arrayList = new MyLinkedList(); 
		for (int i = 0; i < a.length; i++){ 
			Node next = new Node(a[i]); 
			arrayList.insertEnd(next);
		}
		return arrayList;
	}
	
	// Values of board
	static int rows	    = 3;
	static int columns  = 3;
	static int startingX = 2; static int startingY = 0;
	static int endingX = 0; static int endingY = 2;
	static Path testPath = new Path(startingX, startingY, 0);
	static int timesWon = 0;
	
	// Method that passes to another with full parameters
	public static void search(int x, int y){
		search(x, y, 100);
	}
	
	public static void search(int x, int y, int remainingMoves){
		// Stop recursion attempts when number of moves goes beyond threshold
		if (remainingMoves == 0){
			System.out.println("END GAME");
		}
		// if current coordinates are winning game coordinates, increase valid path count
		else if (x == endingX && y == endingY){
			timesWon++;
		}
		
		// RIGHT
		if (isValidMove("right", x, y) ){
			y++;
			if (!testPath.isCoordinateInPath(x, y)){ // Next move must not be in the the Path
				testPath.insertTile(x, y, 0);
				remainingMoves--;
				search(x, y, remainingMoves); // Recursively attempt a move
				testPath.deleteLast(); // After recursion, delete path attempts and choose another direction
			}
			y--; // Reverse move if it is not valid
		}
		// UP
		if(isValidMove("up", x, y) ){
			x--;
			if (!testPath.isCoordinateInPath(x, y)){ // Next move must not be in the the Path
				testPath.insertTile(x, y, 0);
				remainingMoves--;
				search(x, y, remainingMoves); // Recursively attempt a move
				testPath.deleteLast(); // After recursion, delete path attempts and choose another direction
			}
			x++; // Reverse move if not valid
		}
		// LEFT
		if (isValidMove("left", x, y) ){
			y--;
			if (!testPath.isCoordinateInPath(x, y)){ // Next move must not be in the the Path
				testPath.insertTile(x, y, 0);
				remainingMoves--;
				search(x, y, remainingMoves); // Recursively attempt a move
				testPath.deleteLast(); // After recursion, delete path attempts and choose another direction
			}
			y++; // Reverse move if not valid
		}
		// DOWN
		if (isValidMove("down", x, y) ){
			x++;
			if (!testPath.isCoordinateInPath(x, y)){ // Next move must not be in the the Path
				testPath.insertTile(x, y, 0);
				remainingMoves--;
				search(x, y, remainingMoves); // Recursively attempt a move
				testPath.deleteLast(); // After recursion, delete path attempts and choose another direction
			}
			x--; // Reverse move if not valid
		}
	}
	
	public static boolean isValidMove(String move, int x, int y){
		if (move.equals("up")){
			return x > 0;
		}
		else if (move.equals("down")){
			return x < rows - 1;
		}
		else if (move.equals("right")){
			return y < columns - 1;
		}
		else if (move.equals("left")){
			return y > 0;
		}
		return true;
	}
}

