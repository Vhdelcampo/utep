/* 
 * Victor Del Campo
 * Lab 6
 * Instructor Martine
 * TA Lauren
 * Due Date: April 11
 * Objective: Test iterative search and binary search on array and find time needed to complete each
 * Purpose: Compare computational time of different methods
 */

import java.util.*;

public class Recursion {
	static int calls = 0;
	static double time = 0;
	static double startingTime = 0;
	static Random r = new Random();
	static int    iterativeCount= 0; static int    recursiveCount= 0;
	static double iterativeTime = 0; static double recursiveTime = 0;
	
	public static void main(String[] args){
		runTests(10000);
		runTests(40000);
		runTests(160000);
		runTests(640000);
		runTests(1280000);
		
		runFibonnacciSimulation(100);
	}
	
	public static void runTests(int size){
		iterativeCount= 0; recursiveCount= 0;
		iterativeTime = 0; recursiveTime = 0;
		calls = 0; time = 0;
		
		int [] array = getRandomArray(size);
		System.out.println();
		System.out.println("Size is: " + size);	
		System.out.println("Searching in sorted array");
		Arrays.sort(array);
		runSimulation(array, 30);
		printData();
		
		iterativeCount= 0; recursiveCount= 0;
		iterativeTime = 0; recursiveTime = 0;
		calls = 0; time = 0;
		
		System.out.println("Searching for even number in array with only odd numbers");
		int[] arr = getRandomEvenArray(size);
		runSimulationFailCase(arr, 100);
		printData();
	}
	
	public static void printData(){
		System.out.println(
				"Iterative Time:  " + (int)iterativeTime + "\n" + 
				"Iterative Calls: " + iterativeCount     + "\n" + 
				"Recursive Time:  " + (int)recursiveTime + "\n" + 
				"Recursive Calls: " + recursiveCount     + "\n"
				);
	}
	
	public static void runFibonnacciSimulation(int simulationsToRun){
		int simulations = simulationsToRun;
		for (int i = 0; i < 31; i++){
			simulationsToRun = simulations;
			while (simulationsToRun > 0){
				simulationsToRun--;
				
				startingTime = System.nanoTime();
				fibonacciIterative(i);
				time = System.nanoTime() - startingTime;
				iterativeTime  += time;
				iterativeCount += calls;
				calls = 0; time = 0;
				
				startingTime = System.nanoTime();
				fibonacciRecursive(i);
				time = System.nanoTime() - startingTime;
				recursiveTime  += time;
				recursiveCount += calls;
				calls = 0; time = 0;
				
			}
			iterativeTime  /= simulations; iterativeCount /= simulations;
			recursiveTime  /= simulations; recursiveCount /= simulations;
			System.out.println("Average in finding " + i + "th Fibonacci number recursively and iteratively");
			printData();
			iterativeCount= 0; recursiveCount= 0;
			iterativeTime = 0; recursiveTime = 0;
			calls = 0; time = 0;
		}
		
	}
	
	public static void runSimulation(int []array, int simulationsToRun){
		int simulations = simulationsToRun;
		iterativeCount= 0; recursiveCount= 0;
		iterativeTime = 0; recursiveTime = 0;
		calls = 0; time = 0;
		while (simulationsToRun > 0){
			simulationsToRun--;
			int randomNumber = (int)(Math.random() * (array.length + 1));
			
			startingTime = System.nanoTime();
			iterativeSearch(array, randomNumber);
			time = System.nanoTime() - startingTime;
			iterativeTime  += time;
			iterativeCount += calls;
			calls = 0; time = 0;
			
			startingTime = System.nanoTime();
			binarySearch(array, randomNumber);
			time = System.nanoTime() - startingTime;
			recursiveTime  += time;
			recursiveCount += calls;
			calls = 0; time = 0;
		}
		iterativeTime  /= simulations; recursiveTime  /= simulations;
		iterativeCount /= simulations; recursiveCount /= simulations;
	}
	
	public static void runSimulationFailCase(int []array, int simulationsToRun){
		int simulations = simulationsToRun;
		calls = 0; time = 0;
		while (simulationsToRun > 0){
			simulationsToRun--;
			int random = r.nextInt(100001)*2;
			
			startingTime = System.nanoTime();
			iterativeSearch(array, random);
			time = System.nanoTime() - startingTime;
			iterativeTime  += time;
			iterativeCount += calls;
			calls = 0; time = 0;
			
			startingTime = System.nanoTime();
			binarySearch(array, random);
			time = System.nanoTime() - startingTime;
			recursiveTime  += time;
			recursiveCount += calls;
			calls = 0; time = 0;
		}
		iterativeTime  /= simulations; recursiveTime  /= simulations;
		iterativeCount /= simulations; recursiveCount /= simulations;
	}
	
	public static int[] getRandomEvenArray(int size){
		int [] array = new int[size];
		for (int i = 0; i < array.length; i++){
			int random = r.nextInt(100000)*2+1;
			array[i] = random;
		}
		return array;
	}
	
	public static int[] getRandomArray(int size){
		int [] array = new int[size];
		for (int i = 0; i < array.length; i++){
			int rand = (int)(Math.random() * 10001);
			array[i] = rand;
		}
		return array;
	}
	
	public static int iterativeSearch(int[] a, int key){
		for (int i = 0; i < a.length; i++){
			calls++;
			if (a[i] == key){
				
				return i;
			}
		}
		return -1;
	}
	
	public static int binarySearch(int[] a, int key){
		int low = 0;
		int high = a.length - 1;
		return binarySearch(a, key, low, high);
	}
	
	private static int binarySearch(int []a, int key, int low, int high){
		if (high < low){
			return -1;
		}
		calls++;
		int m = (int)((low + high) / 2);
		if (a[m] == key){
			return m;
		}
		else if (a[m] > key){
			high = m - 1;
			return binarySearch(a, key, low, high);
		}
		else {
			low = m + 1;
			return binarySearch(a, key, low, high);
		}
	}
	
	public static int fibonacciRecursive(int n){
		calls++;
		if (n == 0)
			return 0;
		else if (n == 1)
			return 1;
		else{
			return fibonacciRecursive(n - 2) + fibonacciRecursive(n - 1);
		}
	}
	
	public static int fibonacciIterative(int n){
		calls++;
		double c1 = ( 1.0 + Math.sqrt(5) ) / 2.0;
		double c2 = ( 1.0 - Math.sqrt(5) ) / 2.0;
		return (int) ( 
		( Math.pow(c1, n) - Math.pow(c2, n) )  
		/ Math.sqrt(5) 
		);

//		( (1.0 / Math.sqrt(5) ) * (Math.pow( ( (1.0 + Math.sqrt(5) ) / 2.0), n) ) - 
//		( (1.0 / Math.sqrt(5) ) * (Math.pow( ( (1.0 - Math.sqrt(5) ) / 2.0), n) ) ) );
	}
}
