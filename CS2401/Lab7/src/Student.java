import java.util.*;
import java.lang.*;

public class Student implements Comparable<Student>{
	
	private double GPA;
	private String name;
	private int ID;
	
	
	public Student(){
		this.ID = (int) (Math.random() * (99999999 - 10000000) + 10000000);
		this.name = UUID.randomUUID().toString();
		this.GPA = Math.random() * 4;
	}
	
	public Student( int ID, String name, double GPA){
		this.name = name;
		this.ID = ID;
		this.GPA = GPA;
	}
	
	public String getName(){
		return name;
	}
	
	public int getID(){
		return ID;
	}
	
	public double getGPA(){
		return GPA;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public void setID(int ID){
		this.ID = ID;
	}
	
	public void setGPA(double GPA){
		this.GPA = GPA;
	}
	
//  Ascending order
	public int compareTo(Student argo){
		return argo.GPA<this.GPA?1:(argo.GPA>this.GPA?-1:0);
	}
	
//  descending order
//	public int compareTo(Student argo){
//		return argo.GPA<GPA?-1:(argo.GPA>GPA?1:0);
//	}
	
}
