/* 
 * Victor Del Campo
 * Lab 7
 * Instructor Martine
 * TA Lauren
 * Due Date: April 25
 * Objective: Compare the efficiency of insert sort, and quick sort with and without modifications
 * Purpose: Learn the difference in computational time of sorting algorithims
 */

import java.util.*;
public class sorting {

	static int swaps = 0;
	static int comparisons = 0;
	static int size = 10000;
	static double time = 0;
	static double startingTime = 0;

	public static void main(String[] args) {

		Student[] randomCase = new Student[size];
		for (int i = 0; i < randomCase.length; i++) {
			randomCase[i] = new Student();
		}

		Student[] worstCase = new Student[size];
		for (int i = 0; i < worstCase.length; i++) {
			worstCase[i] = new Student( (int) 
					(Math.random() * (99999999 - 10000000) + 10000000), 
					UUID.randomUUID().toString(), 
					(double) (4 - i * 4.0 / size)
					);
		}

		Student[] bestCase = new Student[size];
		for (int i = 0; i < bestCase.length; i++) {
			bestCase[i] = new Student( (int) 
					(Math.random() * (99999999 - 10000000) + 10000000), 
					UUID.randomUUID().toString(), 
					(double) (i * 4.0 / size));
		}
		
		System.out.println("Insert Sort");
		 insertSort( copyOf(randomCase) );
		 setRandom();
		 insertSort( copyOf(bestCase) );
		 setBest();
		 insertSort( copyOf(worstCase) );
		 setWorst();
		 printData();
		 
		 System.out.println("Quick Sort");
		 quickSort( copyOf(randomCase) );
		 setRandom();
		 quickSort( copyOf(bestCase));
		 setBest();
		 quickSort( copyOf(worstCase) );
		 setWorst();
		 printData();
		 
		 System.out.println("Quick Sort with median pivot");
		 quickSortMedianMethod( copyOf(randomCase) );
		 setRandom();
		 quickSortMedianMethod( copyOf(bestCase) );
		 setBest();
		 quickSortMedianMethod( copyOf(worstCase) );
		 setWorst();
		 printData();

		 
		 System.out.println("Quick Sort");
		 runQuickSortSimulation( randomCase, 100);
		 System.out.println();
		 
		 System.out.println("Quick Sort with median pivot");
		 runQuickSortMedianSimulation( randomCase, 100);
		 System.out.println();
		 
		 System.out.println("Quick Sort with insert sort");
		 runQuickSortPlusInsertSortSimulation( randomCase, 100);
		 System.out.println();
		 
	}
	
	static int randomCaseSwaps = 0;
	static int randomCaseComparisons = 0;
	static int bestCaseSwaps = 0;
	static int bestCaseComparisons = 0;
	static int worstCaseSwaps = 0;
	static int worstCaseComparisons = 0;
	
	public static void printData(){
		System.out.print(
				 "Swaps for random case were "   + randomCaseSwaps + "\n" +
				 "Swaps for best   case were "   + bestCaseSwaps   + "\n" + 
				 "Swaps for worst  case were "   + worstCaseSwaps  + "\n" +
				 "Comparisons for random case were " + randomCaseComparisons + "\n" +
				 "Comparisons for best   case were " + bestCaseComparisons   + "\n" + 
				 "Comparisons for worst  case were " + worstCaseComparisons  + "\n"
				 );
		System.out.println();
	}
	
	
	public static void setRandom(){
		 randomCaseSwaps = swaps;
		 randomCaseComparisons = comparisons;
		 swaps = 0; comparisons = 0;
	}

	public static void setBest(){
		 bestCaseSwaps = swaps;
		 bestCaseComparisons = comparisons;
		 swaps = 0; comparisons = 0;
	}
	
	public static void setWorst(){
		 worstCaseSwaps = swaps;
		 worstCaseComparisons = comparisons;
		 swaps = 0; comparisons = 0;
	}
	
	public static void runQuickSortSimulation(Student[] randomCase, int simulationsToRun){
		int simulations = simulationsToRun;
		int averageTime = 0;
		int greatestTime = 0;
		int smallestTime = Integer.MAX_VALUE;
		
		while (simulations > 0){
			simulations--;
			
			startingTime = System.nanoTime();
			quickSort( copyOf(randomCase) );
			time = System.nanoTime() - startingTime;
			averageTime += time;
			if (time > greatestTime) greatestTime = (int) time;
			if (time < smallestTime) smallestTime = (int) time;
			
			
		}
		averageTime /= simulationsToRun;
		
		System.out.println(
				"Average  time: " + averageTime + "\n" +
				"Greatest time: " + greatestTime + "\n" +
				"Smallest time: " + smallestTime
				);
	}
	
	public static void runQuickSortMedianSimulation(Student[] randomCase, int simulationsToRun){
		int simulations = simulationsToRun;
		int averageTime = 0;
		int greatestTime = 0;
		int smallestTime = Integer.MAX_VALUE;
		
		while (simulations > 0){
			simulations--;
			
			startingTime = System.nanoTime();
			quickSortMedianMethod( copyOf(randomCase) );
			time = System.nanoTime() - startingTime;
			averageTime += time;
			if (time > greatestTime) greatestTime = (int) time;
			if (time < smallestTime) smallestTime = (int) time;
			
			
		}
		averageTime /= simulationsToRun;
		
		System.out.println(
				"Average  time: " + averageTime + "\n" +
				"Greatest time: " + greatestTime + "\n" +
				"Smallest time: " + smallestTime
				);
	}
	
	public static void runQuickSortPlusInsertSortSimulation(Student[] randomCase, int simulationsToRun){
		int simulations = simulationsToRun;
		int averageTime = 0;
		int greatestTime = 0;
		int smallestTime = Integer.MAX_VALUE;
		
		while (simulations > 0){
			simulations--;
			
			startingTime = System.nanoTime();
			quickSortPlusInsertSort( copyOf(randomCase) );
			time = System.nanoTime() - startingTime;
			averageTime += time;
			if (time > greatestTime) greatestTime = (int) time;
			if (time < smallestTime) smallestTime = (int) time;
			
			
		}
		averageTime /= simulationsToRun;
		
		System.out.println(
				"Average  time: " + averageTime + "\n" +
				"Greatest time: " + greatestTime + "\n" +
				"Smallest time: " + smallestTime
				);
	}
	
	public static Student[] copyOf(Student[] array){
		Student[] copy = new Student[array.length];
		
		for (int i = 0; i < array.length; i++){
			copy[i] = new Student(
					array[i].getID(),
					array[i].getName(),
					array[i].getGPA()
					);
		}
		return copy;
	}
	
	

	public static void insertSort(Student[] x) {
		for (int unsorted = 1; unsorted < x.length; unsorted++) {
			comparisons++;
			double temp = x[unsorted].getGPA();
			int i = unsorted;
			while (i > 0 && x[i - 1].getGPA() > temp) {
				comparisons++;
				swaps++;
				x[i].setGPA( x[i - 1].getGPA() );
				i--;
			}
			x[i].setGPA(temp);
		}
	}

	public static void quickSort(Student[] list) {
		quickSort(list, 0, list.length - 1);
	}

	private static void quickSort(Student[] list, int first, int last) {
		if (last > first) {
			comparisons++;
			int pivotIndex = partition(list, first, last);
			quickSort(list, first, pivotIndex - 1);
			quickSort(list, pivotIndex + 1, last);
		}
	}

	/** Partition the array list[first..last] */
	private static int partition(Student[] list, int first, int last) {
		double pivot = list[first].getGPA(); // Choose the first element as the pivot
		int low = first + 1; // Index for forward search
		int high = last; // Index for backward search

		while (high > low) {
			comparisons++;
			// Search forward from left
			while (low <= high && list[low].getGPA() <= pivot){
				comparisons+=2;
				low++;
			}
			// Search backward from right
			while (low <= high && list[high].getGPA() > pivot){
				comparisons+=2;
				high--;
			}
			// Swap two elements in the list
			if (high > low) {
				comparisons++;
				swaps++;
				double temp = list[high].getGPA();
				list[high].setGPA(list[low].getGPA());
				list[low].setGPA(temp);
			}
		}

		while (high > first && list[high].getGPA() >= pivot){
			comparisons+=2;
			high--;
		}
		// Swap pivot with list[high]
		if (pivot > list[high].getGPA()) {
			comparisons++;
			swaps++;
			list[first].setGPA(list[high].getGPA());
			list[high].setGPA(pivot);
			return high;
		} else {
			return first;
		}
	}

	public static void quickSortMedianMethod(Student[] list) {
		quickSortMedianMethod(list, 0, list.length - 1);
	}

	private static void quickSortMedianMethod(Student[] list, int first, int last) {
		if (last > first) {
			comparisons++;
			int pivotIndex = partitionMedianMethod(list, first, last);
			quickSort(list, first, pivotIndex - 1);
			quickSort(list, pivotIndex + 1, last);
		}
	}

	private static int partitionMedianMethod(Student[] list, int first, int last) {
		// Select 3 random elements from the list and choose the median to be the pivot
		
		Random rand = new Random();
		int rand1 = rand.nextInt(list.length);
		int rand2 = rand.nextInt(list.length);
		int rand3 = rand.nextInt(list.length);
		double[] random = {  list[rand1].getGPA(), list[rand2].getGPA(), list[rand3].getGPA() };
		double pivot = median(random);

		int low = first + 1; // Index for forward search
		int high = last; // Index for backward search

		while (high > low) {
			comparisons++;
			// Search forward from left
			while (low <= high && list[low].getGPA() <= pivot){
				comparisons+=2;
				low++;
			}
			// Search backward from right0
			while (low <= high && list[high].getGPA() > pivot){
				comparisons+=2;
				high--;
			}
			// Swap two elements in the list
			if (high > low) {
				comparisons++;
				swaps++;
				double temp = list[high].getGPA();
				list[high].setGPA(list[low].getGPA());
				list[low].setGPA(temp);
			}
		}

		while (high > first && list[high].getGPA() >= pivot){
			comparisons+=2;
			high--;
		}
		// Swap pivot with list[high]
		if (pivot > list[high].getGPA()) {
			comparisons++;
			swaps++;
			list[first].setGPA(list[high].getGPA());
			list[high].setGPA(pivot);
			return high;
		} else {
			return first;
		}
	}
	
	public static void quickSortPlusInsertSort(Student[] list) {
		quickSortPlusInsertSort(list, 0, list.length - 1);
	}

	private static void quickSortPlusInsertSort(Student[] list, int first, int last) {
		if (last > first) {
			comparisons++;
			if ( list.length > 20){
				int pivotIndex = partition(list, first, last);
				quickSort(list, first, pivotIndex - 1);
				quickSort(list, pivotIndex + 1, last);
			} else {
				insertSort(list);
			}
		}
	}

	public static double median(double[] array) {
		Arrays.sort(array);
		int middle = array.length / 2;
		return array[middle];
	}
	
	public static void print(Student[] array){
		for (Student s : array){
			System.out.println( s.getGPA() );
		}
	}
}
