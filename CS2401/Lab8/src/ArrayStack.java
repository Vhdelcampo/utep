/**
 * An implementation of a stack using a fixed, non-expandable array whose
 * capacity is set in its constructor.
 */
public class ArrayStack<E> implements Stack<E> {
    private E[] array;
    private int size = 0;

    public ArrayStack(int capacity) {
        array = (E[]) new Object [capacity];
    }

    public void push(E E) {
        if (size == array.length) {
            throw new RuntimeException("Stack overflow");
        }
        array[size++] = E;
    }

    public E pop() {
        if (size == 0) {
            throw new RuntimeException("Stack underflow");
        }
        E result = array[size-1];
        array[--size] = null;
        return result;
    }

    public E peek() {
        if (size == 0) {
        	throw new RuntimeException("Stack underflow");
        }
        return array[size - 1];
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public int size() {
        return size;
    }
}