import java.util.*;
public class Calculator {
	public static void main (String[] args){
		Scanner input = new Scanner(System.in);
		calculateWithArray( "5 1 2 + 4 * + 3 -");
		calculateWithLinkedList( "3!");
		input.close();
	}
	
	public static void calculateWithArray(String expression){
		Stack<String> S = new ArrayStack<String>(expression.length() ); // Create new stack of type String
		calculate(expression, S);
	}
	
	public static void calculateWithLinkedList(String expression){
		Stack<String> S = new LinkedStack<String>(); // Create new stack of type String
		calculate(expression, S);
	}
	
	private static void calculate(String expression, Stack<String> S){
		double result;
		for (int i = 0; i < expression.length(); i++){
			// If statement checks if character at index is a digit (uses regex)
			// Must first convert to string from character for regex to work.
			if ( Character.toString( expression.charAt(i) ).matches("^[\\d]") ) { 
				S.push(Character.toString(expression.charAt(i) ) ); // Convert char at position i to String and then push it
			} else if (expression.charAt(i) == '+' ) {
				String num1 = S.pop();
				String num2 = S.pop();
				result = Double.parseDouble(num2) + Double.parseDouble(num1);
				S.push(Double.toString(result) );
			} else if (expression.charAt(i) == '-' ) {
				String num1 = S.pop();
				String num2 = S.pop();
				result = Double.parseDouble(num2) - Double.parseDouble(num1);
				S.push(Double.toString(result) );
			} else if (expression.charAt(i) == '*' ) {
				String num1 = S.pop();
				String num2 = S.pop();
				result = Double.parseDouble(num2) * Double.parseDouble(num1);
				S.push(Double.toString(result) );
			} else if ( expression.charAt(i) == '/' ) {
				String num1 = S.pop();
				String num2 = S.pop();
				result = Double.parseDouble(num2) / Double.parseDouble(num1);
				S.push(Double.toString(result) );
			} else if ( expression.charAt(i) == '!' ) {
				String num1 = S.pop();
				result = factorial(Integer.parseInt(num1) );
				S.push(Double.toString(result) );
			}
			else
				throw new InputMismatchException("Not in postfix notation");
			
		}
		System.out.println( S.peek() );
	}
	
	public static int factorial(int n){
		if (n < 0) throw new ArithmeticException("Negative numbers are invalid");
		if (n == 0) return 1;
		return n * factorial(n - 1);
	}
	
	public static String toPostfix(String expression){
		
		
		return "";
	}
	
}
