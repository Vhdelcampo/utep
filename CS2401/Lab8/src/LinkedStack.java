/**
 * An implementation of the stack interface using singly-linked
 * nodes.
 */
public class LinkedStack<E> implements Stack<E> {
    private class Node<E> {
        public E data;
        public Node<E> next;
        public Node(E data, Node<E> next) {
            this.data = data;
            this.next = next;
        }
    }

    private Node<E> top = null;

    public void push(E item) {
        top = new Node<E>(item, top);
    }

    public E pop() {
        E item = peek();
        top = top.next;
        return item;
    }

    public boolean isEmpty() {
        return top == null;
    }

    public E peek() {
        if (top == null) {
        	throw new RuntimeException("Stack underflow");
        }
        return top.data;
    }

    public int size() {
        int count = 0;
        for (Node<E> node = top; node != null; node = node.next) {
            count++;
        }
        return count;
    }
}