/**
 * A small stack interface.  You can query the size of the stack and
 * ask whether it is empty, push items, pop items, and peek at the top
 * item.
 */
public interface Stack <E>{

    /**
     * Adds the given item to the top of the stack.
     */
    void push (E item);

    /**
     * Removes the top item from the stack and returns it.
     * @exception RuntimeException("Stack overflow") if the stack is empty.
     */
    E pop();

    /**
     * Returns the top item from the stack without popping it.
     * @exception RuntimeException("Stack underflow") if the stack is empty.
     */
    E peek();

    /**
     * Returns the number of items currently in the stack.
     */
    int size();

    /**
     * Returns whether the stack is empty or not.
     */
    boolean isEmpty();
}