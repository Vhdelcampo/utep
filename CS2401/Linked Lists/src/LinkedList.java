public class LinkedList {
	public Node head; // First node of list
	public Node tail; // Last node of list
	
	LinkedList(){ // default constructor
		this.head = null;
		this.tail = head;
	}
	
	LinkedList(Node node){ // Constructor if given only node.
		this.head = node;
		this.tail = head;
	}
	
	LinkedList(Object content){ // Constructor if given only content of node.
		this.head = new Node(content);
		this.tail = head;
	}
	
	LinkedList(Object content, Node link){
		this.head = new Node(content, link);
		this.tail = head;
	}
	
	public boolean checkEmpty(){
		if (head == null)
			return true;
		return false;
	}
	
	public void printList(){
		Node current = head;
		while (current != null){
			System.out.print(current.info + " ");
			current = current.link;
		}
		System.out.println();
	}
	
	public void insertAtHead(Node node){
		node.link = head;
		this.head = node;
	}
	
	public void insertAtHead(Object content){
		Node node = new Node(content); // Create node if given only content
		node.link = head;
		this.head = node; // head now points to the node that was inserted at the the beginning of the list
	}
	
	public void insertAtEnd(Node node){
		Node current = head;
		while(current.link != null){ // Traverse to the second to last node
			current = current.link;
		}
		current.link = node;
		this.tail = node; // tail now points the the node that was inserted at the end of the list
	}
	
	public void insertAtEnd(Object content){
		Node node = new Node(content); // Create node if given only content
		Node current = head;
		while(current.link != null){ // Traverse to the second to last node
			current = current.link;
		}
		current.link = node;
		this.tail = node;
	}
	
	public void insertAfterNode(Node given, Node node){
		if (given.link == null) // If given node is the last element in list
			this.tail = node; // then make tail point to the node that will be inserted
		node.link = given.link;
		given.link = node;
	}
	
	public void insertAfterNode(Node given, Object content){
		Node node = new Node(content); // Create node if given only content
		if (given.link == null) 
			this.tail = node;
		node.link = given.link;
		given.link = node;
	}
	
	public void deleteFirst(){
		this.head = head.link;
	}
	
	public void deleteLast(){
		Node current = head;
		if (current.link == null) // if current is only node
			head = null; // makes list empty
		while(current.link != null){ // Traverse to the second to last node of the list
			current = current.link;
		}
		current.link = null;
		this.tail = current;
	}
	
	public void deleteNodeAfter(Node node){
		if (node.link.link == null) // if node is second to last element
			this.tail = node;
		node.link = node.link.link;
	}
	
	public void deleteNodeAfter(Object content){
		Node node = new Node(content); // Create node if given only content
		if (node.link.link == null)
			this.tail = node;
		node.link = node.link.link;
	}
	
	public int size(){
		int count = 0; // Create counter to count the number of nodes
		if (head == null) // if list is empty
			return count;
		Node current = head;
		while(current != null){ // Traverse until the last element of the list
			current = current.link;
			count++;
		}
		return count;
	}
}