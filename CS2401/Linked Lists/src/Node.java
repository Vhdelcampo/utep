public class Node {
	public Node link;
	public Object info;
	
	Node(){ // Default constructor is an empty node or node with null values
		link = null;
		info = null;
	}
	
	Node(Object info){ // If only content is given Node wil be constructed with a null link
		this.info = info;
		this.link = null;
	}
	
	Node(Object info, Node link){
		this.info = info;
		this.link = link;
	}
	
	public void printNode(){
		System.out.println(info);
	}
}
