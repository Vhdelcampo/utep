public class TwoWayLinkedList {
	public TwoWayNode head; // First TwoWayNode of list
	public TwoWayNode tail; // Last TwoWayNode of list
	
	TwoWayLinkedList(){ // default constructor
		this.head = null;
		this.tail = head;
	}
	
	TwoWayLinkedList(TwoWayNode n){ // Constructor if given only TwoWayNode.
		this.head = n;
		this.tail = head;
	}
	
	TwoWayLinkedList(Node n){ // Constructor if given only content of TwoWayNode.
		this.head = new TwoWayNode(n.info);
		this.tail = head;
	}
	
	TwoWayLinkedList(Object content){ // Constructor if given only content of TwoWayNode.
		this.head = new TwoWayNode(content);
		this.tail = head;
	}
	
	TwoWayLinkedList(Object content, TwoWayNode next, TwoWayNode back){
		this.head = new TwoWayNode(content, next, back);
		this.tail = head;
	}
	
	public boolean checkEmpty(){
		if (head == null)
			return true;
		return false;
	}
	
	public void printList(){
	  TwoWayNode current = head;
		while (current != null){
			System.out.print(current.info + " ");
			current = current.next;
		}
		System.out.println();
	}
	
	public void insertAtHead(TwoWayNode TwoWayNode){
		TwoWayNode.next = head;
		this.head = TwoWayNode;
	}
	
	public void insertAtHead(Object content){
		TwoWayNode TwoWayNode = new TwoWayNode(content); // Create TwoWayNode if given only content
		TwoWayNode.next = head;
		this.head = TwoWayNode; // head now points to the TwoWayNode that was inserted at the the beginning of the list
	}
	
	public void insertAtEnd(TwoWayNode n){
		TwoWayNode current = head;
		while(current.next != null){ // Traverse to the second to last TwoWayNode
			current = current.next;
		}
		current.next = n;
		this.tail = n; // tail now points the the TwoWayNode that was inserted at the end of the list
	}
	
	public void insertNode(TwoWayNode n){
			if (checkEmpty() == true){
				head = n;
				tail = n;
			}
			tail.next = n;
			n.back = tail;
			tail = n;
	}
	
	public void insertAtEnd(Object content){
		TwoWayNode TwoWayNode = new TwoWayNode(content); // Create TwoWayNode if given only content
		TwoWayNode current = head;
		while(current.next != null){ // Traverse to the second to last TwoWayNode
			current = current.next;
		}
		current.next = TwoWayNode;
		this.tail = TwoWayNode;
	}
	
	public void insertAfterTwoWayNode(TwoWayNode given, TwoWayNode TwoWayNode){
		if (given.next == null) // If given TwoWayNode is the last element in list
			this.tail = TwoWayNode; // then make tail point to the TwoWayNode that will be inserted
		TwoWayNode.next = given.next;
		given.next = TwoWayNode;
	}
	
	public void insertAfterTwoWayNode(TwoWayNode given, Object content){
		TwoWayNode TwoWayNode = new TwoWayNode(content); // Create TwoWayNode if given only content
		if (given.next == null) 
			this.tail = TwoWayNode;
		TwoWayNode.next = given.next;
		given.next = TwoWayNode;
	}
	
	public void deleteFirst(){
		this.head = head.next;
	}
	
	public void deleteLast(){
		TwoWayNode current = head;
		if (current.next == null) // if current is only TwoWayNode
			head = null; // makes list empty
		while(current.next != null){ // Traverse to the second to last TwoWayNode of the list
			current = current.next;
		}
		current.next = null;
		this.tail = current;
	}
	
	public void deleteTwoWayNodeAfter(TwoWayNode TwoWayNode){
		if (TwoWayNode.next.next == null) // if TwoWayNode is second to last element
			this.tail = TwoWayNode;
		TwoWayNode.next = TwoWayNode.next.next;
	}
	
	public void deleteTwoWayNodeAfter(Object content){
		TwoWayNode TwoWayNode = new TwoWayNode(content); // Create TwoWayNode if given only content
		if (TwoWayNode.next.next == null)
			this.tail = TwoWayNode;
		TwoWayNode.next = TwoWayNode.next.next;
	}
	
	public int size(){
		int count = 0; // Create counter to count the number of TwoWayNodes
		if (head == null) // if list is empty
			return count;
		TwoWayNode current = head;
		while(current != null){ // Traverse until the last element of the list
			current = current.next;
			count++;
		}
		return count;
	}
}