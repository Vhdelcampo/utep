public class TwoWayNode {
	public TwoWayNode next;
	public TwoWayNode back;
	public Object info;
	
	TwoWayNode(){ // Default constructor is an empty node or node with null values
		this.next  = null;
		this.info = null;
		this.back  = null;
	}
	
	TwoWayNode(Object info){ // If only content is given Node wil be constructed with a null link
		this.info = info;
		this.next = null;
		this.back = null;
	}
	
	TwoWayNode(Object info, TwoWayNode back, TwoWayNode next){
		this.info = info;
		this.next = next;
		this.back = back;
	}
	
	TwoWayNode(Object info, Node back, Node next){
		this.info = info;
		this.next = new TwoWayNode(next);
		this.back = new TwoWayNode(back);
	}
	
	public void printNode(){
		System.out.println(info);
	}
}
