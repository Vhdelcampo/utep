/*
 * Victor Del Campo
 * Assignment: To create LinkedList and Node class, and to create a method that converts an array to a linked list
 * Instructor Ceberio
 * Due Date: 2014-03-05
 * Objective: Learn to use linked lists
 */

public class main {
	public static void main(String[] args) {
	
	// Nodes to use in the linked list
	Node n1 = new Node(1);
	Node n2 = new Node(2);
	Node n3 = new Node(3);
	
	LinkedList a = new LinkedList(n1); // Create LinkedList object containing node n1 as the head
	a.printList(); // Print the contents of the list
	
	a.insertAtEnd(n2); // Insert node at end of list
	a.printList();
	
	a.insertAfterNode(n2, n3); // Insert n3 after n2
	a.printList();
	
	a.deleteNodeAfter(n1); // Delete node after n1
	a.printList();
	
	a.deleteFirst(); // Delete first node, make list empty if only node
	a.printList();
	
	a.deleteLast(); // Delete last node, make list empty if only node
	a.printList();
	
	LinkedList b = new LinkedList(); // Create empty list
	System.out.println(b.checkEmpty()); // Return true if list is empty
	System.out.println(b.size()); // Print size of list
	
	System.out.println();
	int [] arr = {1,2};
	LinkedList T = toLinkedListFromArray(arr);
	T.printList();
	}

	public static LinkedList toLinkedListFromArray(int[] a){
		Node head = new Node(a[0]); // Head becomes first number in array
		Node current = head;
		for (int i = 1; i < a.length; i++){ // Start process to build a linked list forward
			Node next = new Node(a[i]); // Create new Node that has content of array and whose link is null
			current.link = next; // Current node will now point to new node
			current = current.link; // Change current to point the the newest node
		}
		LinkedList arrayList = new LinkedList(head); // Linked list is created using the head node
		return arrayList;
	}
}