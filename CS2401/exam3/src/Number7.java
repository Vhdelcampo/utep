public class Number7 {

	public static void main(String[] args) {

		NodeFor7 head = new NodeFor7((int)(Math.random()* (100)), null);	
		NodeFor7 temp = head;


		for (int i = 0; i <20; i++){
			temp.next = new NodeFor7((int)(Math.random()* (100)), null);	
			temp=temp.next;

		}

		printInfo(head);
		head = sortStocks(head);
		printInfo(head);
	}
	
	private static void printInfo(NodeFor7 head) {
		while(head != null){
			System.out.print(" " + head.data);
			head=head.next;
		}
		System.out.println();
	}


	private static NodeFor7 sortStocks(NodeFor7 head) {
		int size = 0;
		NodeFor7 first = head;
		while(first != null){
			size++;
			first=first.next;
		}
		boolean needNextPass = true;    
		NodeFor7 previous = head;
		for (int k = 1; k < size && needNextPass; k++) {
			needNextPass = false;			
			for (int i = 0; i < size- k; i++) {					
				if ((previous.data) > (previous.next.data)) {
					int temp = previous.data;
					previous.data = (int)previous.next.data;
					previous.next.data=temp;
					needNextPass = true; 
				}
				previous = previous.next;
			}
			previous = head;
		}
		return head;
	}

	private static NodeFor7 insertSorted(NodeFor7 list, int newValue) {
		if (list == null){
			NodeFor7 newNode = new NodeFor7(newValue,null);
			list = newNode;
		}
		else if(list.data> newValue){		
			NodeFor7 newNode = new NodeFor7(newValue, list);
			list = newNode;
		}
		else{
			NodeFor7 first = list;
			NodeFor7 second = first.next;
			while(second != null && newValue>second.data){
				first=first.next;
				second=second.next;
			}
			NodeFor7 newNode = new NodeFor7(newValue, second);
			first.next = newNode;
		}
		return list;
	}

	public static NodeFor7 insertionSortLinkedList(NodeFor7 head) {
		NodeFor7 newHead=null;
		NodeFor7 temp = newHead;
		while(head != null){
			newHead = insertSorted(temp, head.data );
			head = head.next;
			temp = newHead;
		}
		return newHead;
	}

}





