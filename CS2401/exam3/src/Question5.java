import java.util.Arrays;


public class Question5 {
	
	public static void main(String[] args){
		int a[] = {6,5,4,5,2,1,4,5,7,3,5,6,7};
		MergeSort(a);
		System.out.println( Arrays.toString(a) );
	}
	
	public static void InsertionSort(int[] x){
		for (int unsorted = 1; unsorted < x.length; unsorted++){
			int temp = x[unsorted];
			int i = unsorted;
			while ( i > 0 && x[i - 1] > temp){
				x[i] = x[i - 1];
				i--;
			}
			x[i] = temp;
		}
	}
	
	public static void MergeSort(int[] array){
		
		if (array.length < 5){
			InsertionSort(array);
		}
		
		
		else{
			int sizeOfFirstHalf = array.length / 2;
			int sizeOfSecondHalf = array.length - sizeOfFirstHalf;
			int[] firstHalf = new int[sizeOfFirstHalf];
			int[] secondHalf = new int[sizeOfSecondHalf];
			
			for (int i = 0; i < sizeOfFirstHalf; i++){
				firstHalf[i] = array[i];
			}
			
			for (int i = 0; i < sizeOfSecondHalf; i++){
				secondHalf[i] = array[i + sizeOfFirstHalf];
			}
		
			MergeSort(firstHalf);
			MergeSort(secondHalf);
			Merge(firstHalf, secondHalf, array);
		}
	}
		
	public static void Merge( int[] firstHalf, int[] secondHalf, int[] array){
		int i = 0;
		int j = 0;
		for (int k = 0; k < array.length; k++){
			
			if ( j >= secondHalf.length || (i < firstHalf.length) && firstHalf[i] < secondHalf[j]) 
				{
				array[k] = firstHalf[i];
				i++;
			}
			else 
				{
				array[k] = secondHalf[j];
				j++;
				}
	
		}
	}	
}
