import java.io.*;
import java.util.*;

/**
 * Problem #1
 * Given a double array describing measurements
 * Find out the difference between f(x1,...,xn) and y for each row
 * y is the first column of the array
 * x is the rest of the columns
 * f(x1,...,xn) = x1+x2+...+xn
 * 
 * @author Victor Del Campo
 * #80509902
 * 2014-02-04
 *
 */

public class exercise1 {

	public static void main(String[] args) {
//		int array[][] = { {5,1,2,3},
//						  {7,1,2,4},
//						  {8,3,5,9} }; // hard code option
		
		Scanner scan = null;
		
		try {
		      scan = new Scanner(new File("array.txt")); //opening input file and holding scanner ref
		    } catch (FileNotFoundException e) {
		      e.printStackTrace();
		      return;
		    }
		
	    int rows = scan.nextInt();
	    int columns = scan.nextInt();
		int array[][] = new int[rows][columns];
		for (int i = 0; i < array.length;i++){
			for (int j = 0; j < array[i].length; j++){
				array[i][j] = scan.nextInt();
			}
		}
		
		int d[] = differences(array);
		for (int i = 0; i < d.length; i++)
			System.out.println("Differnce for row " + (i + 1) + ": " + d[i]);
		double avg = average(d);
		if (avg != 0) // Print average only when it is not 0
			System.out.println("Average difference:  " + avg);
		double percent = percentCorrect(d);
		System.out.println("Percentage correct:  " + percent + "%");
	}
	public static int[] differences(int[][] function){
		int diff[] = new int[function.length];
		int sum;
		for (int i = 0; i < function.length; i++){
			sum = 0; // Reset sum for each row
			for (int j = 1; j < function[i].length; j++){
				sum += function[i][j]; // Calculate sum (x1+x2+...+xn)
				if (j == function[i].length - 1) // Change difference array value only when j reaches end of row
					diff[i] = Math.abs(sum - function[i][0]); // difference between first value in row (y) and sum (x1+x2+...+xn)
			}	
		}
		return diff;
	}
	public static double average(int[] diffs){ // average of values in difference array
		double sum = 0;
		for (int i = 0; i < diffs.length; i++)
			sum += diffs[i];
		return (sum / diffs.length);
	}
	public static double percentCorrect(int[] di){ // percent correct of values in difference array
		double correct = 0;
		for (int i = 0; i < di.length; i++){
			if (di[i] == 0)
				correct++;
		}
		return (correct / di.length * 100); // returns in percent form
	}
}
