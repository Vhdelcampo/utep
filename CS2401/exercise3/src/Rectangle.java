
public class Rectangle {
	private double length;
	private double width;
	Rectangle(){
		length = 1;
		width = 1;
	}
	
	Rectangle(double l, double w){
		length = l;
		width = w;
	}
	
	void setLength(double l){
		length = l;
	}
	
	void setWidth(double w){
		width = w;
	}
	
	double getLength(){
		return length;
	}
	
	double getWidth(){
		return width;
	}
	
	double getArea(){
		return length * width;
	}
	
	double getPerimeter(){
		return 2 * length + 2 * width;
	}
	

	
}
