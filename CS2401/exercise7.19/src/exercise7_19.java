/**
 * Exercise #7.19
 * 
 * Assignment: Write the following method that tests whether a two-dimensional array has 
 * four consecutive numbers of the same value, either horizontally, vertically, or diagonally.
 * 
 * @author Victor Del Campo
 * #80509902
 * 2014-02-13
 * CS2401 MW 12-120
 */
import java.util.*;

public class exercise7_19 {
	public static Scanner scan = new Scanner(System.in);
	
	public static void main(String[] args){

		System.out.print("Enter number of rows: ");
		int rows = scan.nextInt();
		System.out.print("Enter number of columns: ");
		int columns = scan.nextInt();
		
		System.out.println("Enter the values of the array");
		int [][]array = createArray(rows,columns); // creates array with user values
		System.out.println("Does the array have four consecutive numbers with the same value?");
		System.out.println(hasConsecutiveFour(array)); // prints out true when it has four consecutive numbers
		}
		
	// method to that uses a nested for loop to fill in an array with user values, given rows and columns
	public static int[][] createArray(int rows, int columns){
		int [][]a = new int[rows][columns];
			for (int i = 0; i < a.length; i++){
				for (int j = 0; j < a[i].length; j++){
					 a[i][j] = scan.nextInt(); // fill in array value with user input
				}
			}
			return a;
		}
		
	public static boolean hasConsecutiveFour(int[][] values){
		for (int k = 0; k < 10; k++){ // loops through array checking with different integers starting from 0 going to 9
			if (checkRows(values,k) == true)
				return true;
			if (checkColumns(values,k) == true)
				return true;
			if (checkBackwardDiagonal(values,k) == true)
				return true;
			if (checkForwardDiagonal(values,k) == true)
				return true;
		}
		return false; // false when all checks fail
	}
	// check rows
	public static boolean checkRows(int[][] values, int k){
		for (int i = 0; i < values.length; i++){
			int count = 0; // reset count to 0 after each row check
			for (int j = 0; j < values[i].length; j++){
				if (values[i][j] == k)
					count++; // how many times a number has appeared
				if (count == 4) // if there are 4 consecutive repeat numbers
					return true;
			}
		}
		return false;
	}	
	
	// check column
	public static boolean checkColumns(int[][] values, int k){
		for (int j = 0; j < values[0].length; j++){ // start at first column and go right
			int count = 0; // reset to count to 0 after each column check
			for (int i = 0; i < values.length; i++){ // start at first row and go down
				if (values[i][j] == k)
					count++; // how many times a number has appeared
				if (count == 4) // if there are 4 consecutive repeat numbers
					return true;
			}
		}
		return false;
	}
	
	// check \ diagonal
	// formula to find forward diagonal length for a matrix m x n at point a_ij is to calculate
	// m - i and n - j, and the smallest result will give the length.
	public static boolean checkBackwardDiagonal(int[][] values, int k){
		int diagonalLength;
		for (int i = 0; i < values.length ; i++){
			for (int j = 0; j < values[i].length; j++){
				int m = values.length; // rows
				int n = values[i].length; // columns
				int d1 = m - i;
				int d2 = n - j;
				if (d1 < d2) // always travel the smallest distance
					diagonalLength = d1;
				else
					diagonalLength = d2;
				int count = 0; // reset count after each diagonal check
				for (int distance = 0; distance < diagonalLength; distance++){ // starts going through diagonal
					if (values[i + distance][j + distance] == k) // moves down and right as distance increases
						count++; // how many times a number has appeared
					if (count == 4) // if there are 4 consecutive numbers
						return true;
				}
			}
		}
		return false;
	}
	
	// check / diagonal
	// formula to find forward diagonal length for a matrix m x n at point a_ij is to calculate
	// j - i - (n - m) + 1 for wide matrixes (rows are greater than columns)
	// j - i - (m - n) + 1 for tall matrixes (columns are greater than rows)
	// for square matrixes either formula will work
	public static boolean checkForwardDiagonal(int[][] values, int k){
		int diagonalLength;
		for (int i = 0; i < values.length; i++){ // start at first row and go down
			for (int j = values[0].length - 1; j > 0; j--){ // start at the the end of the row and go left
				int m = values.length; // rows
				int n = values[i].length; // columns
				if (m < n) // if it is a wide matrix
					diagonalLength = j - i- (n - m) + 1;
				else // if it is a tall matrix
					diagonalLength = j - i - (m - n) + 1;
				int count = 0; // reset count after each diagonal check
				for (int distance = 0; distance < diagonalLength; distance++){ // starts going through diagonal
					if (values[i + distance][j - distance] == k) // moves down and left as distance increases
						count++;
					if (count == 4)
						return true;
				}
			}
		}
		return false;
	}
}
	