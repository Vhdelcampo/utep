
public class LinearEquation {

private double a; 
private double b; 
private double c; 
private double d; 
private double e; 
private double f;

LinearEquation(double a1, double b1, double c1, double d1, double e1, double f1){
	a = a1;
	b = b1;
	c = c1;
	d = d1;
	e = e1;
	f = f1;
}

public double getA(){
	return a;
}

public double getB(){
	return b;
}

public double getD(){
	return d;
}

public double getE(){
	return e;
}

public double getF(){
	return f;
}

public void setA(int a1){
	a = a1;
}

public void setB(int b1){
	b = b1;
}

public void setC(int c1){
	c = c1;
}

public void setD(int d1){
	d = d1;
}

public void setE(int e1){
	e = e1;
}

public void setF(int f1){
	f = f1;
}

public boolean isSolvable(){
	if ((a * d - b * c) != 0)
		return true;
	else
		return false;
}

public double getX(){ // Cramer's Rule, determinant of x / denominator determinant
	return (e * d - b * f)/
		   (a * d - b * c);
}

public double getY(){ // Cramer's Rule, determinant of y / denominator determinant
	return (a * f - e * c)/
		   (a * d - b * c);
}


}
