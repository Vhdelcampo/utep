
public class PointForm {

	private double x1;
	private double y1;
	private double x2;
	private double y2;
	
	PointForm(double px1, double py1, double px2, double py2){
		x1 = px1;
		y1 = py1;
		x2 = px2;
		y2 = py2;
	}
	
	public double getX1(){
		return x1;
	}
	
	public double getY1(){
		return y1;
	}
	
	public double getX2(){
		return x2;
	}
	
	public double getY2(){
		return y2;
	}
	
	public void setX1(int px1){
		x1 = px1;
	}
	
	public void setY1(int py1){
		y1 = py1;
	}
	
	public void setX2(int px2){
		x2 = px2;
	}
	
	public void setY2(int py2){
		y2 = py2;
	}
	
	public double getSlope(){
		return (y2 - y1)/(x2 - x1);
	}
	
	// y - y1 = m(x - x1)    point slope
	// y = m(x - x1) + y1
	// y = mx - mx1 + y1
	// y - mx = (-mx1 + y1)
	// by - ax = - (c)
	
	// ax + by = c       	 standard form
	
	public double getA(){
		return -getSlope(); // -mx
	}
	
	public double getB(){
		return 1;
	}
	
	public double getC(){
		return (-getSlope() * x1 + getY1()); // -mx1 + y1
	}
	
}
