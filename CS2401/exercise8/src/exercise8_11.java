
public class exercise8_11 {

	public static void main(String[] args) {
		
		LinearEquation e = new LinearEquation(2.0, 2.0, 1.0, 1.0, 1.0, 1.0);
		boolean solved = e.isSolvable();
		if (solved == false)
			System.out.println("The equation has no solution");
		else
			System.out.print("Solution is : (" + e.getX() + "," + e.getY() + ")");
	}

}
