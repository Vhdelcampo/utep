import java.util.*;

public class exercise8_12 {
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Enter the endpoints of the first line segment: ");
		int x1 = sc.nextInt();
		int y1 = sc.nextInt();
		int x2 = sc.nextInt();
		int y2 = sc.nextInt();
		
		PointForm p1 = new PointForm(x1,y1, x2,y2); // Construct PointForm p1 using points given
		
		System.out.print("Enter the endpoints of the second line segment: ");
		x1 = sc.nextInt();
		y1 = sc.nextInt();
		x2 = sc.nextInt();
		y2 = sc.nextInt();
		PointForm p2 = new PointForm(x1,y1, x2,y2); // Construct PointForm p2 using points given
		
		// Construct LinearEquation object which contains two lines in standard form, by grabbing A, B, and C
		// from the PointForm objects and feeding it into the LinearEquation object.
		LinearEquation e = new LinearEquation( 
				p1.getA(),p1.getB(),p2.getA(), p2.getB(), // ax + by parts
				p1.getC(),p2.getC() // c parts
				);
		
		if (e.isSolvable() == false)
			System.out.println("The equation has no solution");
		else
			// Use getX and getY methods in LinearEquation object to get solution.
			System.out.print("The intersecting point is : (" + e.getX() + ", " + e.getY() + ")");
		sc.close();
	}
}
