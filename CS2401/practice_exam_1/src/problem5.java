
public class problem5 {

	public static void main(String[] args) {
		double[][]a = {
				{1,1,1},
				{2,2,2},
				{3,3,3}};
		
		shuffle2Darray(a);
		printBoard(a);
		}
	
	public static void shuffle2Darray(double[][] input) {
	for (int row = 0; row < input.length; row++) {
		int randomIndex = (int)(Math.random() * input.length);
		double[] tmp = input[row];
		input[row] = input[randomIndex];
		input[randomIndex] = tmp;
		}
	}
	
	public static void printBoard(double [][]board){
		for (int i = 0; i < board.length; i++){
			for (int j = 0; j < board[i].length; j++){
				if (j == 0) // Prints left edge at the beginning of a row
					System.out.print("|"); // Edge line
				if (board[i][j] < 0)
					// Negative numbers have an character so only one space is needed
					System.out.print(board[i][j] + " "); 
				else // Otherwise put two spaces between each number
					System.out.print(" " + board[i][j] + " ");
				if (j == board[i].length -1) // Prints right edge at the end of a row
					System.out.print("|"); // Edge line
			}
			System.out.println();
		}

	}
	
}
