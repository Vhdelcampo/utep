
public class problem6 {
	
	public static void main(String[] args){
		double[]a = {1,2,3,4};
		normalize(a);
		printBoard(a);
	}

	public static void normalize(double[] input) {
		double sum = arraySum(input);
		for (int i = 0; i < input.length; i++){
			input[i] = input[i]/sum;
		}
	}
	
	public static double arraySum(double [] a){
		double sum = 0;
		for (int i = 0; i < a.length; i++){
			sum += a[i];
		}
		return sum;
	}
	
	public static void printBoard(double []board){
		for (int i = 0; i < board.length; i++){
			System.out.println(board[i]);
		}
	}
}
