
public class problem7 {
	
	public static void main(String[] args){
		
		int[][]array = {{1,2},{3,4},{5,6}};
		
		printBoard(array);
		System.out.println();
		printBoard(transposeArray(array));
		
	}
	public static int[][] transposeArray(int[][]a){
		int m = a.length;
		int n = a[0].length;
		int b[][] = new int[n][m];
		for (int i = 0; i < b.length; i++){
			for (int j = 0; j < b[0].length; j++){
				b[i][j] = a[j][i];
			}
		}
		return b;
	}

	public static void printBoard(int [][]board){
		for (int i = 0; i < board.length; i++){
			for (int j = 0; j < board[i].length; j++){
				if (board[i][j] < 0)
					// Negative numbers have an character so only one space is needed
					System.out.print(board[i][j] + " "); 
				else // Otherwise put two spaces between each number
					System.out.print(" " + board[i][j] + " ");
			}
			System.out.println();
		}
	}
}
