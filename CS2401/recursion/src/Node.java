public class Node {

	// ATTRIBUTES: *****************************************************************
	// Note that all attributes are private (encapsulated)
	// I am designing nodes whose important content is data1 and data2
	private int data1;
	private double data2;
	// the following attribute is the link to / address of the next node 
	private Node nextNode;
	// this last attribute is to store information regarding whether the node at hand
	// is the first or last, or anything else as we see fit
	// useful for circular lists for instance where you might want to keep track differently about
	// which node in the circle you are at
	private String info;
	
	// METHODS: *******************************************************************
	
	// CONSTRUCTOR(S):
	//Node constructor
	public Node(int d1, double d2) {
		data1 = d1;
		data2 = d2;
	}
	
	public Node(int d1) {
		data1 = d1;
	}

	// ACCESSORS:
	// accessors to the private fields
	public int getData1() {
		return data1;
	}
	
	public double getData2() {
		return data2;
	}
	
	public Node getNext() {
		return nextNode;
	}
	
	public String getInfo() {
		return info;
	}
	
	// MUTATORS:
	// setters to set the fields to some "value"
	public void setData(int n) {
		data1=n;
	}
	
	public void setData(double d) {
		data2=d;
	}

	public void setNext(Node N) {
		nextNode = N;
	}

	public void setInfo(String s) {
		info = s;
	}

	// OTHER FUNCTIONALITIES:
	//Print Node data
	public void printNode() {
		System.out.print("{" + data1 + ", " + data2 + "} ");
	}

}
