import java.util.*;

public class r	{
	static int fibonacciCalls = 0;
	static int binaryCalls = 0;
	
	public static void main(String[] args){
	System.out.println("Fibonacci number 6 is: " + Fibonacci(6));
	System.out.println("Calls that have been made: " + fibonacciCalls);
	
	int[] arr = {1,2,3,4,5,6,7,8,9,10};
	System.out.println("New sorted array is:");
	System.out.println(Arrays.toString(arr));
	int foundIndex = binarySearch(arr, 6);
	System.out.println("The index at which 6 was found is: " + foundIndex);
	System.out.println("Calls that have been made: " + binaryCalls);
	
	int[] array = {1,2,3,4,5,6,7,8,9,10};
	MyLinkedList l = toLinkedListFromArray(array);
	printReverse(l.getFirst());
}
	
	public static int Fibonacci(int n){
		fibonacciCalls++;
		if (n == 0)
			return 0;
		else if (n == 1)
			return 1;
		else{
			return Fibonacci(n - 2) + Fibonacci(n - 1);
		}
	}
	
	public static int binarySearch(int[] a, int key){
		binaryCalls++;
		int low = 0;
		int high = a.length - 1;
		return BS(a, key, low, high);
	}
	
	
	private static int BS(int []a, int key, int low, int high){
		binaryCalls++;
		if (high < low)
			return -1;
		int m = (int)((low + high) / 2);
		if (a[m] == key)
			return m;
		else if (a[m] > key){
			high = m - 1;
			return BS(a, key, low, high);
		}
		else {
			low = m + 1;
			return BS(a, key, low, high);
		}
	}
	
	public static void printReverse(Node n){
		if (n.getNext() == null)
			System.out.print(n.getData1() + " ");
		else{
			printReverse(n.getNext());
			System.out.print(n.getData1()  + " ");
		}
	}
	
	public static int GCD(int m, int n){
		if (m % n == 0)
			return n;
		else
			return GCD(n, m % n);
	}
	
	public static MyLinkedList toLinkedListFromArray(int[] a){
		MyLinkedList arrayList = new MyLinkedList(); // Linked list
		for (int i = 0; i < a.length; i++){ // Start process to build a linked list forward
			arrayList.insertEnd(a[i], 0);
		}
		return arrayList;
	}
	
//	public static insertSort(int[] array){
//		if (arraylength == 1){
//			
//		}
//	}
//		
//	public static insertSort(int[] array, int length){
//		
//	}
//	
//	public static insertAfterPosition(int[]a, int position){
//		
//	}
}
