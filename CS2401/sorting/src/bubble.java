import java.util	.*;
public class bubble {
	public static void main(String[] args){
		int a[] = {5,4,3,2,1};
		bubbleSort(a);
		System.out.println(Arrays.toString(a));
	}
	
public static void bubbleSort(int [] array){
	int temp;
	int count = array.length;
	while (count > 0){
		for (int i = 0; i < array.length - 1; i++){
			if (array[i] > array[i + 1]){
				temp = array[i];
				array[i] = array[i + 1];
				array[i + 1] = temp;
			}
		}
		count--;
	}
}
}
