import java.util.Arrays;


public class insert {
	public static void main(String[] args){
		int a[] = {6,5,4,5,2,1,4,5,7,3,5,6,7};
		insertSort(a);
		System.out.println(Arrays.toString(a));
	}
	
	public static void insertSort(int[] x){
		for (int unsorted = 1; unsorted < x.length; unsorted++){
			int temp = x[unsorted];
			int i = unsorted;
			while ( i > 0 && x[i - 1] > temp){
				x[i] = x[i - 1];
				i--;
			}
			x[i] = temp;
		}
	}
}
