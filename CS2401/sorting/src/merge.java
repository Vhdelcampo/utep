import java.util.Arrays;
public class merge {
	
	public static void main(String[] args){
		int a[] = {6,5,4,5,2,1,4,5,7,3,5,6,7};
		mergeSort(a);
		System.out.println(Arrays.toString(a));
	}
	
public static void mergeSort(int[] array){
	if ( array.length == 1 || array.length == 0){
		return;
	}
	int sizeOfFirstHalf = array.length / 2;
	int sizeOfSecondHalf = array.length - sizeOfFirstHalf;
	int []firstHalf = new int[sizeOfFirstHalf];
	int []secondHalf = new int[sizeOfSecondHalf];
		
	for (int i = 0; i < sizeOfFirstHalf; i++){
		firstHalf[i] = array[i];
	}
	for (int i = 0; i < sizeOfSecondHalf; i++){
		secondHalf[i] = array[i + sizeOfFirstHalf];
	}
			
	mergeSort(firstHalf);
	mergeSort(secondHalf);
			
	int i = 0;
	int j = 0;
	for (int k = 0; k < array.length; k++){
		if ( j >= secondHalf.length || (i < firstHalf.length) && firstHalf[i] < secondHalf[j]) 
			{
				array[k] = firstHalf[i];
				i++;
			}
		else 
			{
				array[k] = secondHalf[j];
				j++;
			}
	}	
}
}
