import java.util.*;
public class ForU {
	public static void main(String[] args){
		int[] A = {0, 0, 1, 1, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7};
		System.out.println(Arrays.toString(ALLX(A) ) );
	}
	public static int[] ALLX(int[] A){
		int[] B = new int[A.length];
		int index = 0;
		int count = 0;
		for (int i=1; i<A.length; i++){
			int previous = A[i - 1];
			int current  = A[i];
			if (current == previous){
				if (count < 2){
					B[index] = current;
					index++;
				}
			}
			else{
				B[index] = current;
				index++;
				count = 0;
			}
			count++;
		}
		return B;
		}
	}

//ALLX
//0 0 0 0 1 1 1 1 4 4 4 4 5 5 5 5
//ALLX in order
//0 0 1 1 4 4 5 5 0 0 1 1 4 4 5 5
//ALLX
//0 0 0 0 1 1 1 1 2 2 3 3 4 4 4 4 5 5 5 5
//ALLX in order
//0 0 1 1 2 2 3 3 4 4 5 5 0 0 1 1 4 4 5 5
//ALLX
//0 0 1 1 2 2 3 3 4 4 4 4 5 5 5 5
//ALLX in order
//0 0 1 1 2 2 3 3 4 4 5 5 4 4 5 5
//ALLX
//0 0 1 1 1 1 2 2 3 3 4 4 5 5 6 6 7 7
//ALLX in order
//1 1 3 3 4 4 5 5 0 0 1 1 2 2 6 6 7 7
