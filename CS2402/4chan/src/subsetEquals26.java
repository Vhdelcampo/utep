import java.util.*;

public class subsetEquals26 {

	static int[][] subsetsEqualTo26(String num){
		int[][] list = new int[num.length()][];
		String temp = "";
		LinkedList<Integer> listWith26Number = new LinkedList<Integer>();
		for (int i=0; i<num.length(); i++){
			temp += num.charAt(i);
			for (int j=0; j<num.length(); j++){
				if (i!=j) temp += num.charAt(j);
				char[] tempChar = temp.toCharArray();
				int sum = 0;
				for (int k=0; k<tempChar.length; k++){
					sum += tempChar[k];
				}
				if (sum < 26) listWith26Number.add(sum);
				else 
					listWith26Number.add(num.charAt(j));
			}
		}
		
		return list;
	}
}
