
public class Edge implements Comparable<Edge>{
	public int u;
	public int v;
	public int weight = 1;
	
	public Edge(int u, int v){
		this.u = u;
		this.v = v;
	}
	
	public int compareTo(Edge E) {
		return 0;
	}
}
