import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;
public class algorithms {
	public static void printList(gNode adjList[]){
		for (int i=0; i<adjList.length; i++){
			System.out.print(i + "| ");
			for (gNode j=adjList[i]; j!=null; j=j.next){
				System.out.print(j.vertex + " ");
			}
			System.out.println();
		}
	}
	
	public static void printPath(int[] path, int s, int d){
		if (path[d]>=1)
			printPath(path, 0, path[d]);
		System.out.print(d + " ");
	}
	
	public static int[] breadthFirstSearch(gNode adjList[], int source){
		int size = adjList.length;
		int[] path = new int[size];
		boolean[] visited = new boolean[size];
		for (int i=0; i<path.length; i++){
			path[i] = -1;
		}
		visited[source] = true;
		Queue<Integer> Q = new LinkedList<Integer>();
		Q.add(source);
		while (!Q.isEmpty()){
			int u = Q.remove();
			for (gNode i=adjList[u]; i!=null; i=i.next){
				int v = i.vertex;
				if (!visited[v]){
					visited[v] = true;
					path[v] = u;
					Q.add(v);
				}
			}
		}
		return path;
	}
	
	public static void depthFirstSearch(gNode[] adjList, boolean[] visited, int[] path, int source){
		visited[source] = true;
		for (gNode i=adjList[source]; i!=null; i=i.next){
			int v = i.vertex;
			if (!visited[v]){
				path[v] = source;
				depthFirstSearch(adjList, visited, path, v);
			}
		}
	}
	
	public static gNode[] kruskal(gNode[] adjList){
		int n = adjList.length;
		gNode[] MST = new gNode[n];
		DSF S = new DSF(n);	
		PriorityQueue<Edge> PQ= new PriorityQueue<Edge>();
		int edges = 0;
		
		for (int i=0; i<n; i++){
			for (gNode j=adjList[i]; j!=null; j=j.next){
				PQ.add(new Edge(i, j.vertex));
				edges++;
			}
		}
		while (edges > n-1){
			edges--;
			Edge ei = PQ.remove();
			int u = ei.u; int v = ei.v;
			if (!S.isInSameSet(u, v)){
				MST[u] = new gNode(v, MST[u]);
				MST[v] = new gNode(u, MST[v]);
				S.union(u, v);
			}
		}
		return MST;
	}
	
	public static gNode[] prim(gNode[] adjList, int source){
		int n = adjList.length;
		gNode[] MST = new gNode[n];
		Heap H = new Heap(n);
		
		
		return MST;
	}
	
	public static gNode[] topological(gNode[] adjList){
		int n = adjList.length;
		gNode[] top = new gNode[n];
		Queue<Integer> Q = new LinkedList<Integer>();
		for (int i=0; i<n; i++){
			if (adjList[i] == null)
				Q.add(i);
		}
		while (!Q.isEmpty()){
			int u = Q.remove();
			
		}
		return top;
	}
}
