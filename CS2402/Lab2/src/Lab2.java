
public class Lab2 {
	public static void main(String[] args){
		Node x = null;
		for (int i = 10; i > 0; i--){
			x = new Node(i,x);
		}
		Node y = null;
		for (int i = 20; i > 10; i--){
			y = new Node(i,y);
		}
	
//		printList(y);
		
//		addK(x,10);
//		printList(x);
		
//		printList( odd(x) );
		
//		append(x,y);
//		printList(x);
		
		printList(x);
		x = even(x);
		printList(x);
		
		x = rotate(x);
		printList(x);
		
	}
	
	public static void printList(Node h){
		while (h != null){
			h.printNode();
			h = h.next;
		}
		System.out.println();
	}
	
	// Receive a reference to the head of a list h and an integer n and add n to each element in the list
	public static Node addK(Node h, int n){
		while (h != null){
			h.item += n;
			h = h.next;
		}
		return h;
	}
	
	public static int kElement(Node h, int n){
		int count = 0;
		while (h != null){
			if (count == n) return h.item;
			h = h.next;
			count++;
		}
		return -1;
	}
	
	public static Node largest(Node h){
		Node largest = new Node(Integer.MIN_VALUE);
		while (h != null){
			if (h.item > largest.item) largest = h;
			h = h.next;
		}
		return largest;
	}
	
	public static Node smallest(Node h){
		Node smallest = new Node(Integer.MAX_VALUE);
		while (h != null){
			if (h.item < smallest.item) smallest = h;
			h = h.next;
		}
		return smallest;
	}
	
	public static int nthSmallest(Node h, int n){
		
		return 0;
	}
	
	public static Node rotate(Node h){
		Node x = null;
		while (h!= null){
			x = new Node(h.item, x);
			h = h.next;
		}
		return x;
	}
	
	public static Node odd(Node h){
		int count = 1;
		Node current;
		Node previous = new Node();
		Node x = previous;
		while (h!= null){
			if (count % 2 == 1){ // if odd
				current = new Node(h.item);
				previous.next = current;
				previous = current;
			}
			count++;
			h = h.next;
		}
		x = x.next;
		return x;
	}
	
	public static Node even(Node h){
		int count = 1;
		Node current;
		Node previous = new Node();
		Node x = previous;
		while (h!= null){
			if (count % 2 == 0){ // if even
				current = new Node(h.item);
				previous.next = current;
				previous = current;
			}
			count++;
			h = h.next;
		}
		x = x.next;
		return x;
	}
	
	public static void append(Node h, Node i){
		if (h != null){
			while (h.next!= null){
				h = h.next;
			}
			h.next = i;
		}
	}
	
	
}


