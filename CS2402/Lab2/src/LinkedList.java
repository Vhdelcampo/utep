
public class LinkedList {
	public Node head; // First node of list
	public Node tail; // Last node of list
	
	LinkedList(){ // default constructor
		this.head = null;
		this.tail = head;
	}
	
	LinkedList(Node node){ // Constructor if given only node.
		this.head = node;
		this.tail = head;
	}
	
	LinkedList(int item){ // Constructor if given only item of node.
		this.head = new Node(item);
		this.tail = head;
	}
	
	LinkedList(int item, Node next){
		this.head = new Node(item, next);
		this.tail = head;
	}
}
