public class Node {
	public Node next;
	public int item;
	
	Node(){ // Default constructor is an empty node or node with null values
	}
	
	Node(int item){ // If only content is given Node will be constructed with a null next
		this.item = item;
		this.next = null;
	}
	
	Node(int item, Node next){
		this.item = item;
		this.next = next;
	}
	
	public void printNode(){
		System.out.print(item + " ");
	}
	
	
}
