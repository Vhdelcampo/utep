/**********************************************
** Simple Program to implement insertions    **
** and traversal on B-trees                  **
** This file includes the basic B-tree class **
** Programmed by Olac Fuentes                **
** Last modified June 5, 2012                **
** Report bugs to me                         **
**********************************************/

public class BTree{
	private BTreeNode root;
	private int T; //2T is the maximum number of children a node can have
	private int height;
	private int keys;
	private int nodes;
	
	public BTree(int t){
		root = new BTreeNode(t);
		T = t;
		height = 0;
	}
	
	public BTree(int t, BTreeNode root){
		this.root = root;
		nodes++;
		T = t;
		height = 0;
	}
	
	public BTreeNode getRoot(){
		return root;
	}
	
	public int getT(){
		return T;
	}
	
	public int getKeys(){
		return keys;
	}
	
	public void printHeight(){
		System.out.println("Tree height is " + height);
	}
	
	public void printNumberOfKeys(){
		System.out.println("Number of keys is " + keys);
	}
	
	public void printNumberOfNodes(){
		System.out.println("Number of nodes is " + (nodes + root.getNumberOfNodes() ) );
	}
	
	public void insert(int newKey){
		if ( !inTree(root,newKey)){
			if (root.isFull() ){//Split root;
				split();
				height++;
			}
			keys++;
			root.insert(newKey);
		}
	}
	
	public static boolean inTree(BTreeNode T, int newKey){
	 	boolean valid = false;
        if(T.isLeaf){
            for(int i = 0; i < T.n; i++){
                if(newKey == T.key[i])
                	return true;
            }
        } 
        else {
            for(int i = 0; i < T.n; i++){
            	if(newKey == T.key[i])
            		return true;
            	valid =  valid || inTree(T.c[i], newKey);
            }
            valid = valid || inTree(T.c[T.n], newKey);
        }
        return valid;
	}	
	
	public void print(){
	// Wrapper for node print method
		root.print();
	}
	
	public void printNodes(){
	// Wrapper for node print method
		root.printNodes();
	}

	public void split(){
	// Splits the root into three nodes.
	// The median element becomes the only element in the root
	// The left subtree contains the elements that are less than the median
	// The right subtree contains the elements that are larger than the median
	// The height of the tree is increased by one

		//System.out.println("Before splitting root");
		//root.printNodes(); // Code used for debugging
		BTreeNode leftChild = new BTreeNode(T);
		BTreeNode rightChild = new BTreeNode(T);
		nodes+=2;
		leftChild.isLeaf = root.isLeaf;
 		rightChild.isLeaf = root.isLeaf;
		leftChild.n = T-1;
		rightChild.n = T-1;
		int median = T-1;
		for (int i = 0;i<T-1;i++){
			leftChild.c[i] = root.c[i];
			leftChild.key[i] = root.key[i];
		}
		leftChild.c[median]= root.c[median];
		for (int i = median+1;i<root.n;i++){
			rightChild.c[i-median-1] = root.c[i];
			rightChild.key[i-median-1] = root.key[i];
		}
		rightChild.c[median]=root.c[root.n];
		root.key[0]=root.key[median];
		root.n = 1;
		root.c[0]=leftChild;
		root.c[1]=rightChild;
		root.isLeaf = false;
		
		//System.out.println("After splitting root");
		//root.printNodes();
	}
}