/***********************************************
 ** Simple Program to implement insertions     **
 ** and traversal on B-trees                   **
 ** Main program to build and print B-tree     **
 ** Programmed by Olac Fuentes                 **
 ** Last modified June 5, 2012                 **
 ** Report bugs to me                          **
 ************************************************/

public class BTreeTest{

	public static void main(String[] args)   {
		BTree tree = new BTree(4);

		int max = 200;
		for (int i = 1; i < max + 1; i++){
			tree.insert(i);
		}

		double startingTime;
		/*
		 * 1. Modify the insertion operation to prevent duplicate elements from being inserted in the tree. If the
		 *user tries to insert an element that is already in the tree, your program should simply ignore the
		 *request.
		 */
		startingTime = System.nanoTime();
		tree.insert(max);
		System.out.println("Tree after trying to insert duplicate element");
		tree.printNodes();

		/* 2. Add a variable to the BTree object to store the number of keys in the tree and make sure the variable 
		 * is kept up to date after every insertion operation (observe how the height of the tree is kept in a 
		 * variable for efficiency, but it could also be easily computed).
		 */
		tree.printNumberOfKeys();

		/* 3. Add a variable to the BTree object to store the number of nodes in the tree and make sure the 
		 * variable is kept up to date after every insertion operation.
		 */
		tree.printNumberOfNodes();
		System.out.println();

		// (a) Print the keys in the tree in ascending order
		startingTime = System.nanoTime();
		System.out.println("Tree in Ascending order");
		printAscending(tree.getRoot() );
		System.out.println();
		timeDifference(startingTime);
		System.out.println();

		// (b) Print the keys in the tree in descending order
		startingTime = System.nanoTime();
		System.out.println("Tree in Descending order");
		printDescending(tree.getRoot() );
		System.out.println();
		timeDifference(startingTime);
		System.out.println();

		// (c) Determine if a given element k is in the tree.
		startingTime = System.nanoTime();
		int k = 26;
		System.out.println( "is " + k  + " in tree? " + inTree(tree.getRoot(), k) );
		timeDifference(startingTime);
		System.out.println();

		// (d) Return the minimum element in the tree
		startingTime = System.nanoTime();
		System.out.print( "Smallest: ");
		printSmallest(tree.getRoot() );
		timeDifference(startingTime);
		System.out.println();

		// (e) Return the maximum element in the tree
		startingTime = System.nanoTime();
		System.out.print( "Largest: ");
		printLargest(tree.getRoot() );
		timeDifference(startingTime);
		System.out.println();

		//(f) Return the number of nodes in the tree
		startingTime = System.nanoTime();
		System.out.println( "Number of nodes: " + findNumberOfNodes( tree.getRoot() ) );
		timeDifference(startingTime);
		System.out.println();

		//(g) Return the number of keys in the tree
		startingTime = System.nanoTime();
		System.out.println( "Number of keys: " + findNumberOfKeys( tree.getRoot() ) );
		timeDifference(startingTime);
		System.out.println();

		//(h) Return the number of leaves in the tree
		startingTime = System.nanoTime();
		System.out.println( "Number of leaves: " + findNumberOfLeaves( tree.getRoot() ) );
		timeDifference(startingTime);
		System.out.println();

		//(i) Given an integer d, print all the keys in the tree that have depth d
		startingTime = System.nanoTime();
		int d = 1;
		System.out.println("Nodes of depth " + d + " ");
		printOfDepth( tree.getRoot() , d);
		System.out.println();
		timeDifference(startingTime);
		System.out.println();

		//(j) Extract the keys in the tree into a sorted array.
		startingTime = System.nanoTime();
		int[] array = createSortedArray(tree);
		System.out.println("Sorted Array");
		for (int i = 0; i < array.length; i++){
			System.out.print( array[i] + " ");
		}
		System.out.println();
		timeDifference(startingTime);
		System.out.println();

		/* 
		 * (k) Given an integer d and a b-tree  T, build and return a B-tree that consists of the first d levels 
		 * of T. Thus, if d == 0 you method should return a copy of the root of T and if d == height(T) 
		 * your method should return a copy of the whole tree T.
		 */
		int depth = 0;
		BTreeNode t = buildTreeOfDepth(tree.getRoot(), depth);
		System.out.println("Tree created from nodes of depth " + depth); 
		t.printNodes();
		timeDifference(startingTime);
	}

	public static void timeDifference(double startingTime){
		double timeElapsed = System.nanoTime() - startingTime;
		System.out.println("Time taken for method: " + timeElapsed);
	}

	public static BTreeNode buildTreeOfDepth(BTreeNode tree, int d){
		if (d == 0){
			BTreeNode t = new BTreeNode( tree.key[0] );
			for (int i = 0; i < tree.n; i++){
				t.insert(tree.key[i]);
			}
			return t;
		}
		if (tree.isLeaf)
			System.out.println("Depth not reachable");
		for (int i = 0; i < tree.n; i++){
			buildTreeOfDepth( tree.c[i], d - 1);
		}

		return tree;
	}

	public static boolean inTree(BTreeNode T, int newKey){
		boolean valid = false;
		if(T.isLeaf){
			for(int i = 0; i < T.n; i++){
				if(newKey == T.key[i])
					return true;
			}
		} 
		else {
			for(int i = 0; i <= T.n; i++){
				if(newKey == T.key[i])
					return true;
				valid =  valid || inTree(T.c[i], newKey);
			}
		}
		return valid;
	}


	static void printOfDepth(BTreeNode tree, int d){
		if (d == 0)
			for (int i = 0; i < tree.n; i++){
				System.out.print( tree.key[i] + " ");
			}
		else if (tree.isLeaf)
			System.out.println("error");
		else {
			for (int i = 0; i < tree.n; i++){
				printOfDepth( tree.c[i], d - 1);
			}
		}
	}

	static void printLargest(BTreeNode tree){
		if (tree.isLeaf)
			System.out.println( ( tree.key[tree.n - 1]) );
		else
			printLargest( tree.c[tree.n]);
	}

	static void printSmallest(BTreeNode tree){
		if (tree.isLeaf)
			System.out.println( ( tree.key[0]) );
		else
			printSmallest( tree.c[0]);
	}

	static void printAscending(BTreeNode tree){
		if (tree.isLeaf){
			for (int i = 0; i < tree.n; i++){
				System.out.print(tree.key[i] + " ");
			}
		}
		else {
			for (int i = 0; i <= tree.n; i++){
				printAscending(tree.c[i]);
				System.out.print(tree.key[i] + " ");
			}
		}
	}

	static int[] createSortedArray(BTree tree){
		int[] sortedArray = new int[tree.getKeys()];
		createSortedArray(tree.getRoot(), sortedArray, 0);
		return sortedArray;
	}

	static void createSortedArray(BTreeNode tree, int[] sortedArray, int index){
		if (tree.isLeaf){
			for (int i = 0; i < tree.n; i++){
				sortedArray[index + i] = tree.key[i];
			}
		}
		else {
			for (int i = 0; i < tree.n; i++){
				createSortedArray( tree.c[i], sortedArray, index);
				index += tree.c[i].n;
				sortedArray[index] = tree.key[i];
			}
			createSortedArray(tree.c[tree.n], sortedArray, index);
		}
	}

	static void printDescending(BTreeNode tree){
		if (tree.isLeaf){
			for (int i = tree.n - 1; i >= 0; i--){
				System.out.print(tree.key[i] + " ");
			}
		}
		else {
			for (int i = tree.n; i > 0; i--){
				printDescending(tree.c[i]);
				System.out.print(tree.key[i - 1] + " ");
			}
			printDescending(tree.c[0]);
		}
	}

	static int height(BTreeNode tree){
		if (tree.isLeaf)
			return 0;
		else
			return height(tree.c[0]) + 1;
	}

	static int findNumberOfKeys(BTreeNode tree){
		if (tree.isLeaf)
			return tree.n;
		else {
			int count = tree.n;
			for (int i = 0; i <= tree.n; i++){
				count += findNumberOfKeys(tree.c[i]);
			}
			return count;
		}
	}

	static int findNumberOfFullNodes(BTreeNode tree){
		int temp = 0;
		if (tree.isLeaf){
			if (tree.isFull() )
				return 1;
		}
		else {
			for (int i = 0; i <= tree.n; i++){
				temp += findNumberOfFullNodes(tree.c[i]);
			}
			if (tree.isFull() )
				temp++;
		}
		return temp;
	}

	static int findNumberOfNodes(BTreeNode tree){
		int count = 0;
		if (tree.isLeaf){
			return 1;
		}
		else {
			for (int i = 0; i <= tree.n; i++){
				count += findNumberOfNodes(tree.c[i]);
			}
			count++;
		}
		return count;
	}

	static int findNumberOfLeaves(BTreeNode tree){
		int count = 0;
		if (tree.isLeaf){
			return 1;
		}
		else {
			for (int i = 0; i <= tree.n; i++){
				count += findNumberOfLeaves(tree.c[i]);
			}
		}
		return count;
	}

}