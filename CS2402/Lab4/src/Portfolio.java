
public class Portfolio {
	Stock []H;
	int totalElements;
	int doubles;
	
	public Portfolio(int k){
		H = new Stock[k];
	}
	
	public int size(){
		return H.length;
	}
	
	public int numberOfElements(){
		return totalElements;
	}
	
	public void printNumberOfElements(){
		System.out.println("Number of Elements: " + totalElements);
	}
	
	public int stringValue(String s){
		int sum = 1;
		for (int i = 0; i < s.length(); i++){
			char letter = s.charAt(i);
			int c = (int)(letter);
			sum *= c;
		}
		return sum;
	}
	
	public void insert(Stock s){
		insert(H, s);
	}
	
	public void insert(Stock[]hash, Stock s){
		int h = stringValue(s.name) % hash.length;
		s.next = hash[h];
		hash[h] = s;
		totalElements++;
		balanceHashTable(1);
	}
	
	
	public void split(){
		int size = H.length * 2 + 1;
		Stock[] H2 = new Stock[size];
		for(int i = 0; i < H.length; i++){
			for (Stock j = H[i]; j != null; j=j .next){
				copy(H2, j);
			}
		}
		H = H2;
	}
	
	public void copy(Stock[]hash, Stock s){
		int h = stringValue(s.name) % hash.length;
		Stock st = new Stock( s.value, s.name, s.transactions);
		st.next = hash[h];
		hash[h] = st;
	}
	
	public double currentLoadFactor(){
		return (double)(totalElements) / H.length;
	}
	
	public void printCurrentLoadFactor(){
		System.out.println("Current load factor: " + currentLoadFactor() );
	}
	
	public void balanceHashTable(double loadFactor){
		while ( currentLoadFactor() >= loadFactor){
			doubles++;
			split();
		}
	}
	
	public Stock find(String n){
		return find(H, n);
	}
	
	public Stock find(Stock[] H, String n){
		int h = stringValue(n) % H.length;
		for (Stock i = H[h]; i != null; i = i.next){
			if ( i.name.equals(n) )
				return i;
		}
		return null;
	}
	
	public int collisions(){
		int numCollisions = 0;
		for (int i = 0; i<H.length; i++){
			for (Stock j = H[i]; j != null; j = j.next){
				if (j.next != null)
					numCollisions++;
			}
		}
		return numCollisions;
	}
	
	public int emptyElements(){
		int numEmptyElements = 0;
		for (int i = 0; i < H.length; i++){
			if (H[i] == null)
				numEmptyElements++;
		}
		return numEmptyElements;
	}
	
	public int numberOfNodes(Stock s){
		if (s == null)
			return 0;
		int count = 1;
		for (Stock i = s; i != null; i = i.next){
			count++;
		}
		return count;
	}
	
	public int longestList(){
		int longestListLength = 0;
		for (int i = 0; i < H.length; i++){
			int count = 0;
			
			for (Stock j = H[i]; j != null; j = j.next){
				count++;
			}
			if (count > longestListLength)
				longestListLength = count;
		}
		return longestListLength;
	}
	
	public void printLongestList(){
		System.out.println("The longest list has length: " + longestList() );
	}
	
	public Stock largest(){
		double largestSoFar = 0;
		Stock largest = new Stock();
		for (int i = 0; i < H.length; i++){
			for (Stock j = H[i]; j != null; j =j .next){
				if (j.value > largestSoFar){
					largestSoFar = j.value;
					largest = j;
				}
			}
		}
		return largest;
	}
	
	public void printLargest(){
		Stock largest = largest();
		System.out.println("The stock with the largest value is " + largest.name + ", with a value of " + largest.value );
	}
	
	public Stock smallest(){
		double smallestSoFar = Double.MAX_VALUE;
		Stock smallest = new Stock();
		for (int i = 0; i < H.length; i++){
			for (Stock j = H[i]; j != null; j = j.next){
				if (j.value < smallestSoFar){
					smallestSoFar = j.value;
					smallest = j;
				}
			}
		}
		return smallest;
	}
	
	public void printSmallest(){
		Stock smallest = smallest();
		System.out.println("The stock with the smallest value is " + smallest.name + ", with a value of " + smallest.value );
	}
	
	public Stock mostTransactions(){
		int t = 0;
		Stock mostTransactions = new Stock();
		for (int i = 0; i < H.length; i++){
			for (Stock j = H[i]; j != null;j =j .next){
				if ( j.transactions > t){
					mostTransactions = j;
					t = j.transactions;
				}
			}
		}
		return mostTransactions;
	}
	
	public void printMostTransactions(){
		Stock most = mostTransactions();
		System.out.println("The stock with the most transactions is " + most.name + ", with total transaction(s) " + most.transactions );
	}
	
	public void printNames(){
		for (int i = 0; i < H.length; i++){
			System.out.println();
			System.out.print(i + "| ");
			for (Stock j = H[i]; j != null; j = j.next){
				System.out.print(j.name + " ");
			}
		}
		System.out.println();
	}
	
	
	public void printTransactions(){
		for (int i = 0; i < H.length; i++){
			System.out.println();
			System.out.print(i + "| ");
			for (Stock j = H[i]; j != null; j = j.next){
				System.out.print(j.transactions + " ");
			}
		}
		System.out.println();
	}
	
	public void printValues(){
		for (int i = 0; i < H.length; i++){
			System.out.println();
			System.out.print(i + "| ");
			for (Stock j = H[i]; j != null; j = j.next){
				System.out.format("%.2f ", j.value);
			}
		}
		System.out.println();
	}
	
}
