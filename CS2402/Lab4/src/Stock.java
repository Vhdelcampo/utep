
public class Stock{
	public double value;
	public String name;
	public Stock next;
	public int transactions;
	
	public Stock(){
	}
	
	public Stock(double value, String name){
		this.value = value;
		this.name = name;
	}
	
	public Stock(double value, String name, int transactions){
		this.value = value;
		this.name = name;
		this.transactions = transactions;
	}
	
	public Stock(double value, String name, Stock next){
		this.value = value;
		this.name = name;
	}
	
	
}
