import java.util.*;

public class Stock_simulator {
	static double startingTime;
	public static void main(String[] args){
		
		Scanner sc = new Scanner(System.in);
		while(true){
			System.out.print( "Enter the default value of the stock: ");
			double defaultValue = sc.nextDouble();
			System.out.print( "Enter the number of transactions: ");
			double numTransactions = sc.nextInt();
			System.out.print("Display Transactions? (Y/N) ");
			
			String choice = sc.next();
			int hashSize = 11;
			Portfolio p = new Portfolio(hashSize);
			Random r = new Random();
			String name;
			
			while ( p.numberOfElements() < numTransactions){
				// Create a string of length 1-4
				name = "" + (char)(65 + r.nextInt(26) );
				for(int k = 0; k < r.nextInt(4); k++) {
					name += (char)(65 + r.nextInt(26) );
				}
				
				double transaction = r.nextGaussian() * 3.;
				if (choice.equals("Y") || choice.equals("y"))
					System.out.println(name + " " + transaction);
				if (transaction != 0){
					Stock s = p.find(name); // check if name is a duplicate of an existing stock
					if (s == null){ // s is null there is no duplicate
						double value = defaultValue + transaction;
						p.insert(new Stock(value, name, 1) );
					} else{ // s is a duplicate so modify it
						s.value += transaction;
						s.transactions++;
					}	
				}
			}
			// Print hash table
//			p.printNames();
//			p.printTransactions();
//			p.printValues();
			
			resetTime();
			p.printSmallest();
			timeDifference();
			
			resetTime();
			p.printLargest();
			timeDifference();
			
			resetTime();
			p.printMostTransactions();
			timeDifference();
			
			resetTime();
			p.printLongestList();
			timeDifference();
			
			resetTime();
			System.out.println("Number of collisions: " + p.collisions() );
			timeDifference();
			
			resetTime();
			System.out.println("Number of empty elements: " + p.emptyElements() );
			timeDifference();
			
			p.printNumberOfElements();
			p.printCurrentLoadFactor();
			System.out.println("Data is stored in a hash table of size: " + p.size() );
			System.out.println("Table was doubled: " + p.doubles + " times");
			
			System.out.println();
			System.out.println("Do you want to run the simulation again? (Y/N) ");
			String repeat = sc.next();
			if( repeat.equals("N") || repeat.equals("n") )
				break;
		}
		sc.close();
	}
	
	public static void timeDifference(){
		double timeElapsed = System.nanoTime() - startingTime;
		System.out.println("Time elapsed: " + timeElapsed);
		System.out.println();
	}
	
	public static void resetTime(){
		startingTime = System.nanoTime();
	}
	
}
