import java.util.*;
public class Lab5 {
	public static void main(String[] args){
		
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter M: ");
		int M = sc.nextInt();
		System.out.print("Enter N: ");
		int N = sc.nextInt();
		System.out.println();
		
		double startingTime = System.currentTimeMillis();
		int size = M * N;
		int sets = size;
		Maze maze = new Maze(M, N);
		StdDraw.show(0); //Comment for live drawing
		DSF s = new DSF(size);
		s.initialize();
		
		while (sets > 1){
			int c1 = 0; int c2 = 0; 
			int x  = 0; int y  = 0;
			char d = ' ';
			int wall = randInt(1, 4); //Wall 0-3 will be converted to N, E, S, or W respectively.
			if (wall == 1) {
				d = 'N';
				x = randInt(1, N);
				y = randInt(1, M - 1);
				c1 = M * (x - 1) + y;
				c2 = M * (x - 1) + y + 1;
			}
			else if (wall == 2) { 
				d = 'E';
				x = randInt(1, N - 1);
				y = randInt(1, M);
				c1 = M * (x - 1) + y;
				c2 = M * (x) + y;
			}
			else if (wall == 3) {
				d = 'S';
				x = randInt(1, N);
				y = randInt(2, M);
				c1 = M * (x - 1) + y;
				c2 = M * (x - 1) + y - 1;
			}
			else {
				d = 'W';
				x = randInt(2, N);
				y = randInt(1, M);
				c1 = M * (x - 1) + y;
				c2 = M * (x - 2) + y;
			}
			
			c1--; c2--; // DSF starts at 0 not 1 so coordinates must be shifted
			if ( !s.isInSameSet(c1, c2) ){
				maze.remove(x, y, d);
				s.union(c1, c2);
				sets--;
			}
			
/////////// Print statements to see maze at each step
//			for (int i = 0; i < size; i++){
//				System.out.print(" " + i + " ");
//			}
//			System.out.println();
//			System.out.println( Arrays.toString(s.DSF) );
//			System.out.println("sets: " + sets);
//			System.out.println( "direction: " + d);
//			System.out.println("x: " + x + " y: " + y);
//			System.out.println("c1: " + c1);
//			System.out.println("c2: " + c2);
//			System.out.println();
/////////////////////////////////////////////////////
			
		}
		
		maze.draw();
		sc.close();
		
		double runningTime = startingTime - System.currentTimeMillis();
		System.out.println("Running time: " + runningTime);
	}
	
	/*
	 * Produces a random number between min and max, including both min and max.
	 */
	public static int randInt(int min, int max) {
	    Random rand = new Random();
	    int randomNum = rand.nextInt((max - min) + 1) + min;
	    return randomNum;
	}
}
