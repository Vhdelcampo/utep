/*************************************************************************
 Code to generate and draw a maze, to be used as starting point for lab 4
 Code borrowed from http://introcs.cs.princeton.edu and modified by Olac Fuentes
 This program needs the StdDraw library http://introcs.cs.princeton.edu/java/stdlib/StdDraw.java.html
 *************************************************************************/
public class Maze {
    private int M;                 // dimension of maze
    private int N;
    private boolean[][] north;     // is there a wall to north of cell i, j
    private boolean[][] east;
    private boolean[][] south;
    private boolean[][] west;
  
    public Maze(int M) {
        this.M = M;
        this.N = M;
        StdDraw.setXscale(0, M+2);
        StdDraw.setYscale(0, M+2);
        init();
    }
    
    public Maze(int M, int N) {
    	this.M = M;
        this.N = N;
        StdDraw.setXscale(0, N+2);
        StdDraw.setYscale(0, M+2);
        init();
    }

    private void init() {
        // initialze all walls as present
		// notice that each wall is stored twice 
        north = new boolean[N+2][M+2];
        east  = new boolean[N+2][M+2];
        south = new boolean[N+2][M+2];
        west  = new boolean[N+2][M+2];
        for (int x = 0; x < N+2; x++)
            for (int y = 0; y < M+2; y++)
                north[x][y] = east[x][y] = south[x][y] = west[x][y] = true;
    }
 
    public void remove(int x, int y, char d){
//	 	int origin = (y-1)*N+(x-1);
//		int dest;
		
	 	if ((d=='N') && (y<M)){
//			dest = origin+N;
			north[x][y] = south[x][y+1] = false;
		}
		else if ((d=='W')&& (x>1)){
//			dest = origin-1;
			west[x][y] = east[x-1][y] = false;
      }
		else if ((d=='S')&& (y>1)){
//			dest = origin-N;
			south[x][y] = north[x][y-1] = false;
		}
		else if ((d=='E')&& (x<N)){
//			dest = origin+1;
			east[x][y] = west[x+1][y] = false;		
		}
	 }	
	 
	 // display the maze 
    public void draw() {
        StdDraw.setPenColor(StdDraw.RED);
        StdDraw.filledCircle(N + 0.5, M + 0.5, 0.375);
        StdDraw.filledCircle(1.5, 1.5, 0.375);

        StdDraw.setPenColor(StdDraw.BLACK);
        for (int x = 1; x <= N; x++) {
            for (int y = 1; y <= M; y++) {
                if (south[x][y]) StdDraw.line(x, y, x + 1, y);
                if (north[x][y]) StdDraw.line(x, y + 1, x + 1, y + 1);
                if (west[x][y])  StdDraw.line(x, y, x, y + 1);
                if (east[x][y])  StdDraw.line(x + 1, y, x + 1, y + 1);
            }
        }
        StdDraw.show(0);
    }
	 
    public static void main(String[] args) {
    	
        int N = 5; // Maze will have 25 rows and 25 columns, you may modify to allow rectangular mazes
        int M = 10;
        Maze maze = new Maze(M, N);
        StdDraw.show(0);
		//Remove three walls
        maze.remove(2,5,'E');  // Remove east wall of cell 2,5 (column 2, row 5, starting from bottom left; this wall is the same as the west wall of cell 3,5
        maze.remove(5,4,'N');  // Remove north wall of cell 5,4
        maze.remove(1,10,'S'); // Remove south wall of cell 1,10
        maze.draw();
    }
}
