import java.util.LinkedList;
import java.util.Queue;

public class GraphAlgorithms {
	public static int[] breadthFirstSearch(gNode adjList[], int source){
		int size = adjList.length;
		int[] path = new int[size];
		boolean[] visited = new boolean[size];
		for (int i=0; i<path.length; i++){
			path[i] = -1;
		}
		visited[source] = true;
		Queue<Integer> Q = new LinkedList<Integer>();
		Q.add(source);
		while (!Q.isEmpty()){
			int u = Q.remove();
			for (gNode i=adjList[u]; i!=null; i=i.next){
				int v = i.vertex;
				if (!visited[v]){
					visited[v] = true;
					path[v] = u;
					Q.add(v);
				}
			}
		}
		return path;
	}

	public static int[] depthFirstSearch(gNode[] adjList, int source, int size){
		int[] path = new int[size];
		boolean[] visited = new boolean[size];
		depthFirstSearch(adjList, visited, path, source);
		return path;
	}
	
	public static void depthFirstSearch(gNode[] adjList, boolean[] visited, int[] path, int source){
		visited[source] = true;
		for (gNode i=adjList[source]; i!=null; i=i.next){
			int v = i.vertex;
			if (!visited[v]){
				path[v] = source;
				depthFirstSearch(adjList, visited, path, v);
			}
		}
	}
}
