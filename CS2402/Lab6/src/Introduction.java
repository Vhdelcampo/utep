import java.util.*;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
public class Introduction {
	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		System.out.print("Enter rows: ");
		int rows = sc.nextInt();
		System.out.print("Enter columns: ");
		int columns = sc.nextInt();
		int size = rows * columns;

		gNode[] adjacencyList = createMaze(rows, columns, size);
		printList(adjacencyList);

		System.out.println("Path using breadth-first search:");
		int[] breadthFirstPath = GraphAlgorithms.breadthFirstSearch(adjacencyList, 0);
		printPath(breadthFirstPath, size, 0, size-1);

		System.out.println("Path using depth-first search:");
		int[] depthFirstPath = GraphAlgorithms.depthFirstSearch(adjacencyList, 0, size);
		printPath(depthFirstPath, size, 0, size-1);
		
		sc.close();
	}

	private static gNode[] createMaze(int rows, int columns, int size) {
		int squares = size;
		System.out.println("Number of squares: " + squares);
		int numInternalWalls = (rows-1)*columns + (columns-1)*rows; 
		System.out.println("Enter between 0 and " + numInternalWalls + " walls to remove, entering " + (size-1) + " gurantees a unique path: ");
		int wallsToRemove = sc.nextInt();
		int wallsRemovedSoFar = 0;

		checkMaze(size, numInternalWalls, wallsToRemove);

		Maze maze = new Maze(rows, columns);
		StdDraw.show(0);
		DSF mazeForest = new DSF(size);
		mazeForest.initialize();
		gNode[] adjacencyList = new gNode[size];

		while (wallsRemovedSoFar < wallsToRemove){
			int x = 0; int y = 0; 
			char direction = randomDirection();

			Wall w = chooseRandomWall(rows, columns, direction);

			x = toCoordinate(rows, w.getX(), w.getY());
			y = toCoordinate(direction, rows, w.getX(), w.getY());

			// Keep removing wall until there is one path to every element
			if (squares > 1){
				if (!mazeForest.isInSameSet(x, y)){
					maze.remove(w.getX(), w.getY(), direction);
					mazeForest.union(x, y);
					squares--;
					wallsRemovedSoFar++;
					addNodes(adjacencyList, x, y);
				}
			} else { // Else remove walls from any left
				if (maze.remove(w.getX(), w.getY(), direction)){
					wallsRemovedSoFar++;
					addNodes(adjacencyList, x, y);
					System.out.println("walls remove so far" + wallsRemovedSoFar);
				}
			}

		}		
		maze.draw();
		System.out.println();
		return adjacencyList;
	}

	private static void checkMaze(int size, int numInternalWalls, int wallsToRemove) {
		if (wallsToRemove < size-1) {
			System.out.println("A path from source to destination is not guaranteed to exist.");
		} else if (wallsToRemove == size-1) {
			System.out.println("The is a unique path from source to destination.");
		} else if(wallsToRemove > numInternalWalls) {
			sc.close();
			throw new IllegalArgumentException("Walls to remove is greater than amount of internal walls");
		} else{
			System.out.println("There is at least one path from source to destination.");
		}
	}

	private static Wall chooseRandomWall(int rows, int columns, char direction) {
		Wall w = new Wall();
		switch (direction) {
		case 'N':
			w.setX(randomInt(1, columns));
			w.setY(randomInt(1, rows - 1)); // Pick walls except the very top
			break;
		case 'E':
			w.setX(randomInt(1, columns - 1)); // Pick walls except the very right
			w.setY(randomInt(1, rows));
			break;
		case 'S':
			w.setX(randomInt(1, columns));
			w.setY(randomInt(2, rows)); // Pick walls except the very bottom
			break;
		case 'W':
			w.setX(randomInt(2, columns)); // Pick walls except the very left
			w.setY(randomInt(1, rows));
			break;
		}
		return w;
	}


	public static int toCoordinate(int rows, int x, int y) {
		return rows * (x - 1) + y - 1;
	}

	public static int toCoordinate(char direction, int rows, int x, int y) {
		switch (direction) {
		case 'N':
			y += 1;
			break;
		case 'E':
			x += 1;
			break;
		case 'S':
			y -= 1;
			break;
		case 'W':
			x -= 1;
			break;
		}
		return toCoordinate(rows, x, y);
	}

	public static char randomDirection() {
		int rand = randomInt(1, 4);
		switch (rand) {
		case 1:
			return 'N';
		case 2:
			return 'E';
		case 3:
			return 'S';
		case 4:
			return 'W';
		}
		return 0;
	}

	// Produces a random number between min and max, including both min and max.
	public static int randomInt(int min, int max) {
		Random rand = new Random();
		int randomNum = rand.nextInt((max - min) + 1) + min;
		return randomNum;
	}

	private static void addNodes(gNode[] adjacencyList, int coordinateOne, int coordinateTwo) {
		adjacencyList[coordinateOne] = new gNode(coordinateTwo, adjacencyList[coordinateOne]);
		adjacencyList[coordinateTwo] = new gNode(coordinateOne, adjacencyList[coordinateTwo]);
	}

	public static void printList(gNode adjList[]){
		for (int i=0; i<adjList.length; i++){
			System.out.print(i + "| ");
			for (gNode j=adjList[i]; j!=null; j=j.next){
				System.out.print(j.vertex + " ");
			}
			System.out.println();
		}
	}

	public static void printPath(int[] path, int size, int source, int destination){
		if (path[source] == path[destination])
			System.out.print("No path exists from source to destination");
		else {
			printNode(path, size, destination);
		}
		System.out.println();
	}

	private static void printNode(int[] path, int size, int destination) {
		if (path[destination] >= 1)
			printNode(path, size, path[destination]);
		System.out.print(destination + " ");
	}
}