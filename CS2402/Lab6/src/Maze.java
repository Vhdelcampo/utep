/*************************************************************************
 Code to generate and draw a maze, to be used as starting point for lab 4
 Code borrowed from http://introcs.cs.princeton.edu and modified by Olac Fuentes
 This program needs the StdDraw library http://introcs.cs.princeton.edu/java/stdlib/StdDraw.java.html
 *************************************************************************/
public class Maze {
	private int rows;                 // dimension of maze
	private int columns;
	private boolean[][] north;     // is there a wall to north of cell i, j
	private boolean[][] east;
	private boolean[][] south;
	private boolean[][] west;

	public Maze(int rows) {
		this.rows = rows;
		this.columns = rows;
		StdDraw.setXscale(0, columns+2);
		StdDraw.setYscale(0, rows+2);
		init();
	}

	public Maze(int rows, int columns) {
		this.rows = rows;
		this.columns = columns;
		StdDraw.setXscale(0, columns+2);
		StdDraw.setYscale(0, rows+2);
		init();
	}

	private void init() {
		// initialize all walls as present
		// notice that each wall is stored twice 
		north = new boolean[columns+2][rows+2];
		east  = new boolean[columns+2][rows+2];
		south = new boolean[columns+2][rows+2];
		west  = new boolean[columns+2][rows+2];
		for (int x = 0; x < columns+2; x++)
			for (int y = 0; y < rows+2; y++)
				north[x][y] = east[x][y] = south[x][y] = west[x][y] = true;
	}

	public boolean hasWall(int x, int y, char direction) {
		if ((direction=='N') && (y<rows)){
			if (north[x][y] && south[x][y+1] == true){
				return true;
			}
		}
		else if ((direction=='W') && (x>1)){
			if (west[x][y] && east[x-1][y] == true){
				return true;
			}
		}
		else if ((direction=='S') && (y>1)){
			if (south[x][y] && north[x][y-1] == true){
				return true;
			}
		}
		else if ((direction=='E')&& (x<columns)){
			if (east[x][y] && west[x+1][y] == true){
				return true;
			}
		}
		return false;
	}

	public boolean remove(int x, int y, char direction){
		if (!hasWall(x, y, direction)) {return false;}
		switch (direction) {
		case 'N':
			north[x][y] = south[x][y+1] = false;
			return true;
		case 'W':
			west[x][y] = east[x-1][y] = false;
			return true;
		case 'S':
			south[x][y] = north[x][y-1] = false;
			return true;
		case 'E':
			east[x][y] = west[x+1][y] = false;
			return true;
		}
		return false;
	}	

	// display the maze 
	public void draw() {
		StdDraw.setPenColor(StdDraw.RED);
		StdDraw.filledCircle(columns + 0.5, rows + 0.5, 0.5);
		StdDraw.filledCircle(1.5, 1.5, 0.5);

		StdDraw.setPenColor(StdDraw.BLACK);
		for (int x = 1; x <= columns; x++) {
			for (int y = 1; y <= rows; y++) {
				if (south[x][y]) StdDraw.line(x, y, x + 1, y);
				if (north[x][y]) StdDraw.line(x, y + 1, x + 1, y + 1);
				if (west[x][y])  StdDraw.line(x, y, x, y + 1);
				if (east[x][y])  StdDraw.line(x + 1, y, x + 1, y + 1);
			}
		}
		StdDraw.show(0);
	}

	public static void main(String[] args) {

		int rows = 25; // Maze will have 25 rows and 25 columns, you may modify to allow rectangular mazes
		int columns = 25;
		Maze maze = new Maze(rows, columns);
		StdDraw.show(0);
		//Remove three walls
		maze.remove(2,5,'E');  // Remove east wall of cell 2,5 (column 2, row 5, starting from bottom left; this wall is the same as the west wall of cell 3,5
		maze.remove(5,4,'N');  // Remove north wall of cell 5,4
		maze.remove(1,10,'S'); // Remove south wall of cell 1,10
		maze.draw();
	}
}
