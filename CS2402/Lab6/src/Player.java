
public class Player {
	private double x;
	private double y;
	
	public Player() {
		this.x = 1.5;
		this.y = 1.5;
	}
	
	public Player(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public double getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public double getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	
	public void moveLeft() {
		x--;
	}
	public void moveDown() {
		y--;
	}
	public void moveUp() {
		y++;
	}
	public void moveRight() {
		x++;
	}
	
}
