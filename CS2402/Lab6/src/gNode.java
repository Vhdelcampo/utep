public class gNode{
	public int vertex;
	public gNode next;
	public int weight = 1;
	public boolean visited = false;
	
	public gNode(){
	}
	
	public gNode(int vertex, int weight, boolean visited, gNode next){
		this.vertex  = vertex;
		this.next    = next;
		this.weight  = weight;
		this.visited = visited;
	}
	
	public gNode(int vertex, int weight, gNode next){
		this.vertex = vertex;
		this.next   = next;
		this.weight = weight;
	}
	
	public gNode(int vertex, gNode next){
		this.vertex = vertex;
		this.next   = next;
	}
	
	public gNode(int vertex){
		this.vertex = vertex;
	}
}