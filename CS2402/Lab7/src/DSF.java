
public class DSF {
	public int[] DSF;
	
	public DSF(){
	}
	public DSF(int n){
		DSF = new int[n];
		initialize();
	}
	
	public void initialize(){
		for (int i = 0; i < DSF.length; i++){
			DSF[i] = -1;
		}
	}
	
	public int find(int i) {
		if (DSF[i] < 0) {
			return i;          
		}else{
			DSF[i] = find(DSF[i]);
			return DSF[i];
		}
	}
	
	public void union(int i, int j){
		int ri = find(i);
		int rj = find(j);
		if(DSF[ri] > DSF[rj])
				DSF[ri] = rj;
		else{
			if (DSF[ri] == DSF[rj])
				DSF[ri]--;
			DSF[rj] = ri;
		}
	}
	
	public boolean isInSameSet(int i, int j){
		return find(i) == find(j);
	}
	
	public int sets(){
		int count = 0;
		for (int i = 0; i < DSF.length; i++){
			if (DSF[i] == -1) count++;
		}
		return count;
	}
	
	public void print(){
		for (int i=0; i<DSF.length; i++){
			System.out.print(DSF[i] + " ");
		}
		System.out.println();
	}
	
}
