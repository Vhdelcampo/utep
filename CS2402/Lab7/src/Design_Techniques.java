import java.util.*;
public class Design_Techniques {
	static double startingTime;
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter size of n to use to use throughout the tests: ");
		int n = sc.nextInt();
		System.out.println();
		
		//	1. Divide and conquer
		System.out.println("Sorted array will be created and quicksort with first element as pivot will be ran.");
		int [] sortedArray = buildSortedArray(n);
		startingTime = System.nanoTime();
		quickSort(sortedArray);
		printRunningTime();
		System.out.println(Arrays.toString(sortedArray));
		System.out.println();
		
		//	4. Randomized algorithms
		System.out.println("Sorted array will be created and quicksort with random pivot will be ran.");
		startingTime = System.nanoTime();
		quickSortRandomPivot(sortedArray);
		printRunningTime();
		System.out.println(Arrays.toString(sortedArray));
		System.out.println();
		
		//	3. Backtracking
		int[] randomSet = buildRandomArray(n);
		System.out.println("Is there a subset with a sum of " + n + " in the following set?");
		System.out.println(Arrays.toString(randomSet));
		startingTime = System.nanoTime();
		System.out.println((subsetSum(randomSet, n, n-1)));
		printRunningTime();
		System.out.println();
		
		//	2. Dynamic programming
		String A = "BRAND";
		String B = "RANDOM";
		System.out.println("Two words " + A + " and " + B + " will be compared to find the edit distance table.");
		System.out.println(A + " is the rows and " + B + " is the columns.");
		int[][] distArray = editDistance(A, B);
		for (int[] row : distArray){
		    System.out.println(Arrays.toString(row));
		}
		System.out.println();

		//	5. Greedy algorithms
		int M = 3; int N = 3;
		System.out.println("A maze will be created with a size of " + M + " by " + N);
		System.out.println("A minimum spanning tree will be created from the graph of the maze.");
		int numInternalWalls = (M-1)*N + M*(N-1);
		System.out.println("Number of interal walls is " + numInternalWalls);
		// Walls to remove should be greater than or equal to M*N to ensure there is more than one path from source to destination and
		// it should be less than numInternalWalls to ensure it stays a maze.
		int wallsToRemove = randInt(M*N, numInternalWalls);
		System.out.println("Random number of walls to remove from maze is " + wallsToRemove);
		gNode[] GraphOfMaze = createGraphFromMaze(M, N, wallsToRemove);
		gNode[] MinSpanTree = kruskal(GraphOfMaze);
		System.out.println("Minimum Spanning Tree: ");
		printAdjacencyList(MinSpanTree);
		
		sc.close();
	}
	
	public static void printRunningTime(){
		double runningTime = System.nanoTime() - startingTime;
		System.out.println("Running time: " + runningTime);
	}
	
	public static int[][] editDistance(String A, String B){
		int[][] table = new int[A.length()+1][B.length()+1];
		for (int i=0; i<table.length; i++){
			for(int j=0; j<table[i].length; j++){
				if (i == 0) 
					table[i][j] = j;
				else if (j == 0) 
					table[i][j] = i;
				else if (A.charAt(i-1) == B.charAt(j-1) ){
					table[i][j] = table[i-1][j-1];
				}else
					// Take the minimum of the 3 adjacent values of left, up, and diagonally up-left.
					table[i][j] = 1 + Math.min(table[i-1][j], Math.min(table[i-1][j-1], table[i][j-1]) );
			}
		}
		return table;
	}
	
	
	public static boolean subsetSum(int[] set, int goal, int last){
		if (goal == 0)
			return true;
		if (goal<0 || last<0){
			return false;
		}
		if (subsetSum(set, goal-set[last], last-1)){
			return true;
		}else
			return subsetSum(set, goal, last-1);
	}

	public static gNode[] createGraphFromMaze(int M, int N, int m){
		int n = M * N;
		int numInternalWalls = (M-1)*N + (N-1)*M; 
		if(m > numInternalWalls)
			throw new IllegalArgumentException("Walls to remove is greater than amount of internal walls");
		Maze maze = new Maze(M, N);
		StdDraw.show(0);
		int sets = n;
		DSF s = new DSF(n);
		gNode[] adjacencyList = new gNode[n];
		int wallsRemoved = 0;

		while (wallsRemoved < m){
			int c1 = 0; int c2 = 0; 
			int x  = 0; int y  = 0;
			char d = ' ';
			int wall = randInt(0, 3); //Wall 0-3 will be converted to N, E, S, or W respectively.
			if (wall == 0) {
				d = 'N';
				x = randInt(1, N);
				y = randInt(1, M - 1);
				c1 = M * (x - 1) + y;
				c2 = M * (x - 1) + y + 1;
			}
			else if (wall == 1) { 
				d = 'E';
				x = randInt(1, N - 1);
				y = randInt(1, M);
				c1 = M * (x - 1) + y;
				c2 = M * (x) + y;
			}
			else if (wall == 2) {
				d = 'S';
				x = randInt(1, N);
				y = randInt(2, M);
				c1 = M * (x - 1) + y;
				c2 = M * (x - 1) + y - 1;
			}
			else {
				d = 'W';
				x = randInt(2, N);
				y = randInt(1, M);
				c1 = M * (x - 1) + y;
				c2 = M * (x - 2) + y;
			}
			c1--; c2--;
			// Keep removing wall until there is one path to every element
			if (sets > 1){
				if (!s.isInSameSet(c1, c2)){
					maze.remove(x, y, d);
					s.union(c2, c1);
					sets--;
					wallsRemoved++;
					adjacencyList[c1] = new gNode(c2, adjacencyList[c1]);
					adjacencyList[c2] = new gNode(c1, adjacencyList[c2]);
				}
			}
			// Else remove walls from any set after there is a path to every element
			else {
				if (maze.remove(x, y, d) == true){
					wallsRemoved++;
					adjacencyList[c1] = new gNode(c2, adjacencyList[c1]);
					adjacencyList[c2] = new gNode(c1, adjacencyList[c2]);
				}
			}
		}
		maze.draw();
		return adjacencyList;
	}

	public static gNode[] kruskal(gNode[] Graph){
		int n = Graph.length;
		gNode[] MinSpanTree = new gNode[n];
		DSF S = new DSF(n);
		PriorityQueue<Edge> PQ = new PriorityQueue<Edge>(); // Heap
		for (int i=0; i<n; i++){
			for (gNode j=Graph[i]; j!=null; j=j.next){
				PQ.add(new Edge(i, j.vertex));
			}
		}
		int edges = 0;
		while (edges < n-1){
			Edge ei = PQ.remove();
			int u = ei.u; int v = ei.v;
			if (!S.isInSameSet(u, v)){
				edges++;
				MinSpanTree[u] = new gNode(v, MinSpanTree[u]);
				MinSpanTree[v] = new gNode(u, MinSpanTree[v]);
				S.union(u, v);
			}
		}
		return MinSpanTree;
	}

	public static void quickSort(int[] list) {
		quickSort(list, 0, list.length - 1);
	}

	private static void quickSort(int[] list, int first, int last) {
		if (last > first) {
			int pivotIndex = partition(list, first, last);
			quickSort(list, first, pivotIndex - 1);
			quickSort(list, pivotIndex + 1, last);
		}
	}

	private static int partition(int[] list, int first, int last) {
		int index = first;
		int pivot = list[index]; // Choose the first element as the pivot
		int low = index + 1; // Index for forward search
		int high = last; // Index for backward search
		while (high > low) {
			// Search forward from left
			while (low <= high && list[low] <= pivot)
				low++;
			// Search backward from right
			while (low <= high && list[high] > pivot)
				high--;
			// Swap two elements in the list
			if (high > low) {
				int temp = list[high];
				list[high] = list[low];
				list[low] = temp;
			}
		}
		while (high > index && list[high] >= pivot)
			high--;
		// Swap pivot with list[high]
		if (pivot > list[high]) {
			list[index] = list[high];
			list[high] = pivot;
			return high;
		}
		else {
			return index;
		}
	}
	
	public static void quickSortRandomPivot(int[] list) {
		quickSortRandomPivot(list, 0, list.length - 1);
	}

	private static void quickSortRandomPivot(int[] list, int first, int last) {
		if (last > first) {
			int pivotIndex = partitionWithRandomPivot(list, first, last);
			quickSortRandomPivot(list, first, pivotIndex - 1);
			quickSortRandomPivot(list, pivotIndex + 1, last);
		}
	}

	private static int partitionWithRandomPivot(int[] list, int first, int last) {
		int index = randInt(first, last); // Chose random element as pivot
		int pivot = list[index]; // Choose the first element as the pivot
		int low = index + 1; // Index for forward search
		int high = last; // Index for backward search
		while (high > low) {
			// Search forward from left
			while (low <= high && list[low] <= pivot)
				low++;
			// Search backward from right
			while (low <= high && list[high] > pivot)
				high--;
			// Swap two elements in the list
			if (high > low) {
				int temp = list[high];
				list[high] = list[low];
				list[low] = temp;
			}
		}
		while (high > index && list[high] >= pivot)
			high--;
		// Swap pivot with list[high]
		if (pivot > list[high]) {
			list[index] = list[high];
			list[high] = pivot;
			return high;
		}
		else {
			return index;
		}
	}

	public static int randInt(int min, int max) {
		Random rand = new Random();
		int randomNum = rand.nextInt((max - min) + 1) + min;
		return randomNum;
	}

	public static int[] buildRandomArray(int n){
		int[] randomArray = new int[n];
		for (int i=0; i<n; i++){
			randomArray[i] = randInt(0, n);
		}
		return randomArray;
	}

	public static int[] buildSortedArray(int n){
		int[] sortedArray = new int[n];
		for (int i=0; i<n; i++){
			sortedArray[i] = i;
		}
		return sortedArray;
	}

	public static void printAdjacencyList(gNode adjList[]){
		for (int i=0; i<adjList.length; i++){
			System.out.print(i + "| ");
			for (gNode j=adjList[i]; j!=null; j=j.next){
				System.out.print(j.vertex + " ");
			}
			System.out.println();
		}
	}
}
