/*************************************************************************
 Code to generate and draw a maze, to be used as starting point for lab 4
 Code borrowed from http://introcs.cs.princeton.edu and modified by Olac Fuentes
 This program needs the StdDraw library http://introcs.cs.princeton.edu/java/stdlib/StdDraw.java.html
 *************************************************************************/
public class Maze {
    private int M;                 // dimension of maze
    private int N;
    private boolean[][] north;     // is there a wall to north of cell i, j
    private boolean[][] east;
    private boolean[][] south;
    private boolean[][] west;
  
    public Maze(int M) {
        this.M = M;
        this.N = M;
        StdDraw.setXscale(0, N+2);
        StdDraw.setYscale(0, M+2);
        init();
    }
    
    public Maze(int M, int N) {
    	this.M = M;
        this.N = N;
        StdDraw.setXscale(0, N+2);
        StdDraw.setYscale(0, M+2);
        init();
    }

    private void init() {
        // initialze all walls as present
		// notice that each wall is stored twice 
        north = new boolean[N+2][M+2];
        east  = new boolean[N+2][M+2];
        south = new boolean[N+2][M+2];
        west  = new boolean[N+2][M+2];
        for (int x = 0; x < N+2; x++)
            for (int y = 0; y < M+2; y++)
                north[x][y] = east[x][y] = south[x][y] = west[x][y] = true;
    }
 
    public boolean remove(int x, int y, char d){
	 	if ((d=='N') && (y<M)){
			if (north[x][y] && south[x][y+1] == true){
				north[x][y] = south[x][y+1] = false;
				return true;
			}
		}
	 	else if ((d=='W')&& (x>1)){
			if (west[x][y] && east[x-1][y] == true){
				west[x][y] = east[x-1][y] = false;
				return true;
			}
	 	}
		else if ((d=='S')&& (y>1)){
			if (south[x][y] && north[x][y-1] == true){
				south[x][y] = north[x][y-1] = false;
				return true;
			}
		}
		else if ((d=='E')&& (x<N)){
			if (east[x][y] && west[x+1][y] == true){
				east[x][y] = west[x+1][y] = false;
				return true;
			}
		}
		return false;
	 }	
	 
	 // display the maze 
    public void draw() {
        StdDraw.setPenColor(StdDraw.RED);
        StdDraw.filledCircle(N + 0.5, M + 0.5, 0.375);
        StdDraw.filledCircle(1.5, 1.5, 0.375);

        StdDraw.setPenColor(StdDraw.BLACK);
        for (int x = 1; x <= N; x++) {
            for (int y = 1; y <= M; y++) {
                if (south[x][y]) StdDraw.line(x, y, x + 1, y);
                if (north[x][y]) StdDraw.line(x, y + 1, x + 1, y + 1);
                if (west[x][y])  StdDraw.line(x, y, x, y + 1);
                if (east[x][y])  StdDraw.line(x + 1, y, x + 1, y + 1);
            }
        }
        StdDraw.show(0);
    }
}
