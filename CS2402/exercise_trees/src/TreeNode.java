
public class TreeNode {
	public int item;
	public TreeNode leftChild;
	public TreeNode rightChild;

	public TreeNode(){
		leftChild = null;
		rightChild = null;
	}
	
	public TreeNode(int newItem){
		item = newItem;
		leftChild = null;
		rightChild = null;
	}
	
	public TreeNode(int newItem, TreeNode left, TreeNode right){
		item = newItem;
		leftChild = left;
		rightChild = right;
	}

}