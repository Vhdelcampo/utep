import java.util.*;

public class problem3 {
	public static void main(String[] args){
	Queue<TreeNode> q = new LinkedList<TreeNode>();
	TreeNode t = new TreeNode(0);
	q.add(t);
	
	for(int i = 1; i < 15 ; i += 2 ){
		TreeNode s = q.remove();
		s.leftChild = new TreeNode(i);
		s.rightChild = new TreeNode(i+1);
		q.add(s.leftChild);
		q.add(s.rightChild);
	}
	
	}
}
