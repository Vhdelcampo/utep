
public class problem4 {
	
	public static TreeNode m(int[] A, int first, int last){
		if (first > last)
			return null;
		int mid = (first + last)/2;
		System.out.println( "first: " + first + " last: " + last + " mid: " + mid);
		return new TreeNode( A[mid], m(A, first, mid - 1), m(A, mid + 1, last) );
	}
	

}
