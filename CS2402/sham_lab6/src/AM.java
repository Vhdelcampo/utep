public class AM {

	boolean [][] AdjacencyMatrix;
	int N;
	int M;
	
	public AM(int M, int N){
		this.M = M;
		this.N = N;
		AdjacencyMatrix = new boolean[N*M][N*M];
		
		for(int i = 0; i < AdjacencyMatrix.length; i++){
			for(int k = 0; k < AdjacencyMatrix[i].length; k++){
				AdjacencyMatrix[i][k] = false;
			}
		}
	}
	
	public boolean getValue(int first, int second){
		return AdjacencyMatrix[first][second];
	}
	
	public void connect(int origin, int destination){
		AdjacencyMatrix[origin][destination] = AdjacencyMatrix[destination][origin] = true;
		
	}
	
	public boolean isConnected(int origin, int destination){
		return AdjacencyMatrix[origin][destination];
	}
	
	
}
