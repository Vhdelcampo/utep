public class DSF{

	int [] DSF;
	int M;
	int N;
	int sets;
	
	
	public DSF(int M, int N){
		DSF = new int [M * N];	
		sets = N * M;
		this.M = M;
		this.N = N;
		
		for(int k = 0; k < DSF.length; k++){
			DSF[k] = -1;
		}
		
	}// T(n) = n + 1		O(n)
	
	
	public void print(){
		for(int i = 1; i <= N; i++){
			for(int k = 1; k <= M; k++){
				System.out.print(DSF[(i - 1) * N + (k - 1)] + " ");
			}
			System.out.println();
		}
		System.out.println();
	}//	T(n) = n^2 + 1		O(n^2)
	

	public int find(int i){
		if(DSF[i] == -1)
			return i;
		else 
			return find(DSF[i]);
	}//	T(n) = T(n - 1) + 1		O(n)
	
	
	public void Union(int i, int j){
		int ri = find(i);
		int rj = find(j);
		DSF[rj] = ri;
//		sets--;		//////commented for lab 5 use
	}//	T(n) = 2n + 1			O(n)
	
}//end DSF class

	

