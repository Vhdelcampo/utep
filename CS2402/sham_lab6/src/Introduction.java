import java.awt.EventQueue;
import java.util.*;

import javax.swing.JOptionPane;

public class Introduction {
	static int M; // Maze rows
	static int N; // Maze columns
	static int n; //cells in maze
	static int m;
	
	static Maze maze;

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		
		int game = 1;
		while(game == 1){
			instructions(userInput);

			Pathway directionMatrix = createMaze(M, N, n, m);

//			String [][] direction = directionMatrix.direction;
//			printDirection(direction);


			String move;
			char dir;
			while(!directionMatrix.mazeGoal()){
				System.out.println("Enter correct direction to go(only upper case: N, S, E, W)");
				move = userInput.nextLine();
				if(move.equals("N") || move.equals("W") || move.equals("S") || move.equals("E")){
					dir = move.charAt(0);
					directionMatrix.checkMove(dir);
				} else {

				}

			}
			int response = JOptionPane.showConfirmDialog(null, "WINNER! Wouls you like to player again?", 
															"MAZE" , JOptionPane.YES_NO_OPTION);
			if(response == JOptionPane.NO_OPTION)
				System.exit(0);
			else if(response == JOptionPane.YES_OPTION){
				maze = null;
			}
		}
	}//end main method


	private static void instructions(Scanner userInput) {
		System.out.println("Please enter the number of rows: ");
		M = userInput.nextInt();
		System.out.println("Please enter the number of columns: ");
		N = userInput.nextInt();

		n = M * N;	//cells in maze
		int number = n + M;

		System.out.println("Total Cells: " + n + 
				"\nHow many walls would you like to remove? " + 
				"\nFor only one path from beginning to end select " + (n - 1) +
				"\nThe recommended max walls removable is " + number +
				"\nWhile anything lower than " + (n - 1) + " does not guarantee a path");


		m = userInput.nextInt();
	}


	private static Pathway createMaze(int M, int N, int n, int m) {
		Random ran = new Random();

		if( m < (n - 1)){
			System.out.println("A path from source to destination is not guaranteed to exist (when m < n - 1)");
		}
		else if( m == n - 1){
			System.out.println("There is a unique path from source to destination (when m = n - 1)");
		}
		else if( m > (n - 1)){
			System.out.println("There is at least on path from source to destination (when m > n - 1)");
		}
		else
			System.out.println("ERROR: Incorrect Selection.\n");

		DSF mazeValues = new DSF(M, N);
		maze = new Maze(M, N);
		AM matrix = new AM(M, N);
		StdDraw.show(0);

		Pathway directionMatrix = new Pathway(M, N);

		while(m > 0){

			int row = 0;
			int column = 0;
			int value = 0;

			int random = ran.nextInt(M);
			row = (random % M) + 1;
			//get row number without exceeding M size

			random = ran.nextInt(N);
			column = (random % N) + 1;    
			//get column number without exceeding N size


			int temp = ran.nextInt() % 4;
			char d = ' ';
			boolean ok = false;	//to ensure only one move is made per coordinates

			if(temp == 0){
				d = 'N';
				if(column == N)
					temp++;	
				else{
					value = 1;
					ok = true;
				}
			}
			if( (temp == 1) && (ok == false) ){
				d = 'E';
				if(row == M)
					temp++;
				else{
					value = N;
					ok = true;
				}
			}
			if( (temp == 2) && (ok == false) ){
				d = 'S';
				if(column == 1)
					temp++;
				else{
					value = -1;
					ok = true;
				}
			}
			if( (temp == 3) && (ok == false) ){
				d = 'W';
				if(row == 1)
					temp++;
				else{
					value = -N;
					ok = true;
				}
			}


			int origin = (row - 1) * N + (column - 1);
			int destination = ( (row - 1) * N + (column - 1) ) + value;


			if(ok){
				if(mazeValues.sets == 1){	//used for when m > n - 1

					if( matrix.isConnected(origin, destination) == false){
						matrix.connect(origin, destination);
						maze.remove(row, column, d);
						directionMatrix.removeWall(row, column, d);
						m--;
					} 

				} else {

					int first = mazeValues.find( (row - 1) * N + (column - 1) );
					int second = mazeValues.find( ((row - 1) * N + (column - 1))  + value);


					if(first != second){
						mazeValues.Union( destination, origin );
						mazeValues.sets--;
						matrix.connect(origin, destination);
						maze.remove(row, column, d);
						directionMatrix.removeWall(row, column, d);
						m--;
					}
				}//ensures there is a path with m = n - 1 elements

			}//check if character was selected
		}//end while loop

		maze.draw();
		return directionMatrix;
	}


	private static void printDirection(String [][] direction) {
		for(int i = 0; i < direction.length; i++){
			for(int j = 0; j < direction[i].length;j++){
				System.out.print(direction[i][j] + "\t");
			}
			System.out.println();
		}
	}


	public static void print(Queue list){
		for(Node temp = list.head; temp != null; temp = temp.next){
			System.out.print(temp.item + " | ");
		}
		System.out.println("\n");
	}


	public static void initialize(boolean [] list){
		for(int i = 0; i < list.length; i++){
			list[i] = false;
		}
	}




}//end lab6 class
