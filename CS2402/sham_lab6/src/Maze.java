/*************************************************************************
 Code borrowed from http://introcs.cs.princeton.edu and modified by Olac Fuentes
 *************************************************************************/
import java.util.*;
public class Maze {
    private int M;                 // dimension of maze rows
    private int N;				   // dimension of maze columns
    private boolean[][] north;     // is there a wall to north of cell i, j
    private boolean[][] east;
    private boolean[][] south;
    private boolean[][] west;
  
    public Maze(int M, int N) {
        this.M = M;
    	this.N = N;
        StdDraw.setXscale(0, M+2);
        StdDraw.setYscale(0, N+2);
        init();
    }    
    
    private void init() {
       // initialze all walls as present
		 // notice that each wall is stored twice 
        north = new boolean[M+2][N+2];
        east  = new boolean[M+2][N+2];
        south = new boolean[M+2][N+2];
        west  = new boolean[M+2][N+2];
        for (int x = 0; x < M+2; x++)
            for (int y = 0; y < N+2; y++)
                north[x][y] = east[x][y] = south[x][y] = west[x][y] = true;
    }
 
    public void remove(int x, int y, char d){
	 	if ((d=='N') && (y<N)){
			north[x][y] = south[x][y+1] = false;
		}
		else if ((d=='W')&& (x>1)){
			west[x][y] = east[x-1][y] = false;
      }
		else if ((d=='S')&& (y>1)){
			south[x][y] = north[x][y-1] = false;
		}
		else if ((d=='E')&& (x<M)){
			east[x][y] = west[x+1][y] = false;		
		}
	 }	
	 
	 // display the maze 
    public void draw() {
        StdDraw.setPenColor(StdDraw.RED);
        StdDraw.filledCircle(M + 0.5, N + 0.5, 0.375);	//end pointer
        StdDraw.filledCircle(1.5, 1.5, 0.375);			//begin pointer

        StdDraw.setPenColor(StdDraw.BLACK);
        for (int x = 1; x <= M; x++) {
            for (int y = 1; y <= N; y++) {
                if (south[x][y]) StdDraw.line(x, y, x + 1, y);
                if (north[x][y]) StdDraw.line(x, y + 1, x + 1, y + 1);
                if (west[x][y])  StdDraw.line(x, y, x, y + 1);
                if (east[x][y])  StdDraw.line(x + 1, y, x + 1, y + 1);
            }
        }
        StdDraw.show(0);
    }
    
    
    
	
	
	
	
}//end Maze class