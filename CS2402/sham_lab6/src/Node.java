public class Node{	
	public int item;
	public Node next;
	
	public Node(int i, Node n){
		item =i;
		next = n;
	}
	
	public Node(int i){
		item =i;
		next = null;
	}
	
	public void setNext(Node n){
		next = n;
	}
}