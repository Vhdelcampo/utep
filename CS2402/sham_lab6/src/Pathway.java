
public class Pathway {

	public String [][] direction;
	private final int M;
	private final int N;
	
	public int xPointer, yPointer;
	
	
	public Pathway(int m, int n){
		direction = new String[m][n];
		M = m;
		N = n;
		
		for(int i = 0; i < direction.length; i++){
			for(int j = 0; j < direction[i].length; j++){
					direction[i][j] = "|";
			}
		}
		
	}//end constructor
		
		
	public boolean checkMove(char d){
		String pathways = direction[yPointer][xPointer];
		for(int i = 0; i < pathways.length(); i++){
			if(d == pathways.charAt(i)){
				if(d == 'N'){
					//either x+1 or y+1
					xPointer++;
				}
				else if(d == 'S'){
					//either x-1 or y-1
					xPointer--;
				}
				else if(d == 'E'){
					//either x+1 or y+1
					yPointer++;
				} else{
					//either x-1 or y-1
					yPointer--;
				}
					
//				System.out.println("Y: " + xPointer + "\tX:  " + yPointer + "\n");
				return true;
			}
		}
		return false;
	}
	
	
	public boolean mazeGoal(){
		if(xPointer == (M - 1) && yPointer == (N - 1) )
			return true;
		else
			return false;
	}
	
	
	public void removeWall(int x, int y, char d){
		x -= 1;
		y -= 1;
		
		if ( (d == 'N') && (y < N) ){
			direction[x][y] += "N";
			direction[x][y + 1] += "S";
		}
		else if ( (d == 'W') && (x > 0) ){
			direction[x][y] += "W";
			direction[x - 1][y] += "E";
		}
		else if ( (d == 'S') && (y > 0) ){
			direction[x][y] += "S";
			direction[x][y - 1] += "N";
		}
		else if ( (d == 'E') && (x < M) ){
			direction[x][y] += "E";		
			direction[x + 1][y] += "W";
		}
		
	}//end removeWall method



}//end Pathway object
