/*
   Modified by: Shammir Ibarra
   Course: 2302 Data Structures
   Assignment: 6
   Instructor: Dr. Olac Fuentes
   TA: M. Abdul Kader
   Date of last modificaton: 12/6/2014
   Purpose of Program:	To implement Adjacency Matrices along with shortest path algorithms into 
   						a GUI object created by a Disjoint Set Forest.
*/

public class Queue {
	
	Node head;
	Node last;
	
	public Queue(){
		head = null;
		last = null;
	}
	
    public void enqueue(int data) {
	    Node node = new Node(data); 
	    node.setNext(null); 
	    if (head == null) { 
	    	head = node;
	    	last = node;
	    }
	    else { 
	    	last.setNext(node); 
	    	last = node; 
	    }
    }
	
    public Node dequeue(){
    	Node temp = head;
    	if(temp == null)
    		return null;
    	head = head.next;
    	return temp;
    }
    
    public void insertFront(int data) {
	    Node temp = new Node(data); 
	    temp.setNext(head); 

	    if (head == null) { 
	    	last = temp;
    	}
	    head = temp; 
    }
    
}
