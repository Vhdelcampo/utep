import java.util.*;
public class Automata {
	public static void main(String args[]) {
		Graph FSM = toGraph("ac");
		FSM.print();
	}

	public static PriorityQueue<Character> createAlphabetList(String input) {
		int length = input.length();
		PriorityQueue<Character> root = new PriorityQueue<Character>(length);

		for (int i = 0; i < length; ++i) {
			char c = input.charAt(i);
			if (c >= 'a' && c <= 'z') {
				root.add(c);
			}
		}
		return root;
	}

	public static PriorityQueue<Character> createOperationList(String input) {
		int length = input.length();
		PriorityQueue<Character> root = new PriorityQueue<Character>(length);

		for (int i = 0; i < length; ++i) {
			char c = input.charAt(i);
			if (c == '+' || c == '.' || c == '*') {
				root.add(c);
			}
		}
		return root;
	}

	public static Graph toGraph(String input) {
		int length = input.length();
		PriorityQueue<Character> Operations = createOperationList(input);
		PriorityQueue<Character> Alphabet = createAlphabetList(input);
		Graph FSM = new Graph(length*2);
		int index = 0;
		System.out.println(Alphabet.toString());
		System.out.println(Operations.toString());
		
		while (Alphabet.isEmpty()) {
			char c = Alphabet.remove();
			FSM.addEdge(index, ++index, c);
		}
		 
		return FSM;
	}
}
