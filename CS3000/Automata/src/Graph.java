import java.util.*;
import java.io.*;

public class Graph {
	private int size; // Number of vertices
	private ArrayList<LinkedList<Edge>> neighbors; // Adjacency lists
	private ArrayList<String> labels; // Vertex labels

	// Inner class to represent edges
	public static class Edge {
		private int v1;
		private int v2;
		private double weight;

		public Edge(int v1, int v2, double weight) {
			this.v1 = v1;
			this.v2 = v2;
			this.weight = weight;
		}
	}

	// Constructs a graph with s vertices and no edges
	public Graph(int s) {
		size = s;
		neighbors = new ArrayList<LinkedList<Edge>>(size);
		for (int i=0; i<size; i++) {
			neighbors.add(new LinkedList<Edge>());
		}
		labels = new ArrayList<String>(size);
		for (int i=0; i<size; i++) {
			labels.add(Integer.toString(i));
		}
	}

	// Adds an edge to the graph
	public void addEdge(int v1, int v2, double weight) {
		Edge edge1 = new Edge(v1, v2, weight);
		neighbors(v1).add(edge1);
	}

	// Sets the label of a vertex
	public void setLabel(int v, String label) {
		labels.set(v, label);
	}

	// Get the index that goes with a label
	public int getVertexNumber(String label) {
		for (int i=0; i<size; i++)
			if (labels.get(i).equals(label))
				return i;
		return -1;
	}

	// Gets the neighbor list of a vertex
	public LinkedList<Edge> neighbors(int v) {
		return neighbors.get(v);
	}

	// Gets the size of the graph
	public int getSize() {
		return size;
	}

	// Displays the graph
	public void print() {
		for (int i=0; i<size; i++) {
			System.out.print(labels.get(i)+" -> ");
			for (Edge edge : neighbors(i))
				System.out.print(labels.get(edge.v2)+"("+edge.weight+") ");
			System.out.println();
		}
	}

	// Given a parent array, print the path between two vertices
	public void printPath(int s, int d, int[] parent) {
		if (parent[d] == -1)
			System.out.println("no path from "+labels.get(s)+" to "+labels.get(d));

		else {
			String path = "->"+labels.get(d);
			while (parent[d] != s) {
				d = parent[d];
				path = "->"+labels.get(d)+path;
			}

			path = labels.get(s)+path;
			System.out.println(path);
		}
	}
}
