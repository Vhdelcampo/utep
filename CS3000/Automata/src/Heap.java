public class Heap {
	private int[] H;

	public Heap() {
		H = new int[16];
		initialize();
	}

	public Heap(int size) {
		H = new int[size];
		initialize();
	}

	public void initialize() {
		H[0] = 0;
		for (int i = 1; i < H.length; i++) {
			H[i] = -1;
		}
	}

	public int extractMin() {
		if (H[0] <= 0) {
			throw new IllegalArgumentException("Removing from empty Heap \n");
		}
		int temp = H[1];
		H[1] = H[H[0]];
		H[0]--;
		int i = 1;
		boolean done = false;
		while (!done) {
			int min = i;
			if ((2 * i <= H[0]) && (H[2 * i] < H[i])) {
				min = 2 * i;
			}
			if ((2 * i + 1 <= H[0]) && (H[2 * i + 1] < H[min])) {
				min = 2 * i + 1;
			}
			if (min != i) {
				int t = H[i];
				H[i] = H[min];
				H[min] = t;
			} else
				done = true;
		}
		return temp;
	}

	public void insert(int k) {
		H[0]++;
		if (H[0] > H.length)
			doubleArraySize();
		H[H[0]] = k;
		int i = H[0];
		while ((i > 1) && (H[i] < H[i / 2])) {
			int temp = H[i];
			H[i] = H[i / 2];
			H[i / 2] = temp;
			i = i / 2;
		}
	}

	public boolean isEmpty() {
		return H[0] == 0;
	}

	public void doubleArraySize() {
		int[] doubledH = new int[H.length * 2];
		for (int i = 0; i < H.length; i++) {
			doubledH[i] = H[i];
		}
		H = doubledH;
	}

	public void print() {
		for (int i = 1; i < H[0]; i++) {
			System.out.print(H[i] + " ");
		}
		System.out.println();
	}
}
