
public class State {
	private int destination;
	private char value;
	private State next;
	
	public State(int destination, char value, State next) {
		this.destination = destination;
		this.value = value;
		this.next = next;
	}
	
	public State(int destination, char value) {
		this.destination = destination;
		this.value = value;
	}
	
	public int getDest() {
		return destination;
	}
	
	public char getValue() {
		return value;
	}
	
	public State getNext() {
		return next;
	}
	
	public void setNext(State next) {
		this.next = next;
	}
	
}
