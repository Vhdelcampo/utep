
public class Transition {
	private int source;
	private int destination;
	private char value;
	
	public Transition(int source, int destination, char value) {
		this.source = source;
		this.destination = destination;
		this.value = value;
	}
	
	public int getDestination() {
		return destination;
	}
	
	public int getSource() {
		return source;
	}
	
	public char getValue() {
		return value;
	}
	
	
}
