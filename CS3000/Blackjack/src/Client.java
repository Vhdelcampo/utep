//SimpleClient.java: A simple client program.
import java.net.*;
import java.io.*;

public class Client {
	public static void main(String args[]) throws IOException {
		Socket gameSocket = new Socket("localhost", 10000);

		OutputStream streamOut = gameSocket.getOutputStream();
		DataOutputStream dataOut = new DataOutputStream (streamOut);
		InputStream streamIn = gameSocket.getInputStream();
		DataInputStream dataIn = new DataInputStream(streamIn);
		
		dataOut.close();
		dataIn.close();
		streamOut.close();
		streamIn.close();
		gameSocket.close();
	}
}
