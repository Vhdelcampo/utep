import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JList;
import javax.swing.JLabel;
import javax.swing.JTextPane;
import javax.swing.JTextArea;
import javax.swing.JButton;
import javax.swing.SpringLayout;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.*;
import java.util.*;
import java.awt.CardLayout;
import java.awt.FlowLayout;

public class GUIDealerRun extends JFrame {
	private JPanel contentPane;
	private static Deck d = GUIPlayerRun.getDeck();
	private static Hand dHand = new Hand();
	private static Hand pHand = GUIPlayerRun.getPlayerHand();

	
	public static Hand getPlayerHand() {
		return pHand;
	}
	
	public static Hand getDealerHand() {
		return dHand;
	}
	
	public static void run() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUIDealerRun frame = new GUIDealerRun();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public GUIDealerRun() {
		dHand.reset();
		final JPanel panel = initializePanel();

		JButton hitButton = new JButton("Hit");
		hitButton.setBounds(10, 150, 100, 20);
		contentPane.add(hitButton);
		hitButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addCard(dHand, panel);
				checkEndConditions(dHand, pHand);
			}

		});
	}

	private JPanel initializePanel() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(0, 0, 600, 250);
		setTitle("Dealer Run");
		
		contentPane = new JPanel();
		contentPane.setBackground(new Color(102, 204, 0));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);
		setContentPane(contentPane);

		final JPanel panel = new JPanel();
		panel.setBounds(0, 0, 600, 100);
		panel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		contentPane.add(panel);
		return panel;
	}

	private void addCard(final Hand dHand, final JPanel panel) {
		JLabel label = new JLabel();
		panel.add(label);
		Card c = d.draw();
		dHand.hit(c);
		String name = c.getImageName();
		label.setIcon(new ImageIcon(GUIPlayerRun.class.getResource(name)));
	}
	
	private void checkEndConditions(final Hand dHand, final Hand pHand) {
		if (dHand.isBlackjack()){
			blackjackDialog();
			dispose();
			GUIEnd.run();
		} else if (dHand.isBust()) {
			bustDialog();
			dispose();
			GUIEnd.run();
		} else if (dHand.getPoints() > pHand.getPoints()) {
			dealerWinDialog();
			dispose();
			GUIEnd.run();
		}
	}

	private static void dealerWinDialog() {
		JOptionPane.showMessageDialog(null,
				"Dealer has more points than Player.");
	}

	private static void bustDialog() {
		JOptionPane.showMessageDialog(null,
				"This hand has busted!");
	}		

	private static void blackjackDialog() {
		JOptionPane.showMessageDialog(null,
				"Dealer has gotten a blackjack!");
	}	

}

