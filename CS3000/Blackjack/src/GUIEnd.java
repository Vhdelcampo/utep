import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import java.awt.Font;
import javax.swing.SwingConstants;
import java.awt.Color;


public class GUIEnd extends JFrame {
	private JPanel contentPane;
	Hand pHand = GUIDealerRun.getPlayerHand();
	Hand dHand = GUIDealerRun.getDealerHand();

	public static void run() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUIEnd frame = new GUIEnd();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public GUIEnd() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(0, 0, 450, 200);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(102, 204, 0));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JTextField txtPlayerPoints;
		JTextField txtDealerPoints;
		JTextField nameOfWinner;
		contentPane.setLayout(null);
		
		JLabel lblPlayerPoints = new JLabel("Player Points");
		lblPlayerPoints.setBounds(10, 10, 100, 15);
		getContentPane().add(lblPlayerPoints);
		
		JLabel lblDealerPoints = new JLabel("Dealer Points");
		lblDealerPoints.setBounds(10, 50, 100, 15);
		getContentPane().add(lblDealerPoints);
		
		txtPlayerPoints = new JTextField();
		txtPlayerPoints.setText("" + pHand.getPoints());
		txtPlayerPoints.setBounds(125, 10, 100, 20);
		getContentPane().add(txtPlayerPoints);
		txtPlayerPoints.setColumns(10);
		
		txtDealerPoints = new JTextField();
		txtDealerPoints.setText("" + dHand.getPoints());
		txtDealerPoints.setBounds(125, 50, 100, 20);
		getContentPane().add(txtDealerPoints);
		txtDealerPoints.setColumns(10);
		
		nameOfWinner = new JTextField();
		nameOfWinner.setHorizontalAlignment(SwingConstants.CENTER);
		nameOfWinner.setFont(new Font("Tahoma", Font.PLAIN, 24));
		nameOfWinner.setBounds(250, 10, 185, 75);
		getContentPane().add(nameOfWinner);
		nameOfWinner.setColumns(10);
		setWinner(nameOfWinner);
		
		JLabel lblWinnerOfThis = new JLabel("Winner of this Blackjack Game");
		lblWinnerOfThis.setBounds(250, 100, 185, 15);
		getContentPane().add(lblWinnerOfThis);
		
		JButton btnNewButton = new JButton("Reset");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				GUIPlayerRun.run();
				dispose();
			}
		});
		btnNewButton.setBounds(10, 100, 170, 50);
		getContentPane().add(btnNewButton);
	}

	private void setWinner(JTextField nameOfWinner) {
		int pPoints = pHand.getPoints();
		int dPoints = dHand.getPoints();
		if ((pPoints > dPoints && !pHand.isBust()) || dHand.isBust()) {
			nameOfWinner.setText(GUIIntroduction.getPlayerName());
		} else {
			nameOfWinner.setText(GUIIntroduction.getDealerName());
		}
	}

}
