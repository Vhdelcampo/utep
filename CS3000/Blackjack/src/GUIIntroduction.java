import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.BoxLayout;
import javax.swing.JTextArea;

import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.JTextField;

import java.awt.CardLayout;

import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.RowSpec;

import javax.swing.JLabel;
import javax.swing.JButton;

import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JRadioButton;

import java.awt.Font;
import java.io.IOException;

import javax.swing.JOptionPane;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;

public class GUIIntroduction extends JFrame {
	
	public static void main(String args[]) {
		run();
	}
	
	public static void startServer() {
		try {
			Server.start();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
	
	public static void startClient() {
		try {
			Server.start();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
	
	private static String playerName;
	private static String dealerName;
	private static boolean dealer;
	
	public static String getPlayerName() {
		return playerName;
	}
	
	public static String getDealerName() {
		return dealerName;
	}
	
	public static boolean isDealer() {
		return dealer;
	}
	
	public static void run() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				GUIIntroduction frame = new GUIIntroduction();
				frame.setVisible(true);
				frame.setResizable(false);
			} 
		});
	}
	
	private final ButtonGroup buttonGroup = new ButtonGroup();
	
	public GUIIntroduction() {
		JPanel contentPane = initializePanel();
	
		JPanel radioButtonPanel = new JPanel();
		radioButtonPanel.setBounds(25, 135, 234, 66);
		contentPane.add(radioButtonPanel);
		
		JRadioButton playerRadioButton = new JRadioButton("I want to be the Player");
		playerRadioButton.setSelected(true);
		playerRadioButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dealer = false;
			}
		});
		playerRadioButton.setBounds(6, 0, 222, 23);
		
		JRadioButton dealerRadioButton = new JRadioButton("I want to be the Dealer");
		dealerRadioButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dealer = true;
			}
		});
		dealerRadioButton.setBounds(6, 36, 222, 23);
		
		buttonGroup.add(playerRadioButton);
		buttonGroup.add(dealerRadioButton);
		radioButtonPanel.add(playerRadioButton);
		radioButtonPanel.add(dealerRadioButton);
		
		final JTextField playerNameField = new JTextField();
		playerNameField.setBounds(15, 103, 129, 20);
		contentPane.add(playerNameField);
		playerNameField.setColumns(10);

		final JTextField dealerNameField = new JTextField();
		dealerNameField.setBounds(265, 103, 129, 20);
		contentPane.add(dealerNameField);
		dealerNameField.setColumns(10);
		
		JButton startButton = new JButton("Start");
		startButton.setBounds(265, 135, 240, 70);
		startButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				playerName = playerNameField.getText();
				dealerName = dealerNameField.getText();
				dispose();
				GUIPlayerRun.run();
			}
		});
		contentPane.add(startButton);
	}

	private JPanel initializePanel() {
		setTitle("Blackjack Introduction");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(0, 0, 500, 250);
		JPanel contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JTextArea txtIntroduction = new JTextArea();
		txtIntroduction.setBounds(5, 5, 500, 86);
		txtIntroduction.setFont(new Font("Monospaced", Font.PLAIN, 13));
		txtIntroduction.setLineWrap(true);
		txtIntroduction.setTabSize(4);
		txtIntroduction.setText(
				"Hello and welcome to an exciting game of Blackjack. "
				+ "\r\nEnter the name of the Player and the name of the Dealer below."
				+ "\r\nThen select wheter you want to be Dealer or the Player."
				+ "\r\nAfter doing so press Start to begin the game!");
		
		contentPane.add(txtIntroduction);
		
		JLabel lblPlayerName = new JLabel("Player Name");
		lblPlayerName.setBounds(162, 103, 101, 14);
		contentPane.add(lblPlayerName);

		JLabel lblDealerName = new JLabel("Dealer Name");
		lblDealerName.setBounds(412, 103, 101, 14);
		contentPane.add(lblDealerName);
		return contentPane;
	}
	
}
