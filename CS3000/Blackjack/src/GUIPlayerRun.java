import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JList;
import javax.swing.JLabel;
import javax.swing.JTextPane;
import javax.swing.JTextArea;
import javax.swing.JButton;
import javax.swing.SpringLayout;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.*;
import java.util.*;
import java.awt.CardLayout;
import java.awt.FlowLayout;

public class GUIPlayerRun extends JFrame {
	private JPanel contentPane;
	private static Deck d = new Deck();
	private static Hand pHand = new Hand();
	
	public static Deck getDeck() {
		return d;
	}
	
	public static Hand getPlayerHand() {
		return pHand;
	}

	public static void run() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUIPlayerRun frame = new GUIPlayerRun();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public GUIPlayerRun() {
		d.reset();
		pHand.reset();
		final JPanel panel = initializePanel();

		JButton hitButton = new JButton("Hit");
		hitButton.setBounds(10, 150, 100, 20);
		contentPane.add(hitButton);
		hitButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addCard(pHand, panel);
				if (pHand.isBlackjack()){
					blackjackDialog();
					dispose();
					GUIDealerRun.run();
				} else if (pHand.isBust()) {
					bustDialog();
					dispose();
					GUIEnd.run();
				}
			}
		});

		JButton stayButton = new JButton("Stay");
		stayButton.setBounds(470, 150, 100, 20);
		contentPane.add(stayButton);
		stayButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				stayDialog();
				dispose();
				GUIDealerRun.run();
			}
		});
	}

	private JPanel initializePanel() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(0, 0, 600, 250);
		setTitle("Dealer Run");
		
		contentPane = new JPanel();
		contentPane.setBackground(new Color(102, 204, 0));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);
		setContentPane(contentPane);

		final JPanel panel = new JPanel();
		panel.setBounds(0, 0, 600, 100);
		panel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		contentPane.add(panel);
		return panel;
	}

	private void addCard(final Hand pHand, final JPanel panel) {
		JLabel label = new JLabel();
		panel.add(label);
		Card c = d.draw();
		pHand.hit(c);
		String name = c.getImageName();
		label.setIcon(new ImageIcon(GUIPlayerRun.class.getResource(name)));
	}
	
	public static void stayDialog() {
		JOptionPane.showMessageDialog(null,
				"Player has stayed. Dealer's turn will now begin");
	}
	public static void bustDialog() {
		JOptionPane.showMessageDialog(null,
				"This hand has busted!");
	}	

	public static void blackjackDialog() {
		JOptionPane.showMessageDialog(null,
				"Player has gotten a blackjack! Dealer's turn will now begin");
	}	

}

