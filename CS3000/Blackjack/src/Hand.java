import java.io.Serializable;

public class Hand implements Serializable{
	
	private static final long serialVersionUID = 999L;
	private int id;
	private Card[] hand;
	private int points;
	private int handSize;

	public Hand() {
		 // 11 is max number of cards in hand
		hand = new Card[11];
		points = 0;
		handSize = 0;
	}
	
	public Hand(int id) {
		this();
		this.id = id;
	}
	
	public int getPoints() {
		calculatePoints();
		return points;
	}
	
	public int getSize() {
		return handSize;
	}
	
	public Card getCardAtIndex(int i) {
		return hand[i];
	}

	public void displayHand() {
		for (int i = 0; i < handSize; i++){
			hand[i].displayCard();
		}
		System.out.println();
	}

	public void displayPoints() {
		System.out.println("Current points are " + points);
	}
	
	public int calculateMaxPoints(int index) {
		int total = 0;
		for (int i = 0; i < index; i++) {
			total += hand[i].getPointValue();
		}
		return  total;
	}

	public void calculatePoints() {
		int total = 0;
		for (int i = 0; i < handSize; i++) {
			total += hand[i].getPointValue();
		}
		points = total;
	}
	
	public boolean isEmpty() {
		return handSize == 0;
	}

	public boolean isBust() {
		calculatePoints();
		return (points > 21);
	}
	
	public boolean isBlackjack() {
		if (handSize != 2) {
			return false;
		}
		if (getPoints() == 21) {
			return true;
		}
		return false;
	}

	public void hit(Card c) {
		if (handSize == 11) {
			System.out.println("Max hand limit reached");
		} else {
		hand[handSize] = c;
		handSize++;
		}
	}
	
	
	public void reset(){
		handSize = 0;
		hand = new Card[11];
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

}
