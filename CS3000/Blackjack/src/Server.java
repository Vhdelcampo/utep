import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

import javax.swing.JOptionPane;

public class Server {
	public static void start() throws IOException {
		// Register service on port 10000
		ServerSocket s = new ServerSocket(10000);
		JOptionPane.showMessageDialog(null, 
				"Trying to connect to server..."
				);
		Socket gameSocket = s.accept(); // Wait and accept a connection
		JOptionPane.getRootFrame().dispose();
		
		// Get a communication stream associated with the socket
		OutputStream streamOut = gameSocket.getOutputStream();
		DataOutputStream dataOut = new DataOutputStream (streamOut);
		InputStream streamIn = gameSocket.getInputStream();
		DataInputStream dataIn = new DataInputStream(streamIn);
		
		dataOut.close();
		dataIn.close();
		streamOut.close();
		streamIn.close();
		gameSocket.close();
		s.close();
	}
}