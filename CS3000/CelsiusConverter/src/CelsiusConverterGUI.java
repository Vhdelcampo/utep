import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.FlowLayout;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.AbstractAction;
import java.awt.event.ActionEvent;
import javax.swing.Action;
import java.awt.event.ActionListener;


public class CelsiusConverterGUI extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CelsiusConverterGUI frame = new CelsiusConverterGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CelsiusConverterGUI() {
		setTitle("Celsius Converter");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 250, 140);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JFormattedTextField tempTextField = new JFormattedTextField();
		tempTextField.setBounds(10, 11, 100, 20);
		contentPane.add(tempTextField);
		
		JLabel celsiusLabel = new JLabel("Celsius");
		celsiusLabel.setBounds(120, 14, 104, 14);
		contentPane.add(celsiusLabel);
		
		JLabel fahrenLabel = new JLabel("Farenheit");
		fahrenLabel.setBounds(120, 46, 104, 14);
		contentPane.add(fahrenLabel);
		
		JButton convertButton = new JButton("Convert");
		convertButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			    //Parse degrees Celsius as a double and convert to Fahrenheit.
			    int tempFahr = (int)((Double.parseDouble(tempTextField.getText())) * 1.8 + 32);
			    fahrenLabel.setText(tempFahr + " Fahrenheit");
			}
		});
		convertButton.setBounds(10, 42, 100, 23);
		contentPane.add(convertButton);
		

	}

}
