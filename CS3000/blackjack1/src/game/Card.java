package game;
public class Card{
	private char rank;
	private char suit;
	private char color;

	public Card(char color, char suit, char rank) {
		this.color = color;
		this.suit = suit;
		this.rank = rank;
	}

	public char getRank() {
		return rank;
	}

	public char getSuit() {
		return suit;
	}

	public char getColor() {
		return color;
	}

	public void setRank(char rank) {
		this.rank = rank;
	}

	public void setSuit(char suit) {
		this.suit = suit;
	}

	public void setColor(char color) {
		this.color = color;
	}
	
	public int getPoints(boolean aceLow) {
		if (rank >= '1' && rank <= '9'){
			return rank - '0';
		}else if (rank == 'A'){
			if (aceLow) return 1;
			return 11;
		}else{
			return 10;
		}
	}

	public void displayCard(){
		if (rank >= '1' && rank <= '9'){
			System.out.print(rank + " of ");
		}else if(rank == '0'){
			System.out.print("10 of ");
		}else if(rank == 'A'){
			System.out.print("Ace of ");
		}else if(rank == 'J'){
			System.out.print("Jack of ");
		}else if(rank == 'Q'){
			System.out.print("Queen of ");
		}else if(rank == 'K'){
			System.out.print("King of ");
		}else{
			System.out.print("Invalid Rank");
		}

		if (suit == 'c'){
			System.out.println("Clubs");
		}else if(suit == 's'){
			System.out.println("Spades");
		}else if(suit == 'd'){
			System.out.println("Diamonds");
		}else if(suit == 'h'){
			System.out.println("Hearts");
		}else{
			System.out.println("Invalid Suit");
		}
	}
}
