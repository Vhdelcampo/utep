package game;
public class Deck {
	private int numCards;
	private Card[] deck;
	private int cardPosition;

	public Deck(){
		this.numCards = 52;
		deck = new Card[numCards];
		char color = ' '; char suit = ' '; char rank = ' ';
		for (int i = 0; i < 52; i++){

			// choose color
			if ( i < 24){
				color = 'b';
			}
			else{
				color = 'r';
			}

			// choose suit
			// 52 / 4 = 13, the number of cards that have one specific suit in a standard 52 card deck
			if ( i < 13){
				suit = 'c';
			}else if( i < 26){
				suit = 's';
			}else if( i < 39){
				suit = 'd';
			}else{
				suit = 'h';
			}

			// choose rank
			// Numbers range from 1 and repeat to 12 in modulo 13.
			int k = i % 13;
			switch ( k ) {
			case 1:
				rank = 'A';
				break;
			case 2:
				rank = '2';
				break;
			case 3:
				rank = '3';
				break;
			case 4:
				rank = '4';
				break;
			case 5:
				rank = '5';
				break;
			case 6:
				rank = '6';
				break;
			case 7:
				rank = '7';
				break;
			case 8:
				rank = '8';
				break;
			case 9:
				rank = '9';
				break;
			case 10:
				rank = '0'; // 0 will represent 10
				break;
			case 11:
				rank = 'J';
				break;
			default:
				rank = 'K';
			}
			// System.out.println("iteration is " + i + " color is " + color + " suit is " + suit + " rank is " + rank);
			deck[i] = new Card(color, suit, rank);
		}
		// Start at top of deck
		cardPosition = 0;
	}

	public int getNumberOfCards(){
		return numCards;
	}

	public int getCardPosition(){
		return cardPosition;
	}

	// Fisher-Yates Shuffle
	public void shuffle(){
		for (int i = numCards - 1; i > 1; i--){
			int j = (int)(Math.random() * 52);
			Card temp = deck[i];
			deck[i] = deck[j];
			deck[j] = temp;
		}
	}

	public Card draw(){
		cardPosition++;
		return deck[cardPosition - 1];
	}
	
	public void reset() {
		cardPosition = 0;
		shuffle();
	}

	public void displayDeck(){
		for (int i = cardPosition; i < numCards; i++){
			deck[i].displayCard();
		}
	}

}
