package game;
public class Hand{
	private Card[] hand;
	private int points;
	private int handSize;

	public Hand() {
		 // 11 is max number of cards in hand
		hand = new Card[11];
		points = 0;
		handSize = 0;
	}
	
	public int getPoints() {
		calculatePoints();
		return points;
	}

	public void displayHand() {
		for (int i = 0; i < handSize; i++){
			hand[i].displayCard();
		}
		System.out.println();
	}

	public void displayPoints() {
		System.out.println("Current points are " + points);
	}
	
	public int calculateMaxPoints(int index) {
		int total = 0;
		for (int i = 0; i < index; i++) {
			total += hand[i].getPoints(false);
		}
		return  total;
	}

	public void calculatePoints() {
		int total = 0;
		for (int i = 0; i < handSize; i++) {
			// Choose to use ace low if doing so would not result in bust.
			boolean aceLow = (calculateMaxPoints(i) > 21);
			total += hand[i].getPoints(aceLow);
		}
		points = total;
	}

	public boolean isBust() {
		calculatePoints();
		return (points > 21);
	}
	
	public boolean isBlackjack() {
		Card first = hand[handSize-1];
		Card second = hand[handSize-2];
		if (first.getSuit() == 'A' || second.getSuit() == 'A') {
			if (first.getPoints(true) + second.getRank() == 21){
					return true;
			}
		}
		return false;
	}

	public void hit(Card c) {
		hand[handSize] = c;
		handSize++;
	}
	
	public void reset(){
		handSize = 0;
		hand = new Card[11];
	}

}
