/*
   Author: Shammir Ibarra
   CS 3331
   Software Development Assignment: 3
   Instructor: Omar Ochoa
   Date: 3/31/2015
   Purpose: export a log of mouse pointer locations along 
   			with a betting tab implemented on the prior program
 */

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
 
public class GameLog{
	
	String [] lines;
	int index;
	
	public GameLog(){
		lines = new String [10000];
		index = 0;
	}//end constructor
	
	public void setLog(String info){
		lines [index] = info;
		index++;
	}//end setLog method
	
	public void catalog() {
		
		try {
			
			String path = "/Users/Shammir/Desktop/gamelog.txt";
						
			File log_File = new File(path);
			FileWriter fw = new FileWriter(log_File.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			for(int i = 0; i < index; i++){
				bw.write(lines[i]);
				bw.newLine();
			}
			bw.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}//end catalog method
	
}//end GameLog Class