/*
   Author: Shammir Ibarra
   CS 3331
   Software Development Assignment: 3
   Instructor: Omar Ochoa
   Date: 3/31/2015
   Purpose: export a log of mouse pointer locations along 
   			with a betting tab implemented on the prior program
 */

import java.awt.EventQueue;
import java.io.Serializable;

import javax.swing.*;


public class Beginning  implements Serializable{
	private static Introduction start;
	private int id = 999;
	
	public Beginning (){
		
		int response = JOptionPane.showConfirmDialog(null, "We will now begin a game of Blackjack. "
				+ "\r\nWe will need to designate a \"Dealer\" and a \"Player\". "
				+ "\r\nThe role of \"Dealer\" will alternate after each game. "
				+ "\r\nIf you would like to quit at any time, press \"exit\"\r\nat any time. "
				+ "\r\nThe objective is to get gain a higher card value than the dealer without passing 21."
				+ "\r\nIf the player passes 21, the player is bust and the dealer wins."
				+ "\r\nIf the dealer passes 21 trying to surpass the player, the player wins"
				+ "\r\nThe player getting a 21 with the first two cards, and the dealer does not, means a blackjack."
				+ "\r\nAces can either be 11 or 1 if an additional 11 means a bust."
				+ "\r\nIf no dealer is assigned, the first player will be assigned as the dealer"
				+ "\r\nIf no names are chosen, first player will be \"name1\", and second player will be \"name2\""
				+ "\r\nWould you like to play?", "Blackjack" , JOptionPane.YES_NO_OPTION);
		if(response == JOptionPane.NO_OPTION)
			System.exit(0);
		else if(response == JOptionPane.YES_OPTION){
			
			start = new Introduction();
			start.frame.setVisible(true);
			
//			EventQueue.invokeLater(new Runnable() {
//				public void run() {
//					try {
//						start = new Introduction();
//						start.frame.setVisible(true);
//					} catch (Exception e) {
//						e.printStackTrace();
//					}
//					
//					
//				}
//				
//			});
			
		}
		
	}//end main method
	
	
}//end Introduction class
