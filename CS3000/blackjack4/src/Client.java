import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.SocketException;

public class Client {
	private Socket socket = null;
	private ObjectInputStream inputStream = null;
	private ObjectOutputStream outputStream = null;
	private boolean isConnected = false;

	public Client() {

	}

	public void communicate() {

		while (!isConnected) {
			try {
				socket = new Socket("localhost", 10000);
				System.out.println("Connected");
				isConnected = true;
				outputStream = new ObjectOutputStream(socket.getOutputStream());
				Beginning game = new Beginning();
				System.out.println("Object to be written = " + game);
				outputStream.writeObject(game);


			} catch (SocketException se) {
				se.printStackTrace();
				// System.exit(0);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static void main(String[] args) {
		Client client = new Client();
		client.communicate();
	}
}
