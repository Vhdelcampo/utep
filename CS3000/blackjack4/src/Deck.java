/*
   Author: Shammir Ibarra
   CS 3331
   Software Development Assignment: 3
   Instructor: Omar Ochoa
   Date: 3/31/2015
   Purpose: export a log of mouse pointer locations along 
   			with a betting tab implemented on the prior program
 */

public class Deck {
	private int [] deck = new int [52];
	private int topOfDeck = 51;

	public Deck(){
		int [] tempDeck = new int[52];

		for(int f = 0; f < 52; f++){
			tempDeck[f] = f;
		}//populates the cardMatrix with 4 cards of each type

		deck = shuffle(tempDeck);

	}//end Deck constructor method


	private int [] shuffle(int [] array) {
		int currentIndex = array.length, temporaryValue, randomIndex ;

		while (0 != currentIndex) {
			// Pick random card
			randomIndex = (int)(Math.floor(Math.random() * currentIndex) );
			currentIndex -= 1;

			// swap with current card.
			temporaryValue = array[currentIndex];
			array[currentIndex] = array[randomIndex];
			array[randomIndex] = temporaryValue;
		}

		return array;

	}//end shuffle method

	
	public int hit(){
		int top = deck[topOfDeck];
		topOfDeck--;
		return top;

	}//end hit method

	
	public int rank(){
		int top = deck[topOfDeck];
		int rank = top % 13;
		rank++;
		return rank;
	}

	
	public String cardPath(){
		int value = hit();
		
		int rank = value % 13;	
		rank++;
		int suitNum = value / 13;
		
		String first = suit(suitNum);
		String second = Integer.toString(rank);
		
		String card = first + second;
		String type = ".gif";
		String path = "C:\\Users\\Shammir\\Desktop\\images\\";

		path = path + card + type;
		return path;
		
	}


	public String suit(int s){
		if(s == 0)
			return "c"; //clubs
		else if(s == 1)
			return "d"; //diamonds
		else if(s == 2)
			return "h"; //hearts
		else
			return "s";	//spades
	}
	
	
}//end Deck object