/*
   Author: Shammir Ibarra
   CS 3331
   Software Development Assignment: 3
   Instructor: Omar Ochoa
   Date: 3/31/2015
   Purpose: export a log of mouse pointer locations along 
   			with a betting tab implemented on the prior program
 */

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;

import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

import javax.swing.JLabel;
import javax.swing.JTextPane;

import java.util.TimerTask;


public class GameTable extends TimerTask implements MouseMotionListener{

	JFrame frame = new JFrame();
	private JPanel contentPane, inner_panel;
	private JLabel [] playerCards = new JLabel[11];
	private JLabel [] dealerCards = new JLabel[11];
	//cards and table
	
	private JTextPane player_name, dealer_name;
	private JTextPane Player_Card_Total, Dealer_Card_Total;
	private JTextPane Player_Money_Total, Dealer_Money_Total;
	private JLabel WinningLabel;
	private int playerTurn = 0, dealerTurn = 0;
	Deck cardStack;
	//players' details
	
	private int playerTotal = 0, dealerTotal = 0;
	private int playerChipTotal = 1000, dealerChipTotal = 1000;
	private boolean blackjack = false, bust = false;
	private String victory_message;
	private String facedown = "C:\\Users\\Shammir\\Desktop\\images\\b2fv.gif";
	//additional player details
	
	JButton btnHit, btnStay, btnExit;
	private int seconds = 1;
	private String component;
	private GameLog mouse_location_log = new GameLog();
	//timing and log file
	
	public void run() {
			String component = this.component;
			mouse_location_log.setLog("On Player " + player_name.getText() + "'s turn, at "
					+ seconds++ + " seconds the mouse button was hovering over the " + component + " with " + player_name.getText() 
					+ "�s " + playerChipTotal + " chip balance.\n");	
			
	}//end constructor	
	
	

	public GameTable(String first, String second, int designation){
		cardStack = new Deck();					
		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setBounds(50, 50, 900, 600);
		frame.setTitle("Game");
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		frame.setContentPane(contentPane);
		contentPane.setLayout(null);
		
		btnHit = new JButton("HIT");
		btnHit.setBounds(40, 527, 79, 23);
		contentPane.add(btnHit);
		btnHit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e){
				setPlayerTotal();
				String path = cardStack.cardPath();
				playerHand(path);
				playerCards[playerTurn - 1].setVisible(true);
				if(playerTotal == 21){
					Dealer_Two_Cards();
					dealerHand();
				}
				
				if(playerTotal > 21){
					rules();	
					endgame();
				}
				
			}
		});

		btnStay = new JButton("STAY");
		btnStay.setBounds(150, 527, 89, 23);
		contentPane.add(btnStay);
		btnStay.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e){
				dealerHand();
			}
		});
		
		
		btnExit = new JButton("EXIT");
		btnExit.setBounds(750, 527, 89, 23);
		contentPane.add(btnExit);
		btnExit.addActionListener( new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
		
		btnHit.addMouseMotionListener(this);
        btnStay.addMouseMotionListener(this);
        btnExit.addMouseMotionListener(this);
		
		inner_panel = new JPanel();
		inner_panel.setBounds(10, 11, 860, 500);
		inner_panel.setBackground(Color.green);
		contentPane.add(inner_panel);
		inner_panel.setLayout(null);
		
		JLabel player_tag = new JLabel("Player:");
		player_tag.setBounds(40, 100, 100, 20);
		inner_panel.add(player_tag);
		
		player_name = new JTextPane();
		player_name.setBounds(40, 120, 200, 20);
		inner_panel.add(player_name);
		
		JLabel dealer_tag = new JLabel("Dealer:");
		dealer_tag.setBounds(40, 290, 100, 20);
		inner_panel.add(dealer_tag);
		
		dealer_name = new JTextPane();
		dealer_name.setBounds(40, 310, 200, 20);
		inner_panel.add(dealer_name);
		
		JLabel Card_Total_Label1 = new JLabel("Card Total:");
		Card_Total_Label1.setBounds(40, 160, 70, 20);
		inner_panel.add(Card_Total_Label1);
		Card_Total_Label1.addMouseMotionListener(this);
		
		JLabel Card_Total_Label2 = new JLabel("Card Total:");
		Card_Total_Label2.setBounds(40, 350, 70, 20);
		inner_panel.add(Card_Total_Label2);
		Card_Total_Label2.addMouseMotionListener(this);
		
		WinningLabel = new JLabel();
		WinningLabel.setBounds(310, 440, 300, 20);
		inner_panel.add(WinningLabel);
		
		Player_Card_Total = new JTextPane();
		Player_Card_Total.setText("PlayerTotal");
		Player_Card_Total.setBounds(40, 180, 70, 20);
		inner_panel.add(Player_Card_Total);
		Player_Card_Total.addMouseMotionListener(this);
		
		Dealer_Card_Total = new JTextPane();
		Dealer_Card_Total.setText("DealerTotal");
		Dealer_Card_Total.setBounds(40, 370, 70, 20);
		inner_panel.add(Dealer_Card_Total);
		Dealer_Card_Total.addMouseMotionListener(this);
	
		
		JLabel Money_Total_Label1 = new JLabel("Chip Total:");
		Money_Total_Label1.setBounds(140, 160, 100, 20);
		inner_panel.add(Money_Total_Label1);
		Money_Total_Label1.addMouseMotionListener(this);
		
		JLabel Money_Total_Label2 = new JLabel("Chip Total:");
		Money_Total_Label2.setBounds(140, 350, 100, 20);
		inner_panel.add(Money_Total_Label2);
		Money_Total_Label2.addMouseMotionListener(this);

		Player_Money_Total = new JTextPane();
		Player_Money_Total.setText("$1000");
		Player_Money_Total.setBounds(140, 180, 50, 20);
		inner_panel.add(Player_Money_Total);
		Player_Money_Total.addMouseMotionListener(this);
		
		Dealer_Money_Total = new JTextPane();
		Dealer_Money_Total.setText("$1000");
		Dealer_Money_Total.setBounds(140, 370, 50, 20);
		inner_panel.add(Dealer_Money_Total);
		Dealer_Money_Total.addMouseMotionListener(this);
		
		
		for(int i = 0; i < 11; i++){
			int temp = i + 1;
			int base = 230;
			int line = base + (temp * 30);
			
			playerCards[i] = new JLabel();
			playerCards[i].setBounds(line, 30, 130, 200);
			inner_panel.add(playerCards[i]);
			playerCards[i].setVisible(false);
			playerCards[i].addMouseMotionListener(this);
			
			
			dealerCards[i] = new JLabel();
			dealerCards[i].setBounds(line, 210, 130, 200);
			inner_panel.add(dealerCards[i]);
			dealerCards[i].setVisible(false);
			dealerCards[i].addMouseMotionListener(this);
		}//initialize invisible cards that will be made visible later
		
		
		String player = "player1";
		String dealer = "player2";
		if(designation == 1){
			player = second;
			dealer = first;
		}
		else if(designation == 2){
			player = first;
			dealer = second;
		}
		player_name.setText(player);
		dealer_name.setText(dealer);
		frame.setVisible(true);
		
		
		Player_Two_Cards();
		Dealer_Two_Cards();
		
		
	}//end constructor method
	
	
	public void Player_Two_Cards(){
		for(int i = 0; i < 2; i++){
			setPlayerTotal();
			String path = cardStack.cardPath();
			playerHand(path);
			playerCards[playerTurn - 1].setVisible(true);
			if(playerTotal == 21)
				blackjack = true;
		}
		
	}
	
	
	public void Dealer_Two_Cards(){
		if(bust){
			for(int i = 0; i < 2; i++){
				setDealerTotal();
				String newpath = cardStack.cardPath();
				ImageIcon card = new ImageIcon(newpath);
				dealerCards[dealerTurn].setIcon(card);
				dealerCards[dealerTurn].setVisible(true);
				dealerTurn++;
			}
		}  
		else if(blackjack){
			
			for(int i = 0; i < 2; i++){
				setDealerTotal();
				String newpath = cardStack.cardPath();
				ImageIcon card = new ImageIcon(newpath);
				dealerCards[dealerTurn].setIcon(card);
				dealerCards[dealerTurn].setVisible(true);
				dealerTurn++;
			}
			rules();
			endgame();
		} else {
			
			for(int i = 0; i < 2; i++){
				ImageIcon card = new ImageIcon(facedown);
				dealerCards[i].setIcon(card);
				dealerCards[i].setVisible(true);
			}
			
		}
		
				
		
	}//end Dealer_Two_Cards method
	
	
	public int currentCard(int total){
		int rank = cardStack.rank();
		if(rank >= 10){
			rank = 10;
		}	
		
		if(rank == 1){
			if(11 + total > 21)
				rank = 1;
			else
				rank = 11;
		}
		return rank;
		
	}

	
	public void setPlayerTotal(){
		int cardvalue = currentCard(playerTotal);
		playerTotal += cardvalue;
		String total = Integer.toString(playerTotal);
		Player_Card_Total.setText(total);
	}
	
	
	public void setDealerTotal(){
		int cardvalue = currentCard(dealerTotal);
		dealerTotal += cardvalue;
		String total = Integer.toString(dealerTotal);
		Dealer_Card_Total.setText(total);
	}

	
	public void playerHand(String path){
		ImageIcon card = new ImageIcon(path);
		playerCards[playerTurn].setIcon(card);
		playerTurn++;
		
		
	}//end playerHand method
	
	
	public void dealerHand(){
		
		while(dealerTotal < playerTotal){
			setDealerTotal();
			String path = cardStack.cardPath(); 
			ImageIcon card = new ImageIcon(path);
			dealerCards[dealerTurn].setIcon(card);
			dealerTurn++;
			dealerCards[dealerTurn - 1].setVisible(true);
			if(rules())
				break;
		}
		endgame();
		
	}
	
	
	public void reset(){
		
		for(int i = 0; i < playerCards.length; i++){
			playerCards[i].setVisible(false);
		}
		
		for(int i = 0; i < dealerCards.length; i++){
			dealerCards[i].setVisible(false);
		}
		playerTurn = 0; dealerTurn = 0;
		Player_Card_Total.setText("0"); Dealer_Card_Total.setText("0");
		playerTotal = 0; dealerTotal = 0;
		blackjack = false; 
		bust = false;
		cardStack = new Deck();
		
		WinningLabel.setVisible(false);
		
		String temp = player_name.getText();
		player_name.setText(dealer_name.getText());
		dealer_name.setText(temp);


		String dealer = Integer.toString(dealerChipTotal);
		String player = Integer.toString(playerChipTotal);
		Player_Money_Total.setText(dealer);
		Dealer_Money_Total.setText(player);
		
		Player_Two_Cards();
		Dealer_Two_Cards();
		
		
	}
	
	
	public void chipTotal(int winner){
		if(winner == 1){
			playerChipTotal += 100;
			dealerChipTotal -= 100;
			System.out.println(player_name.getText() + " earned 100");
		}
		else if(winner == 2){
			dealerChipTotal += 100;
			playerChipTotal -= 100;
			System.out.println(dealer_name.getText() + " earned 100\n");
		}
		String player = Integer.toString(playerChipTotal);
		String dealer = Integer.toString(dealerChipTotal);
		Player_Money_Total.setText(player);
		Dealer_Money_Total.setText(dealer);
			
	}
	
	
	public boolean rules(){
		
		if(playerTotal == 21 && blackjack){
			if(playerTotal > dealerTotal){
				victory_message = "Blackjack! Player " + player_name.getText() + " wins!";
				chipTotal(1);
				return true;
			}
			else if(playerTotal == dealerTotal){
				victory_message = "Tie!";
				return true;
			}
			
		}
		else if(playerTotal > 21){
			bust = true;
			Dealer_Two_Cards();
			victory_message = "Bust! Dealer " + dealer_name.getText() + " wins!";
			chipTotal(2);
			return true;
		}
		else if(playerTotal == dealerTotal){
			victory_message = "Tie!";
			return true;
		}
		else if(dealerTotal > playerTotal){
			if(dealerTotal > playerTotal && dealerTotal <= 21){
				victory_message = "Dealer " + dealer_name.getText() + " wins!";
				chipTotal(2);
				return true;
			}
			else if(dealerTotal > 21){
				victory_message = "Player " + player_name.getText() + " wins!";
				chipTotal(1);
				return true;
			}
			
			
		} 
		return false;
		
	}

	
	public void endgame(){
		
		JOptionPane.showMessageDialog(null, victory_message);
		
		if(playerChipTotal == 0)
			Ultimate_Winner( player_name.getText() + " defeated player" + dealer_name.getText() + " by winnning all of the chips!");
		else if(dealerChipTotal == 0)
			Ultimate_Winner( dealer_name.getText() + " defeated player" + player_name.getText() + " by winnning all of the chips!");			
		else
			reset();
		
	}
	
	
	public void Ultimate_Winner(String ultimate_winner){
		
		int response = JOptionPane.showConfirmDialog(null, ultimate_winner 
				+ "\nWould you like to play again?", "Game" , JOptionPane.YES_NO_OPTION);
		if(response == JOptionPane.YES_OPTION){
			playerChipTotal = 1000;
			dealerChipTotal = 1000;
			reset();
		}
		else if(response == JOptionPane.NO_OPTION){
			mouse_location_log.catalog();
			System.exit(0);
		}
		
	}
	

     
    public void mouseMoved(MouseEvent event) {
    	component = event.getComponent().getClass().getName();
    }
    
    
    public void mouseDragged(MouseEvent event) {
    	component = event.getComponent().getClass().getName();
    }
    
	
	
}//Game object

