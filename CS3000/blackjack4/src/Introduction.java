/*
   Author: Shammir Ibarra
   CS 3331
   Software Development Assignment: 3
   Instructor: Omar Ochoa
   Date: 3/31/2015
   Purpose: export a log of mouse pointer locations along 
   			with a betting tab implemented on the prior program
 */


import java.awt.EventQueue;
import java.awt.event.*;
import java.util.Timer;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.*;


public class Introduction implements ActionListener{

	public JFrame frame = new JFrame();
	private JTextField Second_Player_Name, First_Player_Name;
	
	private int dealer;
	private GameTable game;
	
	public Introduction() {
		ButtonGroup buttonGroup = new ButtonGroup();
		JPanel contentPane;
		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setBounds(100, 100, 498, 350);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		frame.setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnBeginGame = new JButton("Begin Game");
		btnBeginGame.setBounds(60, 266, 150, 23);
		contentPane.add(btnBeginGame);
		
		JButton btnExit = new JButton("Exit");
		btnExit.setBounds(250, 266, 150, 23);
		contentPane.add(btnExit);
		btnExit.addActionListener( new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	System.exit(0);
            }
        });
		
		JLabel firstPlayerDesignating = new JLabel("First Player Name:");
		firstPlayerDesignating.setBounds(33, 87, 166, 14);
		contentPane.add(firstPlayerDesignating);
		
		JRadioButton FirstPlayerDealer = new JRadioButton("Dealer");
		FirstPlayerDealer.setBounds(62, 154, 137, 23);
		buttonGroup.add(FirstPlayerDealer);
		contentPane.add(FirstPlayerDealer);
		FirstPlayerDealer.addActionListener( new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                dealer = 1;
            }
        });
		
		JRadioButton SecondPlayerDealer = new JRadioButton("Dealer");
		SecondPlayerDealer.setBounds(238, 154, 134, 23);
		buttonGroup.add(SecondPlayerDealer);
		contentPane.add(SecondPlayerDealer);
		SecondPlayerDealer.addActionListener( new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                dealer = 2;
            }
        });
		
		Second_Player_Name = new JTextField();
		Second_Player_Name.setBounds(209, 112, 86, 20);
		contentPane.add(Second_Player_Name);
		Second_Player_Name.setColumns(10);
		
		First_Player_Name = new JTextField();
		First_Player_Name.setBounds(33, 112, 86, 20);
		contentPane.add(First_Player_Name);
		First_Player_Name.setColumns(10);
		frame.setContentPane(contentPane);
		
		JLabel lblNewLabel = new JLabel("Second Player Name:");
		lblNewLabel.setBounds(209, 87, 154, 14);
		contentPane.add(lblNewLabel);
		
		JLabel Instructions = new JLabel("Please enter First and Second player names");
		Instructions.setBounds(61, 11, 302, 53);
		contentPane.add(Instructions);
		
		JLabel lblNewLabel_1 = new JLabel("and select dealer designation.");
		lblNewLabel_1.setBounds(62, 48, 262, 14);
		contentPane.add(lblNewLabel_1);

		btnBeginGame.addActionListener(this);
	}
	
	
	@Override
	public void actionPerformed(ActionEvent event) {
			frame.dispose();
	        Timer timer = new Timer();
			timer.schedule(game = new GameTable(First_Player_Name.getText(), Second_Player_Name.getText(), dealer), 0, 1000);
	}
	
}//end Introduction class

