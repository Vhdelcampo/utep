package student_test;
import junit.framework.*;
import junit.textui.TestRunner;
import java.util.*;

public class IMathTest extends TestCase { 
	/** Run the tests. */
	public static void main(String[] args) {
		TestRunner.run(suite());
		// junit.swingui.TestRunner.run(suite());
	}

	/** Returns the test suite for this test class. */
	public static Test suite() {
		return new TestSuite(IMathTest.class);
	}

	public void testMin() { 
		assertEquals(1, min(1,2));
		assertEquals(1, min(1,2));
		assertEquals(0, min(0,0));
		assertEquals(-5, min(-5,10));
	} 

	public static int min(int x, int y) {
		return x <= y ? x : y;
	}

} 



