import java.util.*;

public class Blackjack {
	static Scanner sc = new Scanner(System.in);
	static Deck d = new Deck();
	static Hand pHand = new Hand();
	static Hand dHand = new Hand();

	public static void main(String[] args) {
		System.out.print("Enter your name: ");
		String name = sc.nextLine();
		System.out.print("Hello " + name + ", welcome to a thrilling game of blackjack");
		int games = 0;
		int pPoints = 0;
		int dPoints = 0;

		while (true){
			games++;
			d.reset();
			pHand.reset();
			dHand.reset();
			System.out.println("\nGame " + games + " will now begin");
			System.out.print("Do you want to be the dealer? (y/n): ");
			String isDealer = sc.nextLine();

			System.out.println("Player's turn starts now");
			pPoints = playerRun();
			if (pHand.isBlackjack()) {System.out.println("Player has a Blackjack");}
			if (pHand.isBust()) {
				System.out.println("Player points are: " + pPoints);
				System.out.println("Player busted");
				System.out.println("Dealer wins");
			}else{
				System.out.println("Dealer's turn starts now");
				pressEnterToContinue();
				dPoints = dealerRun(pPoints);
				if (dHand.isBlackjack()) {System.out.println("Dealer has a Blackjac");}
				System.out.println("Player points are: " + pPoints);
				System.out.println("Dealer points are: " + dPoints);
				if (pPoints > dPoints || dHand.isBust()){
					if (dHand.isBust()) {System.out.println("Dealer busted");}
					System.out.println("Player wins");
				}else if (dPoints > pPoints){
					System.out.println("Dealer wins");
				}else{
					System.out.println("Tie");
				}


				System.out.print("Do you want to play again? (y/n) ");
				String ans = sc.nextLine();
				if (ans.equals("n") || ans.equals("N")) {
					break;
				}
			}
		}
		sc.close();
	}

	public static int playerRun() {
		boolean stay = false;
		pHand.hit(d.draw());
		pHand.hit(d.draw());
		System.out.println("\nPlayer's starting hand is: ");
		pHand.displayHand();

		while (!stay && !pHand.isBust() && !pHand.isBlackjack() && pHand.getPoints() != 21) {
			System.out.print("Hit or stay? (h/s): ");
			String move = sc.nextLine();
			if (move.equals("hit") || move.equals("h")) {
				pHand.hit(d.draw());
			}else if(move.equals("stay") || move.equals("s")) {
				stay = true;
			}else{
				System.out.println("Invalid input");
			}
			System.out.println("\nPlayer hand:");
			pHand.displayHand();
		}
		return pHand.getPoints();
	}

	public static int dealerRun(int pPoints){
		dHand.hit(d.draw());
		dHand.hit(d.draw());
		System.out.println("Dealer's starting hand is: ");
		dHand.displayHand();
		int dPoints = dHand.getPoints();

		while (!dHand.isBust() && dPoints < pPoints && !dHand.isBlackjack()) {
			System.out.println("Dealer will now hit");
			pressEnterToContinue();
			dHand.hit(d.draw());
			System.out.println("Dealer hand after hitting: ");
			dHand.displayHand();
			dPoints = dHand.getPoints();
		}
		return dPoints;
	}

	private static void pressEnterToContinue() { 
		System.out.println("Press enter to continue...");
		Scanner  s = new Scanner(System.in);
		String temp = s.nextLine();
	}

}
