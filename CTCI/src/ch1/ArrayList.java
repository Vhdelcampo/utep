package ch1;

public class ArrayList {
	private final int DEFAULT_CAPACITY = 16;
	private String[] array;
	private int size = 0;

	public ArrayList() {
		array = new String[DEFAULT_CAPACITY];
	}

	public ArrayList(int size) {
		array = new String[DEFAULT_CAPACITY];
		this.size = size;
	}
	
	public int getSize() {
		return size;
	}

	public void insert(String item) {
		if (size == array.length) {grow();}
		array[size] = item;
		size++;
	}
	
	public String extract() {
		if (size == 0) {return "";}
		String s = array[size-1];
		size--;
		return s;
	}
	
	public String get(int i) {
		return array[i];
	}

	private void grow(){
		String[] new_array = new String[array.length*2];
		for (int i = 0; i < size; i++) {
			new_array[i] = array[i];
		}
	}
	
}
