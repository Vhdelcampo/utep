package ch1;

import java.util.Arrays;

/**
 * Given two strings, write a method to decide if one is a permutation of the other.
 * @author victor
 *
 */

public class CheckPermutation_2 {
	
	
	public static void main(String args[]) {
		String a = "memes";
		String b = "smmee";
		System.out.println(checkPermutation(a, b));
	}
	public static boolean checkPermutation(String a, String b) {
		if (a.length() != b.length()) {
			return false;
		}
		
		int[] charset_count_a = new int[126];
		int[] charset_count_b = new int[126];
		 
		for (int i = 0; i < a.length(); i++) {
			int val = a.charAt(i);
			charset_count_a[val]++;
		}
		for (int i = 0; i < b.length(); i++) {
			int val = b.charAt(i);
			charset_count_b[val]++;
		}
		
		for (int i = 0; i < charset_count_a.length; i++) {
			if (charset_count_a[i] != charset_count_b[i]) {
				return false;
			}
		}
		
		System.out.println(Arrays.toString(charset_count_a));
		System.out.println(Arrays.toString(charset_count_b));
		
		return true;
	}
}
