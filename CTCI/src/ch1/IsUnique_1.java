package ch1;

/**
 * 
 * Implement an algorithm to determine if a string has all unique characters. 
 * What if you cannot use additional data structures?
 * @author victor
 *
 */

public class IsUnique_1 {
	
	public static void main(String args[]) {
		System.out.println(isUnique("the lazydogjumpse"));
	}
	
	public static boolean isUnique(String s) {
		boolean[] alphabet = new boolean[126];
		int value = 0;
		
		for (int i = 0; i < s.length(); i++) {
			value = s.charAt(i);
			if(alphabet[value] == true) {
				return false;
			}
			alphabet[value] = true;
		}
		return true;
	}
	
	public static boolean isUnique2(String s) {
		char a, b;
		
		for (int i = 0; i < s.length()-1; i++) {
			a = s.charAt(i);
			for (int j = i+1; j < s.length(); j++) {
				b = s.charAt(j);
				if (a == b) {
					return false;
				}
			}
		}
		return true;
	}
	
	
}
