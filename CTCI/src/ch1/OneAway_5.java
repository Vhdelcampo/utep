package ch1;

public class OneAway_5 {
	
	public static boolean oneAway(String a, String b) {
		int[] charset_a = new int[126];
		for (int i = 0; i < a.length(); i++) {
			charset_a[a.charAt(i)]++;
		}
		
		int[] charset_b = new int[126];
		for (int i = 0; i < b.length(); i++) {
			charset_b[b.charAt(i)]++;
		}
		
		boolean difference = false;
		for (int i = 0; i < charset_a.length; i++) {
			if (charset_a[i] != charset_b[i]) {
				if (difference) {return false;}
				difference = true;
			}
		}
		
		return false;
	}
}
