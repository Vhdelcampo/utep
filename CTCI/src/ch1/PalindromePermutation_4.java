package ch1;

import java.util.Arrays;

/*
 * 1.4 Palindrome Permutation: Given a string, write a function to check if it is a permutation of a 
 * palindrome. A palindrome is a word or phrase that is the same forwards and backwards. A permutation
 * is a rearrangement of letters. The palindrome does not need to be limited to just dictionary words.
 * EXAMPLE
 * Input: Tact Coa
 * Output: True (permutations: "taco cat", "atco eta", etc.)
 */

public class PalindromePermutation_4 {
	
	public static void main(String args[]) {
		System.out.println(isPalindromePermutation("tactcoa"));
	}
	
	public static boolean isPalindromePermutation(String s) {
		int[] charset = new int[126];
		for (int i = 0; i < s.length(); i++) {
			charset[s.charAt(i)]++;
		}
		boolean hasOdd = false;
		
		System.out.println(Arrays.toString(charset));
		for (int i = 0; i < charset.length; i++) {
			if (charset[i] > 0 ) {
				System.out.println("index: " + i + " count: " + charset[i]);
			}
			if(charset[i] % 2 == 1) {
				System.out.println("found odd hasodd: " + hasOdd);
				if (hasOdd) {return false;}
				hasOdd = true;
			}
		}
		return true;
	}
}
