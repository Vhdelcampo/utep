package ch1;

/* 1.7 Rotate Matrix: Given an image represented by an NxN matrix, where each pixel in the image is 4
 * bytes, write a method to rotate the image by 90 degrees. Can you do this in place?
 */
public class RotateMatrix_7 {
	public static void rotate90(byte[][] img) {
		for (int i = 0; i < img.length; i++) {
			for (int j = 0; j < img[i].length; j++) {
				byte pixel_copy = img[i][j];
				img[i][j] = img[i][img[i].length-j];
				img[img.length-i][j] = pixel_copy;
				pixel_copy = img[i][img[i].length-j];
				img[i][img[i].length-j] = img[img.length-i][img[i].length-j];
				img[img.length-i][img[i].length-j] = pixel_copy;
				
			}
			
		}
	}
}
