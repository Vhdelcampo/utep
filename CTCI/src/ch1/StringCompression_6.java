package ch1;

public class StringCompression_6 {
	
	public static void main(String args[]) {
		System.out.println(stringCompression("mmmmmmeeeeeeee"));
	}
	public static String stringCompression(String s) {
		char previousChar = s.charAt(0);
		StringBuilder sb = new StringBuilder();
		int count = 1;
		
		for (int i = 1; i < s.length(); i++) {
			char currentChar = s.charAt(i);
			boolean atEnd = i+1>=s.length();
			if (previousChar != currentChar || atEnd) {
				if (atEnd) {count++;}
				sb.append(previousChar);
				sb.append(count);
				count = 0;
			}
			count++;
			previousChar = currentChar;
		}
		String compressed_string = sb.toString();
		if (s.length() < compressed_string.length()) {
			return s;
		}
		return compressed_string;
	}
}
