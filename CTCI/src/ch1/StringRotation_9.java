package ch1;
/*
 * 1.9 String Rotation: Assume you have a method isSubst ring which checks if one word is a substring
 * of another. Given two strings, si and s2, write code to check if s 2 is a rotation of si using only one
 * call to isSubstring (e.g., "water bottle" is a rotation of "erbottlewat").
 */
public class StringRotation_9 {
	
	
	public boolean isStringRotation(String s1, String s2) {
		if (s1.length() != s2.length()) {
			return false;
		}
		char firstChar = s1.charAt(0);
		int[] firstCharLocation = new int[s2.length()];
		for (int i = 0; i < firstCharLocation.length; i++) {
			if (s2.charAt(i) == firstChar) {
				firstCharLocation[i] = i;
			}
		}
		
		return true;
	}
}
