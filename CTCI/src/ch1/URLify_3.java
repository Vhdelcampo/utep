package ch1;

/*
 * 1.3 URLify: Write a method to replace all spaces in a string with '%20' You may assume that the string
 * has sufficient space at the end to hold the additional characters, and that you are given the "true"
 * length of the string. (Note: If implementing in Java, please use a character array so that you can
 * perform this operation in place.)
 */

public class URLify_3 {

	public static void main(String args[]) {
		char[] a = {'m', 'e', 'm', 'e', ' ', 'r', 'e', 'v', ' ', 'k', '-', '-', '-', '-'};
		URLify2(a);
		System.out.println(a);
	}

	public static void URLify(char[] s) {
		int index = 0;
		char[] currentWord = new char[s.length];
		ArrayList words = new ArrayList();
		for (int i = 0; i < s.length; i++) {
			if (s[i] == ' ') {
				words.insert(new String(currentWord));
				currentWord = new char[s.length];
				index = 0;
			} else if (i == s.length-1) {
				currentWord[index] = s[i];
				words.insert(new String(currentWord));
			}
			else {
				currentWord[index] = s[i];
				index++;
			}
		}

		for (int i = 0; i < words.getSize(); i++) {

		}

	}

	public static void URLify2(char[] s) {
		char[] s_copy = s.clone();
		for (int i = 0; i < s.length; i++) {
			System.out.println(s[i]);
			if (s[i] == ' ') {
				System.out.println(s[i]);
				replaceSpace(s, s_copy, i);
			}
		}
	}
	
	public static char[] replaceSpace(char[] s, char[] s_copy, int i) {
		s[i] = '%';
		s[i+1] = '2';
		s[i+2] = '0';
		for (int j = i+3; j < s_copy.length; j++) {
			s[j] = s_copy[j-2];
		}
		return s;
	}
	//  0123456789
	// "meme review"
	// "meme%20review"
}
