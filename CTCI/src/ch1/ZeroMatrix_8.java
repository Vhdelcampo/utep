package ch1;

import java.util.Arrays;
import java.util.Iterator;

/*
 * 1.8 Zero Matrix: Write an algorithm such that if an element in an MxN matrix is 0, its entire row and
 * column are set to 0.
 */
public class ZeroMatrix_8 {

	public static void main(String args[]) {
		int[][] meme = {{1,2,3,4}, {1,0,3,4}, {1,2,0,4}};
		zeroMatrix2(meme);
		printArray(meme);
	}

	public static void printArray(int[][] a) {
		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j < a[i].length; j++) {
				System.out.print(a[i][j] + " ");
			}
			System.out.println();
		}
	}
	public static void zeroMatrix(int [][] arr) {
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {
				System.out.println("hi");
				if (arr[i][j] == 0) {
					for (int row = 0; row < arr.length; row++) {
						arr[row][j] = 0;
					}
					for (int col = 0; col < arr[i].length; col++) {
						arr[i][col] = 0;
					}
				}
			}
		}
	}

	public static void zeroMatrix2(int [][] arr) {
		boolean[][] zero = new boolean[arr.length][arr[0].length];
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {
				if (arr[i][j] == 0) {
					zero[i][j] = true;
				}
			}	
		}
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {
				if (zero[i][j]) {
					for (int x = 0; x < arr.length; x++) {
						arr[x][j] = 0;
					}
					for (int y = 0; y < arr[i].length; y++) {
						arr[i][y] = 0;
					}
				}
			}
		}
	}
}
