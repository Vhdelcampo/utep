package ch2;

/*
 * 2.3 Delete Middle Node: Implement an algorithm to delete a node in the middle (i.e., any node but
the first and last node, not necessarily the exact middle) of a singly linked list, given only access to
that node.
EXAMPLE
Input: the node c from the linked list a - >b - >c - >d - >e- >f
Result: nothing is returned, but the new linked list looks like a- >b- >d- >e->f
 */

public class DeleteMiddleNode_3 {
	
	public static void main(String args[]) {
		Node head = new Node(1);
		Node n2 = new Node(2);
		Node n3 = new Node(3);
		Node n4 = new Node(4);
		Node n5 = new Node(5);
		head.next = n2;
		n2.next = n3;
		n3.next = n4;
		n4.next = n5;
		deleteMiddleNode(head);
		LinkedList.printList(head);
	}
	
	public static void deleteMiddleNode(Node head) {
		Node slow_curr = head;
		Node fast_curr = head;
		Node slow_prev = null;
		boolean on = false;
		for (; fast_curr!=null; fast_curr=fast_curr.next) {
			if (on) {
				slow_prev = slow_curr;
				slow_curr = slow_curr.next;
				on = false;
			} else {
				on = true;
			}
		}
		slow_prev.next = slow_curr.next;
	}
}
