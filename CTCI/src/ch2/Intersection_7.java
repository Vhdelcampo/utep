package ch2;
/*
 * 2.7 Intersection: Given two (singly) linked lists, determine if the two lists intersect. Return the 
intersecting node. Note that the intersection is defined based on reference, not value. That is, if the kth
node of the first linked list is the exact same node (by reference) as the jth node of the second
linked list, then they are intersecting.
 */

public class Intersection_7 {
	
	public static void main(String args[]) {
		Node a1 = new Node(6);	
		Node a2 = new Node(1);
		Node a3 = new Node(7);

		Node b1 = new Node(2);
		Node b2 = new Node(9);
		Node b3 = new Node(5);

		LinkedList a = new LinkedList(a1);
		LinkedList b = new LinkedList(b1);
		a.insertAtEnd(a2);
		a.insertAtEnd(a3);
		b.insertAtEnd(b2);
		b.insertAtEnd(a1);
		a.printList();
		b.printList();
		
		System.out.println(doIntersect(a, b));
	}
	public static boolean doIntersect(LinkedList a, LinkedList b) {
		int a_size = 0; int b_size = 0;
		Node curr_a; Node curr_b;
		Node tail_a = null; Node tail_b = null;
		
		for(curr_a = a.head; curr_a != null; curr_a=curr_a.next) {
			a_size++;
			if (curr_a.next == null) {tail_a = curr_a;}
		}
		for(curr_b = b.head; curr_b != null; curr_b=curr_b.next) {
			b_size++;
			if (curr_b.next == null) {tail_b = curr_b;}
		} 
		
		System.out.println("tail a: " + tail_a);
		System.out.println("tail b: " + tail_b);
		if (tail_a != tail_b) {
			return false;
		}
		
		if (a_size > b_size) {
			trimFromStart(a, a_size - b_size);
		} else if (b_size > a_size) {
			trimFromStart(b, b_size - a_size);
		}
		
		curr_a = a.head; curr_b = b.head;
		while (curr_a != curr_b) {
			curr_a = curr_a.next; 
			curr_b = curr_b.next;
		}
		
		return true;
	}
	
	public static void trimFromStart(LinkedList l, int n) {
		for (int i = 0; i < n; i++) {
			l.head = l.head.next;
		}
	}
	
	public static void advanceForward(LinkedList l, int n) {
		int i = 0; 
		Node curr;
		for (curr = l.head; i<n; i++) {
			curr = curr.next;
		}
		l.head = curr;
	}
}
