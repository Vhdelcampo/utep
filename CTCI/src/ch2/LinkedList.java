package ch2;

import java.util.HashSet;

public class LinkedList {
	public Node head; // First node of list
	public Node tail; // Last node of list

	LinkedList(){ // default constructor
		this.head = null;
		this.tail = null;
	}

	LinkedList(Node node){ // Constructor if given only node.
		this.head = node;
		this.tail = head;
	}

	LinkedList(int item){ // Constructor if given only item of node.
		this.head = new Node(item);
		this.tail = head;
	}

	LinkedList(int item, Node next){
		this.head = new Node(item, next);
		this.tail = head;
	}

	public void insertAtEnd(int item) {
		Node n = new Node(item);
		if (tail == null) {
			head = n;
			tail = n;
		} else {
			tail.next = n;
			tail = n;
		}
	}
	
	public void insertAtEnd(Node n) {
		if (tail == null) {
			head = n;
			tail = n;
		} else {
			tail.next = n;
			tail = n;
		}
	}
	

	public void insertAtBeginning(int item) {
		Node n = new Node(item, head);
		head = n;
		if(tail == null) {
			tail = n;
		}
	}
	
	public void insertAtBeginning(Node n) {
		n.next = head;
		head = n;
		if(tail == null) {
			tail = n;
		}
	}

	public static Node removeDups(Node head) {
		HashSet<Integer> ht = new HashSet<Integer>();
		Node previous = null;
		for (Node curr = head; curr != null; curr=curr.next) {
			if (ht.contains(curr.item)) {
				previous.next = curr.next;
			} else {
				ht.add(curr.item);
				previous = curr;
			}
		}
		return null;
	}

	public static void printList(Node head) {
		for (Node curr = head; curr!=null; curr=curr.next) {
			System.out.print(curr.item + "->");
		}
		System.out.println();
	}

	public void printList() {
		for (Node curr = head; curr!=null; curr=curr.next) {
			System.out.print(curr.item + "->");
		}
		System.out.println();
	}
}
