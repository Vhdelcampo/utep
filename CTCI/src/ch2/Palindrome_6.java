package ch2;

public class Palindrome_6 {
	
	public static void main(String args[]) {
		Node head = new Node(1);
		Node n2 = new Node(2);
		Node n3 = new Node(3);
		Node n4 = new Node(4);
		Node n5 = new Node(3);
		Node n6 = new Node(2);
		Node n7 = new Node(1);
		head.next = n2;
		n2.next = n3;
		n3.next = n4;
		n4.next = n5;
		n5.next = n6;
		n6.next = n7;
		LinkedList l = new LinkedList(head);
		LinkedList r = createReversed(l);
		l.printList();
		r.printList();
		System.out.println(isPalindrome(l));
	}
	
	public static boolean isPalindrome(LinkedList l) {
		LinkedList r = createReversed(l);
		Node r_curr = r.head;
		for (Node curr = l.head; curr != null; curr = curr.next) {
			if (curr.item != r_curr.item) {
				return false;
			}
			r_curr = r_curr.next;
		}
		return true;
	}
	
	public static LinkedList createReversed(LinkedList l) {
		Node prev = null; Node curr; Node curr_clone = null;
		for (curr = l.head; curr != null; curr = curr.next) {
			curr_clone = new Node(curr.item, prev);
			prev = curr_clone;
		}
		return new LinkedList(curr_clone);
	}
}
