package ch2;
/*
 * Partition: Write code to partition a linked list a round a value x, such that all nodes less than x come
before all nodes greater than or equal to x. If x is contained within the list, the values of x only need
to be after the elements less than x (see below). The partition element x can appear anywhere in the
"right partition"; it does not need to appear between the left and right partitions.
EXAMPLE
Input: 3 -> 5 -> 8 -> 5 -> 10 -> 2 -> 1 [partition = 5]
Output: 3 -> 1 -> 2 -> 10 -> 5 -> 5 -> 8
 */

public class Partition_4 {
	public static void main(String args[]) {
		Node head = new Node(3);
		Node n2 = new Node(5);
		Node n3 = new Node(8);
		Node n4 = new Node(5);
		Node n5 = new Node(10);
		Node n6 = new Node(2);
		Node n7 = new Node(1);
		head.next = n2;
		n2.next = n3;
		n3.next = n4;
		n4.next = n5;
		n5.next = n6;
		n6.next = n7;
		LinkedList ll = partition(head, 5);
		ll.printList();
	}
	
	public static LinkedList partition(Node head, int x) {
		LinkedList ll = new LinkedList();
		
		for (Node curr = head; curr != null; curr = curr.next) {
			if (curr.item < x) {
				ll.insertAtBeginning(curr.item);
			} else {
				ll.insertAtEnd(curr.item);
			}
		}
		return ll;
	}
}
