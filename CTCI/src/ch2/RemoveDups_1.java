package ch2;

import java.util.HashSet;
import java.util.Hashtable;

/*
 * 2.1 Remove Dupsr Write code to remove duplicates from an unsorted linked list.
 * FOLLOW UP
 * How would you solve this problem if a temporary buffer is not allowed?
 */

public class RemoveDups_1 {
	
	public static void main(String args[]) {
		Node n1 = new Node(1);
		Node n2 = new Node(3);
		Node n3 = new Node(3);
		Node n4 = new Node(5);
		Node n5 = new Node(5);
		n1.next = n2;
		n2.next = n3;
		n3.next = n4;
		n4.next = n5;
		removeDups(n1);
		printList(n1);
	}
	
	public static Node removeDups(Node head) {
		HashSet<Integer> ht = new HashSet<Integer>();
		Node previous = null;
		for (Node curr = head; curr != null; curr=curr.next) {
			if (ht.contains(curr.item)) {
				previous.next = curr.next;
			} else {
				ht.add(curr.item);
				previous = curr;
			}
		}
		return null;
	}
	
	public static Node deleteNode(Node head, int d) {
		Node n = head;

		if (n.item == d) {return head.next;}
		while (n.next != null) {
			if (n.next.item == d) {
				n.next = n.next.next;
				return head;
			}
			n = n.next;
		}
		return head;
	}
	
	public static void printList(Node head) {
		for (Node curr = head; curr!=null; curr=curr.next) {
			System.out.print(curr.item + "->");
		}
	}
}
