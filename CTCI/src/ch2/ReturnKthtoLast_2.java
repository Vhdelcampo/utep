package ch2;

/*
 * 2.2 Return Kthto Last: Implement an algorithm to find the kth to last element of a singly linked list.
 */
public class ReturnKthtoLast_2 {

	public static void main(String args[]) {
		Node head = new Node(1);
		Node n2 = new Node(2);
		Node n3 = new Node(3);
		Node n4 = new Node(4);
		Node n5 = new Node(5);
		head.next = n2;
		n2.next = n3;
		n3.next = n4;
		n4.next = n5;
		int item = returnKthToLast(head, 3);
		System.out.println(item);
	}

	public static int returnKthToLast(Node head, int k) {
		int size = 0;
		for (Node curr = head; curr!=null; curr=curr.next) {
			size++;
		}
		int index=0;
		System.out.println("size " + size);
		int position = size-k+1;
		for (Node curr = head; curr!=null; curr=curr.next) {
			index++;
			if (index == position) {
				return curr.item;
			}
		}
		return -1;
	}
}
