package ch2;

public class SumLists_5 {

	public static void main(String args[]) {
		Node a1 = new Node(6);	
		Node a2 = new Node(1);
		Node a3 = new Node(7);
		a1.next = a2;
		a2.next = a3;

		Node b1 = new Node(2);
		Node b2 = new Node(9);
		Node b3 = new Node(5);
		b1.next = b2;
		b2.next = b3;

		LinkedList a = new LinkedList(a1);
		LinkedList b = new LinkedList(b1);
		LinkedList c = sumLists2(a, b);
		c.printList();
	}
	public static LinkedList sumLists(LinkedList a, LinkedList b) {
		LinkedList result = new LinkedList();
		int sum = 0; int digit = 0; int rem = 0;
		Node curr_a = a.head; Node curr_b = b.head;

		while (curr_a != null && curr_b != null) {
			sum = curr_a.item + curr_b.item;
			sum += rem;
			digit = sum % 10;
			rem = sum / 10;
			result.insertAtEnd(digit);
			curr_a = curr_a.next; curr_b = curr_b.next;
		}
		return result;
	}

	public static LinkedList sumLists2(LinkedList a, LinkedList b) {
		LinkedList result = new LinkedList();
		int sum = 0; int digit = 0; int past_sum = 0;
		Node curr_a = a.head; Node curr_b = b.head;
		int rem = 0;
		while (curr_a != null && curr_b != null) {
			sum = (past_sum * 10) + curr_a.item + curr_b.item;
			if (sum >= 10) {
				sum = sum / 10;
				while (sum > 0) {
					System.out.println(sum);
					result.insertAtEnd(sum);
					sum = sum / 10;
				}
				past_sum = sum % 10;
			} else {
				past_sum = sum;
			}
			curr_a = curr_a.next; curr_b = curr_b.next;
		}
		return result;
	}
}


