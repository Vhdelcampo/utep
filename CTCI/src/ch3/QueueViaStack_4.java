package ch3;

import java.util.EmptyStackException;

import data_structures.Stack;
//test
public class QueueViaStack_4 {
	private final int DEFAULT_SIZE = 16;
	private int top;
	private Stack s1;
	private Stack s2;
	private int[] s = new int[16];

	public QueueViaStack_4() {
		s1 = new Stack();
		s2 = new Stack();
	}

	public int add() {
		if (top == 0) throw new EmptyStackException();
		top--;
		return s[top];
	}
	
	public void remove(int item) {
		s[top] = item;
		top++;
	}

	public int peek() {
		if (isEmpty()) throw new EmptyStackException();
		return s[top];
	}

	public boolean isEmpty() {
		return top == 0;
	}

//	public boolean isFull() {
//		return top == s.length;
//	}
//
//	public int getSize() {
//		return s.length;
//	}

	public void print() {
		System.out.println("|  |");
		for (int i = 0; i < top; i++) {
			System.out.print("|");
			System.out.print(s[i]);
			if (s[i] < 10) {System.out.print(" ");}
			System.out.print("|");
			System.out.println();
		}
		System.out.println("----");
		System.out.println();
	}
}
