package ch3;

/*
 * 3.2 Stack Min: How would you design a stack which, in addition to push and pop, has a function min
which returns the minimum element? Push, pop and min should all operate in 0(1) time.
 */

import java.util.EmptyStackException;

import data_structures.MyLinkedList;

public class StackMin_2 {
	private static class StackNode<T> {
		private int data;
		private StackNode next;

		public StackNode(int data) {
			this.data = data;
		}
	}

	private StackNode top;
	private int min = Integer.MAX_VALUE;
	MyLinkedList mins = new MyLinkedList();

	public int pop() {
		if (top == null) throw new EmptyStackException();
		int item = top.data;
		top = top.next;
		return item;
	}

	public void push(int item) {
		StackNode n = new StackNode(item);
		n.next = top;
		top = n;
		if (item < min) {
			mins.insertAtEnd(item);
		}
	}

	public int peek() {
		if (top == null) throw new EmptyStackException();
		return top.data;
	}

	public boolean isEmpty() {
		return top == null;
	}
	
	public int min() {
		
		if (min == Integer.MAX_VALUE) {
			return -1;
		}
		return min;
	}
}
