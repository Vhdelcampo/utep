package ch3;
/*
 * Stack of Plates: Imagine a (literal) stack of plates. If the stack gets too high, it might topple.
Therefore, in real life, we would likely start a new stack when the previous stack exceeds some
threshold Implement a data structure SetOfStacks that mimics this, setofstacks should be
composed of several stacks and should create a new stack once the previous one exceeds capacity.
SetOf Stacks, push () and SetOf Stacks .pop() should behave identically to a single stack
(that is, pop ( ) should return the same values as it would if there were just a single stack).
FOLLOW UP
Implement a function popAt(int index) which performs a pop operation on a specific sub-stack.
Hints: #64, #81
 */

import java.util.EmptyStackException;
import java.util.*;
import data_structures.*;

public class StackOfPlates_3 {
	
	public static void main(String args[]) {
		StackOfPlates_3 sp = new StackOfPlates_3();
		sp.push(1);
		sp.push(2);
		sp.push(3);
		sp.push(4);
		sp.push(5);
		sp.push(6);
		sp.push(7);
		sp.push(8);
		sp.push(9);
		sp.push(10);
		sp.push(11);
		sp.push(12);
		sp.push(13);
		sp.push(14);
		sp.push(15);
		sp.push(16);
		sp.push(17);
		sp.push(18);
		sp.print();
	}

	private ArrayList<StackArray> stackSet = new ArrayList<StackArray>();
	private int stackIndex = 0;
	StackArray currentStack;
	private ArrayList<Integer>[] top;
	
	public StackOfPlates_3() {
		StackArray s = new StackArray();
		stackSet.add(s);
	}
	
	public StackOfPlates_3(int size) {
		StackArray s = new StackArray(size);
		stackSet.add(s);
	}
	
	public void push(int item) {
		currentStack = stackSet.get(stackIndex);
		if (currentStack.isFull()) {
			stackSet.add(new StackArray());
			stackIndex++;
			currentStack = stackSet.get(stackIndex);
		}
		currentStack.push(item);
	}
	
	public int pop() {
		currentStack = stackSet.get(stackIndex);
		if (currentStack.getSize() == 1 && stackIndex > 0) {
			stackIndex--;
		}
		return currentStack.pop();
	}
	
	public int popAt(int index) {
		currentStack = stackSet.get(index);
		return currentStack.pop();
	}

	public int peek() {
		return stackSet.get(stackIndex).peek();
	}

	public boolean isEmpty() {
		return stackSet.get(stackIndex).isEmpty();
	}
	
	public void print() {
		for (int i = 0; i < stackIndex; i++) {
			stackSet.get(i).print();
		}
	}
}


