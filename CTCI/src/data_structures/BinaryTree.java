package data_structures;

import sun.util.resources.cldr.ext.CurrencyNames_tr;

public class BinaryTree {
	TreeNode root;

	public BinaryTree(TreeNode n) {
		root = n;
	}

	public void add(int value) {
		root = addRecursive(root, value);	
	}

	public TreeNode addRecursive(TreeNode current, int value) {
		if (current == null) {
			return new TreeNode(value);
		}

		if (value < current.value) {
			current.left = addRecursive(current.left, value);
		} else if (value > current.value) {
			current.right = addRecursive(current.right, value);
		}

		return current;
	}

	private TreeNode deleteRecursive(TreeNode current, int value) {
		if (current == null) {return null;}

		if (value == current.value) {
			if (current.left == null) {
				return current.right;
			} else if (current.right == null) {
				return current.left;
			} else {
				current.value = retrieveData(current.left);
				current.left = deleteRecursive(current.left, current.value);
			}
		} else if (value < current.value) {
			current.left = deleteRecursive(current.left, value);
			return current;
		} else if (value > current.value) {
			current.right = deleteRecursive(current.right, value);
		}
		return current;
	}

	private int retrieveData(TreeNode p)
	{
		while (p.right != null) p = p.right;
		
		return p.value;
	}

	public void inOrderTraversal(TreeNode node) {
		if (node != null) {
			inOrderTraversal(node.left);
			System.out.println(node.value);
			inOrderTraversal(node.right);
		}
	}
	
	public void preOrderTraversal(TreeNode node) {
		if (node != null) {
			System.out.println(node.value);
			preOrderTraversal(node.left);
			preOrderTraversal(node.right);
		}
	}
	
	public void postOrderTraversal(TreeNode node) {
		if (node != null) {
			postOrderTraversal(node.left);
			postOrderTraversal(node.right);
			System.out.println(node.value);
		}
	}
}
