package data_structures;

public class Graph {
	public GraphNode[] nodes;
	
	private class GraphNode {
		public String name;
		public GraphNode children;
	}
}
