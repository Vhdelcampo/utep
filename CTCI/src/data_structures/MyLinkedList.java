package data_structures;

public class MyLinkedList {
	public Node head; // First node of list
	public Node tail; // Last node of list
	
	public MyLinkedList(){ // default constructor
		this.head = null;
		this.tail = head;
	}
	
	MyLinkedList(Node node){ // Constructor if given only node.
		this.head = node;
		this.tail = head;
	}
	
	MyLinkedList(int item){ // Constructor if given only item of node.
		this.head = new Node(item);
		this.tail = head;
	}
	
	MyLinkedList(int item, Node next){
		this.head = new Node(item, next);
		this.tail = head;
	}
	
	public void insertAtEnd(int item) {
		tail.next = new Node(item);
	}
}
