package data_structures;

public class Node {
	public Node next;
	public int item;

	Node(){ // Default constructor is an empty node or node with null values
	}

	Node(int item){ // If only content is given Node will be constructed with a null next
		this.item = item;
		this.next = null;
	}

	Node(int item, Node next){
		this.item = item;
		this.next = next;
	}

	void appendToTail(int d) {
		Node end = new Node(d);
		Node n = this;
		while (n.next != null) {
			n = n.next;
		}
		n.next = end;
	}

	Node deleteNode(Node head, int d) {
		Node n = head;

		if (n.item == d) {
			return head.next;
		}

		while (n.next != null) {
			if (n.next.item == d) {
				n.next = n.next.next;
				return head;
			}
			n = n.next;
		}
		return head;
	}

	public void printNode(){
		System.out.print(item + " ");
	}

}
