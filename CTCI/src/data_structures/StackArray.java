package data_structures;

import java.util.EmptyStackException;
import java.util.ArrayList;

public class StackArray {
	
	private final int DEFAULT_SIZE = 16;
	private int top;
	private int[] s;	
	
	public StackArray() {
		s = new int[DEFAULT_SIZE];
	}
	
	public StackArray(int size) {
		s = new int[size];
	}

	public int pop() {
		if (top == 0) throw new EmptyStackException();
		top--;
		return s[top];
	}

	public void push(int item) {
		s[top] = item;
		top++;
	}
	
	public void expand() {
		
	}

	public int peek() {
		if (isEmpty()) throw new EmptyStackException();
		return s[top];
	}

	public boolean isEmpty() {
		return top == 0;
	}
	
	public boolean isFull() {
		return top == s.length;
	}
	
	public int getSize() {
		return s.length;
	}
	
	public void print() {
		System.out.println("|  |");
		for (int i = 0; i < top; i++) {
			System.out.print("|");
			System.out.print(s[i]);
			if (s[i] < 10) {System.out.print(" ");}
			System.out.print("|");
			System.out.println();
		}
		System.out.println("----");
		System.out.println();
	}
}
