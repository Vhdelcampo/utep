package data_structures;

public class StringBuild {

	public static void main(String args[]) {
		StringBuild sb = new StringBuild();
		sb.append("meme");
		sb.append("review");
		sb.append("melon");
		System.out.println(sb.toString());
	}

	MyArrayList sb = new MyArrayList();
	
	public StringBuild() {
		sb = new MyArrayList();
	}

	public StringBuild(String s) {
		sb = new MyArrayList(s.length());
	}

	public void append(String s) {
		sb.insert(s);
	}

	public String toString() {
		int catArraySize = 0;
		int size = sb.getSize();
		int index = 0;
		
		for (int i = 0; i < size; i++) {
			catArraySize += sb.get(i).length();
		}
		char[] catString = new char[catArraySize];

		for (int i = 0; i < size; i++) {
			String s = sb.get(i);
			for (int j = 0; j < s.length(); j++) {
				catString[index] = s.charAt(j);
				index++;
			}
		}
		
		return new String(catString);
	}
}
