function f = bisection(x)

a = 0;
b = 1;
fa = f(a);
fb = f(b);
steps = 0;
eps = 10^-4
while abd(a - c) <= eps
  steps = steps+1;
  c = (a+b)/2;
  fc = f(c);    
  if fc == 0
    break
  end
  if sign(fb)*sign(fc) < 0
    a = c;     
    fa = fc;
  else        
    b = c;     
    fb = fc;
  end
end
xc = (a+b)/2;  