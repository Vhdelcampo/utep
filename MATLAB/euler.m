function derivative = euler(f,x0,y0,a,b,h)
n = (b-a) / h;
for k=1:n
  x1 = x0 + h;
  y1 = y0 + h * f(x0,y0)
  x0 = x1;
  y0 = y1;
  end
end