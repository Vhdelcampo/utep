function integral = gauss3point(f,a,b,n)
h = (b-a) / n;
w1 = 5. / 18.;
w2 = 8. / 18.;
w3 = 5. / 18.;
r1 = (1 - sqrt(.6) ) / 2.;
r2 = 1. / 2.;
r3 = (1 + sqrt(.6) ) / 2.;
sum = 0;
for i = 1:n
    xi = a + (i-1) * h;
    sum = sum + h*w1*f(xi + r1 * h) + h*w2*f(xi + r2 * h) + h*w3*f(xi + r3 * h);
end
integral = sum;