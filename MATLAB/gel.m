function x = gel(A,b,n)
for k=1:n-1
% find largest potential pivot in column k
   ibig = k;
   big = abs(A(k,k)); 
   for i=k:n
      if abs(A(i,k)) > big
         big = abs(A(i,k));
         ibig = i;
      end
   end
% switch rows k and ibig, and b(k) and b(ibig)
   for i=k:n
      tempA = A(k,i);
      A(k,i) = A(ibig,i);
      A(ibig,i) = tempA;
   end
   tempb = b(k);
   b(k) = b(ibig);
   b(ibig) = tempb;
   if big == 0
      error('Singular matrix')
   end
   for i=k+1:n
      amul = -A(i,k)/A(k,k);
% add amul times row k to row i
      for j=k:n
         A(i,j) = A(i,j) + amul*A(k,j);
      end
      b(i) = b(i) + amul*b(k);
   end
end
% do back substitution
if A(n,n) == 0
   error('Singular matrix')
end
x(n) = b(n)/A(n,n);
for k=n-1:-1:1
   sum = 0;
   for j=k+1:n
      sum = sum + A(k,j)*x(j);
   end
   x(k) = (b(k)-sum)/A(k,k);
end

   

