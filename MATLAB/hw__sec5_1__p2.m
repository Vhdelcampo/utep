function [a,b,c,d,e,f] = hw__sec5_1__p2
for i=1:6
    [f,a,b] = problem2(i);
    integral = trapezoid(f,a,b,16);
    a(1,i) = integral;
end
