function derivative = newton_method(f,x,h)
    derivative = ( f(x + h) - f(x) ) / h;
end