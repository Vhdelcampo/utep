function [y,a,b,answer] = p2_functions(subproblem)
switch(subproblem)
    case 1
         y = @(x) exp(x) * cos(4*x);
         a = 0;
         b = pi;
         answer = ( exp(pi) - 1 ) / 17.;
    case 2
         y = @(x) x^(5/2);
         a = 0;
         b = 1;
         answer = 2./7 ;
    case 3
         y = @(x) 1 / ( 1 + (x - pi) ^ 2 );
         a = 0;
         b = 5;
         answer = atan(5 - pi) + atan(pi);
    case 4
         y = @(x) exp( cos(x) );
         a = -pi;
         b = pi;
         answer = 7.95492652101284;
    case 5
         y = @(x) exp( cos(x) );
         a = 0;
         b = pi/4;
         answer = 1.93973485062365;
    case 6
         y = @(x) sqrt(x);
         a = 0;
         b = 1;
         answer = 2./3;
end