function eigenvalue = power_method(A, n)
x0 = [1; 1];
data = zeros(n,5);
diff = 0; ratio = 0;
for i=1:n
    x1 = A*x0;
    eigenvalue = x1(1) / x0(1); % 1 is chosen arbitrarily
    x0 = x1 / max(x1); % normalize vector
    
    data(i,1) = x0(1);
    data(i,2) = x0(2);
    data(i,3) = eigenvalue;
    if (i > 1) % There must be two values at least
        diff = abs(eigenvalue - data(i-1,3) );
    end
    data(i,4) = diff;
    if (i > 2) % There must be two differences at least
        ratio = data(i,4) / data(i-1,4);
    end
    data(i,5) = ratio;
    
end
disp(data);
end