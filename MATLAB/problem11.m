function y = problem11(x)
y = x^4 - 5.4*x^3 + 10.56*x^2 - 8.954*x + 2.7951;