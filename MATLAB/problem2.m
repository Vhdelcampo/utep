function [y,a,b] = problem2(subproblem)
switch(subproblem)
    case 1
         y = @(x) exp(x) * cos(4*x);
         a = 0;
         b = pi;
    case 2
         y = @(x) x^(5/2);
         a = 0;
         b = 1;
    case 3
         y = @(x) 1 / ( 1 + (x - pi) ^ 2 );
         a = 0;
         b = 5;
    case 4
         y = @(x) exp( cos(x) );
         a = -pi;
         b = pi;
    case 5
         y = @(x) exp( cos(x) );
         a = 0;
         b = pi/4;
    case 6
         y = @(x) sqrt(x);
         a = 0;
         b = 1;
end

