function s53p2        
    integral = zeros(6,2);
    difference = zeros(6,2);
    order = zeros(6,1);
    exact = zeros(6,1);
    
    for i=1:6
        [f,a,b,answer] = p2_functions(i);
        exact(i) = answer;
        integral(i,1) = gauss3point(f,a,b,8);
        integral(i,2) = gauss3point(f,a,b,16);
        difference(i,1) = abs(answer - integral(i,1) );
        difference(i,2) = abs(answer - integral(i,2) );
        order(i) = log( difference(i,1) / difference(i,2) ) / log(2);
    end
    values = [exact,integral]
    calculations = [difference,order]
end

