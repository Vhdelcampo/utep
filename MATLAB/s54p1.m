function s54p1
    f1 = zeros(5,1);
    f2 = zeros(5,1);
    f3 = zeros(5,1);
    errorf1 = zeros(5,1);
    errorf2 = zeros(5,1);
    errorf3 = zeros(5,1);
    ratiof1 = zeros(5,1);
    ratiof2 = zeros(5,1);
    ratiof3 = zeros(5,1);
    
    h = .1;
    j = 1;
    while h > .003125
        
        [f,a,exact] = s54p1_functions(1);
        f1(j,1) = newton_method(f, a, h);
        errorf1(j) = abs(exact - f1(j,1));
        if (j>1)
            ratiof1(j) = errorf1(j-1) / errorf1(j);
        end
        
        
        [f,a,exact] = s54p1_functions(2);
        f2(j,1) = newton_method(f, a, h);
        errorf2(j) = abs(exact - f2(j,1));
        if (j>1)
            ratiof2(j) = errorf2(j-1) / errorf2(j);
        end
        
        [f,a,exact] = s54p1_functions(3);
        f3(j,1) = newton_method(f, a, h);
        errorf3(j) = abs(exact - f3(j,1));
        if (j>1)
            ratiof3(j) = errorf3(j-1) / errorf3(j);
        end

        h = h/2;
        j = j + 1;
    end
    
    tablef1 = [f1,errorf1,ratiof1]
    tablef2 = [f2,errorf2,ratiof2]
    tablef3 = [f3,errorf3,ratiof3]
end