function [f,a,exact] = s54p1_functions(subproblem)
    switch (subproblem)
        case 1
            f = @(x) exp(x);
            a = 0;
            exact = 1;
        case 2
            f = @(x) atan(x^2 -x + 1);
            a = 1;
            exact = 1./2.;
        case 3
            f = @(x) atan(100*x^2 - 199*x + 100);
            a = 1;
            exact = 1./2.;
    end
               
end

