function s63p5
    error_a = zeros(1,4);
    error_b = zeros(1,4);
    
    [A,b,x] = s63p5_matrices(1);
    results_a = gel (A,b,4)
    error_a = x - results_a
    
    [A,b,x] = s63p5_matrices(2);
    results_b = gel (A,b,4)
    error_b = x - results_b
end