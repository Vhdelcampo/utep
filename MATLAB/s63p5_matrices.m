function [matrix, b, x] = s63p5_matrices(subproblem)
 switch (subproblem)
     case 1
        A = [5 7 6 5
             7 10 8 7
             6 8 10 9
             5 7 9 10];
         matrix = A;
         b = [1; -1; -1; 1];
         x = [136 -82 -35 21];
     case 2
        % Hilbert matrix
        n = 4;
        A = zeros(n,n);
        for i = 1:n
            for j = 1:n
                A(i,j) = 1 / (i + j - 1);
            end
        end
        matrix = A;
        b = [1; -1; 1; -1];
        x = [516 -5700 13620 -8820];
 end

