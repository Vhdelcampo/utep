function s65p1
% Hilbert matrix
n = 12;
A = zeros(n,n);
b = zeros(1,n);
r = zeros(1,n);
for i = 1:n
    b(i) = 1;
    for j = 1:n
        A(i,j) = 1 / (i + j - 1);
    end
end
A
b
xe = gel(A,b,n)
for i=1:n
    for j=1:n
        r(i) = b(i) - A(i,j) * xe(j);
    end
end
r