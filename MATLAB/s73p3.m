function s73p3(iterates)
data = zeros(iterates,5);
for starting_vertex=1:4
    switch (starting_vertex)
        case 1
            x_0 = [1.2; 2.5];
        case 2
            x_0 = [-2; 2.5];
        case 3
            x_0 = [-1.2; -2.5];
        case 4
            x_0 = [2; -2.5];
    end
    for i=1:iterates
        x_1 = x_0 - J(x_0) \ F(x_0);
        data(i,1) = i;
        data(i,2) = x_0(1);
        data(i,3) = x_0(2);
        data(i,4) = x_1(1);
        data(i,5) = x_1(2);
        x_0 = x_1;
    end
    display(data);
end
end

function F_system = F(x)
    F_system = [x(1)^2 + x(1)*x(2)^3 - 9   ; 
                3*x(1)^2*x(2) - x(2)^3 - 4];
end

function Jacobi = J(x)
    Jacobi = [2*x(1) + x(2)^3, 2*x(1)*x(2)^2       ; 
              6*x(1)*x(2)    , 3*x(1)^2 - 3*x(2)^2];
end
      
      