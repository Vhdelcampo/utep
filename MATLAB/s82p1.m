% y'(x) = -[y(x)]^2
% 1<=x<=10
% y(1) = 1
% Y(x) = 1/x
% Euler: y_n+1 = y_n - h*y_n^2

function s82p1
n = 100; %Iterations to do for each h value.
for i=1:3
    h = .2/2^(i-1);
    data = zeros(i,5);
    y_0 = 1;
    for j=1:n
        x = j*h;
        y_1 = y_0 + h*f(x, y_0);
        y_0 = y_1;
        exact = Y(x);
        error = abs(y_1 - exact);
        rel_error = abs(exact - y_1) / exact;
        data(j,1) = x;
        data(j,2) = y_1;
        data(j,3) = exact;
        data(j,4) = error;
        data(j,5) = rel_error;
    end
    % Display values of x only when it is an integer from 1 to 5.
    for k=1:5
        disp(data(k*(1/h),:)); 
    end
end
end

function exact = Y(x)
exact = 1/x;
end

function euler_method = f(x, y)
euler_method = -y^2;
end
