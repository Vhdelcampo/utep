function s87p7a
% y''' + 4y'' + 5y' + 2y = 2x^2 + 10x + 8
% y(0) = 1, y'(0) = -1; y''(0) = 3
% True Solution: Y(x) = e^-x + x^2

xMax = 3; %X value to reach for each iteration
for i=1:3
    h = .1/2^(i);
    n = xMax/h;
    data = zeros(i,5);
    y = [1; -1; 3];
    h
    for j=1:n
        x = j*h;
        y = y + h*f(x, y);
        
        exact = Y(x);
        estimate = norm(y);
        error = abs(norm(y) - exact);
        rel_error = abs(exact - norm(y)) / exact;
        data(j,1) = x;
        data(j,2) = exact;
        data(j,3) = estimate;
        data(j,4) = error;
        data(j,5) = rel_error;
    end
    data
end
end

function exact = Y(x)
exact = exp(-x) + x^2;
end

function euler_system = f(x, y)
yp = y(2);
up= y(3);
vp = -4*y(3) - 5*y(2) - 2*y(1) + 2*x^2 + 10*x + 8;
euler_system = [yp; up; vp;];
end
