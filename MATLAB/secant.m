function f = secant(fun,x0,x1,tol,max)
x(1) = x0;
x(2) = x1;
y(1) = feval(fun,x(1));
y(2) = feval(fun,x(2));
    for i = 3 : max
        x(i) = x(i-1) - y(i-1)/((y(i-1)-y(i-2))/(x(i-1)-x(i-2)));
        y(i) = feval(fun,x(i));
        if abs(x(i) - x(i-1)) < tol
            break;
        end
        if i== max
            disp('Zero not found!');
        end
    end
n = length(x);
k = 1:n;
    out = [k' x' y']; 
disp('                 x         y');
disp(out)