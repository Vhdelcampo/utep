function integral = trapezoid(f,a,b,n)
h = (b-a) / n;
w1 = 1. / 2.;
w2 = 1. / 2.;
r1 = 0;
r2 = 1;
sum = 0;
for i = 1:n
    xi = a + (i-1) * h;
    sum = sum + h*w1*f(xi + r1 * h) + h*w2*f(xi + r2 * h);
end
integral = sum;