#!/bin/bash
echo
echo Beginning script
echo
sleep 3

# clear the terminal screen;
echo The text will now be cleared
sleep 3
clear
echo

# display your working directory;
echo Directory is displayed
pwd
sleep 3
echo

# list the contents of your directory;
echo Contents of directory are displayed
ls   
sleep 3
echo

# make a directory called lab01 and labA;
echo Directories lab01 and labA are created
mkdir lab01 labA
sleep 3
echo

# list the contents of your directory showing the additions of lab01 and labA;
echo Contents of directory are displayed
ls
sleep 3
echo
# remove directory labA;
echo Removing directory labA
rm -r labA/
sleep 3
echo

# list the contents of your directory showing the deletion of directory labA;
echo Contents of directory are displayed
ls
sleep 3
echo

# copy lab0_1.sh to the lab01 directory;
echo File lab0_1.sh is moved to lab01/
cp lab0_1.sh lab01/
sleep 3
echo

# change to the lab01 directory;
echo Directory is changed to lab01/
cd lab01/
sleep 3
echo

# display your working directory;
echo Directory is displayed
pwd
sleep 3
echo

# list the contents of your directory showing the addition of the script file;
echo Contents of directory are displayed
ls
sleep 3
echo

# delete the lab01_script.sh file from the lab01 directory;
echo File lab-0_1.sh is deleted
rm lab0_1.sh
sleep 3
echo

# display your working directory;
echo Directory is displayed
pwd
sleep 3
echo

# list the contents of your lab01 directory;
echo Contents of directory are displayed
ls
sleep 3
echo

# get out of lab01 folder
echo Directory changed to one level higher
cd ..
sleep 3
echo

# delete lab01 directory;
echo Folder lab01/ is deleted
rm -r lab01/
sleep 3
echo

# list the contents of your Scripts directory showing the deletion of directory lab01.
echo Script directory is displayed
ls
