/* Exercise 1-10. Write a program to copy its input to its output, 
   replacing each tab by \t, each backspace by \b, and each backslash by \\. 
   This makes tabs and backspaces visible in an unambiguous way. */
#include <stdio.h>
int main(){
    int currChar;
    while((currChar=getchar() ) != EOF) {
	if(currChar == '\\') {
            printf("\\\\");
	}else if(currChar == '\t') {
            printf("\\\t");
	}else if(currChar == '\b') {
            printf("\\\b");
        }else
	    putchar(currChar);        
    }
    return 0;
}
