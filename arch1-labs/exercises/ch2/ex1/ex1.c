#include <stdio.h>

int main(void){
    
    unsigned char ucharmax = ~0;
    unsigned short ushortmax = ~0;
    unsigned int uintmax = ~0;
    unsigned long ulongmax = ~0;
    signed char scharmin = ucharmax>>1;
    signed short sshortmin = ushortmax>>1;
    signed int sintmin = uintmax>>1;
    signed long slongmin = ulongmax>>1;
    
    printf("Size of unsigned char:  %d\t\t%u\n", 0, ucharmax);
    printf("Size of unsigned short: %d\t\t%u\n", 0, ushortmax);
    printf("Size of unsigned int:   %d\t\t%u\n", 0, uintmax);
    printf("Size of unsigned long:  %d\t\t%u\n", 0, ulongmax);
    printf("Size of signed char:    %d\t\t%u\n", ~scharmin, scharmin);
    printf("Size of signed short:   %d\t\t%u\n", ~sshortmin, sshortmin);
    printf("Size of signed int:     %d\t%u\n",   ~sintmin, sintmin);
    printf("Size of signed long:    %d\t%u\n",   ~slongmin, slongmin);
    
}
