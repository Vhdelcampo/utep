#include <stdio.h>
#include "ex3.h"

int main(void){
    char hexString[] = "0xFF";
    printf("Hex number %s in decimal is %d\n", hexString, hexToInt(hexString));
    return 0;
}
