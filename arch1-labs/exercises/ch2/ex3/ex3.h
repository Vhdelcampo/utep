/* strlen: return length of s */
int strlen(char s[]){
    int i = 0;
    while (s[i] != '\0')
	i++;
    return i;
}

/* Exercise 2-3. Write the function htoi (s), which converts a string of hexa-
   decimal digits (including an optional Ox or ox) into its equivalent integer value.
   The allowable digits are 0 through 9, a through f, and A through F. */
int hexToInt(char hex[]){
    int totalIntegerNumber = 0;
    int digitValue = 0;
    int hexPower = 1;
    int i;
    for(i=strlen(hex)-1; i>=0; i--){ // Start at least significant bit and go left each time
	char currChar = hex[i];
	if (currChar == 'x') break; // When we reach the 0x part of the hex string break.
	if (currChar >= '0' && currChar <= '9'){
	    digitValue = hexPower * (currChar - '0');
	}
	else if(currChar >= 'A' && currChar <= 'F'){
	    digitValue = hexPower * (currChar - 'A' + 10); // 10 is added as A starts at 10
	}
	hexPower *= 16;
	totalIntegerNumber += digitValue; 
    } 
    return totalIntegerNumber;
}
