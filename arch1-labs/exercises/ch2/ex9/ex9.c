#include <stdio.h>
#include "ex9.h"

int main(void){
    signed int number = 255;
    int numOfBits = countBits(number);
    printf("Number of one-bits in %d is %d\n", number, numOfBits );
    return 0;
}
