#include <stdio.h>
#include <limits.h>
#include "exX.h"

int main(void){
    int nSet = 32;
    int lsb = 0;
    unsigned long val = 0xaaaaaaaa;
    printf("Given nSet %d, number is %x\n", nSet, set_bits(nSet)) ;
    printf("If nSet is %d and lsb %d, result is %x\n", nSet, lsb, set_range(nSet,lsb));
    printf("Given val %d, nSet %d, and lsb %d, result is %x\n", val, nSet, lsb, invert_range(val,nSet,lsb));
    return 0;
}

/* set_bits(0) = 0L */
/* set_bits(5) = 0x1fL */
/* set_bits(32) = -1L */

/* set_range(32,0) = -1L */
/* set_range(5,0) = 0x1fL */
/* set_range(5,1) = 0x3eL */

/* invert_range(0xaaaaaaaaL, 0,0) = 0xaaaaaaaaL */
/* invert_range(0xaaaaaaaaL, 32,0) = 0x55555555L */
/* invert_range(0xaaaaaaaaL, 5,1) = 0xaaaaaa90L */
