#include <limits.h>

// Returns an integer whose nset least significant bits are set (the rest are zero). 
unsigned long set_bits(unsigned nSet){
    if (nSet > sizeof(ULONG_MAX)) return ~0;
    unsigned long setBits = (1 << nSet) - 1;
    return setBits;
}

// Returns an integer whose lsb least significant bits are zero, and the successive nset bits are one.
unsigned long set_range(unsigned nSet, unsigned lsb){
    return ~set_bits(lsb) & set_bits(nSet + lsb);
}

// Selectively inverts nset bits in val starting at bit lsb and returns that value.
unsigned long invert_range(unsigned long val, unsigned nSet, unsigned lsb){
    return val ^= set_range(nSet, lsb);
}
