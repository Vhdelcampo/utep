#include <stdio.h>
/* Exercise 2-3. Write the function htoi (s), which converts a string of hexa-
   decimal digits (including an optional Ox or ox) into its equivalent integer value.
   The allowable digits are 0 through 9, a through f, and A through F. */

/* strlen:
   return length of s */
int strlen(char s[]){
    int i = 0;
    while (s[i] != '\0')
	i++;
    return i;
}

int hexToInt(char hex[]){
    int totalIntegerNumber = 0;
    int digitValue = 0;
    int hexPower = 1;
    int i;
    for(i=strlen(hex)-1; i>=0; i--){ // Start at least significant bit and go left each time
	char currChar = hex[i];
	if (currChar == 'x') break; // When we reach the 0x part of the hex string break.
	if (currChar >= '0' && currChar <= '9'){
	    digitValue = hexPower * (currChar - '0');
	    // printf("digit value is %d hex value is %d\n", digitValue, hexPower);
	}
	else if(currChar >= 'A' && currChar <= 'F'){
	    digitValue = hexPower * (currChar - 'A' + 10); // 10 is added as A starts at 10
	    // printf("digit value is %d hex value is %d\n", digitValue, hexPower);
	}
	hexPower *= 16;
	totalIntegerNumber += digitValue; 
    } 
    return totalIntegerNumber;
}

int main(void){
    char hexString[] = "0xFF";
    printf("Hex number %s in decimal is %d\n", hexString, hexToInt(hexString));
    return 0;
}
