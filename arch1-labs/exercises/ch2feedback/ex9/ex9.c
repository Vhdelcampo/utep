#include <stdio.h>
/* Exercise 2-9. In a two's complement number system, x &= ( x-1 ) deletes the
   rightmost 1-bit in x. Explain why. Use this observation to write a faster ver-
   sion of bitcount. */

int countBits(signed int number){
    int numOfOneBits = 0;
    while (number != 0){
	number &= (number-1); // Deletes least significant 1-bit
	numOfOneBits++;
	// printf("current number of one-bits is %d\n", numOfOneBits);
    }
    return numOfOneBits;
}

int main(void){
    signed int number = 255;
    int numOfBits = countBits(number);
    printf("Number of one-bits in %d is %d\n", number, numOfBits );
    return 0;
}
