/* Exercise 3-2. Write a function escape (s, t) that converts characters like */
/* newline and tab into visible escape sequences like \n and \t as it copies the */
/* string t to s. Use a switch. Write a function for the other direction as well, */
/* converting escape sequences into the real characters. */
void escape(char source[], char target[]){
    int i;
    int i2 = 0;
    
    for(i = 0; i < strlen(source); i++) {
	char currChar = source[i];
	switch(currChar) {
	case '\t':
	    target[i2] = '\\';
	    i2++;
	    target[i2] = 't';
	    break;
	case '\n':
	    target[i2] = '\\';
	    i2++;
	    target[i2] = 'n';
	    break;
	default:
	    target[i2] = currChar;
	}
	i2++;
    }
    target[i2] = '\0';
}

void escapeReturn(char source[], char target[]){
    int i;
    int i2 = 0;
    int state = 0;
    
    for(i = 0; i < strlen(source); i++) {
	char currChar = source[i];
	switch(currChar) {
	case '\\':
	    state = 1;
	    break;
	case 't': case 'n':
	    if (state) {
		if (currChar == 't'){
		    target[i2] = '\t';
		}else {
		    target[i2] = '\n';
		}
	    }
	default:
	    if (state == 0){
		target[i2] = currChar;
	    }
	    state = 0;
	    i2++;
	}
    }
    target[i2] = '\0';
}
