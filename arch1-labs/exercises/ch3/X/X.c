#include <stdio.h>
#include "X.h"

int main(void) {
    int value = 255;
    int base = 8;
    int numChars = 4;
    char string[100];
    int returnValue = intToByte(value, base, string, numChars);
    printf("String after being converted is %s. Return is %d\n", string, returnValue );
}

/* val = 255 base 2 nchars 4
   return str = 1111 return -1
   val = 255 base 15 nchars 2
   return str = ff return 2 */
