#include <string.h>
/* Exercise 3-X. Write a function int itob(int val, int base, char *str, int nChars)
   that converts integer val into a string in str in radix base with limited field width nChars. 
   The resulting should be no longer than nChars long. If the representation of requires more than nChars, 
   the string should be truncated. If ascii representation of val fits in nChars bytes, itob's return
   value should be the length of that string. Otherwise, it return a negative value. */

void reverse(char s[]) {
      int length = strlen(s) ;
      int c, i, j;

      for (i = 0, j = length - 1; i < j; i++, j--)
     {
         c = s[i];
         s[i] = s[j];
         s[j] = c;
      }
}

int intToByte(int value, int base, char *string, int numChars){
    char s[100];
    int i = 0;
    
    for (value; value != 0; value /= base) {
	int digitValue = value % base;
	char c;
	if (digitValue >= 0 && digitValue <= 9){
	    c = (char)(digitValue + '0');
	}else{
	    c = (char)(digitValue + 'A' - 10);
	}
	s[i] = c;
	// printf("current digit value: %d\n", digitValue);
	i++;
    }
    s[i] = '\0';
    reverse(s);
    string = s;
    return (numChars < strlen(s)) ? -1 : i;
}
