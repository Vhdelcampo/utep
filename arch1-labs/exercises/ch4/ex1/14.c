/* Exercise 4-14. Define a macro swap(t,x,y) that interchanges two arguments of type t. (Block structure will help.) */
#include <stdio.h>
#define swap(t,x,y) {t temp = x; x = y; y = temp;}

int main(void){
    int a = 0;
    int b = 1;
    printf("a is %d, b is %d\n", a, b);
    swap(int, a, b);
    printf("After being swapped a is now %d, b is now %d\n", a, b);
    return 0;
}
