#include "quicksort_goto.h"
#include "swap.h"

/* 
 * helper fuction that actually implements quicksort
 * a[left] through a[right] is recursively sorted
 *
 * @param int to_sort[] the array being sorted
 * @param int left the lower bound of range being sorted
 * @param int right the higher bound of range being sorted
 */
static void qGotoHelper(int to_sort[], int left, int right) {
    int i, last, pivot;
    if (left < right) {goto else1;}        /* array range smaller than 0 */
    return;
    else1:
    swap(to_sort[left], to_sort[(left+right)/2]); /* pivot into a[left] */
    pivot = to_sort[left];
    last = left; /* last+1 is first destionation for swaps of vals<pivot */

    i = left + 1;
    if1:
    if (i > right) {goto else2;}
    if (to_sort[i] >= pivot) {goto else3;}
    last++;
    swap (to_sort[last], to_sort[i]);
    else3:
    i++;
    goto if1;
    else2:
  
    swap(to_sort[left], to_sort[last]);	/* put pivot where it belongs */
    /* recursively sort left and right sub-ranges */
    qGotoHelper(to_sort, left, last-1); qGotoHelper(to_sort, last+1, right);
}

/* public interface for qGotoHelper() that sorts an array a of length len 
 *
 * @param int to_sort[] the array to be sorted
 * @param int len the length of the array being sorted
 */
void quicksort_goto(int to_sort[], int len) {
    qGotoHelper(to_sort, 0, len-1);
}
