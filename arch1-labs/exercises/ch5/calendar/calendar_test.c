#include <stdio.h>
#include "calendar_utils.h"

int main(void)
{
  int year = 2100;
  int month = 12;
  int day = 50;
  
  int yearday = day_of_year(year, month, day);
  printf("Date: %d-%d-%d\nYearday from date: %d\n", month, day, year, yearday);
  
  month_day(year, yearday, &month, &day);
  printf("Date from yearday: %d-%d-%d\n", month, day, year);

  return 0;
}
