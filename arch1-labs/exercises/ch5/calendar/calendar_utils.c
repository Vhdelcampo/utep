#include "calendar_utils.h"
#include <stdio.h>

static char daytab[2][13] = {
    {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31},
    {0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}
};

/* day_of_year:  set day of year from month & day */
/* Returns -1 for invalid values below min and -2 for invalid values that go above max */
int day_of_year(int year, int month, int day) {
    int i, leap;
    leap = (year%4 == 0 && year%100) != 0 || year%400 == 0;
    int daysInMonth = daytab[leap][month];
    if (year < 0)  { printf("Negative year value\n"); return -1; }
    if (month < 0) { printf("Negative month value\n"); return -1; }
    if (day < 0)   { printf("Negative day value\n"); return -1; }
    if (month > 12){ printf("Month value exceeds max amount of months of in a year\n"); return -2; }
    if (day > daysInMonth) { printf("Number of days exceeds amount of days in month\n"); return -2; }
    
    for (i = 1; i < month; i++)
	day += daytab[leap][i];
    return day;
}

/* month_day:  set month, day from day of year */
/* Returns -1 for invalid values below min and -2 for invalid values that go above max */
int month_day(int year, int yearday, int *pmonth, int *pday) {
    int i, leap;
    leap = (year%4 == 0 && year%100) != 0 || year%400 == 0;
    if (year < 0)      { printf("Negative year value\n"); return -1; }
    if (yearday < 0)   { printf("Negative yearday value\n"); return -1; }
    if (yearday > 365) { printf("Yearday exceeds 365"); return -2; }
    
    for (i = 1; yearday > daytab[leap][i]; i++) {
	yearday -= daytab[leap][i];
    }
    *pmonth = i;
    *pday = yearday;
    return 0;
}
