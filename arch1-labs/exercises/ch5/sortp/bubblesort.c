#include "bubblesort.h"
#include "swap.h"
/* bubblesort implementation
 *
 * @param int to_sort[] the array to be sorted
 * @param int len the length of the array being sorted
 */
void bubblesort(int to_sort[], int len) {
    int i, k, largest, swapped;
    swapped = 1;
    while (swapped != 0) {
	swapped = 0;
	for (i = 1; i < len; i++) {
	    if (to_sort[i] < to_sort[i-1]) {
		swap(to_sort[i], to_sort[i-1]);
		swapped = 1;
	    }
	}
    }
}
