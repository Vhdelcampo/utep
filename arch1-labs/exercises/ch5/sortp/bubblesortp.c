#include "bubblesortp.h"
#include "swapp.h"
#include <stdio.h>
/* bubblesort implementation
 *
 * @param int to_sort[] the array to be sorted
 * @param int len the length of the array being sorted
 */
void bubblesortp(int **p, int len) {
    int i, k, swapped;
    int **to_sort = p;
    swapped = 1;
    while (swapped != 0) {
	swapped = 0;
	for (i = 1; i < len; i++) {
	    if (**(to_sort+i) < **(to_sort+i-1)) {
		swapp(*(to_sort+i), *(to_sort+i-1));
		swapped = 1;
	    }
	}
    }
}
