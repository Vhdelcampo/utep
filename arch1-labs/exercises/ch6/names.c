// (*b_person)age = 10
// b_person -> age = 20
#include <stdlib.h> // for malloc
#include <errno.h>  // for errno 
#include <string.h> // for strerror
#include "names.h"
#define pmalloc (struct person*)malloc(sizeof(struct person));

void get_names(struct person **people_head, char *file_name);
void print_names(struct person *people_head);
void free_people(struct person *people_head);

/*
 * main driver
 */
int main(void) {
    struct person *people_head = NULL;
    get_names(&people_head, "names.txt");
    print_names(people_head);
    //  free_people(people_head);
}

/*
 * Print a list of names
 * @param people_head start of the list to print
 */
void print_names(struct person *people_head) {
    struct person *current = people_head;
  
    printf("First name   Last name   Age\n");
    printf("----------------------------\n");
    print_tree(person_head);    
    putchar('\n');
}

void print_tree(struct person *current) {
    printf("%10s   %9s   %d\n", current->first_name, current->last_name, current->age);
    if (!current->left_child && !current->right_child) {
	return;
    }
    if (current->left_child) {
	return print_tree(current->left_child);
    }
    if (current->right_child) {
	return print_tree(current->right_child);
    }
}

/*
 * Frees a list of people
 * @param current head of list of people to fail
 */
void free_people_tree(struct person *current) {
    free(current);
    if (!people_head->left_child && !current->right_child) {
	return;
    }
    if (people_head->left_child) {
	return traverse_tree(current->left_child);
    }
    if (people_head->right_child) {
	return traverse_tree(current->right_child);
    }
}

/*
 * Reads a list of people from a file into the
 * specified list
 * @param head the head 
 * @param file_name name of the file to open
 */
void get_names(struct person **head, char *file_name) {
    struct person *current = NULL, *prev = NULL;
  
    FILE *names_file;

    // open a the specified file to read names
    names_file = fopen(file_name, "rt");
    if (!names_file) {
	printf("Error opening file aborting\n%s\n", strerror(errno));
	exit(1);
    }

    // read a person from a file
    current = pmalloc;
    while (fscanf(names_file, "%10s", current->first_name) > 0) {
	fscanf(names_file, "%10s", current->last_name);
	fscanf(names_file, "%d", &current->age);

	current->next_person = NULL;
	if (prev) {
	    prev->next_person = current;
	} else {
	    *head = current;
	}
	prev = current;
	// alloc the next entry
	current = pmalloc;
    }

    return;
}
