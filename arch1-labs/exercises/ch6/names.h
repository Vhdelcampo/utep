#include <stdio.h> // for scanf/printf

#define NAME_LENGTH 10
struct person {
  char first_name[NAME_LENGTH + 1];
  char last_name[NAME_LENGTH + 1];
  int age;
  struct person* left_child;
  struct person* right_child;
};

enum field {
  first_name,
  last_name,
  age,
};
