#
# Makefile - blink.elf
#
# @author Adrian Veliz
# @author Eric Freudenthal
# @version 0.1.20130815
#

## Every line beginning with VAR = blablabla makes so that every time $(VAR) appears blablabla
## will be inserted instead. This is convenient so that way you can keep track of compiler and
## object changes by only changing the the VAR.
# makfile configuration
COMMON_OBJECTS         = blink.o config_clocks.o sleep_600th.o sleep_millisecond_s.o
CPU             = msp430g2553
CFLAGS          = -mmcu=${CPU} 

#switch the compiler (for the internal make rules)
CC              = msp430-gcc
AS              = msp430-as

all: blink_c.elf 

## .elf files are the executable file type needed by msp430.
## The $(cc) makes so that the msp430 compiler is used.
## The -mmcu are options to the compiler and the ${CPU} part tells the compiler which type of CPU to use.
## The -o has the $@ part which tells the compiler to put everything seen before the objects and
## the $@ part tells it to put stuff after the object files.
#additional rules for files

blink_c.elf: ${COMMON_OBJECTS} sleep_sec_c.o sleep_millisecond_c.o led_c.o
	${CC} -mmcu=${CPU} -o $@ $^

blink_s.elf: ${COMMON_OBJECTS} sleep_sec_c.o led_s.o
	${CC} -mmcu=${CPU} -o $@ $^

## mspdebug moves the program from the computer to the msp430. The prog part gives the name of the program and
## rf2500 give the type of hardware needed.
install_c: blink_c.elf
	mspdebug rf2500 "prog blink_c.elf"

install_s: blink_s.elf
	mspdebug rf2500 "prog blink_s.elf"


clean:
	rm -f *.o *.elf

## Here are all the files needed to make the object files.
## All are C files except sleep_600th.s which is in assembly.
#project dependencies
blink.o: blink.c

config_clocks_.o: config_clocks_.c

sleep_sec_c.o: sleep_sec_c.c

sleep_600th.o: sleep_600th.s

sleep_millisecond_s.o: sleep_millisecond_s.s

sleep_millisecond_c.o: sleep_millisecond_c.c

led_c.o: led_c.c

led_s.o: led_s.s
