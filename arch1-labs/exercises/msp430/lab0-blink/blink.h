#ifndef BLINK_INCLUDED
#define BLINK_INCLUDED

void sleep(unsigned short);
void sleep_millisecond(unsigned short);
void sleep_600th(void);
void sleep_millisecond_assembly(void);
void led_control(unsigned short, int);
/* void brightness_10(int); */
/* void brightness_50(int); */
/* void brightness_90(int); */

#endif	/* BLINK_INCLUDED */
