/** 
 * Alternate Red and Greed LEDs every second.
 * 
 * @author Adrian Veliz
 * @author Eric Freudenthal
 * @version 0.1.20130815
 */

#include "msp430g2553.h"
#include "blink.h"

#define P1OUT_ADDR 0x21

void brightness_10(unsigned short v) {
  if (v) {
    int i;
    for(i = 0; i < 20; i++){
      *((char *)P1OUT_ADDR) = 0x0000;
      sleep_millisecond(5);
      *((char *)P1OUT_ADDR) = 0x0040;
    }
  } else {
    int i;
    for(i = 0; i < 20; i++){
      *((char *)P1OUT_ADDR) = 0x0000;
      sleep_millisecond(50);
      *((char *)P1OUT_ADDR) = 0x0001;
    }
  }
}

void brightness_50(unsigned short v) {
  if (v) {
    int i;
    for(i = 0; i < 10; i++){
      *((char *)P1OUT_ADDR) = 0x0000;
      sleep_millisecond(10);
      *((char *)P1OUT_ADDR) = 0x0040;
      sleep_millisecond(10);
    }
  } else {
    int i;
    for(i = 0; i < 10; i++){
      *((char *)P1OUT_ADDR) = 0x0000;
      sleep_millisecond(10);
      *((char *)P1OUT_ADDR) = 0x0001;
      sleep_millisecond(10);
    }
  }
}

void brightness_90(unsigned short v) {
  if (v) {
    *((char *)P1OUT_ADDR) = 0x0000;
    sleep_millisecond(100);
    *((char *)P1OUT_ADDR) = 0x0040;
  } else {
    *((char *)P1OUT_ADDR) = 0x0000;
    sleep_millisecond(100);
    *((char *)P1OUT_ADDR) = 0x0001;
  }
}

void led_control(unsigned short v, int brightness)
{
  if (brightness == 10) { 
    brightness_10(v); 
  } else if (brightness == 50) { 
    brightness_50(v); 
  } else if (brightness == 90) { 
    brightness_90(v); 
  }
}
