; 
; led_s.s - Assembly version of led_c.c
;
; @author Victor Del Campo
;
	.arch			msp430g2553
	.p2align		1,0
	.text
	
	.globl led_control
	
led_control:
	
	cmp #0, r15
	jnz else 
	mov.b #0x40, &0x21
	jmp end
else:	
	mov.b #0x1, &0x21
end:	
	ret

