/** 
 * sleep_c.c - Example sleep function in c.
 * 
 * @author Adrian Veliz
 * @author Eric Freudenthal
 * @version 0.1.20130815
 */

#include "blink.h"

void sleep_millisecond(unsigned short millisecond) {
  while (millisecond--) {
    sleep_millisecond_assembly();
  }
}

