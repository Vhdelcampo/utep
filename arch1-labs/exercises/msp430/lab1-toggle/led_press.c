#include "msp430.h"

void configureClocks();
static unsigned char state = 0;
static unsigned char old_state = 0;
#define forever 1
#define switch_bit BIT3
#define btn_up (BIT3 & P1IN)
#define btn_down (!(BIT3 & P1IN))
#define green_bit BIT6
#define red_bit BIT0
#define red_up 0
#define green_down 1
#define green_up 2
#define red_down 3



void main(void) {
  WDTCTL = WDTPW + WDTHOLD;  //Disable watchdog timer
  configureClocks();
  P1DIR = green_bit + red_bit; 		/* LED bits are for output */
  // Configure Switch on P1.3 //
  P1REN |= switch_bit;		   // Resistor ENable for switch (P1.3)
  P1OUT =  green_bit | switch_bit; // Send 1 to switch, red LED on
  state = green_up;
  while (forever) {
    chk_state();
    set_led();
  }
}

int did_bounce() {
  old_state = state;
  sleep_millisecond(10);
  if (state != old_state) { return 1; }
  return 0;
}

void chk_state() {
  switch (state) {
  case red_up:
    if (btn_down) {
      if (!did_bounce()) { state = green_down; }
    }
    break;
  case green_down:
    if (btn_up) {
      if (!did_bounce()) { state = green_up; }
    }
    break;
  case green_up:
    if (btn_down) {
      if (!did_bounce()) { state = red_down; }
    }
    break;
  case red_down:
    if (btn_up) {
      if (!did_bounce()) { state = red_up; }
      break;
    }
  }
}

void set_led() {
  if (state == red_up) {
    P1OUT = red_bit | switch_bit;
  }
  else if (state == green_up) {
    P1OUT = green_bit | switch_bit;
  }
  else if (state == green_down) {
    P1OUT = green_bit | switch_bit;
  }
  else if (state == red_down) {
    P1OUT = red_bit | switch_bit;
  }
}

void configureClocks() {
  // Set system DCO to 1MHz
  BCSCTL1 = CALBC1_1MHZ;
  DCOCTL = CALDCO_1MHZ;
  // Set LFXT1 to the VLO @ 12kHz
  BCSCTL3 |= LFXT1S_2;
}




 
