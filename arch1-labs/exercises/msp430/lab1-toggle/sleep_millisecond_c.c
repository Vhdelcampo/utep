#include "toggle.h"

void sleep_millisecond(unsigned short milliseconds) {
  while (milliseconds--) {
    sleep_millisecond_s();
  }
}

