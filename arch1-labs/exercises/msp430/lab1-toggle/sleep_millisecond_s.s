; @author Victor Del Campo
;
	.arch			msp430g2553
	.p2align		1,0
	.text

	.globl sleep_millisecond_s
sleep_millisecond_s:
	;; 2 cycles
	mov #333, r15	;number of iterations
loop:			
	;; 1 cycle
	sub #1, r15
	;; 2 cycles
	jne loop	;loop until zero
	ret
