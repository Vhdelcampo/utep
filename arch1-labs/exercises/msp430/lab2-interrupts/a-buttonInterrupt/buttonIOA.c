#include <msp430.h>

// for button state machine
#define off_up 0
#define on_dn 1
#define on_up 2
#define off_dn 3
static unsigned char state = off_up;

// for flashRED state machine
#define waitUp 0
#define waitDn 1

unsigned char buttonPosition = waitUp;

void p1handler()	/* called when p1 interrupt occurs - for buttons */
{
  if (buttonPosition == waitUp) {	/* button is only input on P1, so don't need to check P1IN */
    buttonPosition = waitDn;
    P1IES |= BIT3;		/* interrupt sensitive to button down */
  } else {			/* button UP */
    buttonPosition = waitUp;
    P1IES &= ~BIT3;		/* interrupt sensitive to button up */
  }
  P1IFG &= ~BIT3;		/* clear interrupt */
  led_control();
}

void green_led()
{
  switch (state) {

  case off_up:
    if (buttonPosition == waitUp) {
      P1OUT |= BIT6;
      state = on_dn;
    }
    break;
  case on_dn:
    if (buttonPosition == waitDn) {
      state = on_up;
    }
    break;
  case on_up:
    if (buttonPosition == waitUp) {
      P1OUT &= ~BIT6;
      state = off_dn;
    }
    break;
  case off_dn:
    if (buttonPosition == waitDn) {
      state = off_up;
    }
    break;
  }
}

void red_led()
{
  if (buttonPosition == waitUp)
    P1OUT |= BIT0;
  else
    P1OUT &= ~BIT0;
}

void led_control()		/* not a state machine:
				   set LEDs from state variables */
{
  red_led();
  green_led();
}
