#include <msp430.h>

// P1 connections:
//    P1.0 red led (1 = 0N)
//	P1.3 button (zero if down, needs resistor pull up)
//	P1.6 green led (1=ON)


void main(void) 
{  
  WDTCTL = WDTPW + WDTHOLD;	// Stop watchdog timer

  P1DIR = BIT0 + BIT6;	   	// bits attached to leds are output
  P1REN = BIT3;			// enable resistor for switch
  P1OUT = BIT3;			// pull up resistor for switch

  P1IE = BIT3;		   	// switch interrupt enabled
  P1IES = BIT3;		 	// switch interrupt high-low transition (down)
  P1IFG &= ~BIT3;	   	// switch interrupt pending flag cleared
  or_sr(0x18);			// CPU off, GIE on
} 



