	.arch msp430g2553
	.p2align 1,0
	.text

;;; Timer Interrupt
;;; interrupt handler for Timer A0
.global __isr_9	
__isr_9:
	push	r15
	push	r14
	push	r13
	push	r12
	pop	r12
	pop	r13
	pop	r14
	pop	r15
	reti


;;; Button Interrupt
;;; interrupt handler for Port one
.global __isr_2
__isr_2:
	push	r15
	push	r14
	push	r13
	push	r12
	call	#p1handler
	pop	r12
	pop	r13
	pop	r14
	pop	r15
	reti


