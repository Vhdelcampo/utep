#include <msp430.h>

// for flashRED state machine
static unsigned char red_state = 0; /* boolean: true = led on */
static unsigned char red_count = 0;
static unsigned char green_state = 0;
static unsigned char green_count = 0;

void timerHandler()
{
  red_count++;
  green_count++;

  if (red_count >= 100) { /* delayed long enough? */
    red_state = 1 - red_state;	/* flip state */
    red_count = 0;               /* reset counter */
    red_led();
  }

  if (green_count >= 25) {
    green_state = 1 - green_state;
    green_count = 0;
    green_led();
  }
}

void red_led()
{
  if (red_state)
    P1OUT |= BIT0;
  else
    P1OUT &= ~BIT0;
}

void green_led()
{
  if (green_state)
    P1OUT |= BIT6;
  else
    P1OUT &= ~BIT6;
}
