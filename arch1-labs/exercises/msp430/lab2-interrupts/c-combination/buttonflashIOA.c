#include <msp430.h>

// for button state machine
#define off_up 0
#define on_dn 1
#define on_up 2
#define off_dn 3
static unsigned char state = off_up;
#define waitUp 0
#define waitDn 1
unsigned char buttonPosition = waitUp;
unsigned char toggle = 0;

// for timer
static unsigned char green_state = 0;
static unsigned char count = 0;
void p1handler()
{
  if (buttonPosition == waitUp) {	
      /* button is only input on P1, so don't need to check P1IN */
      buttonPosition = waitDn;
      P1IES |= BIT3;		
      /* interrupt sensitive to button down */
    } 
  else {			
    /* button UP */
    buttonPosition = waitUp;
    P1IES &= ~BIT3;		/* interrupt sensitive to button up */
  }
  P1IFG &= ~BIT3;
  red_led();
}




void timerHandler()
{
  count++;
  if (!toggle && count >= 100) {
    green_state = 1 - green_state;
    count = 0;
    green_led();
  } else if (toggle && count >= 25)
    {
      green_state = 1 - green_state;
      count = 0;
      green_led();
    }
}


void green_led()
{
  if (green_state)
    P1OUT |= BIT6;
  else
    P1OUT &= ~BIT6;
}


void red_led()
{
  switch (state) {
    case off_up:
      if (buttonPosition == waitUp) {
	P1OUT |= BIT0;
	state = on_dn;
	toggle = 1;
      }
      break;
    case on_dn:
      if (buttonPosition == waitDn) {
	state = on_up;
      }
      break;
    case on_up:
      if (buttonPosition == waitUp) {
	P1OUT &= ~BIT0;
	state = off_dn;
	toggle = 0;
      }
      break;
    case off_dn:
      if (buttonPosition == waitDn) {
	state = off_up;
      }
      break;
    }
}


