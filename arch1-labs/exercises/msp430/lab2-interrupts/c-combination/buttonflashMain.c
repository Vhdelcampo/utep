#include <msp430.h>

// P1 connections:
// P1.0 red led (1 = 0N)
// P1.3 button (zero if down, needs resistor pull up)
// P1.6 green led (1=ON)

void mcOsc1MHz(void) /* set master clock to 1MHz */
{ 
  BCSCTL1 = CALBC1_1MHZ;   // Set range 
  DCOCTL = CALDCO_1MHZ;	   
  BCSCTL2 &= ~(DIVS_3);    // SMCLK = DCO / 8 = 1MHz
} 

void main(void) 
{
  WDTCTL = WDTPW + WDTHOLD;
  mcOsc1MHz();
  CCTL0 = CCIE;
  TACTL = TASSEL_2 + MC_1;  
  CCR0 = 10000;

  P1DIR = BIT0 + BIT6;
  P1REN = BIT3;
  P1OUT = BIT3;
  P1IE = BIT3;
  P1IES = BIT3;
  P1IFG &= ~BIT3;
  or_sr(0x18);
} 



