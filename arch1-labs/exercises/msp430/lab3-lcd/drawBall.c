#include "lcd_utils.h"
#include "drawBall.h"

static u_char last_ball_x = 0, last_ball_y = 0;

static void clearBall(u_char x, u_char y)
{
  u_char yMajor = lcd_getYMajor(y);
  lcd_writeChunkAddr(0, x, yMajor); lcd_writeChunk(0);
  lcd_writeChunkAddr(0, x, yMajor + 1); lcd_writeChunk(0);
}

void drawBall(u_char x, u_char y)
{
  u_char majorYTop = lcd_getYMajor(y);
  u_char majorYBot = lcd_getYMajor(y+1);
  u_char minorYTop = lcd_getYMinor(y);
  u_char minorYBot = lcd_getYMinor(y+1);
  
  // Clear the ball
  clearBall(last_ball_x, last_ball_y);
  
  if (majorYTop == majorYBot) {
    // set the appropriate bits within the upper byte
    u_char chunk = (3 << minorYTop);    
    lcd_writeChunkAddr(chunk, x, majorYTop); lcd_writeChunk(chunk);
  } else {
    // set the appropriate bit for the upper byte
    u_char chunk = (1 << minorYTop);
    lcd_writeChunkAddr(chunk, x, majorYTop); lcd_writeChunk(chunk);
    // set the appropriate bit for the lower byte
    chunk = (1 << minorYBot);
    lcd_writeChunkAddr(chunk, x, majorYBot);  lcd_writeChunk(chunk);

  }
  last_ball_x = x;
  last_ball_y = y;
}
    

