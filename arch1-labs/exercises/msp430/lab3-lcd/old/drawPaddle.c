#include "drawPaddle.h"

#define min(a,b) ((a)<(b)?(a):(b))
#define max(a,b) ((a)>(b)?(a):(b))

#define paddleSize 6		/* # of pixels above and below paddle position middle */

// old paddle top and bottoms
static u_char oldPaddleTopYMajor, oldPaddleBotYMajor;

// masks used to clear bits from chunks
// least significant i bits are 1 in chunkMasks[i] 
static const u_char chunkMasks[9] = {0x0, 0x1, 0x3, 0x7, 0xf, 0x1f, 0x3f, 0x7f, 0xff};

void drawPaddle(u_char yPos)	/* ypos is middle of paddle */
{
  char paddleTopYMajor = lcd_getYMajor(yPos-paddleSize);
  char paddleBotYMajor = lcd_getYMajor(yPos+paddleSize);

  u_char yMajor; /* yMajor row index */
  u_char yMajorLimit = max(paddleBotYMajor, oldPaddleBotYMajor); /* max yMajor affected */

  /* write each affected yMajor exactly once */
  for (yMajor = min(paddleTopYMajor, oldPaddleTopYMajor); /* min yMajor affected */
       yMajor <= yMajorLimit;				  /* max yMajor affected */
       yMajor++) {
    lcd_setAddr(0, yMajor);
    if (yMajor < paddleTopYMajor || yMajor > paddleBotYMajor) 
      lcd_writeChunk(0);	/* erase if above or below paddle */
    else {
      u_char chunk = 0xffu;	       /* default: all pixels on */
      if (yMajor == paddleTopYMajor) { /* top chunk, clear top pixels */
	chunk &= ~chunkMasks[lcd_getYMinor(yPos-paddleSize)];
      }
      if (yMajor == paddleBotYMajor) { /* bot chunk, clear bottom pixels */
	chunk &= chunkMasks[1+lcd_getYMinor(yPos+paddleSize)];
      }
      lcd_writeChunk(chunk);
    }
  }
  oldPaddleBotYMajor = paddleBotYMajor; /* remember state */
  oldPaddleTopYMajor = paddleTopYMajor;
}

