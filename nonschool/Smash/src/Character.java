
public class Character {
	String name;
	int weight;
	int weightRank;
	double fallspeed;
	int fallRank;
	double gravity;
	int airdodge;
	
	public Character(String name, double fallspeed, int weight, double gravity, int airdodge) {
		this.name = name;
		this.fallspeed = fallspeed;
		this.weight = weight;
		this.gravity = gravity;
		this.airdodge = airdodge;
	}
	public Character(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getFallspeed() {
		return fallspeed;
	}
	public void setFallspeed(double fallspeed) {
		this.fallspeed = fallspeed;
	}
	public int getWeight() {
		return weight;
	}
	public void setWeight(int weight) {
		this.weight = weight;
	}
	public double getGravity() {
		return gravity;
	}
	public void setGravity(double gravity) {
		this.gravity = gravity;
	}
	public int getAirdodge() {
		return airdodge;
	}
	public void setAirdodge(int airdodge) {
		this.airdodge = airdodge;
	}
	public int getWeightRank() {
		return weightRank;
	}
	public void setWeightRank(int weightRank) {
		this.weightRank = weightRank;
	}
	public int getFallRank() {
		return fallRank;
	}
	public void setFallRank(int fallRank) {
		this.fallRank = fallRank;
	}
	
}
