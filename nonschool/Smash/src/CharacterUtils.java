import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Arrays;
import java.util.Scanner;

public class CharacterUtils {
	
	public static void readWeightFile(File file, Roster roster) throws FileNotFoundException {
		Scanner fileScanner = new Scanner(file);
		String line;
		
		while (fileScanner.hasNextLine()){
			line = fileScanner.nextLine();
			String[] row = line.split("\t");
			System.out.println(Arrays.toString(row));
			// row[0] = rank, row[1] = name, row[2] = weight
			String name = row[1].trim();
			int rank = Integer.parseInt(row[0]);
			int weight = Integer.parseInt(row[2]);
			Character character = roster.find(name);
			character.setWeight(weight);
			character.setWeightRank(rank);
		}
	}
	
	public static Roster createRoster(File file) throws FileNotFoundException {
		Roster roster = new Roster();
		Scanner in = new Scanner(new FileReader(file));
		
		while (in.hasNextLine()) {
			String name = in.nextLine();
			name = name.trim();
			Character character = new Character(name);
			// System.out.println(character.getName());
			roster.addCharacter(character);
		}
		
		return roster;
	}
	
}
