import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Main {
	public static void main (String args[]) throws FileNotFoundException {
		File rosterFile = new File("roster.txt");
		File weightFile = new File("weight.txt");
		Roster roster = CharacterUtils.createRoster(rosterFile);
		// roster.print();
		CharacterUtils.readWeightFile(weightFile, roster);
	}	
}
