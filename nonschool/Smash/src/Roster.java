
public class Roster {
	Character[] roster;
	int size;
	final int ROSTER_SIZE = 58;
	int index = 0;
	
	public Roster(){
		this.roster = new Character[ROSTER_SIZE];
		this.size = ROSTER_SIZE;
	}
	
	public void addCharacter(Character character) {
		roster[index] = character;
		index++;
	}
	
	public Character find(String name) {
		for (int i = 0; i < this.size; i++) {
			Character character = roster[i];
			System.out.println(character.getName());
			System.out.println(name);
			if (character.getName() == name) {
				return character;
			}
		}
		return null;
	}
	
	public void print() {
		for (Character character : roster) {
			System.out.println(character.getName());
		}
	}
}
