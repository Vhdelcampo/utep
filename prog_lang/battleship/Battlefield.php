<?php
include_once "Battleship.php";
include_once "Place.php";
/**
 * A game board consisting of >size * size places
 * where battleships can be placed. A place of the board is denoted 
 * by a pair of 1-based indices (x, y), where x is the column index 
 * and y is the row index. A place of the board can be shot, resulting 
 * in either a hit (ship) or mishit. 
 */
class BattleField {
    private $size;
    private $numOfShots;
    private $places = [];
    private $ships = [];
    
    public function __construct($size) {
        $DEFAULT_SHIPS = array(
            new Battleship("Aircraft carrier", 5),
            new Battleship("Battleship", 4),
            new Battleship("Frigate", 3),
            new Battleship("Submarine", 3),
            new Battleship("Minesweeper", 2)
        );
        $this->Battlefield($size, $DEFAULT_SHIPS);
    }

    /**
     * Create an empty board of the given dimension that can host the given
     * fleet of ships. The board size must be at least equal to the largest
     * ship size.
     */
    public function Battlefield($size, $ships) {
        $this->size = $size;
        $this->numOfShots = 0;
        for ($x = 1; $x <= $size; $x++) {
            for ($y = 1; $y <= $size; $y++) {
                $this->places[] = new Place($x, $y, $this);
            }
        }
        $this->ships = $ships;               
    }

    /**
     * Remove all the battleships placed on this board. This method should
     * be called before this board is reused for another play.
     */
    public function reset() {
        $numOfShots = 0;
        // places.stream().forEach(p -> p.reset());
        foreach ($places as $p) {
            $p->reset();
        }
    }
    
    /**
     * Place the given ship at the specified starting position horizontally 
     * or vertically. It is assumed the given ship is not already placed.
     */
    public function placeShip($x, $y, $dir, $ship) {
        $len = $ship->size();
        if ($dir // horizontal
            && ($x > 0) && ($x + $len-1 <= $this->size)
            && $this->placesAreEmpty($x, $y, $len, $dir)) {
            $this->setShip($x, $y, $len, $dir, $ship);
            return true;
        }
        if (!$dir // vertical
            && ($y > 0) && ($y + $len - 1 <= $this->size)
            && $this->placesAreEmpty($x, $y, $len, $dir)) {
            $this->setShip($x, $y, $len, $dir, $ship);
            return true;
        }
        return false;
    }

    // Checks that the places where ship is to be set are empty
    public function placesAreEmpty($x, $y, $len, $dir) {
        $j=0; $p = $this->places[$j];
        // Search for initial x,y coordinates
        while (!($p->getX()==$x && $p->getY()==$y)) {
            $j++;
            $p = $this->places[$j];
        }
        // Search each place where ship should be and return false if not empty
        for ($i=0; $i<$len; $i++) {
            if ($dir) {
                // Increments of 10 give the next row with same column
                $p = $this->places[$j+$i*10];
            }
            else {
                $p = $this->places[$j+$i];
            }
            if ($p->hasShip()) {return false;}
        }
        return true;
    }

    // Sets a ship by adding the ship to the Battlefield places array
    // and including the ship in each place.
    public function setShip($x, $y, $len, $dir, $ship) {
        $j=0; $p = $this->places[$j];
        // Search for initial x,y coordinates
        while (!($p->getX()==$x && $p->getY()==$y)) {
            $j++;
            $p = $this->places[$j];
        }
        // Place ship starting at x,y and ending with x+len or y+len coord
        for ($i=0; $i<$len; $i++) {
            if ($dir) {
                // Increments of 10 give the next row with same column
                $p = $this->places[$j+$i*10];
            } else {
                $p = $this->places[$j+$i];
            }
            $p->placeShip($ship);
        }
    }
    
    public function places() {
        return $this->places;
    }
    
    public function ships() {
        return $this->ships;
    }

    /**
     * Return the ship of the given name that can be placed in this board.
     * If there is no such a ship, return a null.
     */
    public function ship($name) {
        foreach($this->ships as $s) {
            if ($s->name() === $name) {return $s;}
            return null;
        }
    }

    /**
     * Return the place at the given indices. Return a null if indices are
     * invalid.
     */
    public function placeAt($x, $y) {
        foreach ($this->places as $p) {
            if ($p->getX()==$x && $p->getY()==$y) {
                return $p;
            }
        }
        return null;
    }

    public function size() {
        return $this->size;
    }

    public function numOfShots() {
        return $this->numOfShots;
    }

    // Return true if all ships are sunk. 
    public function isGameOver() {
        foreach($ships as $s) {
            if (!$s->isSunk()) {return true;}
            return false;
        }
    }
    
    /** Record that the given place is hit. This method will call the
     * hit method on the given place if the place is
     * not already marked as hit.
     */
    public function hit($place) {
        if ($place->canHitShip()) {
            $place->hit();
        } else {
            return false;
        }
        $this->numOfShots++;
        notifyHit($place, $numOfShots);

        if ($place->ship()->isSunk()) {
            notifyShipSunk($place.ship());
            if (isGameOver()) {
                notifyGameOver($numOfShots);
            }
        }
    }
}

?>