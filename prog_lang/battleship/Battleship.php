<?php
include_once "Place.php";
include_once "Battlefield.php";
/**
 * A battle ship that can be placed in a game board. A battle ship has 
 * a name and a size representing the number of sections of the ship.
 */
class Battleship {
    private $name;
    private $size;
    private $places = []; // Places in the board where this ship is placed on.

    /**
     * Create a new ship of the given name and size. Initially, the 
     * created ship is not placed in a board. 
     **/
    public function __construct($name, $size) {
        $this->name = $name;
        $this->size = $size;
        $this->places = array($size);
    }

    public function name() {
        return $this->name;
    }

    public function size() {
        return $this->size;
    }

    public function head() {
        return $places[0];
    }

    public function tail() {
        return $places[$places.size() - 1];
    }

    public function isHorizontal() {
        return head()->getY() == tail()->getY();
    }

    public function isVertical() {
        return head()->getX() == tail()->getX();
    }

    public function places() {
        return $this->places;
    }

    public function isSunk() {
        if (!$size == $places->size()) {return false;}
        foreach ($places as $val) {
            if (!$val->isHit()) {return false;}
        }
        return true;
    }

    public function addPlace($place) {
        $this->places[] = $place;
    }

    /**
     * Remove the given place from the set of places on which this ship is
     * placed. This method should be used only from a place to break a mutual
     * recursive link between a place and a ship; see the reset method of the
     * Place class. 
     */
    public function removePlace($place) {
        $places->remove($place);
    }
    
    public function isDeployed() {
        return !$places->isEmpty();
    }

    public function toString() {
        // Ex. String {"name": "Aircraft carrier", "size": 5}
        $name = $this->name;
        $size = $this->size;
        $s = "&emsp;&emsp;{".'"name": "'.$name.'", "size": '.$size.'}'."<br/>";
        return $s;
    }
}
?>