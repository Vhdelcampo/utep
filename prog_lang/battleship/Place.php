<?php
include_once "Battleship.php";
include_once "Battlefield.php";

class Place {
    private $x;
    private $y;
    private $isHit;
    private $ship; /** Battleship placed on this place; null if no ship is placed. */
    private $battleField; /** Board which this place belongs to. */

    public function __construct($x, $y, $battleField) {
        $this->x = $x;
        $this->y = $y;
        $this->battleField = $battleField;
    }

    public function getX() {
        return $this->x;
    }
    
    public function getY() {
        return $this->y;
    }
    
    public function isHit() {
        return $this->isHit;   
    }
    
    public function ship() {
        return $this->ship;
    }

    public function canHitShip() {
        return !$this->isHit && !$this->isEmpty();
    }
    
    public function hit() {
        $isHit = true;
    }

    public function hasShip() {
        return $this->ship != null;
    }

    public function isEmpty() {
        return $this->ship == null;
    }

    public function placeShip($ship) {
        $this->ship = $ship;
        $ship->addPlace($this);
    }

    /**
     * Reset this place. This method clears the shot made on this place 
     * and remove the ship placed on it.
     */
    public function reset() {
        $isHit = false;
        if ($ship != null) {
            $ship->removePlace($this);
            $ship = null;
        }
    }
}
?>