<?php
include_once "Battlefield.php";
include_once "Battleship.php";
include_once "Place.php";
include_once "info.php";

$field; // global battlefield variable

$url = $_SERVER[REQUEST_URI];
$path = parse_url($url, PHP_URL_PATH);
$query = parse_url($url, PHP_URL_QUERY);

if ($path === '/new') {
    $string = newGame($query);
    echo $string;
}
else if ($path === '/play') {
    $string = playGame($query);
    echo $string;
}

// Used to create a board given a new? query
function newGame($query) {
    parse_str($query);
    $valid_query = isValidGameQuery($strategy, $ships, $message);
    $valid_placement = placeShips($ships, $message);
    $boolean_string = true ? 'true' : 'false'; // make booleans display as string
    if ($valid_query && $valid_placement) {
        $string='{"response": '."$boolean_string".', "pid": "'."$message".'"}';
    } else {
        $string='{"response": '."$boolean_string".', "reason": "'."$message".'"}';
    }
    return $string;
}

// Used to hit a ship given play? query
function playGame($query) {
    parse_str($query);
    global $field;
    $field->hit($x, $y);
}

function isValidGameQuery($strategy, $ships, &$message) {
    if ($strategy=='' && $ships=='') {
        $message = "No parameters given";
        return false;
    } else if ($strategy == '') {
        $message = 'Strategy not specified';
        return false;
    } else if ($strategy!='Smart' && $strategy!='Random' && $strategy!='Sweep') {
        $message = 'Unknown strategy';
        return false;
    } else if (!is_string($ships)) {
        $message = "Ships have not been initialized correctly";
        return false;
    } else {
        $message = uniqid();
        return true;
    }
}

function placeShips($ships, &$message) {
    $ship_string_array = explode(";", $ships); // Grab each ship as a string

    // Start reading string and from it create 5 Battleship objects
    foreach ($ship_string_array as $s) {
        $params = explode(",", $s); // Grab each parameter of the ship as a string
        $name=$params[0];  $x=$params[1]; $y = $params[2]; $dir=$params[3];
        $ship_object_array[] = new Battleship($name, getShipLength($name));
        $x_arr[] = $x; $y_arr[] = $y; $dir_arr[] = $dir;
    }  
    global $field; $field = new Battlefield(10, $ship_object_array);
    $n = count($field->ships());

    // Start placing each ship in field and check if it worked
    for ($i=0; $i<$n; $i++) {
        $can_place = $field->placeShip($x_arr[$i], $y_arr[$i], $dir_arr[$i], $ship_object_array[$i]);
        if (!$can_place) {
            $message = "Invalid ship placements";
            return false;
        }
    }
    return true;
}

function getShipLength($name) {
    switch ($name) {
    case "Aircraft carrier":
        return 5;
    case "Battleship":
        return 4;
    case "Frigate":
        return 3;
    case "Submarine":
        return 3;
    case "Minesweeper":
        return 2;
    default:
        return 0;
    }
}

?>