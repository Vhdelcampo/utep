package edu.utep.cs.cs3331.battleship;

public abstract class Constants {

    public static final String SOUND_DIR = "/sounds/";
    public static final String BOMB_SOUND = "bomb.wav";
    public static final String OPPONENT_BOMB_SOUND = "bomb2.wav";
    public static final String EMERGENCY_SOUND = "emergency.wav";

}
