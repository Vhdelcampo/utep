package edu.utep.cs.cs3331.battleship;

import java.io.IOException;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import static edu.utep.cs.cs3331.battleship.Constants.*;

public abstract class Utils {

    /**
     * Play the given audio file. Inefficient, as the file will be (re)loaded
     * each time it is being played.
     */
    public static void playAudio(String filename) {
        try {
            AudioInputStream audioIn = AudioSystem
                    .getAudioInputStream(Utils.class.getResource(SOUND_DIR
                            + filename));
            Clip clip = AudioSystem.getClip();
            clip.open(audioIn);
            clip.start();
        } catch (UnsupportedAudioFileException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (LineUnavailableException e) {
            e.printStackTrace();
        }
    }
}
