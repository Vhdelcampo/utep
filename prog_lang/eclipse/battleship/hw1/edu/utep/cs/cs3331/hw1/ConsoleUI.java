package edu.utep.cs.cs3331.hw1;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.InputMismatchException;
import java.util.Scanner;

import edu.utep.cs.cs3331.battleship.model.BattleField;
import edu.utep.cs.cs3331.battleship.model.Battleship;
import edu.utep.cs.cs3331.battleship.model.Place;
import static edu.utep.cs.cs3331.battleship.Constants.*;
import static edu.utep.cs.cs3331.battleship.Utils.*;

/**
 * The UI of the game to interact with the player through a pair of 
 * an input stream and a print stream such as <code>System.in</code> and
 * <code>System.out</code>.
 * 
 * @see GameUI
 */
public class ConsoleUI implements GameUI {

    /** Model of the game. */
    private final BattleField battleField;

    /** To get inputs from the player. */
    private final Scanner scanner;

    /** To show responses to the player. */
    private final PrintStream out;

    /**
     * Create a new game UI.
     * 
     * @param battleField
     *            Model of the game.
     * @param in
     *            Input stream to read user inputs.
     * @param out
     *            Print stream to show responses and game results.
     */
    public ConsoleUI(BattleField battleField, InputStream in, 
            PrintStream out) {
        this.battleField = battleField;
        scanner = new Scanner(in);
        this.out = out;
        battleField.addBoardChangeListener(createBoardChangeListener());
    }

    /**
     * Create a new game UI that interacts with the player through
     * <code>System.in</code> and <code>System.out</code>.
     * 
     * @param battleField
     *            Model of the game.
     */
    public ConsoleUI(BattleField battleField) {
        this(battleField, System.in, System.out);
    }

    /** Overridden here to print the initial battle field. */
    public void showSplash() {
        printBattleField();
    }

    /** Return the model of the game. */
    public BattleField battleField() {
        return battleField;
    }

    /** Prompt the user and return the next place to shoot. */
    public Place placeToShoot() {
        int x = 0;
        int y = 0;
        while (true) {
            do {
                out.print("Place to shoot (x y)? ");
                out.flush();
                try {
                    x = scanner.nextInt();
                    y = scanner.nextInt();
                } catch (InputMismatchException e) {
                }
                scanner.nextLine();
            } while (x < 1 || x > battleField.size() || y < 1
                    || y > battleField.size());
            if (!battleField.at(x, y).isHit()) {
                break;
            }
            out.println("Place already hit!");
        }
        return battleField.at(x, y);
    }

    /** Return a listener to handle changes in the game model. */
    protected BattleField.BoardChangeListener createBoardChangeListener() {
        return new BattleField.BoardChangeListener() {
            public void hit(Place place, int numOfShots) {
                playAudio(BOMB_SOUND);
                printBattleField();
                out.println("Number of shots: " + numOfShots);
            }

            public void gameOver(int numOfShots) {
                out.println("All ships destroyed in " + numOfShots + " shots!");
            }

            public void shipSunk(Battleship ship) {
                playAudio(EMERGENCY_SOUND);
            }
        };
    }

    /** Print a 2-d grid representation of the battle field. */
    private void printBattleField() {
        StringBuilder buf = new StringBuilder();
        int size = battleField.size();

        // column header, i.e., 1 2 3 ...
        buf.append("  ");
        for (int x = 1; x <= size; x++) {
            buf.append(String.format("%4d", x));
        }
        buf.append(System.lineSeparator());

        for (int y = 1; y <= size; y++) {
            buf.append(String.format("%2d", y) + " |");
            for (int x = 1; x <= size; x++) {
                Place p = battleField.at(x, y);
                String marker = p.isHit() ? 
                        (p.isEmpty() ? " X |" : " O |") : "   |";
                buf.append(marker);
            }
            buf.append(System.lineSeparator());
        }
        out.println(buf.toString());
    }

}
