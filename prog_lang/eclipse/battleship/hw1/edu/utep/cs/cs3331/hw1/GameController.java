package edu.utep.cs.cs3331.hw1;

import java.util.Random;
import edu.utep.cs.cs3331.battleship.model.BattleField;
import edu.utep.cs.cs3331.battleship.model.Battleship;

/** A view-dependent game controller that allows a player to play one round
 * of the game. The game controller interacts with the player through 
 * a {@link GameUI} interface.
 * 
 * @see GameUI
 */
public class GameController {

    /** To deploy ships randomly. */
    private final static Random random = new Random();

    /** UI of the game to interact with the player. */
    private final GameUI ui;

    /** Model of the game. */
    private final BattleField battleField;

    /**
     * Create a new game controller that uses a default, console-based UI.
     */
    public GameController() {
        this(new ConsoleUI(new BattleField(10), System.in, System.out));
    }

    /** Create a new game controller with the given UI. */
    public GameController(GameUI ui) {
        this.ui = ui;
        this.battleField = ui.battleField();
    }

    /** Play a round of the game. This is the main entry point of the game. */
    public void play() {
        placeShips();
        ui.showSplash();
        while (!battleField.isGameOver()) {
            ui.placeToShoot().hit();
        }
    }

    /** Place ships at arbitrary positions. */
    private void placeShips() {
        int size = battleField.size();
        for (Battleship ship : battleField.ships()) {
            int x = 0;
            int y = 0;
            boolean dir = false;
            do {
                x = random.nextInt(size) + 1;
                y = random.nextInt(size) + 1;
                dir = random.nextBoolean();
            } while (!battleField.placeShip(ship, x, y, dir));
        }
    }

}
