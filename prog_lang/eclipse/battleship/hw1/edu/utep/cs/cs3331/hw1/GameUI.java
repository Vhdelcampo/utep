package edu.utep.cs.cs3331.hw1;

import edu.utep.cs.cs3331.battleship.model.BattleField;
import edu.utep.cs.cs3331.battleship.model.Place;

/** The UI of the game to interact with the player. The methods declared 
 * in this interface will be invoked by the game controller. 
 * 
 * @see GameController 
 */
public interface GameUI {

    /**
     * Show the initial screen of the game. This method will be invoked 
     * by the controller at the start of the game.
     */
    void showSplash();

    /**
     * Return the next place to shoot. This method will be invoked by the
     * controller to obtain the next place to shoot from the player.
     */
    Place placeToShoot();

    /** Return the battle field model of the game. */
    BattleField battleField();
}
