package edu.utep.cs.cs3331.hw1;

public class Main {

    public static void main(String[] args) {
        GameController game = new GameController();
        game.play();
    }
}
