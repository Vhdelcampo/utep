package edu.utep.cs.cs3331.hw1;

import java.util.Random;

import edu.utep.cs.cs3331.battleship.model.BattleField;
import edu.utep.cs.cs3331.battleship.model.Place;

/** Test the game by continually providing arbitrary places to shoot. */
public class MainTest {

    private static final int SHOT_INTERVAL = 500; // 0.5 sec

    public static void main(String[] args) {
        GameUI ui = new AutoConsoleUI(new BattleField(10));
        GameController game = new GameController(ui);
        game.play();
    }

    /** Console-based UI that provides a place to shoot randomly. */
    private static class AutoConsoleUI extends ConsoleUI {
        private BattleField battleField;
        private final Random random = new Random();

        public AutoConsoleUI(BattleField battleField) {
            super(battleField);
            this.battleField = battleField;
        }

        public Place placeToShoot() {
            // wait SHOT_INTERVAL mili-sec
            try {
                Thread.sleep(SHOT_INTERVAL);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            // pick an arbitrary place
            int size = battleField.size();
            int x = 0;
            int y = 0;
            do { // TODO: not efficient when few empty places exist
                x = random.nextInt(size) + 1;
                y = random.nextInt(size) + 1;
            } while (battleField.at(x, y).isHit());
            return battleField.at(x, y);
        }
    }
}
