// @author Victor Del Campo
package battleship;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.lang.reflect.Field;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.KeyStroke;

import battleship.model.BattleBoard;
import battleship.model.Place;
import static battleship.Constants.*;

public aspect Cheat {
	private static boolean cheatEnabled;
	public static void toggleCheat() {
		cheatEnabled = !cheatEnabled;
	}

	after(BattleBoardPanel b, Graphics g): this(b) && args(g) && execution(void paint(..)){
		if(cheatEnabled){
			revealShips(b, g);
		}
	}
	
	before(BattleBoardPanel b): this(b) && execution(BattleBoardPanel.new(..)) {
		addKeybind(b);
	}

	private void revealShips(BattleBoardPanel b, Graphics g) {

		try {
			Field bp = b.getClass().getDeclaredField("battleBoard");
			bp.setAccessible(true);
			BattleBoard board = (BattleBoard) bp.get(b);

			Field pl =  board.getClass().getDeclaredField("places");
			pl.setAccessible(true);
			@SuppressWarnings("unchecked")
			List<Place> places = (List<Place>)pl.get(board);

			final Color oldColor = g.getColor();
			final Color hiddenColor = new Color(255, 128, 128);
			for (Place p: places) {
				if (p.hasShip()) {
					int x = DEFAULT_LEFT_MARGIN + (p.getX() - 1) * DEFAULT_PLACE_SIZE;
					int y = DEFAULT_TOP_MARGIN + (p.getY() - 1) * DEFAULT_PLACE_SIZE;
					g.setColor(p.isHit() ? DEFAULT_HIT_COLOR : hiddenColor);
					g.fillRect(x + 1, y + 1, DEFAULT_PLACE_SIZE - 1, DEFAULT_PLACE_SIZE - 1);
				}
			}
			g.setColor(oldColor);

		}
		catch(NoSuchFieldException e){}
		catch(SecurityException e){}
		catch(IllegalArgumentException e){}
		catch(IllegalAccessException e){}
	}

	@SuppressWarnings("serial")
	private void addKeybind(BattleBoardPanel b) {
		InputMap im = b.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
		ActionMap am = b.getActionMap();
		im.put(KeyStroke.getKeyStroke("F5"), "revealShips");
		am.put("revealShips", new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				toggleCheat();
				b.repaint();
			}
		});
	}
}
