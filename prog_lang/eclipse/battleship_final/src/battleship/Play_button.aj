// @author John Ramirez
import java.awt.event.ActionEvent;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import battleship.BattleshipDialog;

public aspect Play_button {

	// private Player_Fleet_Info player_fleet = new Player_Fleet_Info();
	private JButton new_play_button;
	private BattleshipDialog human_battlefield;
	private boolean if_player_window = true;

	pointcut changePlayButton(JPanel buttonPanel, JButton old_play_button): call(* javax.swing.JPanel.add(*)) && 
	args(old_play_button) && target(buttonPanel)  && within(battleship.BattleshipDialog);

	after(JPanel buttonPanel, JButton old_play_button): changePlayButton(buttonPanel, old_play_button){
		if(if_player_window){
			old_play_button.setText("Practice");
			new_play_button = new JButton();
			new_play_button.setText("Play");
			new_play_button.addActionListener(this::newPlayButtonClicked);
			buttonPanel.add(new_play_button);
			if_player_window = false;
		}
		else {
			buttonPanel.remove(old_play_button);
		}
	}

	private void newPlayButtonClicked(ActionEvent event){
		if (JOptionPane.showConfirmDialog( 
				new_play_button, "Play a new game against the CPU?", "Battleship", JOptionPane.YES_NO_OPTION)
				== JOptionPane.YES_OPTION){
			create_Windows();
		}
	}

	private void create_Windows(){
		human_battlefield = new BattleshipDialog();
		human_battlefield.setTitle("Player Fleet");
		human_battlefield.setVisible(true);
		human_battlefield.setLocation(900, 175);
		human_battlefield.setEnabled(false);
		// player_fleet.showShips(human_battlefield);
	}
	
	public BattleshipDialog getPlayerBattlefield(){
		return human_battlefield;
	}
	
}
