// @author John Ramirez
package battleship;
import battleship.model.Place;
import battleship.model.BattleBoard;
import java.io.*;
import sun.audio.*;

public aspect Shot_sound {
	final String hit_miss_sound_filepath = "Sound/Water Explosion Sound Effect.wav";
	final String hit_sound_filepath = "Sound/Explosion sound effect.wav";
	final String hit_sunk_sound_filepath = "Sound/The Wilhelm scream sound effect.wav";
	
	pointcut playHitSong(Place place): call(void BattleBoard.hit(Place)) && args(place);
	after(Place place): playHitSong(place){
		if(place.hasShip()){
			play(hit_sound_filepath);
			if(place.ship().isSunk()){
				play(hit_sunk_sound_filepath);
			}
		}
		else {
			play(hit_miss_sound_filepath);
		}
	}

	public void play(String soundFile){
		new Thread(){
			public void run(){
					File sound = new File(soundFile);
					try {
						InputStream in = new FileInputStream(sound);
						AudioStream as = new AudioStream(in);
						AudioPlayer.player.start(as);
					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			}
		}.start();
	}
}
