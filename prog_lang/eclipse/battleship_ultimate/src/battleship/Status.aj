// @author Victor Del Campo
package battleship;

import java.awt.BorderLayout;
import java.awt.ComponentOrientation;
import java.awt.FlowLayout;
import java.lang.reflect.Field;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

import battleship.model.BattleBoard;
import battleship.model.Battleship;
import static battleship.Constants.*;

public aspect Status {

	pointcut addButton():
		execution(JPanel battleship.BattleshipDialog.makeControlPane());

	JPanel around(BattleshipDialog dialog): this(dialog) && addButton(){
		JPanel content = proceed(dialog);
		try {
			Field bo = dialog.getClass().getDeclaredField("board");
			bo.setAccessible(true);
			BattleBoard board = (BattleBoard) bo.get(dialog);
			Field sh =  board.getClass().getDeclaredField("ships");
			sh.setAccessible(true);
			@SuppressWarnings("unchecked")
			List<Battleship> ships = (List<Battleship>)sh.get(board);

			JPanel statusScreen = new JPanel(new FlowLayout(FlowLayout.LEADING));
			statusScreen.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
			JPanel shipPane = new JPanel();
			shipPane.setLayout(new BoxLayout(shipPane, BoxLayout.PAGE_AXIS));

			for (Battleship s : ships) {
				shipPane.add(new JLabel(s.name()));
				JPanel ship = new BattleBoardPanel(board, 0, 0, 18, 
						DEFAULT_BOARD_COLOR, DEFAULT_HIT_COLOR, DEFAULT_BOARD_COLOR);
				shipPane.add(ship);
				ship.repaint();
			}

			statusScreen.add(shipPane, BorderLayout.LINE_START);
			content.add(statusScreen, BorderLayout.CENTER);

		}
		catch(NoSuchFieldException e){}
		catch(SecurityException e){}
		catch(IllegalArgumentException e){}
		catch(IllegalAccessException e){}
		return content;
	}
}
