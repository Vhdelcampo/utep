<?xml version="1.0" encoding="UTF-8"?>
<project name="battleship" basedir="." default="jar">
    <property name="ajrt.dir"   value="C:/User/Victor/eclipse"/>
    <property name="ajrt.jar"   value="org.aspectj.runtime_1.8.6.20150608154244.jar"/>
    <property name="class.dir"  value="bin"/>
    <property name="jar.file"   value="${ant.project.name}.jar"/>
    <property name="main-class" value="battleship.BattleshipDialog"/>

    <target name="jar">
        <jar destfile="${jar.file}" basedir="${class.dir}">
            <zipfileset src="${ajrt.dir}/${ajrt.jar}" excludes="META-INF/*"/>
            <manifest>
                <attribute name="Main-Class" value="${main-class}"/>
            </manifest>
        </jar>
    </target>
</project>