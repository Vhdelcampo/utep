
public class Character {
	private String name;
	private double weight;
	private double fall;
	private double gravity;
	
	public Character(String name, double weight, double fall) {
		this.name = name;
		this.weight = weight;
		this.fall = fall;
	}
	
	public String getName() {
		return name;
	}
	public double getWeight() {
		return weight;
	}
	public double getFall() {
		return fall;
	}
	public double getGravity() {
		return gravity;
	}
}
