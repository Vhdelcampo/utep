import java.util.*;

public class sheildstun {

	public static void main(String[] args) {
		for(int i=1; i<=30; i++) {
			int n = newSheildstun(i);
			int p = projectileStun(i);
			System.out.println(i + "\t" + n + "\t" + p);
		}
	}
	
	public static int newSheildstun(double dmg) {
		return (int) Math.floor(dmg / 1.75 + 2);
	}
	
	public static int projectileStun(double dmg) {
		return (int) Math.floor((dmg / 3.5) + 2);
	}

}
