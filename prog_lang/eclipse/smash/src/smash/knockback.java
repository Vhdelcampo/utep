package smash;
import java.lang.Math;

public class knockback {
	public static void main(String args[]) {
		double percent = 9;
		double weight = 79;
		double gravity = .19;
		double luigiFair = calculateKnockback(
				8, percent, weight, 30, 80, 45, gravity);
		double luigiDThrow = calculateKnockback(
				6, percent, weight, 55, 83, 80, gravity);
		double luigiFThrow = calculateKnockback(
				9, percent, weight, 60, 65, 45, gravity);
//		System.out.println(luigiFair);
//		System.out.println(calculateHitstun(luigiFair));
		System.out.println("luigi dthrow KB: " + luigiDThrow);
		System.out.println("luigi dthrow hitstun: " + calculateHitstun(luigiDThrow));
		System.out.println("luigi fthrow KB: " + luigiFThrow);
		System.out.println("luigit fthrow hitstun: " + calculateHitstun(luigiFThrow));
	}

	public static double calculateKnockback(
			double p, double d, double w,
			double b, double s, double a,
			double g){
		p += d;
		s /= 100;
		double KB = (((((p/10 + p*d/20)*200/(w+100)*1.4)+18)*s)+b);
		double angle = Math.toRadians(a);
		double xComponent1 = Math.cos(angle)*KB;
		double yComponent1 = Math.sin(angle)*KB;
//		System.out.println("xcomp1: " + xComponent1 + " ycomp1: " + yComponent1);
		double xComponent2 = 0;
		double yComponent2 = gravity(g);
//		System.out.println("xcomp2: " + xComponent2 + " ycomp2: " + yComponent2);
		double rx = xComponent1 + xComponent2;
		double ry = yComponent1 + yComponent2;
		double newKB = Math.sqrt(Math.pow(rx, 2) + Math.pow(ry, 2));
//		System.out.println("rx: " + rx + " ry: " + ry);
//		double final_angle = Math.atan(ry/rx);
//		final_angle = Math.toDegrees(final_angle);
//		System.out.println("final_angle: " + final_angle);
		return newKB;
	}

	public static double calculateKnockback2(
			double p, double d, double w,
			double b, double g){
		p += d;
		g /= 100;
		return 	g*(((14*p*(d+2))/(w+100))+18)+b;
	}
	
	public static double gravity(double g) {
		return (g - .075) * 5;
	}

	public static double calculateHitstun(double knockback) {
		return knockback*.4;
	}
}
