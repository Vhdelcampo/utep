import java.io.*;
import java.util.Arrays;

public class weight_knockback {
	
	public static void main(String[] args) {
		String[][] weight_table = fileToArray("weight_list");
		String[][] fall_table = fileToArray("fall_list");
//		getAvgHeavyweight(weight_table);
//		getAvgMediumweight(weight_table);
//		getAvgLightweight(weight_table);
//		getAvgFastfall(fall_table);
//		getAvgMediumfall(fall_table);
//		getAvgFloaty(fall_table);
		System.out.println("mediumweight mediumfall: " + getAvgWeightAndMediumFall(weight_table, fall_table)[0]);
		System.out.println(getAvgFallAndMediumweight(weight_table, fall_table)[0]);
		System.out.println(getAvgWeightAndFastfall(weight_table, fall_table)[0]);
		System.out.println(getAvgWeightAndFloaty(weight_table, fall_table)[0]);
		System.out.println(getAvgFallAndHeavyweight(weight_table, fall_table)[0]);
		System.out.println(getAvgFallAndLightweight(weight_table, fall_table)[0]);
	}
	
	public static String[][] fileToArray(String file_name) {
		String[][] table = new String[53][2];
		try(BufferedReader br = new BufferedReader(new FileReader("./src/" + file_name))) {
			String line = br.readLine();
			int row_i = 0;
			
			while (line != null) {
				table[row_i] = line.split("-");
				row_i++;
				line = br.readLine();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return table;
	}
	
	public static boolean isTopTen(String[][] table, String name) {
		for (int i = 0; i < table.length; i++) {
			String[] row = table[i];
			int rank = i;
			if (row[0].equals(name) && rank<=10) {return true;}
		}
		return false;
	}
	
	public static boolean isBotTen(String[][] table, String name) {
		for (int i = 0; i < table.length; i++) {
			String[] row = table[i];
			int rank = i;
			if (row[0].equals(name) && rank>=43) {return true;}
		}
		return false;
	}
	
	public static boolean isMid(String[][] table, String name) {
		for (int i = 0; i < table.length; i++) {
			String[] row = table[i];
			int rank = i;
			if (row[0].equals(name) && rank>=22 && rank <=32) {
				return true;
				}
		}
		return false;
	}
	
	public static int getQ3(String[][] table) {
		int length = table.length;
		return (int) Math.round(length/4);
	}
	
	public static int getMedian(String[][] table) {
		int length = table.length;
		return (int) Math.round(length/2);
	}
	
	public static int getQ1(String[][] table) {
		int length = table.length;
		return (int) Math.round(length/1.33);
	}
	
	public static String[][] getMaxToQ3(String[][] table) {
		return copyOfRange(table, 0, getQ3(table));
	}
	
	public static String[][] getQ3ToMedian(String[][] table) {
		return copyOfRange(table, getQ3(table), getMedian(table));
	}
	
	public static String[][] getMedianToQ3(String[][] table) {
		return copyOfRange(table, getMedian(table), getQ3(table));
	}
	
	public static String[][] getQ1ToMin(String[][] table) {
		return copyOfRange(table, getQ1(table), table.length);
	}
	
	public static String[][] getQ3ToQ1(String[][] table) {
		// System.out.println("q1 is " + getQ3(table) + " " + "q3 is " + getQ1(table));
		return copyOfRange(table, getQ3(table), getQ1(table));
	}
	
	public static String[][] getMaxToMedian(String[][] table) {
		return copyOfRange(table, 0, getMedian(table));
	}
	
	public static String[][] getMedianToMin(String[][] table) {
		return copyOfRange(table, getMedian(table), table.length);
	}
	
	public static String[] getAvgHeavyweight(String[][] table) {
		String[] character = table[getQ3(table)];
		System.out.println(character[0]);
		return character;
	}
	
	public static String[] getAvgMediumweight(String[][] table) {
		String[] character = table[getMedian(table)];
		System.out.println(character[0]);
		return character;
	}
	
	public static String[] getAvgLightweight(String[][] table) {
		String[] character = table[getQ1(table)];
		System.out.println(character[0]);
		return character;
	}
	
	public static String[] getAvgFastfall(String[][] table) {
		String[] character = table[getQ3(table)];
		System.out.println(character[0]);
		return character;
	}
	
	public static String[] getAvgMediumfall(String[][] table) {
		String[] character = table[getMedian(table)];
		System.out.println(character[0]);
		return character;
	}
	
	public static String[] getAvgFloaty(String[][] table) {
		String[] character = table[getQ1(table)];
		System.out.println(character[0]);
		return character;
	}
	
	public static String[][] copyOfRange(String[][] original, int from, int to) {
		int newLength = to - from;
        if (newLength <  0)
            throw new IllegalArgumentException(from + "  > " + to);
        String[][] copy = new String[newLength][2];
        System.arraycopy(original, from, copy, 0,
                         Math.min(original.length - from, newLength));
        return copy;
	}
	
	public static String[] getAvgWeightAndMediumFall(String[][] weight_table, String[][] fall_table) {
		double avg_fall = Double.parseDouble(fall_table[getMedian(fall_table)][1]);
		System.out.println("avg_fall: " + avg_fall);
		return returnAverage(fall_table, weight_table, avg_fall);
	}
	
	public static String[] getAvgWeightAndFastfall(String[][] weight_table, String[][] fall_table) {
		double avg_fall = Double.parseDouble(fall_table[getQ3(fall_table)][1]);
		System.out.println("avg_fastfall " + avg_fall);
		return returnAverage(fall_table, weight_table, avg_fall);
	}
	
	public static String[] getAvgWeightAndFloaty(String[][] weight_table, String[][] fall_table) {
		double avg_fall = Double.parseDouble(fall_table[getQ1(fall_table)][1]);
		System.out.println("avg_floaty: " + avg_fall);
		return returnAverage(fall_table, weight_table, avg_fall);
	}
	
	public static String[] getAvgFallAndMediumweight(String[][] weight_table, String[][] fall_table) {
		double avg_weight = Double.parseDouble(weight_table[getMedian(weight_table)][1]);
		System.out.println("avg_mediumweight: " + avg_weight);
		return returnAverage(weight_table, fall_table, avg_weight);
	}
	
	public static String[] getAvgFallAndHeavyweight(String[][] weight_table, String[][] fall_table) {
		double avg_weight = Double.parseDouble(weight_table[getQ3(weight_table)][1]);
		System.out.println("avg_heavyweight: " + avg_weight);
		return returnAverage(weight_table, fall_table, avg_weight);
	}
	
	public static String[] getAvgFallAndLightweight(String[][] weight_table, String[][] fall_table) {
		double avg_weight = Double.parseDouble(weight_table[getQ1(weight_table)][1]);
		System.out.println("avg_lightweight: " + avg_weight);
		return returnAverage(weight_table, fall_table, avg_weight);
	}

	private static String[] returnAverage(String[][] table1, String[][] table2, double avg_value) {
		String[][] avg_table2 = getQ3ToQ1(table2);
		double min_difference = Integer.MAX_VALUE;
		double difference;
		int length = avg_table2.length;
		int i = (int) length/2;
		String[] most_average_yet = avg_table2[0];
		for (int q = 0; q < length; q++) {
			int index = i + ( q%2 == 0 ? q/2 : -(q/2+1));
			String[] character = avg_table2[index];
			// System.out.println(character[0] + " " + character[1]);
			Double character_fall = getFall(character, table1);
			difference = character_fall - avg_value;
			if (difference == 0) {return character;}
			if (difference < min_difference) {
				most_average_yet = character;
				min_difference = difference;
			}
		}
		return most_average_yet;
	}
	
	public static double getFall(String[] weight_character, String[][] fall_table) {
		for (String [] fall_character : fall_table) {
			if (fall_character[0].equals(weight_character[0])) {
				return Double.parseDouble(fall_character[1]);
			}
		}
		return -1;
	}
	
}
