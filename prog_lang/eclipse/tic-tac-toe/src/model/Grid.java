package model;
import java.util.*;
import model.Place.mark;

public class Grid {
	private final int size;
	private final static int DEFAULT_SIZE = 3;
	private final List<Place> places;
	private mark winner;

	public Grid() {
		this(DEFAULT_SIZE);
	}

	public Grid(int size) {
		this.size = size;
		places = new ArrayList<Place>(size * size);
		for (int x = 0; x < size; x++) {
			for (int y = 0; y < size; y++) {
				places.add(new Place(x, y));
			}
		}
	}

	public int size() {
		return size;
	}
	
	public Iterable<Place> places() {
		return places;
	}
	
	public void reset() {
		places.stream().forEach(p -> p.erase());
	}

	public Place at(int x, int y) {
		for (Place p : places) {
			if (p.x == x && p.y == y) {
				return p;
			}
		}
		return null;
	}

	public void markAt(int x, int y, mark marking) {
		at(x,y).setState(marking);
	}

	/** 
	 *  0,0|0,1|0,2
	 *  ---|---|---
	 *  1,0|1,1|1,2
	 *  ---|---|---
	 *  2,0|2,1|2,2
	 *   
	 *  */
	public void display() {
		for (int x = 0; x < size; x++) {
			for (int y = 0; y < size; y++) {
				System.out.print(" " +  at(x,y).display() + " ");
				if (y != size-1) System.out.print("|");
			}
			System.out.println();
			if (x != size-1) System.out.println("---|---|---");
		}
	}

	public boolean isGameOver() {
		//Check row and columns
		for(int i = 0; i < size; i++) {
			mark win = checkRow(i);
			if(win != mark.EMPTY || (win = checkColumn(i)) != mark.EMPTY) {
				this.winner = win;
				return true;
			}
		}
		//Check diagonal top left to bottom right
		if(this.at(0,0).state() != mark.EMPTY) {
			if(this.at(0,0).state() == this.at(1,1).state() && 
					this.at(1,1).state() == this.at(2,2).state()) {
				this.winner = this.at(0,0).state();
				return true;
			}
		}
		//Check diagonal top right to bottom left
		else if(this.at(0,2).state() != mark.EMPTY) {
			if(this.at(0,2).state() == this.at(1,1).state() && 
					this.at(1,1).state() == this.at(2,0).state()) {
				this.winner = this.at(0,2).state();
				return true;
			}
		}
		return false;
	}

	private mark checkRow(int row) {
		if(this.at(0,0).state() == mark.EMPTY) {
			return mark.EMPTY;
		}
		if(this.at(0,0).state() == this.at(row,1).state() && 
				this.at(row,1).state() == this.at(row,2).state()) 
		{
			return this.at(0,0).state();
		}
		return mark.EMPTY;
	}

	private mark checkColumn(int column) {
		if(this.at(0,column).state() == mark.EMPTY) {
			return mark.EMPTY;
		}
		if(this.at(0,column).state() == this.at(1,column).state() &&
				this.at(1,column).state() == this.at(2,column).state()) {
			return this.at(0,column).state();
		}
		return mark.EMPTY;
	}
}
