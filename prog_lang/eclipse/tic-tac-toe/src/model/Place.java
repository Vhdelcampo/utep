package model;

public class Place {
	/** x and y keeps track of each individual square of grid by starting at
	 * 	top left as 0 to top right as 2, and bottom left as 6 to bottom right as 8.
     /* 
	 *  0,0|0,1|0,2
	 *  ---|---|---
	 *  1,0|1,1|1,2
	 *  ---|---|---
	 *  2,0|2,1|2,2
	 *   
	 *  */
	public int x;
	public int y;

	/** The type of markings on each square are either EMPTY, X, or O */
	public enum mark {
		EMPTY, X, O
	}

	/** Each square defaults to empty */
	private mark state = mark.EMPTY;

	public Place(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public mark state() {
		return state;
	}

	public void setState(mark marking) {
		state = marking;
	}

	public void erase() {
		this.state = mark.EMPTY;
	}

	public mark isEmpty() {
		return state = mark.EMPTY;
	}

	public String display() {
		switch(state) {
		case X : return "X";
		case O : return "Y";
		default : return " ";
		}
	}

}
