data Place = Place Int Int Bool Ship deriving (Show)
data Ship = Ship String Int [Place] deriving (Show)
-- mkBoard :: Int -> [[Int]]
-- mkBoard n = [[] | x <- [1..n], y <- [1..n]]

mkBoard :: Int -> [[Place]]
mkBoard n = [[] | x <- [1..n], y <- [1..n]]

main = print(mkBoard 10)


