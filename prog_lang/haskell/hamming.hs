hamming 1 = True
hamming n
  | n `mod` 2 == 0 = hamming(n `div` 2)
  | n `mod` 3 == 0 = hamming(n `div` 3)
  | n `mod` 5 == 0 = hamming(n `div` 5)
  | otherwise = False                     
main = print(hamming 7)
