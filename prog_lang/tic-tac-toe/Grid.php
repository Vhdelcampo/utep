<?php
include("Place.php");
class Grid {
	private $size;
    private $places;
	private $winner;

	public function __construct() {
		$this->size = 3;
		for ($x = 0; $x < $this->size; $x++) {
			for ($y = 0; $y < $this->size; $y++) {
				$this->places[] = (new Place($x, $y));
			}
		}
    }
	
	public function reset() {
        foreach ($this->places as $p) {
            $p->erase();
        }
	}

	public function at($x, $y) {
        foreach ($this->places as $p) {
            $px = $p->x;
            $py = $p->y;
			if ($px == $x && $py == $y) {
				return $p;
			}
		}
        return null;
	}

	public function markAt($x, $y, $marking) {
		$p = $this->at($x,$y);
        $p->state = $marking;
    }

	/** 
	 *  0,0|0,1|0,2
	 *  ---|---|---
	 *  1,0|1,1|1,2
	 *  ---|---|---
	 *  2,0|2,1|2,2
	 *   
	 *  */
	public function display() {
        for ($x = 0; $x < $this->size; $x++) {
            echo("<br>&emsp;|&emsp;|<br>");
			for ($y = 0; $y < $this->size; $y++) {
                // $p = $this->at($x,$y);
                // $p->display();
                if ($x != 2) {
                    echo("---");
                    if ($y != $this->size-1) echo("|");
                }
			}
        }
        echo("<br>");
    }

	public function isGameOver() {
		//Check row and columns
		for($i = 0; $i < $this->size; $i++) {
			$win = $this->checkRow($i);
			if($win != BLANK || ($win = $this->checkColumn($i)) != BLANK) {
				$this->winner = $win;
				return true;
			}
		}
		//Check diagonal top left to bottom right
		if($this->at(0,0)->state != BLANK) {
			if($this->at(0,0)->state == $this->at(1,1)->state && 
               $this->at(1,1)->state == $this->at(2,2)->state) {
				this.$winner = $this->at(0,0)->state;
                return true;
			}
		}
		//Check diagonal top right to bottom left
		else if($this->at(0,2)->state != BLANK) {
			if($this->at(0,2)->state == $this->at(1,1)->state && 
               $this->at(1,1)->state == $this->at(2,0)->state) {
				this.$winner = $this->at(0,2)->state;
				return true;
			}
		}
		return false;
	}

	private function checkRow($row) {
		if($this->at($row,0)->state == BLANK) {
			return BLANK;
		}
		if($this->at($row,0)->state == $this->at($row,1)->state && 
           $this->at($row,1)->state == $this->at($row,2)->state) 
		{
			return $this->at($row,0)->state;
		}
		return BLANK;
	}

	private function checkColumn($column) {
		if($this->at(0,$column)->state == BLANK) {
			return BLANK;
		}
		if($this->at(0,$column)->state == $this->at(1,$column)->state &&
           $this->at(1,$column)->state == $this->at(2,$column)->state) {
			return $this->at(0,$column)->state;
		}
		return BLANK;
	}
}
?>