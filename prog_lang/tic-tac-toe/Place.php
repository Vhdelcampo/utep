<?php
class Place {
	/** x and y keeps track of each individual square of grid by starting at
	 * 	top left as 0 to top right as 2, and bottom left as 6 to bottom right as 8.
     /* 
	 *  0,0|0,1|0,2
	 *  ---|---|---
	 *  1,0|1,1|1,2
	 *  ---|---|---
	 *  2,0|2,1|2,2
	 *   
	 *  */
	public $x;
	public $y;
    
    /** The type of markings on each square are either BLANK, X, or O */
    const BLANK = 0;
    const CROSS = 1;
    const NAUGHT = 2;
    
   	/** Each square defaults to blank */
	public $state = BLANK;

	public function __construct($x, $y) {
		$this->x = $x;
		$this->y = $y;
	}

	public function erase() {
		$this->state = BLANK;
	}

    public function setState($marking) {
        $this->state = $marking;
    }

	public function isEmpty() {
		return $this->state == BLANK;
	}

	public function display() {
		switch($this->state) {
		case CROSS : echo "X"; break;
		case NAUGHT : echo "O"; break;
		default : echo " ";
		}
	}

}
?>