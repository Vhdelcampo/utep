<?php
include "Grid.php";

static $grid;
$grid = new Grid();

$url = $_SERVER[REQUEST_URI];
$path = parse_url($url, PHP_URL_PATH);
$query = parse_url($url, PHP_URL_QUERY);

if ($path === '/new') {
    $grid = newGame($query);
}

else if ($path === '/play') {
    $string = playGame($query, $grid);
    echo $string;
}

// Used to create a board given a new? query
function newGame($query) {
    parse_str($query);
    $valid_query = isValidGameQuery($strategy, $message);
    $boolean_string = $valid_query ? 'true' : 'false'; // make booleans display as string
    if ($valid_query) {
        echo $string='{"response": '."$boolean_string".'"}';
        return new Grid();
    } else {
        echo $string='{"response": '."$boolean_string".', "reason": "'."$message".'"}';
    }
    return;
}

// http://<battleship-home>/play?x=0&y=0&mark=CROSS
// Used to hit a ship given play? query
function playGame($query, $grid) {
    parse_str($query);
    $grid->markAt($x, $y, $move);
    $grid->display();
    var_dump($grid);
    if($grid->isGameOver()) { echo "game over";}
}

function isValidGameQuery($strategy, &$message) {
    if ($strategy!='Smart' && $strategy!='Random') {
        $message = 'Unknown strategy';
        return false;
    } 
    return true;
}

?>