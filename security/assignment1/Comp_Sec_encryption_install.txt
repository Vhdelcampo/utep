Instructions for enabling unlimited strength encryption and 
install a cryptography provider

(for Windows, as of December 2015)

------------------------------------------------------------------------------------

To enable unlimited strength encryption.

Search "Java Cryptography Extension" on the web. I was sent to web site: http://www.oracle.com/technetwork/java/javase/downloads/jce-7-download-432124.html.
Download "Java Cryptography Extension (JCE) Unlimited Strength Jurisdiction 
Policy Files 7".
Extract the downloaded zip. Following  instructions in README,
I copied the files "local_policy.jar" and "US_export_policy.jar" into 
the "C:\Program Files\Java\jre7\lib\security" directory (replacing the old ones).
This is the version of java I am using. There may be many version of java 
installed on your machine. 
Make sure you copy to the appropriate directory. 
Depending on how you run Java, it may use different directories. 
Search for copies of the .jar files on your computer.

You can test the install with the java program SimplePolicyTest.java. 
Without the unlimited strength, it will pass the 64 bit test, 
but not the 192 bit test.


