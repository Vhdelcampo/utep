/* stack.c */

/* This program has a buffer overflow vulnerability. */
/* Our task is to exploit this vulnerability */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "stack.h"

int main(int argc, char **argv)
{
  char str[517];
  //FILE *badfile;
  // badfile = fopen("badfile", "r");
  // fread(str, sizeof(char), 517, badfile);
  fill(str, 255);
  bof(str, 3);
  
  printf("Returned Properly\n");
  return 1;
}

int bof(char *str, int a)
{
  char buffer[24];
  fill(buffer, 10);
  a = 6;
  printf("printing buffer addr after being filled with value 10\n");
  printAddr(buffer, 15, 15);
  
  /* /\* The following statement has a buffer overflow problem *\/ */
  /* strcpy(buffer, str); */
  /* printf("printing buffer addr after being filled with value 10\n"); */
  /* printAddr(buffer, 15, 15); */
  return 1;
}

void fill(char *str, char val)
{
  int i;
  for (i = 0; i<sizeof(str); i++)
    {
      str[i] = val;
    }
}

void printAddr(char *buffer, int wordsAbove, int wordsBelow)
{
  long *ptr= (long *) buffer;
  int i;
  if (wordsAbove > 0) {printf("%i words above stack pointer:\n", wordsAbove);}
  for (i=wordsAbove; i>0; i--)
    {
      printf("address %08x is %08x\n",
	     (unsigned int) (ptr-i),
	     (unsigned int) (*(ptr-i)));
    }
  printf("stack pointer or &buffer[0]:\n");
  printf("address %08x is %08x\n",
	 (unsigned int) (ptr+i),
	 (unsigned int) (*(ptr+i)));
  if (wordsBelow > 0) {printf("%i words below stack pointer:\n", wordsBelow);}
  for (i=1; i<=wordsBelow; i++)
    {
      printf("address %08x is %08x\n",
	     (unsigned int) (ptr+i),
	     (unsigned int) (*(ptr+i)));
    }
}
