
import java.io.*;
import java.net.*;
import java.security.*;
import java.util.Scanner;
import javax.crypto.*;

class CryptoServer {
// This code is a modification of the SimpleServer.java
// It implements AES encryption through the socket    
// This version used for Computer Security, Spring 2016.

    public static void main(String[] args) {

        File file = new File("AES.key");
        ObjectInputStream inputFile;
        SecretKey secretKey;
        byte[] encryptedByte;
        // reading the AES cryptographic key        
        try {
            inputFile = new ObjectInputStream(new FileInputStream(file));
            secretKey = (SecretKey) inputFile.readObject();
            inputFile.close();
        } catch (IOException | ClassNotFoundException ex) {
            System.out.println(ex);
            return;
        }

        try (ServerSocket s = new ServerSocket(8008)) {
            Cipher cipher = Cipher.getInstance("AES");
            System.out.println("Crypto Server started.");
            Scanner serverInput = new Scanner(System.in);
            while (true) {
                try {
                    System.out.println("Waiting for client connection");
                    Socket incoming = s.accept();
                    System.out.println("Connected to: "
                            + incoming.getInetAddress()
                            + " at port: " + incoming.getLocalPort());
                    // We could use Base64 encoding and communicate with strings like SimpleServer
                    // However, we show here how to send and receive serializable java objects                    
                    ObjectInputStream objectInput = new ObjectInputStream(incoming.getInputStream());
                    ObjectOutputStream objectOutput = new ObjectOutputStream(incoming.getOutputStream());

                    boolean done = false;
                    while (!done) {
                        // Waiting for Client message                
                        System.out.println("Waiting for Client message");
                        encryptedByte = (byte[]) objectInput.readObject();
                        cipher.init(Cipher.DECRYPT_MODE, secretKey);
                        String decryptedMessage = new String(cipher.doFinal(encryptedByte));
                        if (decryptedMessage.trim().equals("BYE")) {
                            System.out.println("Conversation ended, waiting for new connection");
                            done = true;
                        } else {
                            System.out.println("Received: " + decryptedMessage);
                            // Read reply from user
                            System.out.println("Reply to client: ");
                            String userStr = serverInput.nextLine();
                            // sending message
                            objectOutput.writeObject(userStr.getBytes());
                        }
                    }
                } catch (IOException | ClassNotFoundException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException e) {
                    System.out.println("Error: " + e);
                }
            }

        } catch (Exception e) {
            System.out.println("Error: " + e);
            System.out.println("Server stopped");
        }
    }
}
