import java.io.*;
import java.security.*;
import java.util.Base64;

class Sign {

    public static void main(String[] args) {

        File file;
        PrivateKey privKey;
        Signature sig;
        String messageToSign = "Hello!";
        byte[] signature;

        // Read public key from file
        privKey = PemUtils.readPrivateKey("CAPHPprivateKey.pem");
        //pubKey = PemUtils.readPrivateKey("privateK.pem");

        try {
            sig = Signature.getInstance("SHA1withRSA");
            sig.initSign(privKey);
            sig.update(messageToSign.getBytes());
            signature = sig.sign();

        } catch (Exception e) {
            System.out.println("Error attempting to sign");
            return;
        }
        file = new File("signature.txt");
        try {
            PrintWriter output = new PrintWriter(file);
            output.print(Base64.getEncoder().encodeToString(signature));
            output.close();
        } catch (Exception e) {
            System.out.println("Could not create signature file");
        }
    }
}
