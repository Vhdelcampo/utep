import java.io.*;
import java.net.*;
import java.util.Scanner;

// This code originated from Yoonsik Cheon at least 10 years ago.
// It was modified several times by Luc Longpre over the years. 
// This version used for Computer Security, Spring 2015 and 2016.

public class SimpleClient {

    public static void main(String[] args) {

        Scanner userInput = new Scanner(System.in);

        String host;
        if (args.length > 0) {
            host = args[0];
        } else {
            System.out.println("Enter the server's address: (IP address or \"localhost\")");
            host = userInput.nextLine();
        }
        try {
            Socket socket = new Socket(host, 8008);
            BufferedReader in
                    = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            PrintWriter out
                    = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));

            System.out.println("Starting conversation. Type messages, type BYE to end");
            boolean done = false;
            while (!done) {
                // Read message from Client
                String userStr = userInput.nextLine();
                // Send the message to the server
                out.println(userStr);
                out.flush();
                // If user says "BYE", end conversation
                if (userStr.trim().equals("BYE")) {
                    System.out.println("conversation ended");
                    done = true;
                } else {
                    // Wait for reply from server
                    System.out.println("Waiting for reply");
                    System.out.println("Received: " + in.readLine());
                }
            }
        } catch (IOException ex) {
            System.out.println("IOException: "+ex);
        }
    }
}
