import java.io.*;
import java.security.*;
import java.util.Base64;
import java.util.Scanner;

public class Verify {

    public static void main(String[] args) {
        File file;
        PublicKey pubKey;
        String signature = "";
        String messageSigned = "Hello!";

        // Read public key from file
        pubKey = PemUtils.readPublicKey("CAPHPpublicKey.pem");

        // Read signature from file
        try {
            file = new File("signature.txt");
            Scanner input = new Scanner(file);
            signature = input.nextLine();
        } catch (FileNotFoundException ex) {
            System.out.println("Could not open signature file: " + ex);
            return;
        }

        try {
            Signature sig = Signature.getInstance("SHA1withRSA");
            sig.initVerify(pubKey);
            sig.update(messageSigned.getBytes());
            if (sig.verify(Base64.getDecoder().decode(signature))) {
                System.out.println("Signature verification succeeded");
            } else {
                System.out.println("Signature verification failed");
            }
        } catch (Exception e) {
            System.out.println("problem verifying signature: " + e);
        }
    }
}
