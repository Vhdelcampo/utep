import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Cracker {
	private static final int WORD_LIST_SIZE = 10000000;
	private static final int nuser = 10;
	private static final String[] files = {"password-2011.lst", "rockyou.txt"};

	public static void main(String args[]) throws IOException, NoSuchAlgorithmException{
		String wordlistFilename = "wordlist.txt";
		mergeFiles(files, wordlistFilename);
		String[] wordlist = fileToArray(wordlistFilename);
		String[] hashedWordlist = hashStringArrayWithoutSalt(wordlist);
		String[] users = readUsersFromFile("hashedRandomPwd.txt");
		String[] hashedPasswords = readPasswordsFromFile("hashedRandomPwd.txt");
		bruteforce(wordlist, hashedWordlist, users, hashedPasswords);
	}

	private static void bruteforce(String[] wordlist, String[] hashedwordlist, String[] users, String[] hashedPasswords) throws IOException {
		PrintWriter crackedPasswordFile = new PrintWriter(new FileWriter("crackedPasswords.txt")); 
		for (int i=0; i<nuser; i++) {
			for (int j = 0; j < WORD_LIST_SIZE; j++) {
				if (hashedPasswords[i].equals(hashedwordlist[j])) {
					crackedPasswordFile.println(users[i] + ": " + wordlist[j]);
				}
			}
		}
		crackedPasswordFile.close();
	}

	public static String[] generateNumbers() throws IOException {
		String[] numbers = new String[1000];
		for (int i=0; i<1000; i++) {
			StringBuilder sb = new StringBuilder();
			if (i < 10) {
				sb.append(0);
			}
			if (i < 100) {
				sb.append(0);
			}
			sb.append(i);
			numbers[i] = sb.toString();
		}
		return numbers;
	}


	public static String[] readUsersFromFile(String Files) throws IOException {
		String[] users = new String[nuser];
		BufferedReader br = new BufferedReader(new FileReader(Files));
		for (int i = 0; i < nuser; i++) {
			users[i] = br.readLine();
		}
		br.close();
		return users;
	}

	public static String[] readPasswordsFromFile(String Files) throws IOException {
		String[] passwords = new String[nuser];
		BufferedReader br = new BufferedReader(new FileReader(Files));
		for (int i = 0; i < nuser; i++) {
			String line = br.readLine();
			int j = 0;
			char c = line.charAt(j);
			while (c != ':') {
				j++;
				c = line.charAt(j);
			}
			j+= 2; // Skip colon and space
			passwords[i] = line.substring(j);
		}
		br.close();
		return passwords;
	}

	private static String[] hashStringArrayWithoutSalt(String[] wordlist) throws IOException, NoSuchAlgorithmException {
		String[] hashedwordlist = wordlist.clone();
		MessageDigest hash = MessageDigest.getInstance("SHA1");
		for (int i = 0; i < wordlist.length && wordlist[i] != null; i++){
			hash.update(Utils.toByteArray(hashedwordlist[i]));
		}
		return hashedwordlist;
	}

	public static String[] fileToArray(String Files) throws IOException {
		String[] wordlist = new String[WORD_LIST_SIZE];
		BufferedReader br = new BufferedReader(new FileReader(Files));
		String line = br.readLine();

		for (int i=0; i<WORD_LIST_SIZE && line != null; i++) {
			wordlist[i] = line;
			line = br.readLine();
		}
		br.close();
		return wordlist;
	}

	public static void mergeFiles(String[] files, String mergedFilename) throws IOException {
		PrintWriter outfile = new PrintWriter(new FileWriter(mergedFilename));
		for (String file : files) {
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line;
			while ((line = br.readLine()) != null) {
				outfile.println(line);
			}
			br.close();
		}
		outfile.close();
	}
}
