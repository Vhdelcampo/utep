/**
 * This program takes an array of user ID's and an array of passwords
 * and produces a file that contains the message digests
 * for each of the string in the array.
 * There is one line in the output file for each user ID.
 */
import java.io.*;
import java.util.*;
import java.security.MessageDigest;

public class CreatePwdFileRandom
{   
  public static void main(String[] args) throws Exception{
    final int nuser = 10;
    long seed = 1234567890L;
    Random generator = new Random(seed);
    char[] allowed = new char[] {
      'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
        'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
        'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '_', '-'};
//
// Generating random passwords
//
    // first compute a random permutation
    int[] perm = new int[nuser];
    int i;
    for (i=0; i<nuser; i++){
      perm[i]=i+1;
    }
    for (i=0; i<nuser-1; i++){
      // generate a number between 0 and nuser-i-1
      int num = generator.nextInt(nuser-i); 
      // toggle elements i and num+i
      int temp = perm[i];
      perm[i]=perm[num+i];
      perm[num+i]=temp;
    }
    
    String[]        user_ids = new String[] {
      "james",                                   
        "linda",
        "sofia",
        "santiago",
        "isabella",
        "diego",
        "robert",
        "mary",
        "patricia",
        "daniela"};  
    
    String[]        pwds = new String[nuser];
    int user;
    
    // generate the random passwords
    for (user=0; user<nuser; user++){
      String pwd = "";
      for (i=0; i<perm[user]; i++)
        pwd = pwd + allowed[generator.nextInt(allowed.length)];
      pwds[user]=pwd;
    }
//
// Generating the plain passwords file
//
    BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    
    System.out.print(" Enter file name for passwords in plaintext: ");
    String fileName = in.readLine();
    PrintWriter pwdfile = new PrintWriter(new FileWriter(fileName)); 
    for (i = 0; i < pwds.length; i++)
      pwdfile.println(user_ids[i] + ": " + pwds[i]);
    pwdfile.close();
    System.out.println("plaintext password file written");
    
//
// Generating the hashed passwords file
//
    System.out.print(" Enter file name for hashed passwords: ");
    fileName = in.readLine();       
    PrintWriter outfile = new PrintWriter(new FileWriter(fileName));
    
//    MessageDigest   hash = MessageDigest.getInstance("SHA1", "BC");
    MessageDigest   hash = MessageDigest.getInstance("SHA1");
    
    for (i = 0; i < pwds.length; i++){
      hash.update(Utils.toByteArray(pwds[i]));
      outfile.println(user_ids[i] + ": " + Utils.toHex(hash.digest()));
    }
    
    outfile.close();
    System.out.println("hashed passwords file written");
  }
}
