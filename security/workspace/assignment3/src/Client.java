import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Scanner;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;

public class Client {
	// This code is a modification of the SimpleClient.java
	// It implements AES encryption through the socket
	// This version used for Computer Security, Spring 2016.    

	public static void main(String[] args) {
		File privClient = new File("privateClient.pem");
		File pubClient = new File("publicClient.pem");
		File pubServer = new File("publicServer.pem");
		String certificateFilename = "certificateClient.txt";
		Cipher cipher;
		ObjectInputStream inputFile;
		SecretKey secretKey;
		byte[] encryptedByte;
		byte[] byteReply;
		String reply;

		System.out.println("Client running");
		Scanner userInput = new Scanner(System.in);
		String host;
		if (args.length > 0) {
			host = args[0];
		} else {
			System.out.println("Enter the server's address: (IP address or \"localhost\")");
			host = userInput.nextLine();
		}
		System.out.println("host: " + host);
		try {
			Socket socket = new Socket(host, 8008);
			// We could use Base64 encoding and communicate with strings like SimpleClient
			// However, we show here how to send and receive serializable java objects
			ObjectOutputStream objectOutput = new ObjectOutputStream(socket.getOutputStream());
			ObjectInputStream objectInput = new ObjectInputStream(socket.getInputStream());

			// Part 1
			System.out.println("Now sending session request");
			objectOutput.writeObject("Session request".getBytes());

			// part 3
			System.out.println("Now waiting for server certificate");
			File certificateServer = (File) objectInput.readObject();
			boolean valid = VerifyCert.verify(certificateServer);
			if (!valid) {
				System.out.println("Invalid certificate;");
				return;
			}
			File certificateClient = new File(certificateFilename);
			System.out.println("Now sending client certificate");
			objectOutput.writeObject(certificateClient);

			// part 5
			System.out.println("Now waiting for server reply");
			byteReply = (byte[]) objectInput.readObject();
			reply = new String(byteReply);
			if (!reply.equals("Certificate verified")) {
				System.out.println("reply:" + reply);
				System.out.println("Invalid reply");
				return;
			}
			System.out.println("Server verified client certificate");
			System.out.println("Now creating AES key and signing");
			File AESkeyfile = CreateAESkey.generateKeyfile("AESclient.key");
			File singedEncryptedAESkey = EncryptThenSignKey.encryptThenSign(AESkeyfile, privClient, pubServer, "signedEncryptedAES.txt");
			System.out.println("Now sending AES key that is encrypted then signed");
			objectOutput.writeObject(singedEncryptedAESkey);

			// part 7
			System.out.println("Now waiting for server reply");
			byteReply = (byte[]) objectInput.readObject();
			reply = new String(byteReply);
			if (!reply.equals("Ready")) {
				System.out.println("reply:" + reply);
				System.out.println("Invalid reply");
				return;
			}
			System.out.println("Server replied with ready message");
			System.out.println("Communication set-up, now ready to send messages.");

			File file = new File(AESkeyfile.getName());
			try {
				inputFile = new ObjectInputStream(new FileInputStream(file));
				secretKey = (SecretKey) inputFile.readObject();
				inputFile.close();
			} catch (IOException | ClassNotFoundException ex) {
				System.out.println(ex);
				socket.close();
				return;
			}
			cipher = Cipher.getInstance("AES");

			boolean done = false;
			System.out.println("Starting conversation. Type messages, type BYE to end");
			while (!done) {
				// Read message from Client
				String userStr = userInput.nextLine();
				cipher.init(Cipher.ENCRYPT_MODE, secretKey);
				encryptedByte = cipher.doFinal(userStr.getBytes());
				// Send encrypted message to the server
				objectOutput.writeObject(encryptedByte);
				if (userStr.trim().equals("BYE")) {
					System.out.println("conversation ended");
					done = true;
				} else {
					// Wait for reply from server
					System.out.println("Waiting for reply from server");
					encryptedByte = (byte[]) objectInput.readObject();
					cipher.init(Cipher.DECRYPT_MODE, secretKey);
					String message = new String(cipher.doFinal(encryptedByte));
					System.out.println("Received: " + message);
					System.out.println("Reply to server: ");
				}
			}
			socket.close();
		} catch (Exception e) {
			System.out.println("Error: " + e);
		}
		userInput.close();
	}
}
