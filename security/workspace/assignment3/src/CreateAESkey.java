
import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.security.NoSuchAlgorithmException;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

public class CreateAESkey {
    
public static File generateKeyfile(String filename) {
        // This program creates an AES cryptographic key
        // and saves it in java object format to a file named AES.key,
        // Written by Luc Longpre for Computer Security, Spring 2016.

        ObjectOutputStream objectOutput;
        File file = null;
        
        // generate key pair
     try{       
        // Initialize a key pair generator
        KeyGenerator keyGen = KeyGenerator.getInstance("AES");
        // Generate a key
        SecretKey secretKey = keyGen.generateKey();
        
        file = new File(filename);
        try{
            objectOutput = new ObjectOutputStream(new FileOutputStream(file));
            objectOutput.writeObject(secretKey);
            objectOutput.close();
        } catch (Exception e){
            System.out.println("Could not create private key file");
        }
    } catch (NoSuchAlgorithmException ex){
        System.out.println(ex);
    }
     return file;
  }    
    
}
