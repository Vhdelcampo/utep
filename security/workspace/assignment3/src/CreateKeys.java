import java.io.FileNotFoundException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;

public class CreateKeys {

    public static void generateKeys(String publicFilename, String privateFilename) {
        // This program creates an RSA key pair, 
        // saves the private key in a file in PEM format named privateKey.pem,
        // saves the public key in a file named publickey.pem.
        // The program uses PemUtils.java.
        // The PemUtils.java uses Base64 encoding, which is available in Java 8.
        // Written by Luc Longpre for Computer Security, Spring 2016

        KeyPair key;

        // generate key pair
        try {
            // Initialize a key pair generator
            KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
            // Generate a key pair	
            key = keyGen.generateKeyPair();
        } catch (NoSuchAlgorithmException e) {
            // If no provider supports RSA, or the key size is not supported
            System.out.println("Key pair generator failed to generate keys, " + e);
            return;
        }
        
        PrivateKey privKeyClient = key.getPrivate();
        PublicKey pubKeyClient = key.getPublic();
        try {
            PemUtils.writePublicKey(pubKeyClient, publicFilename);
        } catch (FileNotFoundException e) {
            System.out.println("Write Public Key: File not found Exception");
        }
        try {
            PemUtils.writePrivateKey(privKeyClient, privateFilename);
        } catch (FileNotFoundException e) {
            System.out.println("Write Private Key: File not found Exception");
        }
    }
}
