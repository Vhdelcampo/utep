import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Scanner;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

public class CryptoClient {
// This code is a modification of the SimpleClient.java
// It implements AES encryption through the socket
// This version used for Computer Security, Spring 2016.    

    public static void main(String[] args) {

        File file = new File("AES.key");
        ObjectInputStream inputFile;
        SecretKey secretKey;
        byte[] encryptedByte;
        try {
            inputFile = new ObjectInputStream(new FileInputStream(file));
            secretKey = (SecretKey) inputFile.readObject();
            inputFile.close();
        } catch (IOException | ClassNotFoundException ex) {
            System.out.println(ex);
            return;
        }
        System.out.println("Client running");
        Scanner userInput = new Scanner(System.in);
        String host;
        if (args.length > 0) {
            host = args[0];
        } else {
            System.out.println("Enter the server's address: (IP address or \"localhost\")");
            host = userInput.nextLine();
        }
        System.out.println("host: " + host);
        try {
            Socket socket = new Socket(host, 8008);
            // We could use Base64 encoding and communicate with strings like SimpleClient
            // However, we show here how to send and receive serializable java objects
            ObjectOutputStream objectOutput = new ObjectOutputStream(socket.getOutputStream());
            ObjectInputStream objectInput = new ObjectInputStream(socket.getInputStream());

            System.out.println("Starting conversation. Type messages, type BYE to end");

            Cipher cipher = Cipher.getInstance("AES");
            boolean done = false;
            while (!done) {
                // Read message from Client
                String userStr = userInput.nextLine();
                cipher.init(Cipher.ENCRYPT_MODE, secretKey);
                encryptedByte = cipher.doFinal(userStr.getBytes());
                // Send encrypted message to the server
                objectOutput.writeObject(encryptedByte);
                if (userStr.trim().equals("BYE")) {
                    System.out.println("conversation ended");
                    done = true;
                } else {
                    // Wait for reply from server
                    System.out.println("Waiting for reply from server");
                    encryptedByte = (byte[]) objectInput.readObject();
                    cipher.init(Cipher.DECRYPT_MODE, secretKey);
                    String message = new String(cipher.doFinal(encryptedByte));
                    System.out.println("Received: " + message);
                    System.out.println("Reply to server: ");

                }
            }
            socket.close();
        } catch (IOException | NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException | ClassNotFoundException e) {
            System.out.println("Error: " + e);
        }
    }
}
