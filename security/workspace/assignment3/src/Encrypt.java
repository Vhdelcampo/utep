import java.io.File;
import java.io.ObjectInputStream;
import java.io.PrintWriter;
import java.security.PublicKey;
import java.util.Base64;
import java.util.Scanner;

import javax.crypto.Cipher;

class Encrypt {
  
  public static void main(String[] args) {
    // This program reads a public key from file
    // converts a message string to a byte array,
    // encrypt the message with the public key
    // and saves the encrypted message.
    
    ObjectInputStream objectInput;
    File file;
    PublicKey pubKey;
    Cipher cipher;
    String messageToEncrypt = "terrafrost";
    byte[] encryptedByteArray;
    String encryptedString;
    
    // Read public key from file
    
    pubKey = PemUtils.readPublicKey("CAPHPpublicKey.pem");
    //pubKey = PemUtils.readPublicKey("publicK.pem");
    
    try{
      cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
      cipher.init(Cipher.ENCRYPT_MODE, pubKey);
    } catch (Exception e) {
      System.out.println("Could not initialize encryption");
      return;
    }
    try{
      encryptedByteArray = cipher.doFinal(messageToEncrypt.getBytes()); 
      encryptedString = Base64.getEncoder().encodeToString(encryptedByteArray);
      System.out.println("The encrypted string is: "+encryptedString);
    } catch (Exception e) {
      System.out.println("Encryption error");
      return;
    }
    file = new File("encryptedMessage.txt");
    try{
        PrintWriter output = new PrintWriter(file);
        output.print(encryptedString);
        output.close();
    } catch (Exception e){
      System.out.println("Could not create encryptedMessage file");
    } 
    try{
      file = new File("encryptedMessage.txt");
      Scanner input = new Scanner(file);
      String newEncryptedString = input.nextLine();
      System.out.println("The encrypted string is: "+newEncryptedString);
      if (newEncryptedString.equals(encryptedString))
          System.out.println("equal");
      else
          System.out.println("not equal");
    } catch (Exception e) {
      System.out.println("Could not open encryptedMessage file");
      return;
    }
  }
}
