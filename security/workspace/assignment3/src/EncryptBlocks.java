import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.util.Base64;
import java.util.Scanner;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

class EncryptBlocks {

	public static void main(String args[]) throws InvalidKeyException, NoSuchAlgorithmException, 
	NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, IOException {
		encryptFile(new File("signedAESkeyfile.txt"), new File("publicClient.pem"), "encryptedKey.txt");
	}

	public static boolean encryptFile(File fileToEncrypt, File publicKey, String encryptedFilename) throws InvalidKeyException, NoSuchAlgorithmException, 
	NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, IOException {
		// This program reads a public key from file
		// converts a message string to a byte array,
		// encrypt the message with the public key
		// and saves the encrypted message.
		String contents = "";
		String AESkey;

		Scanner input = new Scanner(fileToEncrypt);
		String line = input.nextLine();
		if (!"-----BEGIN AES KEY-----".equals(line)) {
			return false;
		}
		contents += line+"\r\n";
		line = input.nextLine();
		AESkey = line;
		contents += line+"\r\n";
		line = input.nextLine();
		if (!"-----END AES KEY-----".equals(line)) {
			return false;
		}
		contents += line + "\r\n";
		line = input.nextLine();

		if (!"-----BEGIN SIGNATURE-----".equals(line)) {
			return false;
		}
		contents += line+"\r\n";
		line = input.nextLine();
		contents += line+"\r\n";
		line = input.nextLine();
		if (!"-----END SIGNATURE-----".equals(line)) {
			return false;
		}
		contents += line;
		input.close();

		return encryptBlocks(publicKey, encryptedFilename, AESkey);
	}

	private static boolean encryptBlocks(File publicKey, String encryptedFilename, String AESkey) {
		String entireEncryptedString = "";
		File file;
		PublicKey pubKey;
		Cipher cipher = null;
		byte[] encryptedByteArray;
		String encryptedString;

		// Read public key from file
		pubKey = PemUtils.readPublicKey(publicKey.getName());
		try {
			cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
			cipher.init(Cipher.ENCRYPT_MODE, pubKey);
		} catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}

		try {
			encryptedByteArray = cipher.doFinal(AESkey.getBytes());
			encryptedString = Base64.getEncoder().encodeToString(encryptedByteArray);
			entireEncryptedString += encryptedString;
		} catch (IllegalBlockSizeException | BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("The encrypted string is:\n" + entireEncryptedString);
		file = new File(encryptedFilename);
		PrintWriter output;
		try {
			output = new PrintWriter(file);
			output.print(entireEncryptedString);
			output.close();
			return false;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}
	
}
