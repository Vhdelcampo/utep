import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.PrintWriter;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

public class EncryptThenSignKey {

	public static void main(String args[]) throws FileNotFoundException {
        // Read private key from file
        File privateKey = new File("privateClient.pem");
        File publicKey = new File("publicServer.pem");
		encryptThenSign(new File("AES.key"), privateKey, publicKey, "signedEncryptedKey.txt");
	}
	
    public static File encryptThenSign(File AESkeyfile, File privateKey, File publicKey, String filename) {
        PrivateKey privKey = PemUtils.readPrivateKey(privateKey.getName());
        PublicKey pubKey = PemUtils.readPublicKey(publicKey.getName());
        byte[] signature = null;
        String AESkey;
        String encryptedAESkey;
        
        AESkey = readAESfile(AESkeyfile);
        encryptedAESkey = encrypt(pubKey, AESkey);
        signature = sign(privKey, encryptedAESkey);
        return createFile(filename, signature, encryptedAESkey);
    }

	private static String readAESfile(File AESkeyfile) {
		String AESkey;
		SecretKey secretKey = null;
		ObjectInputStream inputFile;
		// reading the AES cryptographic key        
        try {
            inputFile = new ObjectInputStream(new FileInputStream(AESkeyfile));
            secretKey = (SecretKey) inputFile.readObject();
            inputFile.close();
        } catch (IOException | ClassNotFoundException ex) {
            System.out.println(ex);
        }
        AESkey = Base64.getEncoder().encodeToString(secretKey.getEncoded());
		return AESkey;
	}

	private static File createFile(String signedAESkeyfile, byte[] signature, String encryptedAESkey) {
		File file;
		file = new File(signedAESkeyfile);
        try {
            PrintWriter output = new PrintWriter(file);
            output.println("-----BEGIN AES KEY-----");
            output.println(encryptedAESkey);
            output.println("-----END AES KEY-----");
            output.println("-----BEGIN SIGNATURE-----");
            output.println(Base64.getEncoder().encodeToString(signature));
            output.print("-----END SIGNATURE-----");
            output.close();
        } catch (Exception e) {
            System.out.println("Could not create signature file");
        }
		return file;
	}
	
	private static String encrypt(PublicKey pubKey, String AESkey) {
		Cipher cipher = null;
		String encryptedString = "";
		byte[] encryptedBytes;

		// Read public key from file
		try {
			cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
			cipher.init(Cipher.ENCRYPT_MODE, pubKey);
			encryptedBytes = cipher.doFinal(AESkey.getBytes());
			System.out.println("Encryption succeeded");
			encryptedString = Base64.getEncoder().encodeToString(encryptedBytes);
		} catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		return encryptedString;
	}

	private static byte[] sign(PrivateKey privKey, String encryptedAESkey) {
		Signature sig;
		byte[] signature = null;
		try {
            sig = Signature.getInstance("SHA1withRSA");
            sig.initSign(privKey);
            sig.update(encryptedAESkey.getBytes());
            System.out.println("Signing succeded");
            signature = sig.sign();

        } catch (Exception e) {
            System.out.println("Error attempting to sign");
        }
		return signature;
	}
    

}
