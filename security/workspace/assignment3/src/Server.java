
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Scanner;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

class Server {
	// This code is a modification of the SimpleServer.java
	// It implements AES encryption through the socket    
	// This version used for Computer Security, Spring 2016.

	public static void main(String[] args) throws NoSuchAlgorithmException, NoSuchPaddingException {
		File privServer = new File("privateServer.pem");
		File pubClient = new File("publicClient.pem");
		File pubServer = new File("publicServer.pem");
		String certificateFilename = "certificateServer.txt";
		ObjectInputStream inputFile;
		SecretKey secretKey;
		Cipher cipher;
		byte[] encryptedByte;
		Scanner serverInput = new Scanner(System.in);

		try (ServerSocket s = new ServerSocket(8008)) {
			while (true) {
				try {
					System.out.println("Waiting for new client connection");
					Socket incoming = s.accept();
					System.out.println("Connected to: "
							+ incoming.getInetAddress()
							+ " at port: " + incoming.getLocalPort());
					// We could use Base64 encoding and communicate with strings like SimpleServer
					// However, we show here how to send and receive serializable java objects                    
					ObjectInputStream objectInput = new ObjectInputStream(incoming.getInputStream());
					ObjectOutputStream objectOutput = new ObjectOutputStream(incoming.getOutputStream());

					// part 2
					System.out.println("Now waitng for client session request");
					byte[] byteMessage = (byte[]) objectInput.readObject();
					String request = new String(byteMessage);
					System.out.println("Request recived: " + request);
					if (!request.equals("Session request")) {
						System.out.println("error");
					}
					File certificateServer = new File(certificateFilename);
					System.out.println("Now sending server certificate");
					objectOutput.writeObject(certificateServer);

					// part 4
					System.out.println("Now waiting for client certificate");
					File certificateClient = (File) objectInput.readObject();
					boolean valid = VerifyCert.verify(certificateClient);
					if (!valid) {
						System.out.println("Invalid certificate;");
						return;
					}
					String message = "Certificate verified";
					objectOutput.writeObject(message.getBytes());

					// part 6
					System.out.println("Now waiting for encrypted then signed AES key");
					File singedEncryptedAESkey = (File) objectInput.readObject();
					File AESkeyfile = VerifyThenDecryptKey.verifyThenDecrypt(singedEncryptedAESkey, privServer, pubClient, "AESserver.key");
					System.out.println("AES key transfer with decryption and verification complete, now sending 'Ready' message");
					objectOutput.writeObject("Ready".getBytes());
					System.out.println("Communication set-up, now ready to receive messages.");

					File file = new File(AESkeyfile.getName());
					try {
						inputFile = new ObjectInputStream(new FileInputStream(file));
						secretKey = (SecretKey) inputFile.readObject();
						inputFile.close();
					} catch (IOException | ClassNotFoundException ex) {
						System.out.println(ex);
						return;
					}
					cipher = Cipher.getInstance("AES");

					boolean done = false;
					while (!done) {
						// Waiting for Client message                
						System.out.println("Waiting for client message");
						encryptedByte = (byte[]) objectInput.readObject();
						cipher.init(Cipher.DECRYPT_MODE, secretKey);
						String decryptedMessage = new String(cipher.doFinal(encryptedByte));
						if (decryptedMessage.trim().equals("BYE")) {
							System.out.println("Conversation ended, waiting for new connection");
							done = true;
						} else {
							System.out.println("Received: " + decryptedMessage);
							System.out.println("Reply to client: ");

							// Read message from Client
							String userStr = serverInput.nextLine();
							cipher.init(Cipher.ENCRYPT_MODE, secretKey);
							encryptedByte = cipher.doFinal(userStr.getBytes());
							// Send encrypted message to the server
							objectOutput.writeObject(encryptedByte);
						}
					}
				} catch (IOException | ClassNotFoundException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException e) {
					System.out.println("Error: " + e);
				}
			}
		} catch (Exception e) {
			System.out.println("Error: " + e);
			System.out.println("Server stopped");
		}
	}
}
