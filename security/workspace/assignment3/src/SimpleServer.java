import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

// This code originated from Yoonsik Cheon at least 10 years ago.
// It was modified several times by Luc Longpre over the years. 
// This version used for Computer Security, Spring 2015 and 2016.

class SimpleServer {
  
  public static void main(String[] args) {
    System.out.println("SimpleServer started."); 
    Scanner serverInput = new Scanner(System.in);
    try {
      ServerSocket s = new ServerSocket(8008); 
      while (true) {
        Socket incoming = s.accept(); 
        System.out.println("Connected to: " + 
                           incoming.getInetAddress() +
                           " at port: " + incoming.getLocalPort()); 
        BufferedReader in = new BufferedReader(
                       new InputStreamReader(incoming.getInputStream())); 
        PrintWriter out = new PrintWriter(
                                          new OutputStreamWriter(incoming.getOutputStream())); 
        boolean done=false;        
        while (!done) {
          // Waiting for Client message
          System.out.println("Waiting for Client message");
          String str = in.readLine(); 
          if (str == null || str.trim().equals("BYE")) {
            System.out.println("Conversation ended, waiting for new connection");
            done=true; 
          } else {
            System.out.println("Received: "+str);              
            // Read reply from user
            String userStr=serverInput.nextLine();
            // Send reply to client
            out.println(userStr); 
            out.flush();
          }
        }
        incoming.close(); 
      }
    } catch (IOException e) {
      System.out.println("Error: " + e); 
    }
    System.out.println("EchoServer stopped."); 
  }
}
