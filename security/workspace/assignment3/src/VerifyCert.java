import java.io.File;
import java.io.FileNotFoundException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.util.Base64;
import java.util.Scanner;

class VerifyCert {

    public static boolean verify(File certificateFile) {
        // This program reads a certificate file named certificate.txt
        // and the certificate authority's public key file CApublicKey.pem,
        // parses the certificate for formatting,
        // and uses the public key to verify the signature.
        // The program uses PemUtils.java.
        // Written by Luc Longpre for Computer Security, Spring 2016

        PublicKey pubKey;
        String contents;
        String signature;
        Signature sig;

        // get the public key of the signer from file
        // Read public key from file
        pubKey = PemUtils.readPublicKey("CApublicKey.pem");

        // get the certificate and signature
        try {
            Scanner input = new Scanner(certificateFile);
            String line = input.nextLine();
            if (!"-----BEGIN INFORMATION-----".equals(line)) {
                System.out.println("expecting:-----BEGIN INFORMATION-----");
                System.out.println("got:" + line);
                return false;
            }
            contents = line+"\r\n";
            line = input.nextLine();
            while (!"-----END PUBLIC KEY-----".equals(line)) {
                contents += line + "\r\n";
                line = input.nextLine();
            }
            contents += line + "\r\n";
            line = input.nextLine();
            if (!"-----BEGIN SIGNATURE-----".equals(line)) {
                System.out.println("expecting:-----BEGIN SIGNATURE-----");
                System.out.println("got:" + line);
                return false;
            }
            signature = input.nextLine();
            line = input.nextLine();
            if (!"-----END SIGNATURE-----".equals(line)) {
                System.out.println("expecting:-----END SIGNATURE-----");
                System.out.println("got:" + line);
                return false;
            }
        } catch (FileNotFoundException e) {
            System.out.println("Problem reading the certificate, "+e);
            return false;
        }  
        // verify the signature
        try {
            // verify the signature
            sig = Signature.getInstance("SHA1withRSA");
            sig.initVerify(pubKey);
            sig.update(contents.getBytes());
            // output the result of the verification
            if (sig.verify(Base64.getDecoder().decode(signature))) {
                System.out.println("Certificate verification succeeded");
                return true;
            } else {
                System.out.println("Certificate verification failed");
                return false;
            }
        } catch (NoSuchAlgorithmException | InvalidKeyException | SignatureException e) {
            System.out.println("error occurred while trying to verify signature"+e);
        }
        return false;
    }
}
