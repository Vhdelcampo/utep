import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.util.Base64;

class VerifySignature {

    public static boolean verify(PublicKey pubKey, String message, String signature) {
        Signature sig;
        
        // verify the signature
        try {
            // verify the signature
            sig = Signature.getInstance("SHA1withRSA");
            sig.initVerify(pubKey);
            sig.update(message.getBytes());
            // output the result of the verification
            if (sig.verify(Base64.getDecoder().decode(signature))) {
                System.out.println("Signature verification succeeded");
                return true;
            } else {
                System.out.println("Signature verification failed");
                return false;
            }
        } catch (NoSuchAlgorithmException | InvalidKeyException | SignatureException e) {
            System.out.println("error occurred while trying to verify signature"+e);
        }
        return false;
    }
}
