import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
//import java.net.*; 
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Base64;
import java.util.Scanner;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

class VerifyThenDecryptKey {
	public static void main(String args[]){
		File encryptedAESKey = new File("signedEncryptedKey.txt");
		File privateKey = new File("privateServer.pem");
		File publicKey = new File("publicClient.pem");
		verifyThenDecrypt(encryptedAESKey, privateKey, publicKey, "AEStest.key");
	}
	public static File verifyThenDecrypt(File signedEncryptedFile, File privateKey, File publicKey, String filename) {
		// This program reads a private key from a file
		// and an encrypted message, decrypts the message
		// and prints it.
		PrivateKey privKey = PemUtils.readPrivateKey(privateKey.getName());
		PublicKey pubKey = PemUtils.readPublicKey(publicKey.getName());
		
		File file = null;
		Scanner input = null;
		String signature;
		String encryptedAESkey;

		try {
			input = new Scanner(signedEncryptedFile);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String contents = "";
		String line = input.nextLine();

		if (!"-----BEGIN AES KEY-----".equals(line)) {
		}
		contents += line+"\r\n";
		line = input.nextLine();
		encryptedAESkey = line;
		contents += line+"\r\n";
		line = input.nextLine();
		if (!"-----END AES KEY-----".equals(line)) {
		}
		contents += line + "\r\n";
		line = input.nextLine();
		if (!"-----BEGIN SIGNATURE-----".equals(line)) {
		}
		contents += line+"\r\n";
		line = input.nextLine();
		signature = line;
		contents += line+"\r\n";
		line = input.nextLine();
		if (!"-----END SIGNATURE-----".equals(line)) {
		}
		contents += line;
		input.close();
		
		if (!VerifySignature.verify(pubKey, encryptedAESkey, signature)){
			System.out.println("Invalid signature");
			return null;
		}
		SecretKey AESkey = decrypt(privKey, encryptedAESkey);
		file = createFile(filename, AESkey);
		return file;
	}
	
	private static File createFile(String filename, SecretKey AESkey) {
        File file = new File(filename);
        ObjectOutputStream objectOutput;
        try{
            objectOutput = new ObjectOutputStream(new FileOutputStream(file));
            objectOutput.writeObject(AESkey);
            objectOutput.close();
        } catch (Exception e){
            System.out.println("Could not create private key file");
        }
        return file;
	}

	private static SecretKey decrypt(PrivateKey privKey, String encryptedAESkey) {
		Cipher cipher = null;
		byte[] byteArray;
		byte[] decryptedByteArray = null;
		String decryptedString;
		SecretKey decryptedKey = null;
		try {
			cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
			cipher.init(Cipher.DECRYPT_MODE, privKey);
		    byteArray = Base64.getDecoder().decode(encryptedAESkey); 
		    decryptedByteArray = cipher.doFinal(byteArray);
		    System.out.println("Decryption succeeded");
		    decryptedString = new String(decryptedByteArray);
		    byte[] decodedKey = Base64.getDecoder().decode(decryptedString);
		    // rebuild key using SecretKeySpec
		    decryptedKey = new SecretKeySpec(decodedKey, 0, decodedKey.length, "AES"); 
		} catch (NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException | 
				 BadPaddingException | InvalidKeyException e) {
			e.printStackTrace();
		}
		return decryptedKey;
	}
}
