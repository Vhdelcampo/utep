from PIL import Image, ImageFilter
from numpy import *
from scipy import ndimage
from scipy.ndimage import filters
import cv2
import scipy.misc

def plane_sweep_ncc(im_l,im_r,start,steps,wid):
    """ Find disparity image using normalized cross-correlation. """
    shape = im_l.shape
    m = shape[0]
    n = shape[1]
    # arrays to hold the different sums
    mean_l = zeros((m,n))
    mean_r = zeros((m,n))
    s = zeros((m,n))
    s_l = zeros((m,n))
    s_r = zeros((m,n))
    # array to hold depth planes
    dmaps = zeros((m,n,steps))
    # compute mean of patch
    filters.uniform_filter(im_l,wid,mean_l)
    filters.uniform_filter(im_r,wid,mean_r)
    # normalized images
    norm_l = im_l - mean_l
    norm_r = im_r - mean_r
    # try different disparities
    for displ in range(steps):
        # move left image to the right, compute sums
        filters.uniform_filter(roll(norm_l,-displ-start)*norm_r,wid,s) # sum nominator
        filters.uniform_filter(roll(norm_l,-displ-start)*roll(norm_l,-displ-start),wid,s_l)
        filters.uniform_filter(norm_r*norm_r,wid,s_r) # sum denominator
        # store ncc scores
        dmaps[:,:,displ] = s/sqrt(s_l*s_r)
        # pick best depth for each pixel
    return argmax(dmaps,axis=2)

l_img = Image.open("left.jpg").convert('L')
r_img = Image.open("right.jpg").convert('L')

l_im = array(l_img)
r_im = array(r_img)
print(l_im.shape)
print(r_im.shape)
steps = 10
start = 4
wid = 5
res = plane_sweep_ncc(l_im, r_im, start, steps, wid)
scipy.misc.imsave('depth.jpg',res)
