from numpy import *
from PIL import Image, ImageFilter
from numpy import *
import os
import PIL
from scipy.ndimage import filters

def graylevelImage(img, n):
    img_array = array(img)
    gray_level_image = zeros(img_array.shape)
    for i in range(0, len(img_array)):
        for j in range(0, len(img_array[0])):
            region = buildRegion(img_array, i, j, n)
            #std = matrix.std(region)
            #gray_level_image.append(std)
    return PIL.Image.fromarray(gray_level_image)
    

def buildRegion(array, i, j, n):
    region = [n*n]
    for x in range(0,n):
        for y in range(0,n):
            region.append(array[i-x, j-y])

    return region
    
img = Image.open('luigi.jpg').convert('L')
grey_img = graylevelImage(img, 2)
