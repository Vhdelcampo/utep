import PIL
from PIL import Image
import numpy as np
from scipy.ndimage import filters

img = Image.open('empire.jpg').convert('L')
im = np.array(img)
n = 10

for i in range(1, n):
        im_blur = filters.gaussian_filter(im, i)
        blur_diff = im - im_blur
        img_blur_diff = Image.fromarray(blur_diff)
        #img.show()
        img_blur_diff.save('empire_blur' + str(i) + '.jpg')


out = img.copy()
size = np.shape(im)
size = (size[1],size[0])
width = size[0]
height = size[1]

for i in range(1, n):
    proportion = 1 - i*.10
    new_size = (int(width * proportion), int(height * proportion))
    print(new_size)
    out = out.resize(new_size)
    out = out.resize(size)
    
    resize_diff = im - np.array(out)
    resize_img_diff = Image.fromarray(resize_diff)
    filename = 'empire_resized' + str(i) + '.jpg'
    resize_img_diff.save(filename)
    #img.show
