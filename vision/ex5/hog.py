import cv2
import PIL
from PIL import Image
import numpy as np
from scipy.ndimage import filters

def hog_description(img, topleft, botright, nbars):
    #sobel_img_x = zeros(im.shape)
    #filters.sobel(im,1,sobel_img_x)
    #sobel_img_y = zeros(im.shape)
    #filters.sobel(im,0,sobel_img_y)
    # (x,y) (x,y)
    #(left, upper, right, lower).
    box = (topleft[0], topleft[1], botright[0], botright[1])
    region = img.crop(box)
        
    filterx = np.array([-1, 0, 1])
    filtery = np.transpose(filterx)
    
    im = np.array(img)

    imgx = zeros(im.shape)
    imgy = zeros(im.shape)
    filtered_imagex = filters.convolve(im, filterx)
    filtered_imagey = filters.convolve(im, filtery)
                          
    #magnitude = sqrt(imx**2+imy**2)
    #figure()
    #hist(img.flatten(), nbars)
    #show()
    im.show()

img = Image.open('empire.jpg').convert('L')

hog_description(img, (0,0), (200,200), 100)
