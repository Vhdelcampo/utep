from PIL import Image, ImageFilter
import numpy as np
import cv2
import sys

refpoint = []
cv2_img = cv2.imread('quijote1.jpg')
img = Image.open("quijote1.jpg").convert('L')
im = np.array(img)

def cropRegion(im, topleft, botright):
    box = (topleft[0], topleft[1], botright[0], botright[1])
    img = Image.fromarray(im)
    region = img.crop(box)
    return region

def regionDiff(reg1, reg2):
    diff = np.subtract(reg1, reg2)
    diff = np.absolute(diff)
    return np.sum(diff)

def mostSimRegion(im, reg):
    move = 100
    im_size = np.shape(im)
    reg_size = np.shape(reg)
    y = im_size[0]
    x = im_size[1]
    n = reg_size[0]
    m = reg_size[1]
    x = x - m;
    y = y - n;
    smallest_regdiff = sys.maxint
    most_sim_reg = reg

    for i in range(0, x/move):
        for j in range (0, y/move):
            topleft = (i,j)
            botright = (i+m, j+n)
            new_reg = cropRegion(im, topleft, botright)
            regdiff = regionDiff(reg, new_reg)
            if (regdiff < smallest_regdiff):
                smallest_regdiff = regdiff
                most_sim_reg = new_reg
            j = j+move
        i = i+move
    return most_sim_reg

def draw_rectangle(event,x,y,flags,param):
    global count, refpoint
    # if the left mouse button was clicked, record the starting
    # (x, y) coordinates and indicate that cropping is being
    # performed
    if event == cv2.EVENT_LBUTTONDOWN:
        refpoint = [(x, y)]
        # check to see if the left mouse button was released
    elif event == cv2.EVENT_LBUTTONUP:
        refpoint.append((x, y))
        # draw a rectangle around the region of interest
        cv2.rectangle(cv2_img, refpoint[0], refpoint[1], (0, 255, 0), 2)

def clickAndDragRegion(cv2_img):
    global ix,iy, count, point1, point2
    cv2.namedWindow('image')
    cv2.setMouseCallback('image',draw_rectangle)
    while(1):
        cv2.imshow('image',cv2_img)
        k = cv2.waitKey(20) & 0xFF
        if k == 27:
            cv2.destroyAllWindows()
            return refpoint

coord = clickAndDragRegion(cv2_img)
print("coordinates are")
print(coord)
reg = cropRegion(im, coord[0], coord[1])
print(np.shape(reg))

regions = []
name = 'quijote'
for i in range(2, 5):
    extension = str(i) + '.jpg'
    filename = name + extension
    img = Image.open(filename).convert('L')
    im = np.array(img)
    sim_region = mostSimRegion(im, reg)
    regions.append(sim_region)
    sim_region.show()
