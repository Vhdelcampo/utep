from PIL import Image
from numpy import *
from scipy.ndimage import filters
from pylab import *

im = array(Image.open('empire.jpg').convert('L'))
im_blur = filters.gaussian_filter(im,5)
img_blur = Image.fromarray(im_blur)

im_unsharp_mask = subtract(im_blur, im)
img_unsharp_mask = Image.fromarray(im_unsharp_mask)
img_unsharp_mask.show()

im_color = array(Image.open('empire.jpg'))
im_color_blur = filters.gaussian_filter(im_color,5)
im_color_unsharp_mask = subtract(im_color_blur, im_color)
img_color_unsharp_mask = Image.fromarray(im_color_unsharp_mask)
img_color_unsharp_mask.show()





img2.save('empire_blurred.jpg', 'JPEG')
# img2.show()

# figure()
# gray()
# contour(im2, origin='image')
# axis('equal')
# axis('off')
# figure()
# hist(im.flatten(),128)
# show()

