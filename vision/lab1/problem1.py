from PIL import Image
from numpy import *
from pylab import *
from scipy.ndimage import filters
import imtools

### Problem 1 ###
#Exercise 1
img = Image.open('empire.jpg').convert('L')
im = array(img)
im_blur = filters.gaussian_filter(im,5)
img_blur = Image.fromarray(im_blur)
figure()
gray()
contour(im_blur, origin='image')
axis('equal')
axis('off')
figure()
hist(im.flatten(),128)
show()
img_graylevel = Image.fromarray(im)
img_graylevel.save('empire_graylevel.jpg')
img_blur.save('empire_blurred.jpg')

# Exercise 2
im_unsharp_mask = subtract(im_blur, im)
img_unsharp_mask = Image.fromarray(im_unsharp_mask)
im_color = array(Image.open('empire.jpg'))
im_color_blur = filters.gaussian_filter(im_color,5)
im_color_unsharp_mask = subtract(im_color_blur, im_color)
img_color_unsharp_mask = Image.fromarray(im_color_unsharp_mask)
# img_unsharp_mask.show()
# img_color_unsharp_mask.show()
img_unsharp_mask.save('empire_unsharp_mask.jpg')
img_color_unsharp_mask.save('empire_color_unsharp_mask.jpg')

# Exercise 3
im_product = multiply(im, im_blur)
im_quotient = divide(im_product, im)
img_quotient = Image.fromarray(im_quotient)
# img_quotient.show()
# img_quotient.save('empire_normalized.jpg')

#Exercise 5

