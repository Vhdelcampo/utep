from PIL import Image
from numpy import *
from pylab import *
from scipy.ndimage import filters
import imtools

### Problem 2 ###
img = Image.open('empire.jpg').convert('L')
im = array(img)
im_histeq,cdf = imtools.histeq(im)
figure()
gray()
contour(im_histeq, origin='image')
axis('equal')
axis('off')
figure()
hist(im_histeq.flatten(),128)
show()
img_histeq = Image.fromarray(im_histeq).convert('L')
img_histeq.save('empire_histeq.jpg')

img2 = Image.open('ocean.jpg').convert('L')
img2.save('ocean_graylevel.jpg')
im2 = array(img2)
im2_histeq,cdf = imtools.histeq(im2)
figure()
gray()
contour(im2_histeq, origin='image')
axis('equal')
axis('off')
figure()
hist(im2_histeq.flatten(),128)
show()
img2_histeq = Image.fromarray(im2_histeq).convert('L')
img2_histeq.save('ocean_histeq.jpg')
