from PIL import Image
from numpy import *
from pylab import *
from scipy.ndimage import filters
import imtools

### Problem 3 ###
img = Image.open('empire.jpg').convert('L')
im = array(img)
U,T = imtools.denoise(im,im)
figure()
gray()
imshow(U)
axis('equal')
axis('off')
show()
img_denoise = Image.fromarray(U).convert('L')
img_denoise.save('empire_denoise.jpg')

img2 = Image.open('skydiver.jpg').convert('L')
img2.save('skydiver_graylevel.jpg')
im2 = array(img2)
U,T = imtools.denoise(im2,im2)
figure()
gray()
imshow(U)
axis('equal')
axis('off')
show()
img2_denoise = Image.fromarray(U).convert('L')
img2_denoise.save('skydiver_denoise.jpg')

U,T = imtools.denoise(im2,im2,tolerance=0.5,tau=0.25,tv_weight=500)
figure()
gray()
imshow(U)
axis('equal')
axis('off')
show()
img2_denoise2 = Image.fromarray(U).convert('L')
img2_denoise2.save('skydiver_denoise2.jpg')

U,T = imtools.denoise(im2,im2,tolerance=0.1,tau=0.1,tv_weight=10)
figure()
gray()
imshow(U)
axis('equal')
axis('off')
show()
img2_denoise3 = Image.fromarray(U).convert('L')
img2_denoise3.save('skydiver_denoise3.jpg')

img3 = Image.open('luigi.jpg').convert('L')
img3.save('luigi_graylevel.jpg')
im3 = array(img3)
U,T = imtools.denoise(im3,im3)
figure()
gray()
imshow(U)
axis('equal')
axis('off')
show()
img3_denoise = Image.fromarray(U).convert('L')
img3_denoise.save('luigi_denoise.jpg')


