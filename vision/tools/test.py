from PIL import Image, ImageFilter
from numpy import *
from pylab import *

#Read image
im = Image.open('empire.jpg')
imarray = array(im)
img = Image.fromarray(imarray)
rows, column = np.shape(im)
rgb_im = im.convert('RGB')
r, g, b = rgb_im.getpixel((1, 1))
print r, g, b
#Display image
im.show()

# create a new figure
figure()
# don’t use colors
gray()
# show contours with origin upper left corner
contour(im, origin='image')
axis('equal')
axis('off')
figure()
hist(im.flatten(),128)
show()

#Applying a filter to the image
im_sharp = im.filter( ImageFilter.SHARPEN )
#Saving the filtered image to a new file
im_sharp.save( 'image_sharpened.jpg', 'JPEG' )

#Splitting the image into its respective bands, i.e. Red, Green,
#and Blue for RGB
r,g,b = im_sharp.split()

#Viewing EXIF data embedded in image
exif_data = im._getexif()
exif_data
