from PIL import Image, ImageFilter
from pylab import *
import numpy as np
from scipy import ndimage
import cv2

refpoint = []
	
def draw_circle(event,x,y,flags,param):
    global refpoint
    if event == cv2.EVENT_LBUTTONDOWN:
        cv2.circle(img,(x,y),5,(0,0,255),-1)
        refpoint.append((x,y))

def selectRegion(img):
    cv2.namedWindow('image')
    cv2.setMouseCallback('image', draw_circle)
    while(1):
        cv2.imshow('image',img)
        k = cv2.waitKey(20) & 0xFF
        if k == 27:
            break
    cv2.destroyAllWindows()
    return refpoint

def line(im, p0, p1, p2, p3):
    for r in range(0, m-1):
        start = r/(m-1)*p3 + (m-1-r)/(m-r)*p0
        end   = r/(m-1)*p2 + (m-1-r)/(m-r)*p1
        for c in range(0, n-1):
            p = end*c/(n-1) + start*(n-1-c)/(n-1)
            S[int(p[1]+.5),int(p[1]+.5)]

img = cv2.imread('luigi.jpg')
points = selectRegion(img)
print(points)
