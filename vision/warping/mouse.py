import cv2
import numpy as np
refpoint = []
	
def draw_circle(event,x,y,flags,param):
    global refpoint
    if event == cv2.EVENT_LBUTTONDOWN:
        cv2.circle(img,(x,y),5,(0,0,255),-1)
        refpoint.append((x,y))

def draw_rectangle(event,x,y,flags,param):
    global refpoint
    # if the left mouse button was clicked, record the starting
    # (x, y) coordinates and indicate that cropping is being
    # performed
    if event == cv2.EVENT_LBUTTONDOWN:
	refpoint = [(x, y)]
	# check to see if the left mouse button was released
    elif event == cv2.EVENT_LBUTTONUP:
	refpoint.append((x, y))
	# draw a rectangle around the region of interest
	cv2.rectangle(img, refpoint[0], refpoint[1], (0, 255, 0), 2)

def selectRegion(img):
    cv2.namedWindow('image')
    cv2.setMouseCallback('image', draw_circle)
    while(1):
        cv2.imshow('image',img)
        k = cv2.waitKey(20) & 0xFF
        if k == 27:
            break
    cv2.destroyAllWindows()
    return refpoint

img = cv2.imread('luigi.jpg')
points = selectRegion(img)
print(points)
